{
	"title": "Zorro, the Gay Blade (1981)",
	"synopsis":"In this spoof, Don Diego Vega (George Hamilton) follows in his father's footsteps as he dons the identity of Zorro in an attempt to defend the weak and innocent from the ravages of the evil. However, when Vega falls victim to a debilitating injury, it is up to his gay twin brother, Bunny Wigglesworth (George Hamilton), to take up the mask and sword. ~ Iotis Erlewine, Rovi",
	"genre":
	[
		{"name":"Action & Adventure"},
		{"name":"Comedy"}

	],
	"rate":3.2,
	"comments":[
		{
			"rate":40,
			"text": "Absolutely hilarious!!"
},
		{
			"rate":25,
			"text": "Curiosity definitely killed the cat or rather it should have killed me. Funny, stupid and all of the above..."
},
		{
			"rate":35,
			"text": "One bit, two bits, three bits a peso! All for Zoro, stand up and say so! I love this old cinemax classic!"
},
		{
			"rate":30,
			"text": "Entertaining, but not quite as good as some of the other spoofs from the time. George Hamilton is good. Enjoyable."
},
		{
			"rate":35,
			"text": " Two bits four bits six bits a pacoe all though's for Zorro stand up and say so!A shame to the Zorro movie's but have to admit very funny."
},
		{
			"rate":50,
			"text": "I love the humor in this movie. It's just awesome! The hero saves the day and wins the girl. Sometimes you just need a movie like that."
},
		{
			"rate":40,
			"text": "If you haven't seen this one, then you don't know what you are missing. This is a classic that never dies!"
},
		{
			"rate":35,
			"text": "Great, hilarious fun. George Hamilton was Zorro to me... Maybe that's not a good thing, but he was wonderful."
},
		{
			"rate":35,
			"text": "I remember watching this as a child and found it funny. Now, as an adult, I got more of the jokes and found it to be hilarious!!!"
},
		{
			"rate":50,
			"text": "I grew up with Zoro and this movie brings back so many memories. A lot of people don't like it but with me and my family, being that we know all the punch lines, it's a great watch every time. A spoof that turned into a classic with my family. I simply love George Hamilton in this movie!!!"
},
		{
			"rate":50,
			"text": "For me the definitive Zorro is Guy Williams from the old Disney series, but George Hamilton's version is a gloriously silly spoof. His just slightly exaggerated Don Diego is offset by the emergency back-up Zorro, Diego's flamboyant cousin Bunny Wigglesworth. For the rest of my life the expression the People will be the Piples, and I'll remember the ships in the fields...the little baa-baa-baas, because of this film. Love it!"
},
		{
			"rate":30,
			"text": "A very silly movie starring Zorro's gay brother. I think that's enough for me to say. (I also need to see it again before I give a better review)"
},
		{
			"rate":45,
			"text": "A very funny, very silly movie. Two bits four bits six bits a dollar all for Zorro stand up and holler"
},
		{
			"rate":40,
			"text": "Two bits four bits six bits a dollar all for Zorro stand up and holla......This comedy parody of Zorro had me rolling on the floor. Out of all the colorful costumes I love the Plum."
},
		{
			"rate":45,
			"text": "This movie a soooo bad, its great! I laugh everytime I see it. Then I end up quoting it for a month afterward. LOL"
},
		{
			"rate":35,
			"text": "The greatest Zorro spoof ever made. Underrated and wholeheartedly funny. He wore, two fruit and a vegetable!"
},
		{
			"rate":35,
			"text": "Okay, let me nip this in the bud. There are some movies that are just bad. There are some that are bad but fun. This is the latter. C'mon - how could you not like a gay Zorro who matches his weaponry to the outfit?"
},
		{
			"rate":45,
			"text": "George Hamilton terrifically underrated. I'm not a huge Ron Liebman fan, but thought he was terrific in this."
},
		{
			"rate":50,
			"text": "I cannot understand why this has a 43% tomatometer rating. I think this movie is hilarious. It's a spoof but at the same time has real characters that keep you into the plot. And George Hamilton gives a super funny performance."
},
		{
			"rate":50,
			"text": "Absolutely hilarious! I've been a Zorro fan from Tyron Power to Antonio Banderas and this spoof is a classic with knee jerk humor. Anyone who thinks otherwise has their pants on too tight!"
},
		{
			"rate":45,
			"text": "George Hamilton terrifically underrated. I'm not a huge Ron Liebman fan, but thought he was terrific in this."
},
		{
			"rate":50,
			"text": "I cannot understand why this has a 43% tomatometer rating. I think this movie is hilarious. It's a spoof but at the same time has real characters that keep you into the plot. And George Hamilton gives a super funny performance."
},
		{
			"rate":50,
			"text": "Absolutely hilarious! I've been a Zorro fan from Tyron Power to Antonio Banderas and this spoof is a classic with knee jerk humor. Anyone who thinks otherwise has their pants on too tight!"
},
		{
			"rate":-1,
			"text": "Swashbuckling, quick witty dialogue and outstanding comedic performances by Ron Leibman and of course Hamilton. Plus, how often do you see homosexual characters as flamboyant but heroic action stars? This is literally the only one I can think of. Classic."
},
		{
			"rate":-1,
			"text": "Want to see it! Will have to take into account!"
},
		{
			"rate":50,
			"text": "Hilariously funny. A must see."
},
		{
			"rate":50,
			"text": "This movie is just too funny, love it, love it, love it! lol"
},
		{
			"rate":30,
			"text": "A light-hearted take on the Zorro legend with George Hamilton in duel roles, flexing his comedic muscles as a philanderer and his flamboyantly gay twin brother who take turns in portraying the titular hero with varying results. But the film belongs to the wonderful pairing of Ron Leibman and Brenda Vaccaro, whose love-hate relationship as Esteban and Florinda showcases a number of scene-stealing physical gags and one-liners. By no means politically correct and falling short of being exceptional, but this is a fun, easy-going ride."
},
		{
			"rate":40,
			"text": "A very funny yet underrated homage to the legendary masked bandido. George Hamilton and Ron Leibman embrace the campiness of the movie and are just hilarious."
},
		{
			"rate":50,
			"text": "This is a great comedy. It had many likable characters and jokes."
},
		{
			"rate":40,
			"text": "One of my favorite movies from my childhood...Probably saw it close to 100 times as a kid. I have not seen it in 25 years or so. Still a fun movie to watch after all that time...and I remembered so much of it."
},
		{
			"rate":25,
			"text": "An annoying score, weak cast and silly premise sliced up this turkey to shreds."
},
		{
			"rate":50,
			"text": "Totally disrespectful, and politically un-correct, but funny!"
},
		{
			"rate":45,
			"text": "A NEW TWIST ON THE MASKED AVENGER, MANY LAUGHS AND VERY INTERTAINING !"
},
		{
			"rate":30,
			"text": "tacky, silly and funny film"
},
		{
			"rate":40,
			"text": "I have to admit, this is one of my favorites. It has stupid humor, granted, but I think it's better than a lot of the humor in newer movies."
},
		{
			"rate":45,
			"text": "Hilarious! Great lines and perfect if you need a good laugh. George Hamilton is a fantastic, comical Zorro and the supporting cast is equally silly & look like they had a great time making this. This movie is a family favorite and constantly quoted by everyone. I laugh throughout every time I see it. Good , silly fun."
},
		{
			"rate":30,
			"text": "ZORRO, THE GAY BLADE (1981)"
},
		{
			"rate":25,
			"text": "Curiosity definitely killed the cat or rather it should have killed me. Funny, stupid and all of the above..."
},
		{
			"rate":50,
			"text": "one of the funniest laugh-aloud movies of all time. only the veddy veddy politically correct could fail to be amused."
},
		{
			"rate":20,
			"text": "silly funny but not enough, even with Brenda Vaccaro"
},
		{
			"rate":35,
			"text": "One bit, two bits, three bits a peso! All for Zoro, stand up and say so! I love this old cinemax classic!"
},
		{
			"rate":20,
			"text": "Comical. A farce on the Zorro theme"
},
		{
			"rate":35,
			"text": "This version of Zorro is probably my favourite. Great cheesy lines and wierd costumes make this bizarre comedy really worth watching! I can't take Zorro seriously anymore!!"
},
		{
			"rate":25,
			"text": "Add a review (optional)..."
},
		{
			"rate":50,
			"text": "Fabulous movie. It has funny one liners and characters who make you laugh. It's been one of my family favorites for years."
},
		{
			"rate":30,
			"text": "Absolutely riddiculous, but funny, anyay... \o/"
},
		{
			"rate":50,
			"text": "Totally disrespectful, and politically un-correct, but funny!"
},
		{
			"rate":30,
			"text": "Entertaining, but not quite as good as some of the other spoofs from the time. George Hamilton is good. Enjoyable."
},
		{
			"rate":30,
			"text": "Entertaining, but not quite as good as some of the other spoofs from the time. George Hamilton is good. Enjoyable."
},
		{
			"rate":50,
			"text": "I definitely cried laughing."
},
		{
			"rate":35,
			"text": " Two bits four bits six bits a pacoe all though's for Zorro stand up and say so!A shame to the Zorro movie's but have to admit very funny."
},
		{
			"rate":50,
			"text": "We were womb-buddies."
},
		{
			"rate":45,
			"text": "great and ahead of its time"
},
		{
			"rate":-1,
			"text": "GEORGE DOES A FANTASTIC JOB PLAYING ZORRO IN THIS SPOOF."
},
		{
			"rate":-1,
			"text": "no thanks not my kinda thing"
},
		{
			"rate":30,
			"text": "ROFL yeah, uhmmm I guess."
},
		{
			"rate":50,
			"text": "Doh!There are currently no reviews to show."
},
		{
			"rate":50,
			"text": "I love the humor in this movie. It's just awesome! The hero saves the day and wins the girl. Sometimes you just need a movie like that."
},
		{
			"rate":40,
			"text": "If you haven't seen this one, then you don't know what you are missing. This is a classic that never dies!"
},
		{
			"rate":35,
			"text": "Great, hilarious fun. George Hamilton was Zorro to me... Maybe that's not a good thing, but he was wonderful."
},
		{
			"rate":45,
			"text": "Oh my God this movie is so great! It's stupid and silly and I love it all the better for it."
},
		{
			"rate":35,
			"text": "I remember watching this as a child and found it funny. Now, as an adult, I got more of the jokes and found it to be hilarious!!!"
},
		{
			"rate":40,
			"text": "he was yellow, like a banana... racial slurs abound... but damn! This was one funny zorro flick!"
},
		{
			"rate":50,
			"text": "I grew up with Zoro and this movie brings back so many memories. A lot of people don't like it but with me and my family, being that we know all the punch lines, it's a great watch every time. A spoof that turned into a classic with my family. I simply love George Hamilton in this movie!!!"
},
		{
			"rate":40,
			"text": "Just about the funniest secret identity film ever, with references to other versions of Zorro"
},
		{
			"rate":-1,
			"text": "um, okay so what I'm getting is either zorro is gay, or his sword is, or maybe they both are and zorro is going to marry his sword =0 thats a twist"
},
		{
			"rate":35,
			"text": "You're right. Everyone is allowed to speak. But arrest anyone who listens!!"
},
		{
			"rate":40,
			"text": "the best zorro, in my opinion..."
},
		{
			"rate":50,
			"text": "For me the definitive Zorro is Guy Williams from the old Disney series, but George Hamilton's version is a gloriously silly spoof. His just slightly exaggerated Don Diego is offset by the emergency back-up Zorro, Diego's flamboyant cousin Bunny Wigglesworth. For the rest of my life the expression the People will be the Piples, and I'll remember the ships in the fields...the little baa-baa-baas, because of this film. Love it!"
},
		{
			"rate":40,
			"text": "Absolutely hilarious!!"
},
		{
			"rate":-1,
			"text": "I totally need to see this"
},
		{
			"rate":35,
			"text": "I saw it once years ago and laughed until I cried. I would love to see it again."
},
		{
			"rate":30,
			"text": "A very silly movie starring Zorro's gay brother. I think that's enough for me to say. (I also need to see it again before I give a better review)"
},
		{
			"rate":50,
			"text": "This movie made me GAY!"
},
		{
			"rate":40,
			"text": "The only Zorro movie in technicolor... his costumes that is... or her..."
},
		{
			"rate":-1,
			"text": "2 many damn remakes n i usually like them better or never c the old version yo"
},
		{
			"rate":-1,
			"text": "Want to see it! Will have to take into account!"
},
		{
			"rate":35,
			"text": "Funny movie, Lauren is gorgeous!"
},
		{
			"rate":35,
			"text": "GOD I LAUGHED... THIS IS SO FUNNY"
},
		{
			"rate":-1,
			"text": "they turned a hero into a POOF nooooooooooooooooooooooooooooooooooooooooooo!!!!!!!!!!!!!!"
},
		{
			"rate":-1,
			"text": "what the fuck???????????"
},
		{
			"rate":45,
			"text": "A very funny, very silly movie. Two bits four bits six bits a dollar all for Zorro stand up and holler"
},
		{
			"rate":-1,
			"text": "I LOVE gay BUTT sex, it really turns me ON."
},
		{
			"rate":40,
			"text": "Two bits four bits six bits a dollar all for Zorro stand up and holla......This comedy parody of Zorro had me rolling on the floor. Out of all the colorful costumes I love the Plum."
},
		{
			"rate":40,
			"text": "My favorite comedy of all time!!"
},
		{
			"rate":45,
			"text": "hilarious movie, george hamilton does great!!"
},
		{
			"rate":50,
			"text": "This spoof of the original Zorro movies was perfect. It's funny. It's got memorable lines that make you giggle again and again when you think on them."
},
		{
			"rate":40,
			"text": "soooo funny!!! it has its serious moments, but mostly is just really funny. :D"
},
		{
			"rate":-1,
			"text": "What in HELL's name is this?!"
},
		{
			"rate":35,
			"text": "If you haven't seen this, you should. It's funny as hell."
},
		{
			"rate":30,
			"text": "haha.. found this funny at the time... george hamilton pulls it off as the fencing fairy LOL"
},
		{
			"rate":35,
			"text": "very funny take on the Zorro legend."
},
		{
			"rate":-1,
			"text": "The gay blade? ok some of these movie titles r just weird."
},
		{
			"rate":45,
			"text": "This movie a soooo bad, its great! I laugh everytime I see it. Then I end up quoting it for a month afterward. LOL"
},
		{
			"rate":40,
			"text": "George Hamiltons best as Dracula. really rib tickling"
},
		{
			"rate":35,
			"text": "George Hamilton is hilarious in the dual role of twin Zorros."
},
		{
			"rate":35,
			"text": "The greatest Zorro spoof ever made. Underrated and wholeheartedly funny. He wore, two fruit and a vegetable!"
},
		{
			"rate":-1,
			"text": "Haven't seen this, but who approved that title? lol"
},
		{
			"rate":-1,
			"text": "Everyone in my family has seen it. I have yet to see it."
},
		{
			"rate":45,
			"text": "A NEW TWIST ON THE MASKED AVENGER, MANY LAUGHS AND VERY INTERTAINING !"
},
		{
			"rate":35,
			"text": "Family movie. Absolutely hilarious! As long as you think it's okay to laugh at gay people! hah"
},
		{
			"rate":50,
			"text": "Just as funny today as it was back then!"
},
		{
			"rate":30,
			"text": "Been a while but remember it being funny as hell."
},
		{
			"rate":-1,
			"text": "think I saw this years ago but cant remember what it was like - dodgy I suspect!"
},
		{
			"rate":-1,
			"text": "I have heard a lot about this one! I'd love to see it!"
},
		{
			"rate":-1,
			"text": "Okay, sounds hilarious..."
},
		{
			"rate":50,
			"text": "Hilariously funny. A must see."
},
		{
			"rate":-1,
			"text": "another one that just sounds too bood to pass up"
},
		{
			"rate":35,
			"text": "still funny after all these years. a comedic romp into silliness, heroism, love, and oh yes, a gay blade."
},
		{
			"rate":-1,
			"text": "zorro, the gay blade? i think zorro is gay"
},
		{
			"rate":30,
			"text": "Silliness. Watch with LOVE AT FIRST BITE."
},
		{
			"rate":-1,
			"text": "Anything that starts out with GAY BLADE - cound me out"
},
		{
			"rate":20,
			"text": "frothy and too stereotypical"
},
		{
			"rate":45,
			"text": "Oh, this was just delightful."
},
		{
			"rate":35,
			"text": "George Hamilton, as gay and straight twin Zorros. ....Nope, that's really all I need to say."
},
		{
			"rate":50,
			"text": "This movie is just too funny, love it, love it, love it! lol"
},
		{
			"rate":50,
			"text": "who doesn't love this film?"
},
		{
			"rate":-1,
			"text": "George Hamilton is gay!"
},
		{
			"rate":30,
			"text": "I saw this as a little kid and I remember laughing a lot."
},
		{
			"rate":35,
			"text": "Great fun, George Hamilton was a hoot in this movie. Lots of laughs."
},
		{
			"rate":45,
			"text": "I remember this it was good and funny."
},
		{
			"rate":-1,
			"text": "havent seen this 1 yet.... herd its ok"
},
		{
			"rate":40,
			"text": "I've seen this movie so many times! It's so hilarious that I never get tired of it."
},
		{
			"rate":50,
			"text": "One of the funniest movies I've ever seen."
},
		{
			"rate":45,
			"text": "Every time I watch this movie I laugh a little more."
},
		{
			"rate":-1,
			"text": "HELLLLLLLL NOOOOOOOO!"
},
		{
			"rate":40,
			"text": "George Hamilton favorite!"
},
		{
			"rate":50,
			"text": "I could never ever get tired of this movie. A great comedy of zero and such a great classic."
},
		{
			"rate":35,
			"text": "i love george hamilton"
},
		{
			"rate":40,
			"text": "Great laughs and quotes from this one."
},
		{
			"rate":15,
			"text": "George Hamilton's best film!"
},
		{
			"rate":50,
			"text": "Funny on so many different levels for so many different reasons."
},
		{
			"rate":50,
			"text": "Funny as all get out"
},
		{
			"rate":25,
			"text": "this is crazy stupid funny type movie......u want to laugh this is the movie......"
},
		{
			"rate":45,
			"text": "I love this movie!!! Its sooo funny."
},
		{
			"rate":25,
			"text": "It's a hiliarious romp"
},
		{
			"rate":45,
			"text": "This film is witty and fun! Give it a view! George Hamilton at his campy best!"
},
		{
			"rate":50,
			"text": "Just give it a chance... you'll Love it!"
},
		{
			"rate":-1,
			"text": "I got no idea what this is, but hey, come on, tell me you didn't laugh when you saw this title"
},
		{
			"rate":-1,
			"text": "what in the world....i must see this!!!"
},
		{
			"rate":-1,
			"text": "gay blade? i think not somehow"
},
		{
			"rate":5,
			"text": "HAHAHAHAHAHHAHAHAHAHAHAHAHAHAHAHAHAHHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHHAHAHAHAHAHAHAHAHAH (breathing) HAHAHAHAHAHAHAHHAHAHAHAHAHAHAHAHHAHAHAHHAHAHA."
},
		{
			"rate":5,
			"text": "one of the worst of the worst...!"
},
		{
			"rate":45,
			"text": "i laughed soooo hard!"
},
		{
			"rate":45,
			"text": "it was sooo funni!!!"
},
		{
			"rate":45,
			"text": "George Hamilton's stunning preformance."
},
		{
			"rate":40,
			"text": "Saw this many years ago."
},
		{
			"rate":50,
			"text": "This was the funniest shit ever (bare in mind i say that sbout everything funny) Really GOOD!!!!"
},
		{
			"rate":-1,
			"text": "Wow this was a funny funny movie. If you are a fan of spoofy slapstick you will be sure to enjoy this. George Hamilton is hilarious with his dual role as Zorro and his rather colorful brother. Same spoofy slapstick as he did in Love At First Bite Plot Summary: When the new Spanish Governor begins to grind the peasants under his heel, Don Diego Vega becomes Zorro..."
},
		{
			"rate":-1,
			"text": "the gay blade??? c'est quoi... zorro est gay dans ste film la ?? eehhh, biz pareil..."
},
		{
			"rate":-1,
			"text": "Definitely want to see this! The preview was hillarious!"
},
		{
			"rate":45,
			"text": "it was funny it showed zorros alterego as a flaming man and zorro being macho"
},
		{
			"rate":35,
			"text": "Okay, let me nip this in the bud. There are some movies that are just bad. There are some that are bad but fun. This is the latter. C'mon - how could you not like a gay Zorro who matches his weaponry to the outfit?"
},
		{
			"rate":25,
			"text": "[size=2]I'm beginning the rather lengthy task of entering into the database every film I have seen, at least those I have a decent memory of. Some may have the briefest of reviews while many will just have a number. Those films that I consider personal favorites and/or those that have some historical signficance I will add later when I have time for more lengthy reviews. [b]Young Einstein[/b], directed by Yahoo Serious, is a rather unfunny slapstick comedy. [b]Young Guns[/b], directed by Christopher Cain, was a Western made for the MTV generation. Although it doesn't really compare to more serious westerns, the film is fun to watch. Emilio Estevez stars as Billy the Kid, the flashiest role. The film also stars Charlie Sheen, Lou Diamond-Phillips, Kiefer Sutherland, Dermot Mulroney and Casey Siemaszko. A good popcorn film, but not very deep. [b]Zorro the Gay Blade[/b], directed by Peter Medak, is a swashbuckler spoof starring George Hamilton, Lauren Hutton and Brenda Vaccaro. Hamilton plays duel roles as Don Diego Vega and his gay twin brother Bunny. The film has some funny moments. [b]Innerspace[/b], directed by Joe Dante, is a visially exciting Sci-Fi comedy with some genuinely funny moments. [/size]"
},
		{
			"rate":50,
			"text": "Zorro, the Gay Blade is terrific."
},
		{
			"rate":45,
			"text": "As funny as any movie released in 2003. Hamilton is incredible in both roles but it's clearly the Alcalde who steals the show. I've been watching this movie for years and can't stop laughing each time."
},
		{
			"rate":50,
			"text": ":D If you haven't seen this movie, you haven't seen campy American comedy at it's absolute pinnacle. The frighteningly tan George Hamilton is brilliant as the mincing, prancing Don Diego de la Vega--heir to the Zorro dynasty. His accent is hysterical--a Spanish caballero a la Cheech Morin via East LA. He is equally funny as his British sailor poof of a twin brother, Ramon, now known as Bunny Wigglesworth. (They say the Navy makes a man of you--well, they certainly made me.) Ron Liebman as the alcalde Esteban steals the movie with his whacky, crypto-homosexual, neurotic behavior. His wife Florinda, played by the always-throaty Brenda Vacaro, is very Mae West in her double entendres (Esteban (admiring Diego's clothing): I could never get into those pants.. Florinda: I bet I could.) Lauren Hutton as Diego's love ineterst, Charlotte Taylor-Wilson, is the weakest link, but who cares? She's just a foil to George Hamilton anyway. The supporting cast is perfect, in particualr Paco, Diego's speechless manservant. This is a movie that on first viewing may leave you thinking it's just plain stupid, but it definitely grows on you--kind of like Caddyshack."
}	]
}