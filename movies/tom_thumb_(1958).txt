{
	"title": "tom thumb (1958)",
	"synopsis":"Producer/animator/special-effect maven George Pal made his feature-film directorial bow with the colorful MGM musical fantasy Tom Thumb (the title of the film was spelled in lower case in the opening credits, and in all studio publicity material). Russ Tamblyn stars as the teeny-tiny titular protagonist, while veteran musicomedy favorite Jessie Mathews and stellar character actor Bernard Miles portray Tom's normal-sized parents. Journeying to the Village, Tom is pounced upon by villains Ivan (Terry-Thomas) and Tony (a corpulent Peter Sellers), who intend to exploit our 5 1/2-inch-tall hero. In-between his misadventures with the villains, Tom helps to expedite the romance between young forester Woody (Alan Young) and the magical Forest Queen (June Thorburn). Throughout, the special effects and oversized sets are first-rate, as are the puppetoons sequences featuring such delightful characters as The Yawning Man (voice by Stan Freberg). Written by several hands, the film's songs are hummable, if not particularly memorable. It is said that some children in the audience in 1958 were genuinely frightened by the more horrific aspects of the story (including the threatened execution of Tom's parents); it may be, however, that the adults were more scared than the kids. Incidentally, while most of Tom Thumb was filmed in MGM's London facilities, the special effects were produced in Hollywood, requiring Russ Tamblyn to do a lot of travelling. ~ Hal Erickson, Rovi",
	"genre":
	[
		{"name":"Kids & Family"},
		{"name":"Musical & Performing Arts"}
,
		{"name":"Classics"}
,
		{"name":"Science Fiction & Fantasy"}

	],
	"rate":3.2,
	"comments":[
		{
			"rate":30,
			"text": "Make way for the town lasher A rich, vibrant musical fantasy based on the Brothers Grimm`s fairy tale that is delightfully fun to watch. Russ Tamblyn plays the diminutive boy Tom who comes to liven up the days of a childless middle-aged couple, a woodcutter and his wife who are granted three wishes by a very beautiful blonde fairy, the Forest Queen (June Thorburn). But then a pair of dastardly villains (played by Terry-Thomas and Peter Sellers) use the boy to steal some gold coins and his foster parents is accused of the theft. Now it`s up Tom Thumb has to save the day by exposing the real thieves. There is also a romantic subplot involving a local musician (Alan Young) who is in love with the immortal Forest Queen and doesn't realize that he can turn her into a mortal via a kiss. A terrific movie for kids to see! As for the adults, it is sure to appeal to the inner child in each of us! The Yawning man/song almost put me to sleep as well.....almost lol!"
},
		{
			"rate":40,
			"text": "a classic.. watched it wen i was 4 years old.. and still remb the quoteeach peach pear plum in comes tom thumb"
},
		{
			"rate":40,
			"text": "This movie deservedly won the Oscar for its Special Effects. Russ Tamblyn was well-cast as Tom. The romantic sub-plot is superfluous and Terry-Thomas and Peter Sellers all but steal the movie as a bumbling pair of thieves. Funny enjoyable puffball of a movie. Well worth watching for the effects and Sellers and Thomas."
},
		{
			"rate":35,
			"text": "6.5/10. Surprisingly charming family film. Some innovative special effects. The musical numbers are not particularly special but they aren't bad. Agile Russ Tamblyn as the title character is remarkable. Well made great fun."
},
		{
			"rate":25,
			"text": "Fun musical adventure for the family."
},
		{
			"rate":40,
			"text": "The talented RUSS TAMBLYN does a superb job of singing, dancing and acting while playing the title role of the miniature boy presented as a gift to a woodsman and his wife by a woodland spirit. They treat him as their own son and the film revolves around his misadventures after his parents are wrongly accused of a crime and he must find the real culprits (TERRY-THOMAS and PETER SELLERS) in time for a happy ending. The trick photography is marvelous, the toys that come to life are inventive and fun, the interaction between Tom and all the other townspeople is well done--and this was all before the CGI effects we have today. There's a lot of charm to several musical numbers, especially one called Yawning Man, and all of the song-and-dance numbers are done in rollicking style. Songstress Peggy Lee wrote several clever songs. Well worth watching, a family film that can be enjoyed by adults or children. Tamblyn's talents are given full reign in this one."
},
		{
			"rate":35,
			"text": "A Big Hand for the Little Man--Wonderful Fantasy!!"
},
		{
			"rate":35,
			"text": "Fun story. Love to watch Russ Tamblyn dance"
},
		{
			"rate":30,
			"text": "i know its old and is old school but may a clean up is nice"
},
		{
			"rate":30,
			"text": "Make way for the town lasher A rich, vibrant musical fantasy based on the Brothers Grimm`s fairy tale that is delightfully fun to watch. Russ Tamblyn plays the diminutive boy Tom who comes to liven up the days of a childless middle-aged couple, a woodcutter and his wife who are granted three wishes by a very beautiful blonde fairy, the Forest Queen (June Thorburn). But then a pair of dastardly villains (played by Terry-Thomas and Peter Sellers) use the boy to steal some gold coins and his foster parents is accused of the theft. Now it`s up Tom Thumb has to save the day by exposing the real thieves. There is also a romantic subplot involving a local musician (Alan Young) who is in love with the immortal Forest Queen and doesn't realize that he can turn her into a mortal via a kiss. A terrific movie for kids to see! As for the adults, it is sure to appeal to the inner child in each of us! The Yawning man/song almost put me to sleep as well.....almost lol!"
},
		{
			"rate":40,
			"text": "This movie deservedly won the Oscar for its Special Effects. Russ Tamblyn was well-cast as Tom. The romantic sub-plot is superfluous and Terry-Thomas and Peter Sellers all but steal the movie as a bumbling pair of thieves. Funny enjoyable puffball of a movie. Well worth watching for the effects and Sellers and Thomas."
},
		{
			"rate":35,
			"text": "absolutely love this film....watched it recentally, and its looking real old now but still every time i watch it, it makes me happy :)"
},
		{
			"rate":40,
			"text": "The talented RUSS TAMBLYN does a superb job of singing, dancing and acting while playing the title role of the miniature boy presented as a gift to a woodsman and his wife by a woodland spirit. They treat him as their own son and the film revolves around his misadventures after his parents are wrongly accused of a crime and he must find the real culprits (TERRY-THOMAS and PETER SELLERS) in time for a happy ending. The trick photography is marvelous, the toys that come to life are inventive and fun, the interaction between Tom and all the other townspeople is well done--and this was all before the CGI effects we have today. There's a lot of charm to several musical numbers, especially one called Yawning Man, and all of the song-and-dance numbers are done in rollicking style. Songstress Peggy Lee wrote several clever songs. Well worth watching, a family film that can be enjoyed by adults or children. Tamblyn's talents are given full reign in this one."
},
		{
			"rate":40,
			"text": "My favorite childhood movie ,i watched it like 27/7 and never get bored."
},
		{
			"rate":-1,
			"text": "nominated for best picture at the golden globes"
},
		{
			"rate":40,
			"text": "Love that film so much! the yawning man rules!"
},
		{
			"rate":40,
			"text": "never seen up to about a week and half ago"
},
		{
			"rate":50,
			"text": "I so use to love this movie. Sunday afternoons, usually at Easter. It brings back memories lol. Russ Tamblyn is great"
},
		{
			"rate":50,
			"text": "another brilliant classic"
},
		{
			"rate":50,
			"text": "Terry Thomas. Best villain ever!"
},
		{
			"rate":40,
			"text": "Lol i used to have this on vid watched it thousands of times when i was miniature, aww bless. Russ' dancing was classic... swinging from cots etc."
},
		{
			"rate":50,
			"text": "THE BEST FILM EVER!!!!!!!!!!!!!!!!!!!!"
},
		{
			"rate":50,
			"text": "i need to see this again....someone taped over the last copy :'("
},
		{
			"rate":35,
			"text": "love this and used to watch it wiv ma grandad."
},
		{
			"rate":50,
			"text": "Steady Boy....lol Its the one things i can remeber from this movie, Tom whiserping 'Steady Boy' into the ear of the horse/pony/donkey. And also Tamblyns erfect aerobatic dance routines as he danced with the enourmous toys in the bedroom."
},
		{
			"rate":50,
			"text": "SUCH A SPECIAL FILM...A MUST SEE."
},
		{
			"rate":45,
			"text": "Used to watch it sooo much as a little kid (whenever it came on the TV). It might not be the same for people watching it now, but its a children's classic! :-D"
},
		{
			"rate":-1,
			"text": "i might like this film"
},
		{
			"rate":50,
			"text": "Good movie. Wilbur Post from Mr. Ed is in this movie."
},
		{
			"rate":45,
			"text": "one for you and two for me..."
},
		{
			"rate":40,
			"text": "This movie was cute but unrealistic but still good"
},
		{
			"rate":50,
			"text": "an oldy but a classic"
},
		{
			"rate":40,
			"text": "a classic.. watched it wen i was 4 years old.. and still remb the quoteeach peach pear plum in comes tom thumb"
},
		{
			"rate":40,
			"text": "Just for The Yawning Man, of course."
},
		{
			"rate":35,
			"text": "I don't remember too much about it."
},
		{
			"rate":35,
			"text": "6.5/10. Surprisingly charming family film. Some innovative special effects. The musical numbers are not particularly special but they aren't bad. Agile Russ Tamblyn as the title character is remarkable. Well made great fun."
},
		{
			"rate":30,
			"text": "I HAVE to watch this again for Peter Sellers! Apparently I missed him the first 10 times as a kid!"
},
		{
			"rate":50,
			"text": "awww this reminds me of being 5 years old yay"
}	]
}