{
	"title": "Say Amen, Somebody (1980)",
	"synopsis":"Say Amen, Somebody is an exuberant documentary spotlighting the special world of gospel music. At least two dozen hand-clapping songs are featured within the film's joyous 100 minutes. Special emphasis is given the lives and careers of two giants in the business: Singers Mother Willie Mae Ford Smith and Professor Thomas A. Dorsey. Other artists spotlighted at work and at leisure include Sallie Martin, the Barrett Sisters, and the O'Neill Brothers. Just try sitting still in your chair while experiencing Say Amen, Somebody. ~ Hal Erickson, Rovi",
	"genre":
	[
		{"name":"Documentary"},
		{"name":"Drama"}
,
		{"name":"Faith & Spirituality"}
,
		{"name":"Musical & Performing Arts"}

	],
	"rate":3.1,
	"comments":[
	]
}