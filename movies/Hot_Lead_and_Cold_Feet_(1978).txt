{
	"title": "Hot Lead and Cold Feet (1978)",
	"synopsis":"In this Disney western, Jim Dale plays Eli Bloodshy, and his twin sons Wild Billy and Jasper. The older man has founded the town of Bloodshy, and now that he has apparently died, his sons must battle for control of his legacy in a wild train race. One of them is a city-slicker, a mild-mannered, bible-spouting fellow; the other is a gun-fighting, drunken, hot-tempered lad, more at home with outlaws than with law-abiding citizens. When they settle with each other, they still have to battle venal Mayor Ragsdale (Darren McGavin) for real control. ~ Clarke Fountain, Rovi",
	"genre":
	[
		{"name":"Western"},
		{"name":"Action & Adventure"}
,
		{"name":"Kids & Family"}
,
		{"name":"Comedy"}

	],
	"rate":3.4,
	"comments":[
		{
			"rate":40,
			"text": "Hot Lead and Cold Feet is a fun Western with the Disney touch. Jim Dale is very entertaining and plays three different characters. (First viewing - In my early mid-twenties)"
},
		{
			"rate":50,
			"text": "This movie was absolutely one of my favorites as a kid. I loved it! You should watch it if you have never seen but through the eyes of a child not us grown up cynical people we are."
},
		{
			"rate":35,
			"text": "Some of the acting is a bit lame and the gags are unbelievable at times, but it's still a fun movie. I saw it as a kid, so that is probably what makes it better to me."
},
		{
			"rate":35,
			"text": "Cute Disney movie that's nearly unknown today. Gave me an appreciation for Jim Dale who had duel roles as twins and Don Knotts co-starred for extra silly fun."
},
		{
			"rate":50,
			"text": "extrmely funny fantasic classic"
},
		{
			"rate":15,
			"text": "HOT LEAD AND COLD FEET (1978)"
},
		{
			"rate":50,
			"text": ":fresh: [CENTER]A real classic Disney favorite.[/CENTER]"
},
		{
			"rate":30,
			"text": "Disney pulls out all the cliches for this one, kids. We've got the separated-at-birth twins, where one's wild and the other's a goody-two-shoes (a trainee minister, yet!). We've got the fish-out-of-water plot. We've got two spunky kids, and two spunky kids being cared for by someone other than a parent, at that. The goody-two-shoes is half pursing, half pursued by a spunky woman. And, of course, they're being done in by crooked men who want their money, which could be used for the betterment of the town. Still, it has its moments. There's a running subplot wherein Don Knotts is attempting to fight a duel with Jack Elam. Hilarity keeps ensuing, as hilarity is wont to do in this sort of film. The spunky schoolmarm is also the spunky bride-to-be from [i]North Avenue Irregulars[/i], but in this, she doesn't have to deal with an evil mother-in-law-to-be. As is par for the Disney course, she's dealing with a passel of orphans. Except not, since her beloved's father's faking his own death, of course. They never do find that out, but it's all a ploy to see both of his sons together for the first time since they were infants. Yes, it's one tired old plot after another, and yet it's still funny. Cute. Not original, not by a long shot. And not clever, either. But cute. The kids are pretty clever when they need to be, and they're not as annoying as a lot of others. Not remotely. The ending's not bad. Not [i]surprising[/i], but not bad. I review a lot of second- and third-rate Disney live-action stuff here, I know, and it's good of all of you who actually bother reading the reviews. I don't really expect any of you to run right out and pick up [i]Hot Lead and Cold Feet[/i], though there are certainly worse movies out there. But if the Disney Channel gets its act together again and starts playing their old stuff, and it happens to be on, maybe you'll take a look, huh? Or the Hallmark Channel might show it, which seems more likely."
},
		{
			"rate":40,
			"text": "Hot Lead and Cold Feet is a fun Western with the Disney touch. Jim Dale is very entertaining and plays three different characters. (First viewing - In my early mid-twenties)"
},
		{
			"rate":35,
			"text": "Cute Disney movie that's nearly unknown today. Gave me an appreciation for Jim Dale who had duel roles as twins and Don Knotts co-starred for extra silly fun."
},
		{
			"rate":40,
			"text": "Fun film. Great movie for an evening with the family."
},
		{
			"rate":40,
			"text": "A GOOD LIGHTHEARTED DISNEY MOVIE FROM OLD...LOL !!"
},
		{
			"rate":50,
			"text": "extrmely funny fantasic classic"
},
		{
			"rate":50,
			"text": "one of my favorites!"
},
		{
			"rate":15,
			"text": "HOT LEAD AND COLD FEET (1978)"
},
		{
			"rate":-1,
			"text": "No thankyou - Not interested"
},
		{
			"rate":50,
			"text": ":fresh: [CENTER]A real classic Disney favorite.[/CENTER]"
},
		{
			"rate":40,
			"text": "good wholesome,, funny family film"
},
		{
			"rate":30,
			"text": "Disney pulls out all the cliches for this one, kids. We've got the separated-at-birth twins, where one's wild and the other's a goody-two-shoes (a trainee minister, yet!). We've got the fish-out-of-water plot. We've got two spunky kids, and two spunky kids being cared for by someone other than a parent, at that. The goody-two-shoes is half pursing, half pursued by a spunky woman. And, of course, they're being done in by crooked men who want their money, which could be used for the betterment of the town. Still, it has its moments. There's a running subplot wherein Don Knotts is attempting to fight a duel with Jack Elam. Hilarity keeps ensuing, as hilarity is wont to do in this sort of film. The spunky schoolmarm is also the spunky bride-to-be from [i]North Avenue Irregulars[/i], but in this, she doesn't have to deal with an evil mother-in-law-to-be. As is par for the Disney course, she's dealing with a passel of orphans. Except not, since her beloved's father's faking his own death, of course. They never do find that out, but it's all a ploy to see both of his sons together for the first time since they were infants. Yes, it's one tired old plot after another, and yet it's still funny. Cute. Not original, not by a long shot. And not clever, either. But cute. The kids are pretty clever when they need to be, and they're not as annoying as a lot of others. Not remotely. The ending's not bad. Not [i]surprising[/i], but not bad. I review a lot of second- and third-rate Disney live-action stuff here, I know, and it's good of all of you who actually bother reading the reviews. I don't really expect any of you to run right out and pick up [i]Hot Lead and Cold Feet[/i], though there are certainly worse movies out there. But if the Disney Channel gets its act together again and starts playing their old stuff, and it happens to be on, maybe you'll take a look, huh? Or the Hallmark Channel might show it, which seems more likely."
},
		{
			"rate":50,
			"text": "Another All Time Disney favorite"
},
		{
			"rate":-1,
			"text": "i would like to see this one.."
},
		{
			"rate":50,
			"text": "This movie was absolutely one of my favorites as a kid. I loved it! You should watch it if you have never seen but through the eyes of a child not us grown up cynical people we are."
},
		{
			"rate":50,
			"text": "this aother movie i love from Don Knott's"
},
		{
			"rate":35,
			"text": "Some of the acting is a bit lame and the gags are unbelievable at times, but it's still a fun movie. I saw it as a kid, so that is probably what makes it better to me."
},
		{
			"rate":50,
			"text": "very good show will make you laught"
},
		{
			"rate":-1,
			"text": "I want to see anything with Don Knotts"
},
		{
			"rate":35,
			"text": "Pretty funny! Love Don Knotts!"
},
		{
			"rate":40,
			"text": "A GOOD LIGHTHEARTED DISNEY MOVIE FROM OLD...LOL !!"
},
		{
			"rate":50,
			"text": "This movie was so good!"
},
		{
			"rate":5,
			"text": "Waste of TIME!! Very Weak movie, DO NOT WATCH, if you do end up watching it, side effects may include: Suicide, murder ramapages, road ramapage, any kinds of ways of harming yourself, and many others..."
},
		{
			"rate":40,
			"text": "one of my favorite western when i was lil'"
},
		{
			"rate":50,
			"text": "This is a awsome movie! One of my favorites."
},
		{
			"rate":50,
			"text": "I remember this from when I was little as a hilarious movie"
}	]
}