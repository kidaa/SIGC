{
	"title": "Happily Ever After (1993)",
	"synopsis":"So-so sequel to Snow White in which the animated princess must rescue her new husband from the wicked Lord Maliss.",
	"genre":
	[
		{"name":"Documentary"},
		{"name":"Animation"}
,
		{"name":"Kids & Family"}
,
		{"name":"Musical & Performing Arts"}
,
		{"name":"Special Interest"}

	],
	"rate":3.1,
	"comments":[
		{
			"rate":25,
			"text": "Snow White is prettier in this, but a thousand times more annoying and helpless. I appreciate the whole female empowerment thing they've got going on here, but next to the original dwarves they pale in comparison."
},
		{
			"rate":25,
			"text": "Enjoyable as a child, but paying attention to it now, it has many flaws, has quite a few good things to it, but putting all down, the movie itself is almost cheesy in every way. Let's start out with the idea of a sequel. As many should know, the idea of a sequel to an already amazing film (Snow White & the Seven Dwarfs) sounds like a bad idea from the start. This sequel failed, but was charming on a stand-alone movie. Characters: The main character in here was supposed to be Snow White herself, but the film also seemed to have been focusing on other, much sillier characters. Let's take the Dwarfells for example; the female 'cousins' or the Dwarfs from the prequel we all know, only with their own special powers, but most of them being useless throughout the entire movie (& Moonbeam being the most useless of them all) except for Thunderella, who had a pretty decent hero-girl movie through the end, but even then. And then comes the villain, his main point, kidnap Snow White, but failed every time due to some predictable cause. The prince, even more useless than the dwarfells, Malice might as well have ridden himself of the prince for good. And finally Scowl the Owl & Batso the Bat; other than having extemely uncreative names, the two have no real purpose through any part of the movie, just there for laughs (which I humbly enjoyed). Animation: Nothing special, even for it's time. It was he 90s, but even then, many 90 cartoons & animated movies had better animation. The film itself would take scenes that happened before & use them again & again for another 2 times in other parts of the journey. And the cell coloring wasn't all too good either. Voice Acting: Pretty good, but there were moments in the film where the characters themselves didn't sound like they were taking themselves seriously. Mainly the magic mirror himself sounded like he was taking his role as a joke. And as for the wolf scene, who's idea was it to make Critterina & the wolves sound like they were speaking in a fake animal gibberish? I even recall hearing one of the wolves saying dessert as the growled at the ladies. Plot/Story: Though decently done, we might have seen it before, & as I mentioned, it was rather cheesy & laughable. Music: As a fan of musicals, this movie's songs couldn't win me. Many of the singers in here had voices that sounded as if they had scratched their own voice boxes (but they sound better than the Jonas Brothers), & the lyrics were just laughable. So overall, this film isn't the best animated sequel but it's also not the worst. Recommendable to kids."
},
		{
			"rate":30,
			"text": "Not so great imitation Disney."
},
		{
			"rate":50,
			"text": "I remember watching this animated, non-Disney sequel to Snow Whie And The Seven Dwarfs. Very entertaining. but too dark for young children."
},
		{
			"rate":30,
			"text": "Wow, I used to love this movie! It's a great continuation from Snow White. Mother Nature is hilarious! Great family movie."
},
		{
			"rate":10,
			"text": "nothing like a happily ever after (just to let u no that will never happen some one that happy pshh.)"
},
		{
			"rate":40,
			"text": "A very own original cartoon classic on the tale of Snow White and her charming prince. Thumbs Up! Characters voiced by today's biggest stars brings this fairytale back alive...Very cute for a low-budget animation."
},
		{
			"rate":50,
			"text": "wow I totally forgot about this movie, I used to watch this all the time when I was younger. Great movie"
},
		{
			"rate":40,
			"text": "Oh I loved this movie! Yeah it was completely crap, Snow White wasn't as hot as the original, but yeah I liked it."
},
		{
			"rate":20,
			"text": "It fits a little wonkily with the set Snow White story, but it's definitely fun for little girl to watch if you ask me. I LOVED THIS movie. Watching Snow White run around to save her pince and all that. GOOD FUN!"
},
		{
			"rate":50,
			"text": "I loved this movie so much when I was little.I think Thunderella's song was the first I ever tried memorizing ^_^"
},
		{
			"rate":50,
			"text": "My kids at work loved it, though they didnt get the irony. THe teachers well, we had to put a depends on the oldest"
},
		{
			"rate":45,
			"text": "I saw this once when I was little and I loved it, but I never could find it again and I wanna watch it."
},
		{
			"rate":45,
			"text": "This used to be like my favorite movie!! I never owned it but I almost always rented it when my parents would let me rent a movie!"
},
		{
			"rate":-1,
			"text": "Although being a so-called sequel to Snow White, it's best that i rather not view this piece of cinema since i have a feeling it will pale in comparison to the original Disney classic."
},
		{
			"rate":20,
			"text": "Poorly animated, weak story, and bad editing. Overall, a not so great animated movie."
},
		{
			"rate":10,
			"text": "Filmation is one animation studio I always overlooked and for a good reason. Most of their output is pretty much like Hanna-Barbera's except the animation is awkward. But putting the studio name and their products aside, this movie is like watching the crappy direct to video Disney sequels if it was made by a different studio. I could go in detail about why this sucks, but that could take more than three paragraphs."
},
		{
			"rate":35,
			"text": "Aunque Happily Ever After sea una forzosa secuela no oficial de Blancanieves hecha por el mismo estudio que hizo a He-Man o She-Ra, hay que reconocer que es bastante imaginativa, y con animaci?n decente. Tal vez no sorprende por su trama sino por su nostalgico toque de emoci?n en las series infantiles de los a?os 90's."
},
		{
			"rate":25,
			"text": "A childhood memory film that now I looked it i'm like I really liked this at one point"
},
		{
			"rate":40,
			"text": "For all of the bad mouthing about this movie I still enjoyed it as a kid and I enjoy it now."
},
		{
			"rate":-1,
			"text": "Although being a so-called sequel to Snow White, it's best that i rather not view this piece of cinema since i have a feeling it will pale in comparison to the original Disney classic."
},
		{
			"rate":20,
			"text": "Poorly animated, weak story, and bad editing. Overall, a not so great animated movie."
},
		{
			"rate":10,
			"text": "Filmation is one animation studio I always overlooked and for a good reason. Most of their output is pretty much like Hanna-Barbera's except the animation is awkward. But putting the studio name and their products aside, this movie is like watching the crappy direct to video Disney sequels if it was made by a different studio. I could go in detail about why this sucks, but that could take more than three paragraphs."
},
		{
			"rate":35,
			"text": "Aunque Happily Ever After sea una forzosa secuela no oficial de Blancanieves hecha por el mismo estudio que hizo a He-Man o She-Ra, hay que reconocer que es bastante imaginativa, y con animaci?n decente. Tal vez no sorprende por su trama sino por su nostalgico toque de emoci?n en las series infantiles de los a?os 90's."
},
		{
			"rate":25,
			"text": "A childhood memory film that now I looked it i'm like I really liked this at one point"
},
		{
			"rate":-1,
			"text": "Worth finding and destroying!"
},
		{
			"rate":50,
			"text": "loved this as a child... i loved hearing about the dwarfettes. they all controlled some force of nature and that pulled me in as a kid."
},
		{
			"rate":5,
			"text": "so stupid!!!! the worst!!!!"
},
		{
			"rate":20,
			"text": "Tries to recapture the magic of the original Disney film, but ultimately fails."
},
		{
			"rate":40,
			"text": "For all of the bad mouthing about this movie I still enjoyed it as a kid and I enjoy it now."
},
		{
			"rate":40,
			"text": "I for one enjoyed Happily Ever after, its Snow White sequel counterpart with its unique tone, and charm. Great film for the kids; with good animation, story, and humor."
},
		{
			"rate":30,
			"text": "OMG so many people didn't believe me that this movie does exist! Yay. I'm happy right now."
},
		{
			"rate":50,
			"text": "Tales for EVERY CHILD. A multi-cultural spin on all of our favorite fairy tales."
},
		{
			"rate":5,
			"text": "As one of the five people on Earth to have spent 1 hour and 20 minutes of their life watching this piece of cinematic wonder, I can now tell you with certainty...I WANT MY TIME BACK."
},
		{
			"rate":20,
			"text": "The animation's choppy, the plot is a total trainwreck and not clear whether it's the true Snow White sequel, the songs are forgettable, and many of the characters are just.........Bullshit, in a way that they don't explain much of their orgin!Ladies and Gentlemen, from the creators of He-Man and the Masters of The Universe, this it their final FAIL."
},
		{
			"rate":-1,
			"text": "A sequel to Snow White? No thanks."
},
		{
			"rate":50,
			"text": "Every time when I went to a movie store when I was younger I would always rent this movie. My grandma would always tell me to try a different one but I could not I loved this movie to much. I probably watched this movie over 200 times. It will always be in my heart as my favorite childhood movie."
},
		{
			"rate":10,
			"text": "An uninvented Disney knock off."
},
		{
			"rate":25,
			"text": "I saw this for the first time a couple of months ago and i have to say it's not their best work."
},
		{
			"rate":0,
			"text": "saw it only once. It was so long ago. All I remember is that I hated it!"
},
		{
			"rate":25,
			"text": "Enjoyable as a child, but paying attention to it now, it has many flaws, has quite a few good things to it, but putting all down, the movie itself is almost cheesy in every way. Let's start out with the idea of a sequel. As many should know, the idea of a sequel to an already amazing film (Snow White & the Seven Dwarfs) sounds like a bad idea from the start. This sequel failed, but was charming on a stand-alone movie. Characters: The main character in here was supposed to be Snow White herself, but the film also seemed to have been focusing on other, much sillier characters. Let's take the Dwarfells for example; the female 'cousins' or the Dwarfs from the prequel we all know, only with their own special powers, but most of them being useless throughout the entire movie (& Moonbeam being the most useless of them all) except for Thunderella, who had a pretty decent hero-girl movie through the end, but even then. And then comes the villain, his main point, kidnap Snow White, but failed every time due to some predictable cause. The prince, even more useless than the dwarfells, Malice might as well have ridden himself of the prince for good. And finally Scowl the Owl & Batso the Bat; other than having extemely uncreative names, the two have no real purpose through any part of the movie, just there for laughs (which I humbly enjoyed). Animation: Nothing special, even for it's time. It was he 90s, but even then, many 90 cartoons & animated movies had better animation. The film itself would take scenes that happened before & use them again & again for another 2 times in other parts of the journey. And the cell coloring wasn't all too good either. Voice Acting: Pretty good, but there were moments in the film where the characters themselves didn't sound like they were taking themselves seriously. Mainly the magic mirror himself sounded like he was taking his role as a joke. And as for the wolf scene, who's idea was it to make Critterina & the wolves sound like they were speaking in a fake animal gibberish? I even recall hearing one of the wolves saying dessert as the growled at the ladies. Plot/Story: Though decently done, we might have seen it before, & as I mentioned, it was rather cheesy & laughable. Music: As a fan of musicals, this movie's songs couldn't win me. Many of the singers in here had voices that sounded as if they had scratched their own voice boxes (but they sound better than the Jonas Brothers), & the lyrics were just laughable. So overall, this film isn't the best animated sequel but it's also not the worst. Recommendable to kids."
},
		{
			"rate":50,
			"text": "Great Movie. But Not as good as the original Snow White by Disney. Great songs sung by Thunderella(Tracy Ullman) And Mother Nature(Phyllis Diller)."
},
		{
			"rate":5,
			"text": "* (out of four) Terrible animated effort with art little better than that of Saturday morning cartoons. This supposed follow-up to Snow White lacks wit, charm, and imagination"
},
		{
			"rate":40,
			"text": "Another movie I liked when I was a child."
},
		{
			"rate":30,
			"text": "Not so great imitation Disney."
},
		{
			"rate":-1,
			"text": "One minor note here. Happily Ever After was originally released in 1990 and again in 1993."
},
		{
			"rate":-1,
			"text": "Geneology doesnt LIE."
},
		{
			"rate":-1,
			"text": "why is this crap even listed?"
},
		{
			"rate":35,
			"text": "it's okay,.. i like it a lot,."
},
		{
			"rate":50,
			"text": "Tales for EVERY CHILD. A multi-cultural spin on all of our favorite fairy tales."
},
		{
			"rate":25,
			"text": "It was pretty good for a little kid movie but I liked it what a twist..."
},
		{
			"rate":30,
			"text": "I loved this movie so much as a child"
},
		{
			"rate":30,
			"text": "i miss movies like this one."
},
		{
			"rate":30,
			"text": "OMG so many people didn't believe me that this movie does exist! Yay. I'm happy right now."
},
		{
			"rate":5,
			"text": "i did not mean too pick this i sware"
},
		{
			"rate":50,
			"text": "I absolutely adored this film when I was younger, even though it scared me!"
},
		{
			"rate":-1,
			"text": "Reason: ha ha ha ha ha!!"
},
		{
			"rate":50,
			"text": "I remember watching this animated, non-Disney sequel to Snow Whie And The Seven Dwarfs. Very entertaining. but too dark for young children."
},
		{
			"rate":15,
			"text": "I cried when I was little girl- It was pretty creepy..."
},
		{
			"rate":45,
			"text": "It might get you in love!"
},
		{
			"rate":50,
			"text": "omg this movie is just hilarious! the dwarfels make the movie fun!"
},
		{
			"rate":50,
			"text": "It is very dark and sinister for a sequel, usually sequals are spending christmas together or some other mushy stuff, but this time prince gets into trouble and Snow White needs to help him. Some things in the movie are also very emotional, which adds to the feeling that this wasn't just the regular make some money sequel that Disney keeps making nowadays.And the best thing? This movie is HILARIOUS!I don't think I've laughed so much watching a movie before, that Owl is so funny!"
},
		{
			"rate":50,
			"text": "thats it!!!!ever after..."
},
		{
			"rate":45,
			"text": "Nice twist on fairy tales, I like it."
},
		{
			"rate":30,
			"text": "Wow, I used to love this movie! It's a great continuation from Snow White. Mother Nature is hilarious! Great family movie."
},
		{
			"rate":-1,
			"text": "My expectations of the plot kinda wrecked it for me I was expecting alot more than what I was given...the Seven Dwarves kicked asss though"
},
		{
			"rate":10,
			"text": "nothing like a happily ever after (just to let u no that will never happen some one that happy pshh.)"
},
		{
			"rate":-1,
			"text": "a sequael to oh my gosh noooo"
},
		{
			"rate":-1,
			"text": "Worth finding and destroying!"
},
		{
			"rate":-1,
			"text": "I saw this when I was little, but for the life of me can't remember much about it."
},
		{
			"rate":30,
			"text": "movie name interesting but i need to watch it"
},
		{
			"rate":50,
			"text": "I love this movie. Long time I haven't watched it. Similar to 'Snow White and the Seven Dwarfs'."
},
		{
			"rate":40,
			"text": "A very own original cartoon classic on the tale of Snow White and her charming prince. Thumbs Up! Characters voiced by today's biggest stars brings this fairytale back alive...Very cute for a low-budget animation."
},
		{
			"rate":50,
			"text": "pretty animation, interesting storyline"
},
		{
			"rate":50,
			"text": "Another one of my favorite old movies"
},
		{
			"rate":30,
			"text": "always seemed to scare me to death as a child"
},
		{
			"rate":20,
			"text": "idk i like cartoons and this seems totally original"
},
		{
			"rate":25,
			"text": "It was funny but just a little strange, but overall a great family movie!"
},
		{
			"rate":30,
			"text": "I havn't seen this movie since I was in 1st Grade so I can't really remember it."
},
		{
			"rate":45,
			"text": "childhood movie and i love it"
},
		{
			"rate":45,
			"text": "wOW!!THYS IS me!!!...lol...."
},
		{
			"rate":50,
			"text": "wow I totally forgot about this movie, I used to watch this all the time when I was younger. Great movie"
},
		{
			"rate":45,
			"text": "my 2nd fave growing up as a kid"
},
		{
			"rate":40,
			"text": "meet the robinsons was better"
},
		{
			"rate":40,
			"text": "Oh I loved this movie! Yeah it was completely crap, Snow White wasn't as hot as the original, but yeah I liked it."
},
		{
			"rate":50,
			"text": "one of the funniest movies i have ever seen."
},
		{
			"rate":-1,
			"text": "it is SOOOOO stupid.. YUK!!!"
},
		{
			"rate":20,
			"text": "It fits a little wonkily with the set Snow White story, but it's definitely fun for little girl to watch if you ask me. I LOVED THIS movie. Watching Snow White run around to save her pince and all that. GOOD FUN!"
},
		{
			"rate":50,
			"text": "I loved this movie so much when I was little.I think Thunderella's song was the first I ever tried memorizing ^_^"
},
		{
			"rate":45,
			"text": "This movie is so weird, but I love it! The villain is really interesting, you assume this is a spoof of Snow White, but it's really alot different."
},
		{
			"rate":50,
			"text": "A good cartoon movie about Snow White after getting married to the prince. Now Lord Maliss is after her."
},
		{
			"rate":50,
			"text": "this is better than snow white and the seven dwarves. it is sooo funny"
},
		{
			"rate":5,
			"text": "so stupid!!!! the worst!!!!"
},
		{
			"rate":-1,
			"text": "looks very interesting"
},
		{
			"rate":50,
			"text": "i went to go see it when it first came out and it was so cool"
},
		{
			"rate":25,
			"text": "Cute...like it almost more than the original Snow White....and the music is great!"
},
		{
			"rate":50,
			"text": "LOVE the music and the story, and Awsome movie"
},
		{
			"rate":35,
			"text": "It was a good little kid show so many years ago"
},
		{
			"rate":50,
			"text": "a new twist to a great classic"
},
		{
			"rate":50,
			"text": "good sequal to Snow White even though it isn't by disney i still like it!!"
},
		{
			"rate":20,
			"text": "this scared me when i was little cos that dragon thig was horrible"
},
		{
			"rate":40,
			"text": "it's super cute i used to watch it all the time when i was younger"
},
		{
			"rate":50,
			"text": "My kids at work loved it, though they didnt get the irony. THe teachers well, we had to put a depends on the oldest"
},
		{
			"rate":10,
			"text": "I recently rented this and I don't see why I watched this repeatedly when I was younger. lol"
},
		{
			"rate":45,
			"text": "I saw this once when I was little and I loved it, but I never could find it again and I wanna watch it."
},
		{
			"rate":25,
			"text": "definently a kid's movie"
},
		{
			"rate":50,
			"text": "love it , even tho its nopt disney for the sequal to snow white its damn good"
},
		{
			"rate":10,
			"text": "As a kid I hated this thing. If I hated it then I am sure I still would."
},
		{
			"rate":45,
			"text": "This used to be like my favorite movie!! I never owned it but I almost always rented it when my parents would let me rent a movie!"
},
		{
			"rate":35,
			"text": "Ah, thsi brings memories..."
},
		{
			"rate":40,
			"text": "I used to love this movie when I was little!"
}	]
}