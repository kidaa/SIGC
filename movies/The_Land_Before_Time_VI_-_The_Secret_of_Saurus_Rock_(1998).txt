{
	"title": "The Land Before Time VI - The Secret of Saurus Rock (1998)",
	"synopsis":"In this animated adventure, Littlefoot the Dinosaur (voice of Thomas Dekker) and his friends return as they live in fear of the Sharp Tooths, a pack of vicious T-Rexes who don't get along with other prehistoric animals. Littlefoot finds inspiration in the stories passed along by his elders of The Lone Dinosaur, a Brontosaurus who helped defend his tribe from angry predators. When Doc (voice of Kris Kristofferson), a strong but soft-spoken older dinosaur, arrives in Littlefoot's camp, the younger lizard wonders if Doc might be the Lone Dinosaur of legend, travelling incognito. Kenneth Mars, Nancy Cartwright, and Miriam Flynn also lend their voices to this film. ~ Mark Deming, Rovi",
	"genre":
	[
		{"name":"Action & Adventure"},
		{"name":"Animation"}
,
		{"name":"Kids & Family"}
,
		{"name":"Science Fiction & Fantasy"}

	],
	"rate":2.9,
	"comments":[
		{
			"rate":35,
			"text": "it wasnt the best good as the other sequels but it was alright"
},
		{
			"rate":20,
			"text": "It's getting on my nerves that the kids aren't aging. Do all of these adventures take place in one year? Maybe. More throw away characters join in the fun as Ceranow has two nieces. I don't know who their parents are or anything, they just seem to be there.The Lon Dinosaur is pretty cool, but again we have the kids getting into trouble because they don't listen to the grownups. Bland animation, forgettable songs, but at least we have Kris Kristofferson."
},
		{
			"rate":5,
			"text": "This one I can barely even remember, but if I'm rating all of them, I'll rate this one too."
},
		{
			"rate":25,
			"text": "The first one was alright. Two three and four were great and then number five made me wonder if the magic had been lost forever. Still nice for kids."
},
		{
			"rate":15,
			"text": "Oh boy, another episode"
},
		{
			"rate":35,
			"text": "The legendary lone dinosaur Doc has returned to the Great Valley. When Littlefoot tells the story of Saurus Rock surrounding this legend, Sara's twin niece and nephew go off to find it. Littlefoot and his friends go to rescue them, breaking the rock in the process, which leads to bad luck in the Great Valley. Now Littlefoot and his friends must fix Saurus Rock to stop the bad luck.Nature/Earth phenomenon: TornadoMoral of the story: Curses are not something to mess with."
},
		{
			"rate":5,
			"text": "NO. They made too many already. Did anyone even notice that these movies jumped the shark after the FIRST movie???"
},
		{
			"rate":5,
			"text": "Okay seriously it's annoying me non-stop at this point that the asteroid hasn't come."
},
		{
			"rate":15,
			"text": "The awkward animation, corny dialogue, and campy songs cannot save Saurus Rock from standing up"
},
		{
			"rate":35,
			"text": "A decent sequel. Animation is about the same, the story is good, the songs are alright, and the characters are nice. Lot more Sharptooth action, which everybody loves, not to mention some fights."
},
		{
			"rate":15,
			"text": "Oh boy, another episode"
},
		{
			"rate":5,
			"text": "This one I can barely even remember, but if I'm rating all of them, I'll rate this one too."
},
		{
			"rate":35,
			"text": "it wasnt the best good as the other sequels but it was alright"
},
		{
			"rate":25,
			"text": "WHAT HAVE THEY DONE! That fantastic witty dark film has become a bright childish sing along mostrosity, absolute rubbish, this isnt the worst sequel, theyre all equally rubbish. instead of andventures they go and do something silly and get grounded. screw this"
},
		{
			"rate":25,
			"text": "classic children's tale"
},
		{
			"rate":-1,
			"text": "No no just the original for me!"
},
		{
			"rate":5,
			"text": "Okay seriously it's annoying me non-stop at this point that the asteroid hasn't come."
},
		{
			"rate":15,
			"text": "The awkward animation, corny dialogue, and campy songs cannot save Saurus Rock from standing up"
},
		{
			"rate":-1,
			"text": "they have made soooo much movies of this, when they gonna stop,LOL"
},
		{
			"rate":-1,
			"text": "NO MORE MOVIES OF THESE F***ING DINOSAURS."
},
		{
			"rate":-1,
			"text": "i want to see all of the land before time movies"
},
		{
			"rate":35,
			"text": "A decent sequel. Animation is about the same, the story is good, the songs are alright, and the characters are nice. Lot more Sharptooth action, which everybody loves, not to mention some fights."
},
		{
			"rate":15,
			"text": "Oh boy, another episode"
},
		{
			"rate":5,
			"text": "This one I can barely even remember, but if I'm rating all of them, I'll rate this one too."
},
		{
			"rate":-1,
			"text": "6? Really? This is so scary."
},
		{
			"rate":-1,
			"text": "my kids gre up on these. they have good stories and the characters are easy to like"
},
		{
			"rate":-1,
			"text": "A terrific first film doesn't warrant a million sequels, though they were cute."
},
		{
			"rate":35,
			"text": "it wasnt the best good as the other sequels but it was alright"
},
		{
			"rate":50,
			"text": "Good movie....well directed & also voiced too!!!"
},
		{
			"rate":40,
			"text": "I was a kid when this film was out. I cannot make a full review, but yes one I can only remember. I love the story-telling, the script as well. The voice overs were really amazing, very dramatic and sometimes cute. I love the way all was made. The drawings are amazing. And yes, the music is very inspiring. 4 stars of 5 to end this review. Enjoy and yes, recommend it for those who have little brothers and sisters or cousings :)"
},
		{
			"rate":25,
			"text": "WHAT HAVE THEY DONE! That fantastic witty dark film has become a bright childish sing along mostrosity, absolute rubbish, this isnt the worst sequel, theyre all equally rubbish. instead of andventures they go and do something silly and get grounded. screw this"
},
		{
			"rate":40,
			"text": "So cute! Love these dinsoaurs! Brings back memories!"
},
		{
			"rate":-1,
			"text": "this was a great movie"
},
		{
			"rate":-1,
			"text": "Don't get me started."
},
		{
			"rate":50,
			"text": "Loved watching these when eye was a little girl~"
},
		{
			"rate":35,
			"text": "The legendary lone dinosaur Doc has returned to the Great Valley. When Littlefoot tells the story of Saurus Rock surrounding this legend, Sara's twin niece and nephew go off to find it. Littlefoot and his friends go to rescue them, breaking the rock in the process, which leads to bad luck in the Great Valley. Now Littlefoot and his friends must fix Saurus Rock to stop the bad luck.Nature/Earth phenomenon: TornadoMoral of the story: Curses are not something to mess with."
},
		{
			"rate":-1,
			"text": "Oh Jesus how many of these movies did they make??? Got to let my baby daughter see this one too..."
},
		{
			"rate":-1,
			"text": "give it up, land before time was finished after the second one"
},
		{
			"rate":45,
			"text": "The Lone Dinosaur!!! Dinosaur! His tail is swift as lightning!!"
},
		{
			"rate":25,
			"text": "classic children's tale"
},
		{
			"rate":45,
			"text": "haha, i remember this. better than most of the LBT, good."
},
		{
			"rate":40,
			"text": "HEHE I love Land Before Time!!"
},
		{
			"rate":20,
			"text": "This series has to be one of the worst I have seen. When a 5 year old can't stand them there has to be something wrong."
},
		{
			"rate":-1,
			"text": ":D love all of these"
},
		{
			"rate":25,
			"text": "they all were pretty good"
},
		{
			"rate":5,
			"text": "the first one was the best, they didnt need to make more!"
},
		{
			"rate":-1,
			"text": "I probably watched it...but whatever."
},
		{
			"rate":-1,
			"text": "Didin't see this 1. Only the 4-5 firsts."
},
		{
			"rate":40,
			"text": "One of my prefered childhood movies. I haven't seen it in a while but I'd love to!"
},
		{
			"rate":5,
			"text": "They need to stop making these movies."
},
		{
			"rate":20,
			"text": "this was a cool movie"
},
		{
			"rate":40,
			"text": "i seen this movie so maney time but i haven not wach it in years"
},
		{
			"rate":45,
			"text": "WWWOOOWWW.I'VE TO SEE.MO."
},
		{
			"rate":-1,
			"text": "I love all the Land Before Time movies."
},
		{
			"rate":35,
			"text": "i remember this. good movie."
},
		{
			"rate":40,
			"text": "I LOVE THISMOVIE LIKE ALL OF THEM I OWN MOST OF THEM...LOL"
},
		{
			"rate":40,
			"text": "this is a very child movie!! i love it!!"
},
		{
			"rate":50,
			"text": "I love dinosaurs!The characters in this are adorable :)"
},
		{
			"rate":30,
			"text": "Unfortunately, while the series is always entertaining they stray a little further into the 'cutesy' format with every sequel. Always be a fan of the original."
},
		{
			"rate":40,
			"text": "I have this one one video"
},
		{
			"rate":50,
			"text": "lol my mum all ways thourght i akted like the long neck"
},
		{
			"rate":20,
			"text": "An ok movie at the time..."
},
		{
			"rate":-1,
			"text": "paraiso merte dugardes dinosriwoei"
},
		{
			"rate":40,
			"text": "wow i used to love the land before time films! XD were amazin!"
},
		{
			"rate":40,
			"text": "loved when i was little =]"
},
		{
			"rate":30,
			"text": "How many can they make?"
},
		{
			"rate":-1,
			"text": "Fun to watch as a child, not so much anymore."
},
		{
			"rate":40,
			"text": "I know they get repetitve after a while but I still love 'em."
},
		{
			"rate":5,
			"text": "NO. They made too many already. Did anyone even notice that these movies jumped the shark after the FIRST movie???"
},
		{
			"rate":50,
			"text": "Synopsis: Ten years and six films (most released straight to video) have made these plucky dinosaurs favorites among the younger set. This new adventure is, well, pretty much like all the others. Littlefoot hears the story of a legendary dinosaur that defe... Starring: No information available. Directed by: Charles Grosvenor LNR"
},
		{
			"rate":45,
			"text": "this is a very nice movie and kind!!!"
},
		{
			"rate":50,
			"text": "I LOVE ALL THE LAND BEFORE TIMES"
},
		{
			"rate":-1,
			"text": "i think i watched it lolbut idk it was along time ago"
},
		{
			"rate":25,
			"text": "I used to love this when i was a 5"
},
		{
			"rate":40,
			"text": "I loved these when I was a youth."
},
		{
			"rate":15,
			"text": "dumb.. there's too many of them"
},
		{
			"rate":-1,
			"text": "they should be up to land before time twenty two by now. my goodness this is worse than freddy or jason"
},
		{
			"rate":-1,
			"text": "they have made soooo much movies of this, when they gonna stop,LOL"
},
		{
			"rate":30,
			"text": "i don't like these dinosaurs"
},
		{
			"rate":30,
			"text": "dang! i cant keep track or remember them anymore for that matter...theyre up to what now 12!?"
},
		{
			"rate":20,
			"text": "their getting very boring!"
},
		{
			"rate":-1,
			"text": "i lub these mvies so i wana c this one"
},
		{
			"rate":40,
			"text": "the land before time movies used to be my favorite"
},
		{
			"rate":-1,
			"text": "Land Before Time 1 was enough to put me to sleep as a kid, I find it odd they were able to pump out 6 of these suckers."
},
		{
			"rate":30,
			"text": "this is my four favortie land before time movie"
},
		{
			"rate":40,
			"text": "Loved these movies when i was a kid!"
},
		{
			"rate":30,
			"text": "that was soo cute!!!"
},
		{
			"rate":-1,
			"text": "Kate Hudson twinkles as the heroine of How to Lose a Guy in 10 Days, a magazine writer assigned to date a guy, make all the mistakes girls make that drive guys away (being clingy, talking in ...( read more )baby-talk, etc.), and record the process like a sociological experiment. However, the guy she picks--rangy Matthew McConaughey--is an advertising executive who's just bet that he can make a woman fall in love with him in ten days; if he succeeds, he'll win a huge account that will make his career. The set-up is completely absurd, but the collision of their efforts to woo and repel creates some pretty funny scenes. McConaughey's easy charm and Hudson's lightweight impishness play well together and the plot, though strictly Hollywood formula, chugs along efficiently. At moments Hudson seems to channel her mother, Goldie Hawn, to slightly unnerving effect. --Bret FetzerKate Hudson twinkles as the heroine of How to Lose a Guy in 10 Days, a magazine writer assigned to date a guy, make all the mistakes girls make that drive guys away (being clingy, talking in ...( read more )baby-talk, etc.), and record the process like a sociological experiment. However, the guy she picks--rangy Matthew McConaughey--is an advertising executive who's just bet that he can make a woman fall in love with him in ten days; if he succeeds, he'll win a huge account that will make his career. The set-up is completely absurd, but the collision of their efforts to woo and repel creates some pretty funny scenes. McConaughey's easy charm and Hudson's lightweight impishness play well together and the plot, though strictly Hollywood formula, chugs along efficiently. At moments Hudson seems to channel her mother, Goldie Hawn, to slightly unnerving effect. --Bret Fetzerjyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy"
},
		{
			"rate":50,
			"text": "Fantastic movie for kids and adults alike"
},
		{
			"rate":25,
			"text": "The first one was alright. Two three and four were great and then number five made me wonder if the magic had been lost forever. Still nice for kids."
},
		{
			"rate":-1,
			"text": "The first one is streets ahead of all the sequels, and the sequels have no input from Don Bluth"
},
		{
			"rate":50,
			"text": "i think i feel like a kid with this movies"
},
		{
			"rate":-1,
			"text": "there's too many ''petit pied le dinosaure...'' T_T"
},
		{
			"rate":50,
			"text": "i love this movie series!!!"
},
		{
			"rate":35,
			"text": "i like to wach this when i whas a kid."
},
		{
			"rate":50,
			"text": "lol, loved this movie. The Land Before Time rox."
},
		{
			"rate":20,
			"text": "they've made too many of these movies"
},
		{
			"rate":-1,
			"text": "NO MORE MOVIES OF THESE F***ING DINOSAURS."
},
		{
			"rate":50,
			"text": "HAHAHAHA....I loved The Land Before Time Series when I was little"
},
		{
			"rate":-1,
			"text": "They should have stopped after the first one"
},
		{
			"rate":20,
			"text": "i prefer the 1st ever one"
},
		{
			"rate":50,
			"text": "i like all the land before times..."
},
		{
			"rate":-1,
			"text": "Fuck all the new LBT movies."
},
		{
			"rate":30,
			"text": "The newer ones all suck...the first ones were the best"
},
		{
			"rate":-1,
			"text": "I don't do straight to DVD flicks."
},
		{
			"rate":-1,
			"text": "Nothing can beat the first Land Before Time"
},
		{
			"rate":-1,
			"text": "ok there r too many of these"
},
		{
			"rate":50,
			"text": "a huge must great film"
},
		{
			"rate":-1,
			"text": "good god! another land before time flick?.... please please be EXTINCT already will you!!!"
},
		{
			"rate":-1,
			"text": "haha wait.. 6? ive seen up to 4 i think.."
},
		{
			"rate":-1,
			"text": "there are four of them?!?!"
},
		{
			"rate":-1,
			"text": "no sequel has stood up to the first LBT"
},
		{
			"rate":-1,
			"text": "Been too long to rate."
},
		{
			"rate":-1,
			"text": "No no just the original for me!"
},
		{
			"rate":45,
			"text": "WHOO! LAND BEFORE TIME ROCKS!"
},
		{
			"rate":50,
			"text": "i love land before time movies"
},
		{
			"rate":30,
			"text": "great animated movie"
},
		{
			"rate":25,
			"text": "yay, though not as good as the first ones...hehe"
},
		{
			"rate":50,
			"text": "I luv all the land beforetim movies!!!!!!!!!!!!!"
},
		{
			"rate":45,
			"text": "I think i've seen this film, lol! i remember watching some of them! There quite good!!"
},
		{
			"rate":35,
			"text": "I have seen all of the Land Before Time movies, and i still love them!"
},
		{
			"rate":-1,
			"text": "isnt there like 50 of these movies now? LOL"
},
		{
			"rate":50,
			"text": "I like all land before time movies!"
},
		{
			"rate":40,
			"text": "i saw this forever ago :D"
},
		{
			"rate":5,
			"text": "Secret of sauraus rock? WHO GIVES A RAT'S ASS ABOUT THESE F#$#ING DINOSUARS!!!!!!"
},
		{
			"rate":-1,
			"text": "Again...too many sequels."
},
		{
			"rate":50,
			"text": "All The Land Before Time movies are great!!!!"
},
		{
			"rate":-1,
			"text": "Should've stopped with one"
},
		{
			"rate":20,
			"text": "I HAVE SEEN ALL OF THESE AND TO BE HONEST THE FIRST ONE IS THE BEST."
},
		{
			"rate":30,
			"text": "these r sweet n funny..."
},
		{
			"rate":50,
			"text": "no matter how old i get ill always love l.b.t."
},
		{
			"rate":-1,
			"text": "I probably own it, but I can't really remember seeing it. For old time's sake a three."
},
		{
			"rate":45,
			"text": "OMG.....MY BFF AND I ARE ALWAYS TALKING ABOUT HOW WE LOVED THESE MOVIES"
},
		{
			"rate":-1,
			"text": "I have seen alot of these movies but not sure about this one...I will put it on my to watch list lol"
},
		{
			"rate":20,
			"text": "I miss the old Land Before Times... lol"
},
		{
			"rate":30,
			"text": "I watched like the first one when I was a kid and it was awesome so this prob is to"
},
		{
			"rate":45,
			"text": "fantastic family movie"
},
		{
			"rate":50,
			"text": "i luv all the land before time movies & i luv it when they sing!!!!!!"
},
		{
			"rate":-1,
			"text": "Not a chance, they should never have made sequels"
},
		{
			"rate":25,
			"text": "The sixth video about Little Foot and his pals in the wondrous world of dinosaurs."
},
		{
			"rate":-1,
			"text": "These movies got a little played out after the sequel to The land before time. III-?? were not that great!"
},
		{
			"rate":-1,
			"text": "They should have stopped after the first Land Before Time."
},
		{
			"rate":50,
			"text": "I love the land before time this is one of my favs"
},
		{
			"rate":50,
			"text": "i luv the land before time!!!!!!!"
},
		{
			"rate":10,
			"text": "I think is the second worset movie of land before Time"
},
		{
			"rate":35,
			"text": "good old land before time"
},
		{
			"rate":-1,
			"text": "STOP MAKING THESE MOVIES!!!"
},
		{
			"rate":-1,
			"text": "Yeah.... I liked the first few ones of these, but its getting soooo old now"
},
		{
			"rate":-1,
			"text": "I think I stopped at 4. Forget this."
},
		{
			"rate":-1,
			"text": "lost interest after part 2"
},
		{
			"rate":35,
			"text": "Used 2 love this movie"
},
		{
			"rate":-1,
			"text": "i love all the land before movies i have 7 of them!"
},
		{
			"rate":35,
			"text": "Just love all the land before times :D"
},
		{
			"rate":30,
			"text": "just wish the dinosaurs would grow up a bit and get bigger with the series!"
},
		{
			"rate":25,
			"text": "I think they made so many of these, they should of just made a TV show."
},
		{
			"rate":50,
			"text": "you got to love the land before time movies there just great no matter what age you are !"
},
		{
			"rate":35,
			"text": "This ones not as good as the other ones, but it's ok."
},
		{
			"rate":30,
			"text": "loved these movies as a kid!"
},
		{
			"rate":50,
			"text": "love all the land before time movies."
},
		{
			"rate":-1,
			"text": "didn't like the first 5"
},
		{
			"rate":30,
			"text": "i love the land before time movies"
},
		{
			"rate":-1,
			"text": "NEVER WILL I WATCH THESE MOVIES AGIAN!!!!!!!!!!!!!!"
},
		{
			"rate":40,
			"text": "The ones I remember from being little get 4 stars, the ones i don't get 3"
},
		{
			"rate":45,
			"text": "Such a cute moive I loved it!"
},
		{
			"rate":35,
			"text": "It's ok and it's kinda funny."
},
		{
			"rate":30,
			"text": "DINASAURS :DSaw this when I was little. There's like 9000 of them, isn't there?"
},
		{
			"rate":20,
			"text": "These are getting old"
},
		{
			"rate":-1,
			"text": "I don't like Land Before Time, or dinosaurs so this movie and me don't go together...therefor I will not watch it."
},
		{
			"rate":20,
			"text": "The Land Before Time......MDXI, stopped watching properly really after the first two. Still nice for kids."
},
		{
			"rate":-1,
			"text": "they should've stopped after the first one!"
},
		{
			"rate":15,
			"text": "They are cute, but after awhile, you just saystop making these!"
},
		{
			"rate":35,
			"text": "only seen up2 the 5th 1"
},
		{
			"rate":-1,
			"text": "kool as man dis movie rulzzz"
},
		{
			"rate":40,
			"text": "I have all of these movies! lol!"
},
		{
			"rate":5,
			"text": "all are stupid i think"
},
		{
			"rate":-1,
			"text": "i loved the 1st one lol"
},
		{
			"rate":-1,
			"text": "I want to see it badly."
},
		{
			"rate":30,
			"text": "the original's the best"
},
		{
			"rate":-1,
			"text": "how many of these movies are there?"
},
		{
			"rate":50,
			"text": "any land before time is good"
},
		{
			"rate":-1,
			"text": "my kids love dinosaur films"
},
		{
			"rate":45,
			"text": "It Brill!! I Watch It Since I Was 5 Lol xx-xxx-xxx"
},
		{
			"rate":30,
			"text": "Ok. Not as good as some of the first ones."
},
		{
			"rate":40,
			"text": "i love land before time!!"
},
		{
			"rate":-1,
			"text": "to many of these already"
},
		{
			"rate":50,
			"text": "Gosh, i still remember their song..."
},
		{
			"rate":40,
			"text": "Max really likes these movies"
},
		{
			"rate":30,
			"text": "Can't remember much except that I saw the first six LBT movies."
},
		{
			"rate":50,
			"text": "have this movie its really good all of there adventures are great!!!!!!!!"
},
		{
			"rate":40,
			"text": "I discovered The Land Before Time's series with my little boy. He just loves it and I think it's really good too. I watched them all with him and not because he asked me too."
},
		{
			"rate":-1,
			"text": "Should have stopped at 1 on this movie!!!"
},
		{
			"rate":-1,
			"text": "How many good movies have to be killed because of stupid sequels?"
},
		{
			"rate":45,
			"text": "i love all these movies!"
},
		{
			"rate":5,
			"text": "The First Is The BEST, I Dont Like The Later Ones."
},
		{
			"rate":-1,
			"text": "sad i know but i'd like to see just how it follows on from the other ones"
},
		{
			"rate":-1,
			"text": "When is enough, enough?"
},
		{
			"rate":-1,
			"text": "Ooo this looks quite interesting, i quite like to see this film!"
},
		{
			"rate":-1,
			"text": "They have a million of these movies"
},
		{
			"rate":5,
			"text": "stop makeing these movies please"
},
		{
			"rate":35,
			"text": "lol no im cooler than cool. im frozen."
},
		{
			"rate":5,
			"text": "when will people learn! dinosaurs do not act like this!"
},
		{
			"rate":-1,
			"text": "I lost count after 3."
},
		{
			"rate":40,
			"text": "I love these movies! (Secretly... so dont tell anyone!)"
},
		{
			"rate":5,
			"text": "The first one was brillient why do so many sequels all together they are 11 film ELEVEN!"
},
		{
			"rate":-1,
			"text": "i so love the dinosaurs they are way cooler than you will ever be!!!!!!!!!!!!"
},
		{
			"rate":40,
			"text": "these creatures adorable.. love this one.."
},
		{
			"rate":5,
			"text": "Seriously! Enough is enough! When is it going to stop!?!?! 100???"
},
		{
			"rate":35,
			"text": "i love the soundtrack dinosaaaaaaauuuur!! lmao"
},
		{
			"rate":35,
			"text": "i love lil foot but there are so many of these"
},
		{
			"rate":30,
			"text": "they made to many of those"
},
		{
			"rate":-1,
			"text": "no. 6 is as bad as the rest"
},
		{
			"rate":15,
			"text": "Kids again. Tired of them!!!!"
},
		{
			"rate":35,
			"text": "instant kid classic again!"
},
		{
			"rate":45,
			"text": "I love all of them. Period."
},
		{
			"rate":35,
			"text": "have all of them very nice very cutie for the small kids"
},
		{
			"rate":-1,
			"text": "How many of these damn things did they make?"
},
		{
			"rate":-1,
			"text": "nooooooooooooooo!!!!!!!!!!!!!!!!!!!"
},
		{
			"rate":-1,
			"text": "i want to see all of the land before time movies"
},
		{
			"rate":25,
			"text": "still cant stand the series"
},
		{
			"rate":-1,
			"text": "there are, like 20 of these bad films!"
},
		{
			"rate":-1,
			"text": "AHHHHHHHHH i said NO"
},
		{
			"rate":50,
			"text": "THIS IS TH EBEST ONE WTF OMG YES!"
},
		{
			"rate":25,
			"text": "i love all the land before time movies because i love dinosauses"
},
		{
			"rate":5,
			"text": "Don't even bother..."
},
		{
			"rate":50,
			"text": "go dock!! sara's dad you know nothing about him.. and little foot your g-pa rox too!"
},
		{
			"rate":45,
			"text": "this one has a great story to it. i like the old longneck in this one."
},
		{
			"rate":40,
			"text": "jia a TOUTE vue..HAHA...Quant chtai jeune son bon!"
},
		{
			"rate":-1,
			"text": "the land before time! ive only seen up to number 4 and this is 6. i think theres more too!"
},
		{
			"rate":10,
			"text": "Now the plotlines just get dafter and dafter, as do the songs. At this point, I wipe my hands clean of commenting on the TLBT films!"
},
		{
			"rate":5,
			"text": "oh, for the love of.."
},
		{
			"rate":-1,
			"text": "In hindsight, This movie was garbage."
},
		{
			"rate":50,
			"text": "all of the land b4 time movies rock"
}	]
}