{
	"title": "Those Magnificent Men in Their Flying Machines (1965)",
	"synopsis":"Ken Annakin's large-canvas comedy Those Magnificent Men in Their Flying Machines is set in 1910. In order to boost circulation of his newspaper, Lord Rawnsley (Robert Morley) offers 10,000 pounds to the first person who can fly across the English Channel. A huge number of hopefuls enter the contest, including the scheming Sir Percy Ware-Armitage (Terry-Thomas), who, with the help of his henchman Courtney (Eric Sykes), attempts to sabotage the other entries. There is also a love triangle featuring Orvil Newton (Stuart Whitman) and Richard Mays (James Fox) competing for the heart of Patricia Rawnsley (Sarah Miles). ~ Perry Seibert, Rovi",
	"genre":
	[
		{"name":"Action & Adventure"},
		{"name":"Art House & International"}
,
		{"name":"Classics"}
,
		{"name":"Comedy"}

	],
	"rate":3.4,
	"comments":[
		{
			"rate":45,
			"text": "A comedy classic, very much of its time, full of slap-stick and silly humour. The cast is brilliant too, an eclectic mix of European favourites but mainly British comedic masters. I love the story, I love the actors and the humour, Ken Annakin is a great British director, often overlooked but more than often his films are universally enjoyed without anyone knowing much about him. For me though, the only fault I have with this film is that the sequel is much better, so if you liked this, please watch Monte Carlo or Bust (AKA Those Daring Young Men in Their Jaunty Jalopies). Fans of The Great Race and It's a Mad mad mad mad World will know what I'm on about! (It also continues the fantastic partnership of Eric Sykes and Terry Thomas, both playing their characters sons)."
},
		{
			"rate":30,
			"text": "One of those childhood classics they always showed on holidays to watch over and over again. Sure, it doesn't work quite as well anymore as it used to, with me growing up and expectations of movies changing so much. But the mere nostalgia was already worth the re-watch.A 1910 flying competition between pilots from several nations causing plenty of plane action and slapstick chases, making fun of pretty much all nations involved: the British snob, the German military man, the French womanizer, the Italian family man and the American hero, all are equally silly and still pretty likable, just like the movie itself. Unforgettable: Gert Fr?be doing his one man marching band sounds. When do we see a revival of these over the top adventure movies?"
},
		{
			"rate":20,
			"text": "This live action version of an airborne wacky races is predictable slapstick but moustache twirling Terry Thomas is always worth watching as the original Dick Dastardly."
},
		{
			"rate":40,
			"text": "Terry-Thomas' so hilarious in this extraordinary comic version of the historic 1910 London to Paris air race."
},
		{
			"rate":35,
			"text": "Another 60's comedy that I had seen years ago and re-watched (on tv) recently. I'm not sure if anyone on the production of this movie worked for Disney's live action division or if it is the general popularity of these big comedies during this decade, but this reminds me of numerous wholesome family comedy adventures. This movie is slightly more subversive though. There is a huge cast with many recognizable faces and several soon to be famous names. The credits are done with that kooky 60's animation. The bookmarks of the picture with Red Skelton, archival footage of attempted flights, and recreated clips of attempted flights are silly, but also curiously of historical value. Once we get into the central adventure of the air race I couldn't help but chuckle at the mostly slapstick misadventure. Sarah Miles as the feisty young English woman challenging a woman's role in mechanics and flight was impressive. The movie keeps track of such a large cast of international characters and yet moves along at a brisk pace. It is corny how the American and British are the ultimately winners. They fight over the girl besides, as expected. The American flyer Orvil (Whitman), who is of course portrayed as a cowboy, is a risk taking hero, but doesn't seem to be a real person. The last standing British flyer Richard (Fox) is a snobbish chauvinist. In short the comedy is full of stereotypes, but it is mostly good fun."
},
		{
			"rate":50,
			"text": "I cant rave about this enough...What a wonderful film. I loved the era and could watch this now."
},
		{
			"rate":25,
			"text": "Worth a viewing just for the absurdity of it all. The flying machines these guys come up with are worth a viewing by themselves. Benny Hill and Gert Frobe (Goldfinger from the James Bond movie with Sean Connery) are funny central characters and the overall cast was very funny. Worth watching if you really like British comedy or Monty Python sillyness."
},
		{
			"rate":30,
			"text": "A comedy classic & major childhood fav. I adore anything with Terry Thomas. Luv the title/opening sequence too. Watch with THE GREAT RACE for a comedy nite."
},
		{
			"rate":45,
			"text": "Romance, adventure, villianry, brilliant scenery, cheesy...a few words that describe a great movie. Lengthy, yet well worth the watch to see all the quaint, recreated flying machines from aviation's infancy. Also some great vintage autos & motorcycles as well. Great flic !!!"
},
		{
			"rate":30,
			"text": "Will always remember this one at the drive in when i was 8 or so. An inspiration to all Aviators. And FUNNY"
},
		{
			"rate":45,
			"text": "Great fun under many aspects."
},
		{
			"rate":35,
			"text": "This film has the same wacky mayhem feel of It's a Mad Mad Mad Mad World but it instead revolves around an international competition to fly across the English Channel. Perhaps there is not that much substance but there are some hilarious moments"
},
		{
			"rate":40,
			"text": "Hysterically funny moments. The love story is garbage but that's life. Amazing theme song. This movie is the superior version of The Great Race."
},
		{
			"rate":40,
			"text": "This was one of my favourite films as a kid! Looking back, it's very silly, and seems a bit outdated, but it's still a lot of fun."
},
		{
			"rate":45,
			"text": "This film is great fun. Set in 1910, it depicts a flying race between the best aviators in the world, and as such is choc-a-bloc full of national stereotypes, and general silliness. It's a great film for all the family which I've thoroughly enjoyed each time I've sat down to watch it."
},
		{
			"rate":35,
			"text": "Classic British comedy with all the charm and warmth you'd expect from some fantastic actors of the time."
},
		{
			"rate":25,
			"text": "Not so much a comedy as an amusement, since it rarely achieves funny. It's an amiable film, though, and shows off some great recreations of early aeroplanes."
},
		{
			"rate":50,
			"text": "This is one of my all-time favorites. The comedy ranges from nice to farce. The cinematography is great. Best of all, they built - and flew for the movie - planes of that era making some of the most beautiful flying video, ever. I bought it as soon as it came out on DVD and I re-watch it from time to time. If you've ever liked or aspired to fly, I'd bet you would like this movie."
},
		{
			"rate":45,
			"text": "Trust Me... Slapstick at it's best... will always hold this film close."
},
		{
			"rate":35,
			"text": "I watch TMMaTFM about every 2 years (VCR). Mainly it's to see ancient aircraft actually fly, in color. True there are several comic scenes that are so obviously contrived as to fast-forward through but the half dozen craft that count: Demoiselle (funderful!), Avro Triplane, Antoinette, Bristol Boxkite (or is it a Farman?) standing in for a Curtis, the Italian monoplane (Caproni?), etc. are by far worth the shananigans. The other trappings and contraptions of the times are also endearing... motorcars, motorcycles, period fashions, etc. Generally the cast is spot on: Sarah Miles is perfect and very cute; I'd risk being tossed from the race for her smile anytime. Whitman plays the American we still wish we were and his actions symbolically mirror US actions of the the early 20th Century. Dastardly Terry Thomas, must win Fox and hail Brittania Morley play quite correct, if way uptight, Edwardians. Jean-Pierre Cassals does Alberto Santos-Dumont proud in the glorious little Demoiselle (world's 1st ultralight), forever running into incarnations of the striking Irena Demick all over the countryside (can't find her pics anywhere online, d-mit!). The sterio-typing of nations reaches it's peak with the Germans, French and Italians, but the Japanese get short shrift in the script. Thank gawd for Benny Hill and his Brooklands FD! The main problem with the film is SILLYNESS. A full star rating is lost to it. A few more real period planes (instead of fake knockups hung from helicoptors) for the stunts alone would've helped immensely. But this is a 60's flick and cest le contest, what? So, to enjoy, pour a tall Guiness, stem of champers, stiff jolt of port (or herbals?). Sit back with the refills and munchies close by, be ready on the FF button and don your flyin' googles. A perfect film to lighten a dull day and be transported away."
},
		{
			"rate":30,
			"text": "Likeable film that was a good way to spend an afternoon, recovering from a bout of the flu. The character types were part of the fun. This film had the dashing English hero, and a villain, who looked to be the inspiration for the cartoon character, Dick Dastardly."
},
		{
			"rate":40,
			"text": "Loved it as a kid... love it as an adult"
},
		{
			"rate":45,
			"text": "Great fun under many aspects."
},
		{
			"rate":35,
			"text": "This film has the same wacky mayhem feel of It's a Mad Mad Mad Mad World but it instead revolves around an international competition to fly across the English Channel. Perhaps there is not that much substance but there are some hilarious moments"
},
		{
			"rate":30,
			"text": "Top gun of slapstick early-aviation comedies."
},
		{
			"rate":40,
			"text": "I loved this movie, it was a very funny movie and I want it on DVD, great funny film, I loved it, it starred Terry Thomas, Eric Sykes and Jean-Pierre Cassel. it's a great funny film I loved it"
},
		{
			"rate":35,
			"text": "This is too slow and talky for most modern audiences, but it's certainly got its moments. I loved the intro on earliest-flight, the production skill in resurrecting the oldest functional planes (and cars), some of the stuntwork, most of the cast, the stunning English countryside location work, and the book end cartoons.The central love triangle is a dud, partly because those three cast members aren't too charismatic. In fact, I rate this film nowhere near as highly as The Great Race - partly because the leads can't compare to Curtis/Lemmon/Falk/Wood/Wynn, partly because of the slower pace, and partly because The Great Race is SO much sillier than TMMITFM.Yet it's still a fun artifact and an impressive production parodying a fascinating age. Highlights include Terry Thomas's moustache twirling, Benny Hill, and Gert Frobe doing un-Goldfinger like comedy. The best thing about this film is that it was the inspiration for Wacky Races."
},
		{
			"rate":40,
			"text": "Hysterically funny moments. The love story is garbage but that's life. Amazing theme song. This movie is the superior version of The Great Race."
},
		{
			"rate":20,
			"text": "I find it still funny even after all of the time since it came out."
},
		{
			"rate":40,
			"text": "This was one of my favourite films as a kid! Looking back, it's very silly, and seems a bit outdated, but it's still a lot of fun."
},
		{
			"rate":45,
			"text": "This film is great fun. Set in 1910, it depicts a flying race between the best aviators in the world, and as such is choc-a-bloc full of national stereotypes, and general silliness. It's a great film for all the family which I've thoroughly enjoyed each time I've sat down to watch it."
},
		{
			"rate":35,
			"text": "Classic British comedy with all the charm and warmth you'd expect from some fantastic actors of the time."
},
		{
			"rate":25,
			"text": "Not so much a comedy as an amusement, since it rarely achieves funny. It's an amiable film, though, and shows off some great recreations of early aeroplanes."
},
		{
			"rate":50,
			"text": "This is one of my all-time favorites. The comedy ranges from nice to farce. The cinematography is great. Best of all, they built - and flew for the movie - planes of that era making some of the most beautiful flying video, ever. I bought it as soon as it came out on DVD and I re-watch it from time to time. If you've ever liked or aspired to fly, I'd bet you would like this movie."
},
		{
			"rate":35,
			"text": "Another 60's comedy that I had seen years ago and re-watched (on tv) recently. I'm not sure if anyone on the production of this movie worked for Disney's live action division or if it is the general popularity of these big comedies during this decade, but this reminds me of numerous wholesome family comedy adventures. This movie is slightly more subversive though. There is a huge cast with many recognizable faces and several soon to be famous names. The credits are done with that kooky 60's animation. The bookmarks of the picture with Red Skelton, archival footage of attempted flights, and recreated clips of attempted flights are silly, but also curiously of historical value. Once we get into the central adventure of the air race I couldn't help but chuckle at the mostly slapstick misadventure. Sarah Miles as the feisty young English woman challenging a woman's role in mechanics and flight was impressive. The movie keeps track of such a large cast of international characters and yet moves along at a brisk pace. It is corny how the American and British are the ultimately winners. They fight over the girl besides, as expected. The American flyer Orvil (Whitman), who is of course portrayed as a cowboy, is a risk taking hero, but doesn't seem to be a real person. The last standing British flyer Richard (Fox) is a snobbish chauvinist. In short the comedy is full of stereotypes, but it is mostly good fun."
},
		{
			"rate":45,
			"text": "A comedy classic, very much of its time, full of slap-stick and silly humour. The cast is brilliant too, an eclectic mix of European favourites but mainly British comedic masters. I love the story, I love the actors and the humour, Ken Annakin is a great British director, often overlooked but more than often his films are universally enjoyed without anyone knowing much about him. For me though, the only fault I have with this film is that the sequel is much better, so if you liked this, please watch Monte Carlo or Bust (AKA Those Daring Young Men in Their Jaunty Jalopies). Fans of The Great Race and It's a Mad mad mad mad World will know what I'm on about! (It also continues the fantastic partnership of Eric Sykes and Terry Thomas, both playing their characters sons)."
},
		{
			"rate":45,
			"text": "Trust Me... Slapstick at it's best... will always hold this film close."
},
		{
			"rate":50,
			"text": "A very amusing film that really takes off and soars."
},
		{
			"rate":35,
			"text": "I love this movie and its companion piece. Charming and fun with an international cast."
},
		{
			"rate":25,
			"text": "Worth a viewing just for the absurdity of it all. The flying machines these guys come up with are worth a viewing by themselves. Benny Hill and Gert Frobe (Goldfinger from the James Bond movie with Sean Connery) are funny central characters and the overall cast was very funny. Worth watching if you really like British comedy or Monty Python sillyness."
},
		{
			"rate":-1,
			"text": "I know the name, but don't remember for sure actually watching"
},
		{
			"rate":35,
			"text": "It's not the type of film I'd usually watch, but it was surprisingly good because of all those different planes and because of the lovely humour."
},
		{
			"rate":25,
			"text": "no its mad mad mad world but still good"
},
		{
			"rate":50,
			"text": "Absolutely hilarious spoof on the London to Paris air race, great viewing!"
},
		{
			"rate":50,
			"text": "This is ever so funny."
},
		{
			"rate":50,
			"text": "A real hoot! Timeless fun."
},
		{
			"rate":45,
			"text": "this is a great 60's british film with a lot of famous faces and such a funny cast. everyone should enjoy it. its just laugh after laugh. nice story to it, dont know why i like it so much but i do. good film."
},
		{
			"rate":-1,
			"text": "I wish I could watch every movie ever made but I sadly don't have the time. I'm not interested in this movie because I don't think I'll like it or don't know enough about it to think I might like it. I may still end up seeing it some day though."
},
		{
			"rate":30,
			"text": "One of those childhood classics they always showed on holidays to watch over and over again. Sure, it doesn't work quite as well anymore as it used to, with me growing up and expectations of movies changing so much. But the mere nostalgia was already worth the re-watch.A 1910 flying competition between pilots from several nations causing plenty of plane action and slapstick chases, making fun of pretty much all nations involved: the British snob, the German military man, the French womanizer, the Italian family man and the American hero, all are equally silly and still pretty likable, just like the movie itself. Unforgettable: Gert Fr?be doing his one man marching band sounds. When do we see a revival of these over the top adventure movies?"
},
		{
			"rate":30,
			"text": "A comedy classic & major childhood fav. I adore anything with Terry Thomas. Luv the title/opening sequence too. Watch with THE GREAT RACE for a comedy nite."
},
		{
			"rate":45,
			"text": "This is a movie that will make you laugh so hard. The cast is just wonderful."
},
		{
			"rate":50,
			"text": "I cant rave about this enough...What a wonderful film. I loved the era and could watch this now."
},
		{
			"rate":30,
			"text": "No, not my kind of movie, but I have to admit that it's funnier than I thought it would be."
},
		{
			"rate":30,
			"text": "Pretty entertaining for an older movie."
},
		{
			"rate":-1,
			"text": "Classified as a classic = Interested."
},
		{
			"rate":50,
			"text": "one of many that was made 70mm film (?)"
},
		{
			"rate":-1,
			"text": "not freaken interested"
},
		{
			"rate":35,
			"text": "I watch TMMaTFM about every 2 years (VCR). Mainly it's to see ancient aircraft actually fly, in color. True there are several comic scenes that are so obviously contrived as to fast-forward through but the half dozen craft that count: Demoiselle (funderful!), Avro Triplane, Antoinette, Bristol Boxkite (or is it a Farman?) standing in for a Curtis, the Italian monoplane (Caproni?), etc. are by far worth the shananigans. The other trappings and contraptions of the times are also endearing... motorcars, motorcycles, period fashions, etc. Generally the cast is spot on: Sarah Miles is perfect and very cute; I'd risk being tossed from the race for her smile anytime. Whitman plays the American we still wish we were and his actions symbolically mirror US actions of the the early 20th Century. Dastardly Terry Thomas, must win Fox and hail Brittania Morley play quite correct, if way uptight, Edwardians. Jean-Pierre Cassals does Alberto Santos-Dumont proud in the glorious little Demoiselle (world's 1st ultralight), forever running into incarnations of the striking Irena Demick all over the countryside (can't find her pics anywhere online, d-mit!). The sterio-typing of nations reaches it's peak with the Germans, French and Italians, but the Japanese get short shrift in the script. Thank gawd for Benny Hill and his Brooklands FD! The main problem with the film is SILLYNESS. A full star rating is lost to it. A few more real period planes (instead of fake knockups hung from helicoptors) for the stunts alone would've helped immensely. But this is a 60's flick and cest le contest, what? So, to enjoy, pour a tall Guiness, stem of champers, stiff jolt of port (or herbals?). Sit back with the refills and munchies close by, be ready on the FF button and don your flyin' googles. A perfect film to lighten a dull day and be transported away."
},
		{
			"rate":45,
			"text": ":0D god, it's great!"
},
		{
			"rate":-1,
			"text": "k im tired of sayin this old films r kool @ times but shit their r way 2 damn many yo"
},
		{
			"rate":35,
			"text": "All aviators must watch this.hehehhe"
},
		{
			"rate":-1,
			"text": "Je l'ai chez moi depuis 12 ans mais je ne l'ai toujours pas regard?..."
},
		{
			"rate":20,
			"text": "This live action version of an airborne wacky races is predictable slapstick but moustache twirling Terry Thomas is always worth watching as the original Dick Dastardly."
},
		{
			"rate":40,
			"text": "All CLASSICS are GOOD"
},
		{
			"rate":45,
			"text": "Romance, adventure, villianry, brilliant scenery, cheesy...a few words that describe a great movie. Lengthy, yet well worth the watch to see all the quaint, recreated flying machines from aviation's infancy. Also some great vintage autos & motorcycles as well. Great flic !!!"
},
		{
			"rate":40,
			"text": "Great humor even if not modern cinematography"
},
		{
			"rate":-1,
			"text": "its not really that magnificent"
},
		{
			"rate":30,
			"text": "this movie his hilarous but it's not suppose to be funny"
},
		{
			"rate":35,
			"text": "I remember see this one when I was younget and it was very funny."
},
		{
			"rate":30,
			"text": "Will always remember this one at the drive in when i was 8 or so. An inspiration to all Aviators. And FUNNY"
},
		{
			"rate":25,
			"text": "last saw this movie round '66???"
},
		{
			"rate":45,
			"text": "extremely funny, but ya got to have a good sence of humor"
},
		{
			"rate":45,
			"text": "Such an old film but soo funny!"
},
		{
			"rate":-1,
			"text": "I wonder what this is about, i think ill see it!"
},
		{
			"rate":40,
			"text": "Terry-Thomas' so hilarious in this extraordinary comic version of the historic 1910 London to Paris air race."
},
		{
			"rate":45,
			"text": "I thought it might be boring, but it's really good!"
},
		{
			"rate":40,
			"text": "Yes I really liked it, very goofball, but still funny all the same."
},
		{
			"rate":-1,
			"text": "i dont klnow why tho"
},
		{
			"rate":35,
			"text": "[font='Times New Roman'][size=4]This was a fun little movie that I remember watching on Saturday afternoon TV several times during my childhood. It?s a fairly simple story of a race set in the early 1900s between international contestants flying from London to Paris. The story adds in a love interest between the daughter of the race sponsor and the American pilot. The characters are several well known comedians from 1960s TV and film, including a bit part appearance by Benny Hill. The effects aren?t too bad and they included a few real aircraft from the era the film covers that actually flew. The music isn?t bad and does add to the comedy scenes especially. I will fully admit that it is not the most amazing film ever made, but it sure is a fun little adventure-comedy that is well worth a viewing.[/size][/font]"
},
		{
			"rate":40,
			"text": "Those Magnificent Men in Their Flying Machines - This film is almost uncoscionably silly. Whether you like that or not is not up to me. It has some pretty good humor, but is camp, camp CAMP. Sylvia - Like Ms. Plath in that it is quite aesthetically beautiful; it is unlike her in that it only has trite things to say. I wanted to like it, really I did."
}	]
}