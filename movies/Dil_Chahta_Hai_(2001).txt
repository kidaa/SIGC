{
	"title": "Dil Chahta Hai (2001)",
	"synopsis":"As Dil Chahta Hai opens, Siddarth, known as Sid (Akshaye Khanna) is racing to the hospital due to an unexplained emergency. There, he is reunited with his old friend, Sameer (Saif Ali Khan). Sameer calls the third member of the trio, Akash (Bombay superstar Aamir Khan from Lagaan), who tells him he's not going to join them at the hospital, due to an unresolved dispute between him and Sid. The film then flashes back to earlier, happier days, to show us what came between the three close friends. Sid was the studious, serious one, with a hidden artistic side, while Sameer foolishly let himself be ruled by the woman in his life. Akash could never limit himself to one woman. He was a playboy who claimed he didn't believe in love. Sameer found himself unexpectedly falling for Pooji (Sonali Kulkarni of Mission Kashmir), the young woman his parents have tried to arrange for him to marry. Akash's father grew concerned about his son's immaturity, and sent Akash to Australia to work for his company. There, he ran into Shalini (Preity Zinta, who also starred in Mission Kashmir), a girl who once rejected him in a Bombay nightclub. She's engaged to be married to Rohit (Ayub Khan), but as the two spend time together, Akash falls hard for Shalini. For his part, Sid met a beautiful divorcee, Tara (Dimple Kapadia), but his friends and family weren't ready to accept their relationship. Eventually, we work our way back to the present, and find out what's become of the three friends and their romantic entanglements. Dil Chahta Hai, a huge hit in India, marked the feature debut of writer-director Farhan Akhtar. ~ Josh Ralske, Rovi",
	"genre":
	[
		{"name":"Art House & International"},
		{"name":"Comedy"}

	],
	"rate":0,
	"comments":[
		{
			"rate":50,
			"text": "Heart-warmingly Funny, technically innovative, with great performances. A movie that revolutionised the way Bollywood FilmMakers think.."
},
		{
			"rate":40,
			"text": "Considering that I've only ever seen two Bollywood movies, (one being this one, the other the newer version of Devdas) I can't really compere this effectively to others. However, as a film in its own right it's pretty damn good. A lot of Western viewers would probably be put off by the musical element and the language barrier/having to read subtitles, but if you suspend disbelief of people spontanously bursting into song and perservere with the subs you'll be suprised at how witty and relevant it is even to a Western audience. The nightclub dance routine is visually spectacular too."
},
		{
			"rate":20,
			"text": "Too long, not really interesting. It had one or two good songs in it, but I felt that overall it wasn't focused."
},
		{
			"rate":45,
			"text": "This movie is absolutely extraordinary! Aamir, Saif, and Akshaye are all remarkable in this movie, their characters reflect a greater bond that stands outside of just mere friendship, but that of brotherhood. This bond is strengthened and tested through all: love, youth, desires, and family. The passage of time marks their friendship as they embrace new boundaries in their lives, together and apart. There is the music as well, it beautifully transitions the sequences of events. A favorite: The three go on a vacation, and drive to the coast. (Dil Chahta Hai..). Another would be with Sameer declaring his love for Puja, and they are in the theatre watching a movie. Great movie!!"
},
		{
			"rate":35,
			"text": "<------------------------------------------WONT BE HERE ALL NIGHT AND IM NOT EVEN HERE AT THE MOMENT"
},
		{
			"rate":45,
			"text": "Excellent story about the friendship between four men and their relationships with women. Aamir Khan and Preity Zinta have wonderful chemistry, and Saif Ali Khan is comical, as always."
},
		{
			"rate":50,
			"text": "I felt like I've seen all the people in the movie before, been with them, talked to them. Then I stepped out of the theatre, and there they were!"
},
		{
			"rate":45,
			"text": "Dil chahata hai kabhi naa bite chamkile din............Dil chahta hai"
},
		{
			"rate":50,
			"text": "Movie which changed the game in Bollywood during early 2000's."
},
		{
			"rate":45,
			"text": "A movie worth watching. Very good movie."
},
		{
			"rate":35,
			"text": "Worthy Bollywood film"
},
		{
			"rate":50,
			"text": "An great movie. The movie is a complete package, everying a good movie needs. The performances, songs, and characters make this movie a classic."
},
		{
			"rate":-1,
			"text": "ma all timee favv <3 <3"
},
		{
			"rate":45,
			"text": "great work by farhan akhtar"
},
		{
			"rate":40,
			"text": "A feel-good and heart warming tale about love and friendship! Funny, sweet and lovable.. a wholesome movie! :)"
},
		{
			"rate":50,
			"text": "this movie bring change in indian cinema screept with different way and at the same time touches the feelings of our real life"
},
		{
			"rate":40,
			"text": "Youths of small towns and villages might not laugh at the jokes cracked but everyone will be easily connected with its characters, because of committed performances & original writing."
},
		{
			"rate":50,
			"text": "One of the best Hindi movies I've ever seen. & maybe my favorite Hindi one yet."
},
		{
			"rate":40,
			"text": "One of the most entertaining and moving Indian films I've watched."
},
		{
			"rate":45,
			"text": "trendsetter, ushering in gen y"
},
		{
			"rate":45,
			"text": "Dil chahata hai kabhi naa bite chamkile din............Dil chahta hai"
},
		{
			"rate":50,
			"text": "Movie which changed the game in Bollywood during early 2000's."
},
		{
			"rate":45,
			"text": "Although a bit cheesy, it is more contemporary and suitable for a global audience. Loved it."
},
		{
			"rate":45,
			"text": "A movie worth watching. Very good movie."
},
		{
			"rate":35,
			"text": "Worthy Bollywood film"
},
		{
			"rate":50,
			"text": "An unconventional Bollywood film that serves as a complex analysis on adult relationships. While the score is great, the songs aren't on the same level of brilliance as what the rest of Bollywood has to offer. Songs take the backseat, while drama stands in the spotlight. As a result, brilliant writing and performances create a unique journey of emotions."
},
		{
			"rate":25,
			"text": "I didn't really get why this movie was such a big deal top bollywood and all. Perhaps I went in with high expectations as a result and thus it was ruined for me. I just really felt the fight between Sid and Akash was too simple to cause such a riff and therefore one of the biggest drama's didn't pull me in. Nor did the relationship between Tara and Sid so it lost me at the most important parts. As for Sameer...his relationships were very much of the dog-humping-leg variety so I took nothing he did very seriously. I guess they might have just picked too many storylines and as a result didn't develop them enough for me. not a terrible movie but not something I'll be rushing to rewatch. Nor music that really grabbed me. I like the saying though, Dil Chahta Hai :)"
},
		{
			"rate":45,
			"text": ": another good one from Aamir Khan!"
},
		{
			"rate":50,
			"text": "An great movie. The movie is a complete package, everying a good movie needs. The performances, songs, and characters make this movie a classic."
},
		{
			"rate":-1,
			"text": "ma all timee favv <3 <3"
},
		{
			"rate":45,
			"text": "great work by farhan akhtar"
},
		{
			"rate":-1,
			"text": "Will have to watch this one. Not necassarily good or bad. Will see. Want to watch!"
},
		{
			"rate":40,
			"text": "A feel-good and heart warming tale about love and friendship! Funny, sweet and lovable.. a wholesome movie! :)"
},
		{
			"rate":50,
			"text": "this movie bring change in indian cinema screept with different way and at the same time touches the feelings of our real life"
},
		{
			"rate":40,
			"text": "Youths of small towns and villages might not laugh at the jokes cracked but everyone will be easily connected with its characters, because of committed performances & original writing."
},
		{
			"rate":50,
			"text": "One of the best Hindi movies I've ever seen. & maybe my favorite Hindi one yet."
},
		{
			"rate":45,
			"text": "Welcome to a summer of their lives you will never forget."
},
		{
			"rate":40,
			"text": "One of the most entertaining and moving Indian films I've watched."
},
		{
			"rate":45,
			"text": "trendsetter, ushering in gen y"
},
		{
			"rate":35,
			"text": "The best movie u can ever seee"
},
		{
			"rate":45,
			"text": "One Of the Best Movies And a game changer for Bollywood!"
},
		{
			"rate":50,
			"text": "Loved everything about it. It's my only favorite Indian movie. Well deserves the praise!"
},
		{
			"rate":50,
			"text": "One of the best Feel Good movies of the Decade"
},
		{
			"rate":50,
			"text": "Heart-warmingly Funny, technically innovative, with great performances. A movie that revolutionised the way Bollywood FilmMakers think.."
},
		{
			"rate":30,
			"text": "Ushered in a new era of Bollywood"
},
		{
			"rate":45,
			"text": "A Fantastic, Spell Bound, over the top Movie!!"
},
		{
			"rate":45,
			"text": "A moving tale of three friends and their paths after graduation. Starting out with a seemingly unknown emergency the film then back-tracks on itself to reveal the story, following the three friends (Akash, Sid and Sameer) in their trails of love. The film is a good blend of comedy and drama to keep the viewer both entertained as well as being able to relate to the characters. A must-see."
},
		{
			"rate":-1,
			"text": "I've seen that first opening dance number a million times! and although it looks quite fun and energetic, it also looked extremely silly and cheesy that sadly, it made an impression on me as what I will expect the movie will be about. Because of that I put off watching this one far too long. And so when I finally got around to watch 'Dil Chahta Hai', I'm happy to report that I was completely wrong, and that this is now one of my favorite Bollywood films, and a great film about love, relationships and lifelong friendships in general.Don't let those creepy gyrating hips fool you. And now, I'm on the lookout for Akshaye Khanna as well. XD"
},
		{
			"rate":50,
			"text": "great movie about friendship and determining what's important in life. Must watch movie of aamir khan!"
},
		{
			"rate":50,
			"text": "One of those rare Dramedies which are timeless despite their length. There is nothing about this movie, to complain. Great ensemble performance by all of the actors, Evergreen Music, Wonderful technicalities and a lovely story. Worth a million watches."
},
		{
			"rate":50,
			"text": "one of the best bollywood movie of all time."
},
		{
			"rate":40,
			"text": "one of my favorite movies"
},
		{
			"rate":50,
			"text": "CLASSICAL BACHELOR STUFF!!"
},
		{
			"rate":45,
			"text": "Friends Friends Friends"
},
		{
			"rate":50,
			"text": "Heart-warmingly Funny, technically innovative, with great performances. A movie that revolutionised the way Bollywood FilmMakers think.."
},
		{
			"rate":40,
			"text": "love it, good story, funny :)"
},
		{
			"rate":50,
			"text": "Dil Chahta Hai is the Beginning of a new generation of Bollywood films A lovely episode of three lives.......story about Friendship, Love & Life.....The Only PERFECT Bollywood Film !!!"
},
		{
			"rate":50,
			"text": "This movie is absolutely extraordinary! Aamir, Saif, and Akshaye are all remarkable in this movie, their characters reflect a greater bond that stands outside of just mere friendship, but that of brotherhood. This bond is strengthened and tested through all: love, youth, desires, and family. The passage of time marks their friendship as they embrace new boundaries in their lives, together and apart. the characters are so rich and interesting.There is the music as well, it beautifully transitions the sequences of events. A favorite: The three go on a vacation, and drive to the coast. (Dil Chahta Hai..). Another would be with Sameer declaring his love for Puja, and they are in the theatre watching a movie. Great movie!! totally outstanding one, one of the best ever. i love it. decent performances too."
},
		{
			"rate":35,
			"text": "A different Bollywood movie that explores about love and friendship. Recommended."
},
		{
			"rate":50,
			"text": "so good movie aamir did it again , he will become Mr. perfectionist"
},
		{
			"rate":50,
			"text": "its a gr8 movies....abot frndship.. love.. n bondings...!!"
},
		{
			"rate":50,
			"text": "i love the concept. It is a movie which you can see over and over again."
},
		{
			"rate":40,
			"text": "youthful and refreshing movie"
},
		{
			"rate":45,
			"text": "the coolest of the ccool movie.....refreshes the mind"
},
		{
			"rate":45,
			"text": "Wow, actually really good. Aamir Khan is magical."
},
		{
			"rate":40,
			"text": "I love the concept. The whole thing about friendship just really won my heart over it. And of course, the music is one of my favorite Bollywood soundtrack. Shankar Ehsaan Loy are amazing. I absolutely love the storyline. I think I could watch this movie again and again and never get bored."
},
		{
			"rate":40,
			"text": "friends...friends...friends! 3 good friends... their mischievous... nice story. ya and the songs are also good.i'm really jealous of their friendship. good climax for the story + nice b.g.m-s.i just can't believe it. Farhan Akthar directed this great movie?? Only after seeing 'Luck by chance', i came to know about this."
},
		{
			"rate":50,
			"text": "The film is about three distinct characters, Akash (Aamir Khan), Sameer (Saif Ali Khan) and Siddharth (Akshaye Khanna), their individual relationships and the effect that these relationships have on them."
},
		{
			"rate":50,
			"text": "ma all time favorite....a classic abt friendship and love..."
},
		{
			"rate":50,
			"text": "Beautiful movie on friendship and love, both inexplicable and unexpected. Fascinating the part showing Akash's catharsis. Must be seen. Must be listened to. Very good soundtracks."
},
		{
			"rate":45,
			"text": "watch it with your friends!"
},
		{
			"rate":45,
			"text": "cool different ideas love isthe women"
},
		{
			"rate":50,
			"text": "yes full rating goes to this one Farhan Akhtar in the directors chair bringing out a superb plot for this film... released in the year 2000...consisting of peppy numbers(like Dil Chahta hai,,Jaane Kyun) and good actors....and event a good work done by Shankar Ehsaan Loy for the music... The story of 3 collage frnds and and their life after they got seperated for their jobs and work...."
},
		{
			"rate":35,
			"text": "i can watch it a thousand times."
},
		{
			"rate":50,
			"text": "This is the Movie is so resemble to My Life.....Love it..."
},
		{
			"rate":-1,
			"text": "i want to see this again"
},
		{
			"rate":50,
			"text": "Really nice movie! For youngsters especially.."
},
		{
			"rate":40,
			"text": "Love the movie..trend setter.."
},
		{
			"rate":35,
			"text": "Hindi movie with something different! Can enjoy!"
},
		{
			"rate":35,
			"text": "after watching this movie, I really want to be in love.. huhu~"
},
		{
			"rate":45,
			"text": "What a brilliant film it was. The most modern,contemperory and youthful film of india. Farhan akhtar showed in his first film itself that he is a brilliant director. Superb and the most natural acting ever in indian cinema by its ensemble cast, gReat music, entertaining screenplay,funny dialogues and the perfect script makes this film a truly outstanding film"
},
		{
			"rate":45,
			"text": "a used to have the same life style"
},
		{
			"rate":10,
			"text": "GREAT GOD, this MOVIE IS SO Extremely HIGHLY RATED. It is a silly Movie. *roll eyes* The characters are not convincing enough for example that Pretty Zinta girl or whatever her name is, she can not act at all, seems sort of wannabe actor I dont believe they hired Dimple, how old is that woman now, 400 years? This Movie gets one star only because of one very nice song.."
},
		{
			"rate":40,
			"text": "one of the first made for youth movies"
},
		{
			"rate":50,
			"text": "friendship, love....and many more!"
},
		{
			"rate":50,
			"text": "every scene and every shot in this movie surpasses all other big hits in excellence...every performance is highly commendable and the direction really fresh!"
},
		{
			"rate":50,
			"text": "great moviereally funny"
},
		{
			"rate":35,
			"text": "<------------------------------------------WONT BE HERE ALL NIGHT AND IM NOT EVEN HERE AT THE MOMENT"
},
		{
			"rate":20,
			"text": "Too long, not really interesting. It had one or two good songs in it, but I felt that overall it wasn't focused."
},
		{
			"rate":40,
			"text": "nice movie to watch, was fun filled and romantic at times."
},
		{
			"rate":50,
			"text": "Both my favoutite stars together and a taut storyline.........Awesome mix of comedy,love and tradgedy...........a must watch for the youth"
},
		{
			"rate":45,
			"text": "its also a must watch..."
},
		{
			"rate":35,
			"text": "i feel like watching this movie again again"
},
		{
			"rate":35,
			"text": "hAve seen it but.. ages ago so i dont remember it7/10"
},
		{
			"rate":40,
			"text": "daz my favourite...."
},
		{
			"rate":50,
			"text": "My all-time favorite"
},
		{
			"rate":40,
			"text": "Clearly a major turn in bollwood ! Hats off to Farhan!"
},
		{
			"rate":45,
			"text": "frends can well cherish dis piece"
},
		{
			"rate":35,
			"text": "Human life, relation,"
},
		{
			"rate":40,
			"text": "Had a big impact on indian hair style. The first movie to change the whole long hair style to something way different."
},
		{
			"rate":50,
			"text": "Amazing, cool, touching and stylish youth movie. It?s among one of my favourites, and it still gives me a brilliant watching experienced after watching it for 4 or 5 times. This movie experienced golden jubilee at the cinema, which means it ran at the cinema halls non-stop for over one year! This tells itself how good it is."
},
		{
			"rate":50,
			"text": "i like this movie because i like romantic movies"
},
		{
			"rate":50,
			"text": "i love indian movies and this is another good one its a must see"
},
		{
			"rate":50,
			"text": "Possibly the most realistic portrayal of friendship to come out of Bollywood."
},
		{
			"rate":50,
			"text": "yipeee................ 1 of my favourite movies is here."
},
		{
			"rate":50,
			"text": "a beautiful frendly movie"
},
		{
			"rate":45,
			"text": "Its too cool movie.Everyone shoud see it."
},
		{
			"rate":-1,
			"text": "i've like seen only 1/2 of it... i should finish it"
},
		{
			"rate":45,
			"text": "friends are foreever"
},
		{
			"rate":30,
			"text": "This is the first Bollywood film that I've watched. I thought it was okay. It definitely picked up as it went along. The songs that they sang were good but I did not like the rest of the soundtrack. It may be awhile before I try another one."
},
		{
			"rate":50,
			"text": "wow i feel it is me and my friends"
},
		{
			"rate":30,
			"text": "saif is the movie's savr"
},
		{
			"rate":45,
			"text": "Welcome to a summer of their lives you will never forget."
},
		{
			"rate":45,
			"text": "ma fav movie its all abt frndship..."
},
		{
			"rate":35,
			"text": "Has a class of its own.A movie all about love&friendship.the movie that reminds me of friends,FUN n masti..."
},
		{
			"rate":40,
			"text": "this is my all time fav. movie"
},
		{
			"rate":50,
			"text": "I M A HUGE FAN OF AAMIR KHAN.AFTER AMITABH THERE IS NO ONE BETTER THEN AAMIR"
},
		{
			"rate":50,
			"text": "The smooth as Butter Movie."
},
		{
			"rate":50,
			"text": "love the music and the plot. good on aamir to prove that perfection is myth and unconventional blockbusters can be made in bollywood too"
},
		{
			"rate":45,
			"text": "good friendship moview"
},
		{
			"rate":45,
			"text": "pretty nice moviemuzic was gr8!!!!!!!!"
},
		{
			"rate":40,
			"text": "i have NEVER seen such a movie, its AWESOME, i just love the story, the humour, the emotions, everything"
},
		{
			"rate":45,
			"text": "This movie is absolutely extraordinary! Aamir, Saif, and Akshaye are all remarkable in this movie, their characters reflect a greater bond that stands outside of just mere friendship, but that of brotherhood. This bond is strengthened and tested through all: love, youth, desires, and family. The passage of time marks their friendship as they embrace new boundaries in their lives, together and apart. There is the music as well, it beautifully transitions the sequences of events. A favorite: The three go on a vacation, and drive to the coast. (Dil Chahta Hai..). Another would be with Sameer declaring his love for Puja, and they are in the theatre watching a movie. Great movie!!"
},
		{
			"rate":30,
			"text": "i'm so glad i know to speack a little hindi....hehe"
},
		{
			"rate":50,
			"text": "the movie of my life amir saif n akhsay wer awsom.gr8 gr8 gr8 work from farhan akhter hes a gr8 asset of hindi cinema...... titel track iz aswom too dil chahta hai kabhi na beetay chamkilay din, simply awsome"
},
		{
			"rate":50,
			"text": "gr8 movie i just love it"
},
		{
			"rate":40,
			"text": "First logical hindi movie"
},
		{
			"rate":45,
			"text": "i love the story and the soundtrack's peachy too ^_^"
},
		{
			"rate":45,
			"text": "a very entertaining movie"
},
		{
			"rate":50,
			"text": "The BEST HINDI FILM of all time 2 me"
},
		{
			"rate":50,
			"text": "d ultimate modern n contemporary hindi feature film!!"
},
		{
			"rate":-1,
			"text": "sounds like it would be funny"
},
		{
			"rate":50,
			"text": "reallly this a fantastic movie. the story of 3 friends is awesome. i mean that their fun and fighting and girls is wonderful...."
},
		{
			"rate":40,
			"text": "Loved it, watched it several times already! Aamir Khan truly is Bollywood's Tom Hanks! He can carry off anything. I loved his naughty character in this movie. His friends, portrayed by Saif Ali Khan and Akshaye Khanna, were a fantastic compliment to the whole friendship theme in this movie. Absolutely a must watch! And the songs were great too!"
},
		{
			"rate":45,
			"text": "lovely story and execution."
},
		{
			"rate":45,
			"text": "aamir is de best....."
},
		{
			"rate":40,
			"text": "a cult classic !!!! one of the most funniest hindi films ever made..... it tells the story of three friends aamir khan, akshaye khanna and saif ali khan..... truly a masterpiece from farhan akhtar"
},
		{
			"rate":30,
			"text": "A feel-good-chummy kinda film. Liked it alot."
},
		{
			"rate":40,
			"text": "well it was a very nice movie. i liked the character played by saif."
},
		{
			"rate":40,
			"text": "The show was nice.. Aamir khan and saif ali khan's performed very well..."
},
		{
			"rate":50,
			"text": "best movie i ever seen"
},
		{
			"rate":50,
			"text": "my favourite hindi movie. A treat to watch"
},
		{
			"rate":40,
			"text": "nice concept. enjoyable."
},
		{
			"rate":-1,
			"text": "Will have to watch this one. Not necassarily good or bad. Will see. Want to watch!"
},
		{
			"rate":-1,
			"text": "The film was okay. But Saif is the best thing about the film!"
},
		{
			"rate":35,
			"text": "Cool and relaxing one"
},
		{
			"rate":40,
			"text": "A must watch 4 those who believe in friendship."
},
		{
			"rate":40,
			"text": "This was a very good movie!!!! I liked Preity and Saif's characters the best."
},
		{
			"rate":25,
			"text": "for all generation kid, young and grand"
},
		{
			"rate":35,
			"text": "nice story line..good songs."
},
		{
			"rate":45,
			"text": "OMG!!!! ethe songs bting back SOOO many memories......(Sharmi!!)"
},
		{
			"rate":50,
			"text": "Excellent movie i have ever seen. lot of humour,a bit sorrow&friendship. different emotions were shown&muzik rocks.aamir was best of 3."
},
		{
			"rate":50,
			"text": "unkahi dil ki dastaan.One of the best film i have ever seen"
},
		{
			"rate":40,
			"text": "great movie... of friendship.. no action movie.. do watch it.."
},
		{
			"rate":25,
			"text": "It was ok, funny at times, but I've seen some better"
},
		{
			"rate":50,
			"text": "ITS MY ALL TIME FAV FILM"
},
		{
			"rate":50,
			"text": "1 of the most enjoyed film eve!"
},
		{
			"rate":50,
			"text": "shows what pain true love ill bring you, if you are not open for it....a romanics story.......what the heart desires...."
},
		{
			"rate":35,
			"text": "truly hillarious...."
},
		{
			"rate":40,
			"text": "A fine balance of fun,laughter,romance and tragedy."
},
		{
			"rate":50,
			"text": "it's a diffrent type of film dosto k liye jina dosto k liye marna ...dekh k maja aa gaya..."
},
		{
			"rate":50,
			"text": "Dil Chahta Hai Moreee ! kewl one"
},
		{
			"rate":45,
			"text": "doston ki yad ajati hai"
},
		{
			"rate":45,
			"text": "shytt its gud!! story abt 3 frnds"
},
		{
			"rate":50,
			"text": "when i see it i miss my friends n our fun..."
},
		{
			"rate":50,
			"text": "life rocks if u let it ;)"
},
		{
			"rate":40,
			"text": "Great movie about friendship and love"
},
		{
			"rate":35,
			"text": "da most fungoing n kool movie."
},
		{
			"rate":50,
			"text": "must watch! godd presentations of day to day relations"
},
		{
			"rate":-1,
			"text": "now they're pulling out indian movies?"
},
		{
			"rate":40,
			"text": "I just love it. Taught me friendship. One of my Top 5 i guess"
},
		{
			"rate":40,
			"text": "Considering that I've only ever seen two Bollywood movies, (one being this one, the other the newer version of Devdas) I can't really compere this effectively to others. However, as a film in its own right it's pretty damn good. A lot of Western viewers would probably be put off by the musical element and the language barrier/having to read subtitles, but if you suspend disbelief of people spontanously bursting into song and perservere with the subs you'll be suprised at how witty and relevant it is even to a Western audience. The nightclub dance routine is visually spectacular too."
},
		{
			"rate":50,
			"text": "It is an evergreen classic...Teen will enjoy the cool and slick Hollywood style humor of it...youth will identify themselves with it and adults would take a trip down memory lane..."
},
		{
			"rate":50,
			"text": "I have watched this movie so many times and still I can't rate it any lower than the highest. Of course, the movie has its own set of flaws. But timing mattered for it. It came out in Indian cinema when rut movies were being churned out every other day. Friendship, Ambition, Emotions, Love, and Fun. You get to see it all, and experience it. We all have experienced a part of the story one time or the other. This movie brings your college days alive again."
},
		{
			"rate":35,
			"text": "My friend from Pakistan made me watch this. It was my first Bollywood film, and was actually pretty good."
},
		{
			"rate":45,
			"text": "kool naaaaaaaaaaaaaa"
},
		{
			"rate":50,
			"text": "very urban. very close to reality for upper class youth. very well depicted emotions. amir khan stole the show. every movie of his is worth watching for some reason or the other. saif ali khan's come back started from here. the music is hummable, debutante farhan akhtar made million of fans thorugh this one."
},
		{
			"rate":45,
			"text": "Excellent story about the friendship between four men and their relationships with women. Aamir Khan and Preity Zinta have wonderful chemistry, and Saif Ali Khan is comical, as always."
},
		{
			"rate":35,
			"text": "A little bit too long, but very cute and great songs! The plot was kinda all over the place...a lil."
},
		{
			"rate":40,
			"text": "new generation of youth"
},
		{
			"rate":50,
			"text": "I felt like I've seen all the people in the movie before, been with them, talked to them. Then I stepped out of the theatre, and there they were!"
},
		{
			"rate":50,
			"text": "one of the best movies on this planet!!!"
},
		{
			"rate":45,
			"text": "Also had great characters and the story was crazy!"
},
		{
			"rate":50,
			"text": "i luved it and so did my family"
},
		{
			"rate":50,
			"text": "Netime!! i can see this flick!!! Its AWSUMM!!!"
},
		{
			"rate":40,
			"text": "good story, loved it...when love hits ...it hits!!!"
},
		{
			"rate":50,
			"text": "a good movie that shows the importance of true friendship"
},
		{
			"rate":30,
			"text": "It has some memorable moments for me."
},
		{
			"rate":40,
			"text": "only one of its kind.."
},
		{
			"rate":50,
			"text": "yet anouther one of my favs!"
},
		{
			"rate":50,
			"text": "No other hindi film even comes close how innovative this movie is in terms of story."
},
		{
			"rate":45,
			"text": "great music great actorsit has comedy romance and drama GREAT"
},
		{
			"rate":35,
			"text": "A well made film by Farhaan Akhtar..simple sweet film on friendship..love n its complexities...great performances by the 3 lead actors.:-)"
},
		{
			"rate":40,
			"text": "SUPERBLY PICTURISED THE STORY OF THREE CLOSE FRIENDS."
},
		{
			"rate":45,
			"text": "this was a good movie"
},
		{
			"rate":45,
			"text": "nice & lil interestin"
},
		{
			"rate":50,
			"text": "cool.. a movie about friendship..."
},
		{
			"rate":50,
			"text": "how friends r important in life"
},
		{
			"rate":50,
			"text": "one of the best movie Everrrr !! all three acted soo good in this movie Amir khan really rockz"
},
		{
			"rate":40,
			"text": "A brillaint take on d urban upper middle class youth of d country..beautiful cinematography..a simple story..gr8 acting n direction make dis a must watch 4 all d urban youth"
},
		{
			"rate":35,
			"text": "a different perspective on love and relationships."
},
		{
			"rate":40,
			"text": "Saw this late on night on TMC's Bollywood night and LOVED it. Great intro for an American."
},
		{
			"rate":35,
			"text": "A move in the right direction for Bollywood, starring the pioneer, Amir Khan"
},
		{
			"rate":50,
			"text": "this is by far the most amazin hindi moive i have seen... the movie is abt frndship and love...its been dealt wit atmost honesty and any1 can relate to this movie...."
},
		{
			"rate":15,
			"text": "not bad not realli gud"
},
		{
			"rate":50,
			"text": "really cool..almost about luv and frnd ship..and both stands in a gr8 respect in front of me...i luve this movie."
},
		{
			"rate":50,
			"text": "This is my favorite movie EVER! i've seen it at lesst two dozen times... probably more!"
},
		{
			"rate":45,
			"text": "One of the best stories about enduring friendship I've seen"
},
		{
			"rate":-1,
			"text": "wth?!?!? what kinda language it that?!?!?!?"
},
		{
			"rate":50,
			"text": "hehehe! this was one of the most funniest movie ever! never expected akshaye to act like this! but it was fab!! all of them looked totally mind blowing!!"
},
		{
			"rate":-1,
			"text": "excellent direction. interesting script. apt cast. excellent photography. gud songs. a movie with totality"
},
		{
			"rate":50,
			"text": "about friends i liked it a lot"
},
		{
			"rate":50,
			"text": "Luved diz film hehe... diz film is very funny hehe..:D:D..."
},
		{
			"rate":50,
			"text": "one of d besttt movie of all timess!!! its a best feelin wen one sees it wid special frndss.."
},
		{
			"rate":35,
			"text": "IT WAS A GREAT MOVIE WORTH WATCHING AGAIN AND AGAIN"
},
		{
			"rate":50,
			"text": "There is no parallel to this movie. My all time favourite... We (my friend's n me) have watched it over a million times.. just perfect!"
},
		{
			"rate":50,
			"text": "A different kind of Bollywood movie"
},
		{
			"rate":-1,
			"text": "lambay arsay ke baad Aamir khan ki best movie... excellent movie wid best best songs..."
},
		{
			"rate":45,
			"text": "No doubt one of the BEST movies made by the indian cinema Farhan Akthar SUPERB job! one of a kind movie.. abt three best friends who are DIFFERENT yet clicks together..i have watched the movie many times and i feel lik i wana watch it again!"
},
		{
			"rate":45,
			"text": "always a pleasure to watch"
},
		{
			"rate":-1,
			"text": "i cud see this movie thousand of time"
},
		{
			"rate":45,
			"text": "cooool movie. everyone likes it."
},
		{
			"rate":45,
			"text": "this is one of ma fav..."
},
		{
			"rate":40,
			"text": "great romantic movie...a new style"
},
		{
			"rate":50,
			"text": "booooooooooooooooooooooooooring"
},
		{
			"rate":45,
			"text": "Changed Bollywood movies forever. The first ultra modern Bollywood movie."
},
		{
			"rate":45,
			"text": "hilarious! Oh Aamir, you are great"
},
		{
			"rate":40,
			"text": "funny, sweet and very entertaining"
},
		{
			"rate":50,
			"text": "Saif is so good and Aamair also did a brilliant job !!!!"
},
		{
			"rate":-1,
			"text": "GR8888888888888888888888"
},
		{
			"rate":50,
			"text": "another landmark movie...moving away frm the general hindi film stereotypes dil chahta hai was the best movie of 2001"
},
		{
			"rate":40,
			"text": "It started a new trend in Indian movies . A different movie altogether."
},
		{
			"rate":50,
			"text": "Amir along with akshay and Saif have spun a crazy but beautiful web of friendship!! Good stuff!!"
},
		{
			"rate":45,
			"text": "it was a gd movie i luv da songs and the movie not alot but a bit!!!!!"
},
		{
			"rate":35,
			"text": "Wonderful Indian movie!!!"
},
		{
			"rate":50,
			"text": "BEST!!! WHO'S MOVIE IS IT??"
},
		{
			"rate":50,
			"text": "well probably i've watche 15 times or more n watched a week ago n plannin when to watch again heehehee"
},
		{
			"rate":50,
			"text": "very original story gr8 performance by Preity"
},
		{
			"rate":50,
			"text": "A very cool movie foe everyone specially those who have friends..."
},
		{
			"rate":35,
			"text": "really this story is all about what ur heart ..."
},
		{
			"rate":50,
			"text": "This film was quite cool!"
},
		{
			"rate":40,
			"text": "excellent movie and amazing performance by all...and especially the cast, like saif ali khan wid AMIR KHAN...n akshay khanna???"
},
		{
			"rate":30,
			"text": "Great movie. Beautiful songs. Great story. Great performances. A little slow some times but never mind."
},
		{
			"rate":45,
			"text": "A fun, light, transition movie for the new generation"
},
		{
			"rate":35,
			"text": "I like the friendship chemistry between the actors."
},
		{
			"rate":50,
			"text": "Turning point for good Hindi Cinema"
},
		{
			"rate":50,
			"text": "Another Great movie. The music, acting, and storyline is pure greatness"
},
		{
			"rate":40,
			"text": "nice movie 4 friends"
},
		{
			"rate":40,
			"text": "good movie, good songs, good acting"
},
		{
			"rate":20,
			"text": "Crap.... pure crap..."
},
		{
			"rate":50,
			"text": "one of da path breakin n' trend setter... hey guyzz watch diis movie... if u wanna taste da neo age bollywoood flickk..."
},
		{
			"rate":50,
			"text": "lol.............a must watch fer erry1!"
},
		{
			"rate":30,
			"text": "aamir khan and da rest r gr8 in dis film"
},
		{
			"rate":50,
			"text": "hey i luv the three guys together. and its funny. lol. then it gets a bit sad at the end. ayway, luved it!"
},
		{
			"rate":40,
			"text": "Brilliant, down to earth movie about love and friendship. Very warm & reality check type film."
},
		{
			"rate":25,
			"text": "PRETTY GUD! ITS WAS ORGINAL I GUESS."
},
		{
			"rate":30,
			"text": "Hey! Now that was something."
},
		{
			"rate":45,
			"text": "One of my fav movies"
},
		{
			"rate":50,
			"text": "The film explores the ways in which the relation between this medley of characters develops. How well the three friends groove in with each other. And what changes come in their lives and their camaraderie with the coming of three women"
},
		{
			"rate":50,
			"text": "AWESOME simply Desi yaar! lol"
},
		{
			"rate":50,
			"text": "my fav bollywood film....truly awesome..."
},
		{
			"rate":35,
			"text": "this movie is funny !!very veyr funny!!"
},
		{
			"rate":35,
			"text": "this was a funny movie i liked the song tanhaii one of my favorites"
},
		{
			"rate":45,
			"text": "loved the title song"
},
		{
			"rate":50,
			"text": "If only we all had friends and a life like that of DCH."
},
		{
			"rate":50,
			"text": "Fantastic bollywood movie fit for the newer generation of asian guys and girls fresh from university, having fun, getting into trouble, falling in love etc - good music too!"
},
		{
			"rate":50,
			"text": "This movie just sooo hits the spot!!! Can watch again and again and again, back-to-back, all day long with buddies."
},
		{
			"rate":30,
			"text": "abt frenship... quite nice but ok la"
}	]
}