{
	"title": "Where the Red Fern Grows (1974)",
	"synopsis":"Based on the novel by Wilson Rawls, this film follows the events that befall a young Oklahoma farm boy as he, with the help of his two beloved hounds, struggles to help his family get by in the hard times of the 1930s. ~ Iotis Erlewine, Rovi",
	"genre":
	[
		{"name":"Drama"},
		{"name":"Action & Adventure"}
,
		{"name":"Kids & Family"}
,
		{"name":"Classics"}

	],
	"rate":3.5,
	"comments":[
		{
			"rate":35,
			"text": "This film is about a boy and two dogs that are on a quest of making the dogs the best hunting dogs Ever. Good plot, acting and music. The down side of this film is that it can be slow and sad. If you want good dog film or tear jerker, then you should give this a try."
},
		{
			"rate":30,
			"text": "a solid film based on a classic novel. like old yeller in its emphasis on a boy and his dogs, although this is better than old yeller and a bit more profound in its development of characters. i took it for granted as a kid but now realize its importance."
},
		{
			"rate":20,
			"text": "Gosh, what a tear-jerker of a movie! Kiddies be warned! It's a fun twin-dog movie for the most part, but I would recommend turning it off before the Red Fern starts growing."
},
		{
			"rate":40,
			"text": "Makes you cry..."
},
		{
			"rate":35,
			"text": "This movie is so sad. I really liked it!"
},
		{
			"rate":30,
			"text": "It's a great story of friendship and love between humans and their pets. A movie in which animals die will always make me cry and squeeze whichever one of my pets is closest to me."
},
		{
			"rate":40,
			"text": "Awww! A childhood favorite."
},
		{
			"rate":50,
			"text": "It was a very enjoyable and very touching live action family classic. It had really great cinematography, very memorable music, great adventure, drama, great characters and a very touching storyline. I remembered when I saw this movie when I was in English class with Mrs. McQueen. I highly recommend this movie to dog lovers of all ages."
},
		{
			"rate":50,
			"text": "Wilson Rawls:It's strange indeed how memories can lie dormant in a man's mind for so many years. Yet those memories can be awakened and brought forth fresh and new, just by something you've seen, or something you've heard, or the sight of an old familiar face"
},
		{
			"rate":50,
			"text": "One of the saddest movies i've ever seen, and one of the few that out of the 15 some odd times I've seen it, I still cry every single time."
},
		{
			"rate":30,
			"text": "Set during the Great Depression, a boy and his beloved hunting dogs go on an adventure, based on a popular children's book. (Not the 2003 remake)."
},
		{
			"rate":50,
			"text": "This is one of the saddest movies i have even seen and has touched my heart everutime i have seen it....if you haven't seen it before please do....you wont regreat it!"
},
		{
			"rate":50,
			"text": "I love this movie and I always cry when I watch this. The book is great and I think back to the good times when I was little when I watched this for the first time."
},
		{
			"rate":45,
			"text": "wat a sad movie its ok i loved it i started out reading the book but then my sixth grad teacher showed the class the movie when i finished the book"
},
		{
			"rate":35,
			"text": "How can anyone watch this movie and not be crying by the end of it? (Of course, I haven't seen it since the 6th grade - but that doesn't matter)"
},
		{
			"rate":50,
			"text": "A really good movie but has a sad ending. If you're one of those people who bawl like a baby, don't watch this movie. But if you can take a little tear now and then, this is a GREAT movie."
},
		{
			"rate":40,
			"text": "this movie was really sad but it was amazing the sacrifices given by the dogs out of their love for their master."
},
		{
			"rate":30,
			"text": "I'm sure I liked this as a kid. They showed it to us twice a year until fourth grade. Haven't seen it since about 1984 though."
},
		{
			"rate":50,
			"text": "Book made movie. Oh, how I cried and cried. Wonderful tale of a boy who loves his pups through and through!!"
},
		{
			"rate":45,
			"text": "This is the best movie you will ever see. It is soooooooo sad and exciting at the same time. This is probably my favorite movie of all times."
},
		{
			"rate":25,
			"text": "Where the Red Fern Grows (Norman Tokar, 1974)I don't keep a Best Books list the way I keep a Best Movies list (yet), but if I did, Wilson Rawls' Where the Red Fern Grows would be somewhere in the top ten. I don't believe there's a single book that requires multiple sittings to read that I have read more often than that one; I probably read it four or five times a year between discovering it in sixth grade (I had Summer of the Monkeys as an assignment in school, and so I decided to find out what else that guy had written) and, oh, 1990 or thereabouts. And yet somehow I had not only never seen the movie adaptation, but never even knew such a thing existed, until a few nights ago. So, with trepidation, I sat down to watch it. Now, I haven't read the book for quite a while, and I tried to take into account that the book is never as good as the movie, but still, I found myself entirely unable to work up a lot of enthusiasm for it.Plot: Billy (Seven Alone's Stewart Petersen in his screen debut; as of this writing, his film career seems to have ended in 1981) is a boy from so far on the wrong side of the tracks in the Ozarks that tracks don't even exist there yet; his family is hardscrabble-poor, trying to make a go of it farming in the middle of a drought, while his parents (Twice-Told Tales' Beverly Garland and High Plains Drifter's Jack Ging) harbor a dream of buying their uncle's feed store in Tulsa and making a living as real businesspeople. Billy's dreams are not nearly so grand; he just wants a couple of coonhounds so he can go hunting. Raccoon skins, we're told, are all the rage in Vermont, and Billy proposes using the money from them to do things like help his father get a new mule. Grandpa (The Shawshank Redemption's James Whitmore) counsels Billy that the lord helps those who helps themselves, and once Billy groks what he's on about, it's time to start scrimping, saving, and doing odd jobs to raise the money to buy a couple of hounds. From there, well, if you haven't read the book yet, get on that....and with a few exceptions, the movie tallies well with what I remember of the book, a statement I have heard numerous times reading comment threads and discussions about it. There were a few discrepancies that I rush to add may be in my head rather than on the screen. I don't remember the book being nearly as religion-centric, nor do I remember Billy being quite to selfless; it's possible to change emphases in a story and still be faithful to the source material, though, and those make sense given the structure (if you need to be clued in, look at the main title credits; the font used is right out of a fifties Western, and Tokar went for both the structure and the morals that come packaged with same). The climactic scene, on the other hand, was changed, and very deliberately; there may have been a reason for doing so, but I have yet to come up with anyone who's advanced a good reason for doing so. I also understand that one cannot fit an entire novel, faithfully, in a movie, and thus cuts need to be made; I had to question why they spent so much of the film pre-dogs, which ended up rushing the dogs section. (As a side note, that balance could have easily been rectified by cutting out all the religion and ?oh look how selfless I am? nonsense.) Maybe (again) I'm misremembering, but the competition seemed especially rushed; (quasi-spoiler alert) what I recall as taking up a good quarter of the book got crammed into about ten minutes of screen time, with lots of minor characters and that sort of thing absent, or present only in passing. Mostly what watching the movie did was remind me I need to re-read the book. ** ?"
},
		{
			"rate":20,
			"text": "reminded me of the superior the yearling"
},
		{
			"rate":35,
			"text": "The book was ok and I remember the movie being the same"
},
		{
			"rate":50,
			"text": "It was a very enjoyable and very touching live action family classic. It had really great cinematography, very memorable music, great adventure, drama, great characters and a very touching storyline. I remembered when I saw this movie when I was in English class with Mrs. McQueen. I highly recommend this movie to dog lovers of all ages."
},
		{
			"rate":50,
			"text": "I loved the book!!! The movie made me cry!!!"
},
		{
			"rate":-1,
			"text": "The book will break your heart."
},
		{
			"rate":-1,
			"text": "Wouldnt mind seeing."
},
		{
			"rate":40,
			"text": "A beautiful love story and tear-jerker."
},
		{
			"rate":10,
			"text": "Slow moving flim. Very slow moving film."
},
		{
			"rate":-1,
			"text": "Good book and movie was almost as good"
},
		{
			"rate":-1,
			"text": "i love this movie so much every time i see it i cry"
},
		{
			"rate":40,
			"text": "One of the best stories of my childhood."
},
		{
			"rate":-1,
			"text": "Such a wonderful story about a boy and his two hound dogs and the strong connection between him and the hounds."
},
		{
			"rate":50,
			"text": "An absolutely wonderful film for the whole family."
},
		{
			"rate":50,
			"text": "OH this is such a good movie, It is one of my faverites!!!!"
},
		{
			"rate":50,
			"text": "i love this movie! this, old yeller, and my dog skip are all touching movies about dogs of all things"
},
		{
			"rate":45,
			"text": "this movie make me cry when I watch it, specially when Dan die and after that Ann !_!"
},
		{
			"rate":40,
			"text": "A beautiful love story and tear-jerker."
},
		{
			"rate":40,
			"text": "It is a story of a passionate little boy and his incredible two hounds. I believe this movie is more meaningful to adults, though it maybe enjoyed by youngsters. It brings back fond memories of childhood."
},
		{
			"rate":35,
			"text": "This film is about a boy and two dogs that are on a quest of making the dogs the best hunting dogs Ever. Good plot, acting and music. The down side of this film is that it can be slow and sad. If you want good dog film or tear jerker, then you should give this a try."
},
		{
			"rate":10,
			"text": "Slow moving flim. Very slow moving film."
},
		{
			"rate":40,
			"text": "This movie was recently remade, but this 1974 version is still the best."
},
		{
			"rate":30,
			"text": "It's a great story of friendship and love between humans and their pets. A movie in which animals die will always make me cry and squeeze whichever one of my pets is closest to me."
},
		{
			"rate":50,
			"text": "Great movie and the book should be required reading for everyone in America."
},
		{
			"rate":5,
			"text": "If I ever committed a crime, the only thing that they would have to do is tie me down to a chair and put this movie on. Only after 15 minutes I would crack. I would tell them anything they wanted to know."
},
		{
			"rate":-1,
			"text": "HEARD it on the AUDIO book."
},
		{
			"rate":-1,
			"text": "had to read the book . ."
},
		{
			"rate":50,
			"text": "I remember watching this movie every time it came on when I was a kid...and cried every time!"
},
		{
			"rate":45,
			"text": "I loved this movie i cried on it but it was really good"
},
		{
			"rate":35,
			"text": "My favorite movie as a child and still is one of the best. I still start to cry at the beginning cuz I know how the story ends."
},
		{
			"rate":50,
			"text": "Wilson Rawls:It's strange indeed how memories can lie dormant in a man's mind for so many years. Yet those memories can be awakened and brought forth fresh and new, just by something you've seen, or something you've heard, or the sight of an old familiar face"
},
		{
			"rate":50,
			"text": "I cry everytime I watch this movie :("
},
		{
			"rate":-1,
			"text": "from what i herd i will probably cry omre then i did to old yeller"
},
		{
			"rate":50,
			"text": "another classic that is hard to beat"
},
		{
			"rate":-1,
			"text": "I had to read this book at school. haha"
},
		{
			"rate":50,
			"text": "One of the saddest movies i've ever seen, and one of the few that out of the 15 some odd times I've seen it, I still cry every single time."
},
		{
			"rate":40,
			"text": "soooo sad but i love it"
},
		{
			"rate":40,
			"text": "I still cry at this movie. I really like watching Beverly Garland as the mom."
},
		{
			"rate":50,
			"text": "crying and still crying,"
},
		{
			"rate":-1,
			"text": "I loved the book, which I read when I was 10. I remember crying, of course."
},
		{
			"rate":45,
			"text": "such a great movie,so sad at the end,and the movie is just as good as the book"
},
		{
			"rate":40,
			"text": "Awww! A childhood favorite."
},
		{
			"rate":30,
			"text": "Set during the Great Depression, a boy and his beloved hunting dogs go on an adventure, based on a popular children's book. (Not the 2003 remake)."
},
		{
			"rate":50,
			"text": "Such a good book, and the film did the book justice. It was very sad, but I loved every minute of it."
},
		{
			"rate":5,
			"text": "one for the whole family my ass!!"
},
		{
			"rate":40,
			"text": "I love this movie but I cry every time I watch it"
},
		{
			"rate":-1,
			"text": "The book will break your heart."
},
		{
			"rate":30,
			"text": "a solid film based on a classic novel. like old yeller in its emphasis on a boy and his dogs, although this is better than old yeller and a bit more profound in its development of characters. i took it for granted as a kid but now realize its importance."
},
		{
			"rate":-1,
			"text": "Where The Red Fern Grows, starring Beverly Garland, Bill Dunbar, Jack Ging, James Whitmore, Jeanna Wilson, Jill Clark, John Lindsey, Lonny Chapman, Rex Corley, and Stewart Peterson. I find this dog movie very interesting for viewing."
},
		{
			"rate":35,
			"text": "the movie is deffinitly not a good as the book, but they did a pretty good job."
},
		{
			"rate":50,
			"text": "Who has never seen this needs to rent it you'll love it trust me!!"
},
		{
			"rate":50,
			"text": "I loved the book!!! The movie made me cry!!!"
},
		{
			"rate":50,
			"text": "This is one of the saddest movies i have even seen and has touched my heart everutime i have seen it....if you haven't seen it before please do....you wont regreat it!"
},
		{
			"rate":20,
			"text": "Not nearly as good as the book!"
},
		{
			"rate":20,
			"text": "this movie was so sad"
},
		{
			"rate":35,
			"text": "There's just something about movies like this that instill in us a tighter bond with our animal world, dogs in particular. I would recommend reading the book rather than seeing the film, but only because most books give a much more rich storyline and depth. However, the movie does serve the book fairly well, and the teary-eyed will be gushing. Beautiful story about a boy and his dogs, and what some animals are willing to do for their human companions."
},
		{
			"rate":30,
			"text": "I watched it in english class"
},
		{
			"rate":50,
			"text": "it is very sad but good"
},
		{
			"rate":45,
			"text": "heartwarming story..."
},
		{
			"rate":35,
			"text": "A really sad movie and book. :("
},
		{
			"rate":45,
			"text": "this film was made in oklahoma"
},
		{
			"rate":40,
			"text": "Just as great as the novel. Very touching children's movie but keep the tissues handy."
},
		{
			"rate":35,
			"text": "The book is just as good -or was it better? Man -it's been a long time! I only watched it once.... a long time ago!"
},
		{
			"rate":50,
			"text": "again....just like old yeller"
},
		{
			"rate":50,
			"text": "PLEASE IF ANYONE HAS THIS MOVIE>>> TELL ME !!!!!!!!!!!!!"
},
		{
			"rate":-1,
			"text": "I'd rather read the book, but I'm sure the movie is good too. Maybe I'll watch it one day."
},
		{
			"rate":50,
			"text": "one of my top ten movies, made me cry and this does not happen so easily"
},
		{
			"rate":50,
			"text": "Classic adaptation of the book!"
},
		{
			"rate":35,
			"text": "This is really good, but very sad."
},
		{
			"rate":50,
			"text": "soooooooooooooo sad! sadder than old yeller!!!"
},
		{
			"rate":-1,
			"text": "Wouldnt mind seeing."
},
		{
			"rate":40,
			"text": "I love this film have the book to"
},
		{
			"rate":50,
			"text": "have only seen this great movie today"
},
		{
			"rate":40,
			"text": "Such a sad movie! Really good though."
},
		{
			"rate":20,
			"text": "Read the book. Trust me."
},
		{
			"rate":40,
			"text": "My first movie i saw at the cinema so this movie always remains a favorite"
},
		{
			"rate":10,
			"text": "I didn't care for it."
},
		{
			"rate":50,
			"text": "very good movie sad but good"
},
		{
			"rate":50,
			"text": "I love this movie and I always cry when I watch this. The book is great and I think back to the good times when I was little when I watched this for the first time."
},
		{
			"rate":50,
			"text": "The book was awesome too."
},
		{
			"rate":50,
			"text": "It made me cry and taught me a great leason."
},
		{
			"rate":50,
			"text": "I saw it and it was the only movie that made me cry besides the Titanic movie!!"
},
		{
			"rate":50,
			"text": "This movie made me cry."
},
		{
			"rate":50,
			"text": "this was a good film and so was the book"
},
		{
			"rate":50,
			"text": "this movie is so sad but i <3 it"
},
		{
			"rate":20,
			"text": "aww this is such a sad movie mr t reapp was the bet teadcher ever"
},
		{
			"rate":-1,
			"text": "O man such a sad movie and book! o gets me everytimg!"
},
		{
			"rate":50,
			"text": "I love and hate this movie. It's so sad!!"
},
		{
			"rate":50,
			"text": "Memories. Great Story!!"
},
		{
			"rate":50,
			"text": "my all time favorite movie"
},
		{
			"rate":30,
			"text": "wat a sad movie its ok i loved it i started out reading the book but then my sixth grad teacher showed the class the movie when i finished the book"
},
		{
			"rate":50,
			"text": "hella fucking tif=ght"
},
		{
			"rate":45,
			"text": "wat a sad movie its ok i loved it i started out reading the book but then my sixth grad teacher showed the class the movie when i finished the book"
},
		{
			"rate":30,
			"text": "saddest movie I've ever seen!"
},
		{
			"rate":50,
			"text": "I love the book , too!! This is so sad I even cried."
},
		{
			"rate":30,
			"text": "Great story that they hollywood'd at little too much."
},
		{
			"rate":50,
			"text": "the best movie in teh world"
},
		{
			"rate":15,
			"text": "I'm kind of sick of books and movies where dogs die."
},
		{
			"rate":45,
			"text": "it is really good at the begining cause it is funny and happy then once u get 2 the ending it is sad"
},
		{
			"rate":25,
			"text": "This movie was kinda weird, but good at the same time."
},
		{
			"rate":25,
			"text": "book was soo much better!!"
},
		{
			"rate":40,
			"text": "great movie, very sad"
},
		{
			"rate":45,
			"text": "sad but good. loved it."
},
		{
			"rate":50,
			"text": "Such an awesome movie!"
},
		{
			"rate":50,
			"text": "its got doggys in it"
},
		{
			"rate":45,
			"text": "Maybe my all time favorite, I only went to see it in the Theatre something like 10 times"
},
		{
			"rate":40,
			"text": "its kinda sad but very cute"
},
		{
			"rate":50,
			"text": "saw it in 6th grade and cryed!"
},
		{
			"rate":50,
			"text": "i cry throughout the whole thing. :'("
},
		{
			"rate":40,
			"text": "One of the best stories of my childhood."
},
		{
			"rate":40,
			"text": "Very sad movie, kinda stupid, but good, the kid is annoying in this."
},
		{
			"rate":40,
			"text": "A true classic if ever there was one."
},
		{
			"rate":40,
			"text": "Very good movie, it is a classic for sure, and it was filmed in my town!"
},
		{
			"rate":45,
			"text": "Another book made into a movie. Both were good."
},
		{
			"rate":40,
			"text": "Ya know, I think I almost cried when I first saw this back in the day! Poor kid having both of his dogs die! Sniff Sniff"
},
		{
			"rate":-1,
			"text": "Ugh, 2 hours of how green the grass is then the dog bites it. I hated this book."
},
		{
			"rate":45,
			"text": "Great movuie..I really like this movie!!"
},
		{
			"rate":50,
			"text": "wonderful story about the love of a boy and his dogs and the love of the dogs"
},
		{
			"rate":-1,
			"text": "I barely remember reading that book"
},
		{
			"rate":45,
			"text": "A great adventure of friendship.."
},
		{
			"rate":40,
			"text": "excellent family movie"
},
		{
			"rate":50,
			"text": "Love the movie and book!!"
},
		{
			"rate":50,
			"text": "this is a good movie"
},
		{
			"rate":40,
			"text": "A must for every boy"
},
		{
			"rate":-1,
			"text": "i loved this movie.. it is a tear jerker"
},
		{
			"rate":50,
			"text": "I love this movie. It is so sad, I cry everytime I watch it!"
},
		{
			"rate":45,
			"text": "Classic sad must see"
},
		{
			"rate":35,
			"text": "this movie is really good...and sad!!!"
},
		{
			"rate":35,
			"text": "How can anyone watch this movie and not be crying by the end of it? (Of course, I haven't seen it since the 6th grade - but that doesn't matter)"
},
		{
			"rate":45,
			"text": "SAD!!! :(( but good..."
},
		{
			"rate":50,
			"text": "An Excellent Heart Warming Story"
},
		{
			"rate":50,
			"text": "this was another good movie ! i like the new one too :) and like old yeller ..it makes me cry :("
},
		{
			"rate":50,
			"text": "Great book and story."
},
		{
			"rate":50,
			"text": "this is the best old movie i've ever seen!!!!!!"
},
		{
			"rate":50,
			"text": "Our teacher read this to us in 6th grade.. the boys even cried...bittersweet"
},
		{
			"rate":50,
			"text": "this movie is sooooo sad but i will luv it 4-ever!!!!!!!!"
},
		{
			"rate":40,
			"text": "Such a sad movie but soooo good!"
},
		{
			"rate":40,
			"text": "favorite kids book!!"
},
		{
			"rate":50,
			"text": "well i love this book and movie it made me cry i read it in gread 4 and now i read it every year atles once i read it 7 tims and saw it like 56 times i love it its realy sad and has a great meaning to it"
},
		{
			"rate":-1,
			"text": "I love this movie it is like my all time favorite movie!! The reason whu is because I love to coon hunt also!! And it is like a love story but instead of between people it is between two puppys and a boy!!"
},
		{
			"rate":50,
			"text": "Very good and touching."
},
		{
			"rate":50,
			"text": "SOOOOO sad, I cried for days after this movie!"
},
		{
			"rate":50,
			"text": "This is a great movie I still cry when I see it."
},
		{
			"rate":45,
			"text": "This movie is a classic, true to the book and a great tale of love and friendship."
},
		{
			"rate":50,
			"text": "a sad sad movie but is a good movie!"
},
		{
			"rate":50,
			"text": "very good but sad movie"
},
		{
			"rate":40,
			"text": "it is sad, but a good family movie."
},
		{
			"rate":45,
			"text": "this movie made me cry. I love animals and I dont know, it just got to me. I also love the book."
},
		{
			"rate":35,
			"text": "okay, i'm in love with old dog movies, so shoot me!"
},
		{
			"rate":-1,
			"text": "haven't seen in years, can't comment"
},
		{
			"rate":35,
			"text": "not completly faithful to the book, but good overall"
},
		{
			"rate":50,
			"text": "i fell in love withthis story after i read the book i had to see the movie"
},
		{
			"rate":50,
			"text": "A real tear jerker. But I loved it!"
},
		{
			"rate":-1,
			"text": "SO WHERE DOES THE RED FERN GROW AT??"
},
		{
			"rate":50,
			"text": "A really good movie but has a sad ending. If you're one of those people who bawl like a baby, don't watch this movie. But if you can take a little tear now and then, this is a GREAT movie."
},
		{
			"rate":40,
			"text": "this movie was really sad but it was amazing the sacrifices given by the dogs out of their love for their master."
},
		{
			"rate":50,
			"text": "If you love dogs and want a good cry this is the movie for you. Old Dan and Little Ann."
},
		{
			"rate":50,
			"text": "I first saw this movie when I was in grade school and still love it to this day."
},
		{
			"rate":50,
			"text": "it was really good but sad @ the same time!!"
},
		{
			"rate":50,
			"text": "Good movie!And sad one!A+"
},
		{
			"rate":40,
			"text": "Sad, but such a good story"
},
		{
			"rate":40,
			"text": "a wonderful little story. The movie does the book a fair amount of justice."
},
		{
			"rate":45,
			"text": "very sas. but not as sad as the book. yet it is very good"
},
		{
			"rate":30,
			"text": "I'm sure I liked this as a kid. They showed it to us twice a year until fourth grade. Haven't seen it since about 1984 though."
},
		{
			"rate":50,
			"text": "Book made movie. Oh, how I cried and cried. Wonderful tale of a boy who loves his pups through and through!!"
},
		{
			"rate":50,
			"text": "don't ask...it made me cry 'cause the doggies died..."
},
		{
			"rate":50,
			"text": "Just sad. I never watched it without crying"
},
		{
			"rate":45,
			"text": "flipin sad movie but VERY good"
},
		{
			"rate":-1,
			"text": "REad the book in junior high or highschool as well as watch a movie version both of which made me to cry"
},
		{
			"rate":-1,
			"text": "I read the book, and I have to say that the book was just amazing. Even though it was boring at parts, the whole story was just great. I really liked it a lot, and thats why I want to see the movie."
},
		{
			"rate":35,
			"text": "HAD TO WACH IN SCHOOL AND READ THE BOOK"
},
		{
			"rate":50,
			"text": "I love this movie, I cried in it to"
},
		{
			"rate":-1,
			"text": "Good book and movie was almost as good"
},
		{
			"rate":45,
			"text": "Really sad, but I loved it!"
},
		{
			"rate":50,
			"text": "Best movie but it was the saddest movie ever"
},
		{
			"rate":15,
			"text": "i havn't seen this forever but i remember liking it as a kid and thinking it was sad"
},
		{
			"rate":-1,
			"text": "ive seen the new one and i loved it cant wait to see this one!"
},
		{
			"rate":35,
			"text": "cry for this one too"
},
		{
			"rate":35,
			"text": "aww this one is a sweet tearjerker"
},
		{
			"rate":45,
			"text": "so sad i cried... I love dog movies... and i also read the book"
},
		{
			"rate":35,
			"text": "very sweet, but the book is SO much better."
},
		{
			"rate":45,
			"text": "This movie is so sad!"
},
		{
			"rate":50,
			"text": "Absolutly the BEST movie EVER made! My Favorite!"
},
		{
			"rate":50,
			"text": "This is a great moie to see after you read the book that goes to it."
},
		{
			"rate":40,
			"text": "good movie for the family"
},
		{
			"rate":50,
			"text": "this is my favorite movie"
},
		{
			"rate":30,
			"text": "the movie is stupid compared to the book or i think so you'd have to actually (READ IT) to compare"
},
		{
			"rate":50,
			"text": "Fantastic movie! One of the best family movies ever."
},
		{
			"rate":30,
			"text": "Really great. The book was wonderful, too."
},
		{
			"rate":35,
			"text": "I like the book more, as always."
},
		{
			"rate":40,
			"text": "classic read the book in elementary school"
},
		{
			"rate":45,
			"text": "realy sad when the dogs dies"
},
		{
			"rate":40,
			"text": "FIRST MOVIE I EVER SEEN IN A THEATER"
},
		{
			"rate":50,
			"text": "A classic movie! The book is better, but the movie is good, too. Read the book first."
},
		{
			"rate":35,
			"text": "i liked the book. the book made me cry. the movie was ok, but still pretty good..."
},
		{
			"rate":40,
			"text": "Teaches you a lesson in life and how dogs really do care for eachother"
},
		{
			"rate":45,
			"text": "This is the best movie you will ever see. It is soooooooo sad and exciting at the same time. This is probably my favorite movie of all times."
},
		{
			"rate":35,
			"text": "its so touching it makes me cry! :'( ,lol"
},
		{
			"rate":-1,
			"text": "i would like to see this"
},
		{
			"rate":35,
			"text": "i liked the book better but whatever"
},
		{
			"rate":45,
			"text": "This was a very touching movie!"
},
		{
			"rate":35,
			"text": "oh my goodness, i cried so hard."
},
		{
			"rate":50,
			"text": "Loved the book, love the movie!"
},
		{
			"rate":45,
			"text": "omg the book i cried and the movie is sad i cried again"
},
		{
			"rate":50,
			"text": "The heart-throbbing tale of a boy and his dogs, a must-see!"
},
		{
			"rate":45,
			"text": "this movie still makes me cry and i've seen it a thousand times"
},
		{
			"rate":50,
			"text": "one of the best movies of all times!"
},
		{
			"rate":50,
			"text": "loved this movie it was so cute but sad"
},
		{
			"rate":50,
			"text": "I saw this when I was a kid and still cry whenever I watch it."
},
		{
			"rate":15,
			"text": "it stunk compared to the book, but it still made me cry"
},
		{
			"rate":40,
			"text": "OMG! This movie brings back old memories. I felt like I watched this movie every year from the time I was in 3rd grade to 5th grade. I can't really remember to much about it because I was sleep most the time on Movie Day, but I can say it's a classic in my book. LOL."
},
		{
			"rate":50,
			"text": "OH this is such a good movie, It is one of my faverites!!!!"
},
		{
			"rate":50,
			"text": "This was an excellent book with a sad ending that I read in elementary schoo. I bought the movie and also part two, which I have not seen yet, but will eventually."
},
		{
			"rate":50,
			"text": "i love this movie it makes me cry"
},
		{
			"rate":30,
			"text": "Hah, we watched this a long time ago in school. I remember it was OK, but I never like movies about animals. Except for My Dog Skip and animated Disney movies (the classics like Fox and theHound, not that crap that's being released now). It was pretty good, I bet, just not my kind of movie and I barely remember it now, anyway."
},
		{
			"rate":10,
			"text": "I really did try to watch this, the new one and read the book, but I could never finish any of them."
},
		{
			"rate":50,
			"text": "sad...i'm so sad that i have nothin to say!"
},
		{
			"rate":50,
			"text": "I loved it but it makes me cry."
},
		{
			"rate":50,
			"text": "This movie made me cry so hard when I was little..."
},
		{
			"rate":40,
			"text": "Loved this movie when I was younger! Haven't seen it in a while 'cause none of my family will watch it with me, they say its too sad!"
},
		{
			"rate":50,
			"text": "The movie was not as good as the book but it was still pretty good"
},
		{
			"rate":45,
			"text": "I liked this movie but I loved the newer version just alittle better."
},
		{
			"rate":50,
			"text": "WAHH!!!! ITS SO SAD I CRY EVERYTIME I WATCH THE MOVIE OR READ THE BOOK!!!!!!!!!!!!"
},
		{
			"rate":20,
			"text": "i didn't like this movie...maybe becuase i thought it was sad and didn't finish watching the last 1/2 of it"
},
		{
			"rate":30,
			"text": "really sad! such a sad movie, but really good"
},
		{
			"rate":50,
			"text": "ooh wow! i cry when i read the book! dont you?????"
},
		{
			"rate":-1,
			"text": "my sister crys in it every time!!"
},
		{
			"rate":35,
			"text": "This movie is so sad. I really liked it!"
},
		{
			"rate":35,
			"text": "Good movie. It follows the book really well. It is a sad one, but also a really good story that translates well on film."
},
		{
			"rate":20,
			"text": "ahh i might have seen this im not sure, but that book...ah"
},
		{
			"rate":50,
			"text": "Who dosen't love this movie? Any kid growing up in the 80's remembers watching this at school!"
},
		{
			"rate":-1,
			"text": "It seems sad. I started reading it at the end of the fifth grade year and couldn't get into it, so I won't watch it until I get my hands on the book."
},
		{
			"rate":20,
			"text": "I saw this a long long time ago, its kind of like Old Yeller."
},
		{
			"rate":40,
			"text": "this was a very good movie but the book is waaay better"
},
		{
			"rate":40,
			"text": "this movie i s relly good the book is awsome tooo! it`s soo sad:( i love it! !"
},
		{
			"rate":50,
			"text": "this movie was awesome but the ending is so sad but it is still one of my favorites!"
},
		{
			"rate":50,
			"text": "This was a good movie. I loved it."
},
		{
			"rate":30,
			"text": "I read the book, so this movie kicked ass when I was just a wee lad."
},
		{
			"rate":45,
			"text": "it might be old but i love it"
},
		{
			"rate":50,
			"text": "this is a sad yet heart warming film....I really liked it ....the book was good too. I suggest u read the book if u haven't already."
},
		{
			"rate":10,
			"text": "this movie is so stupid"
},
		{
			"rate":50,
			"text": "this was 1 of the best movies ever."
},
		{
			"rate":50,
			"text": "SO SAD! BUT SO GOOD!"
},
		{
			"rate":50,
			"text": "A Greaaaaaaaaaaaaaaaatttttttttttttt Movie!!!!!!!!!!!!!!!!!!!!!"
},
		{
			"rate":50,
			"text": "ooooooh sooooo sad i cried"
},
		{
			"rate":50,
			"text": "[color=purple]I cried when I read this book in school. But I won't spoil it 4- u.[/color]"
}	]
}