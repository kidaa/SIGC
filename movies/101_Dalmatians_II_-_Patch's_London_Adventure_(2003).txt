{
	"title": "101 Dalmatians II - Patch's London Adventure (2003)",
	"synopsis":"A sequel to the original Disney classic, 101 Dalmatians II: Patch's London Adventure arrives straight-to-video. Roger and Anita are planning to move to their Dalmatian Plantation with their dogs Pongo and Perdita to get away from Cruella DeVil and make room for all 101 puppies. However, young Patch (voice ofBobby Lockwood) gets left behind in London and wanders into an audition for his favorite TV show, The Thunderbolt Adventure Hour. Meanwhile, the superhero dog Thunderbolt (Barry Bostwick) almost loses his job. Patch is eager to help his TV hero, but Cruella DeVil (Susanne Blakeslee) intervenes with a kidnapping scheme. This time, she has gained an ally as the muse to artist Lars (Martin Short). ~ Andrea LeVasseur, Rovi",
	"genre":
	[
		{"name":"Animation"},
		{"name":"Kids & Family"}
,
		{"name":"Musical & Performing Arts"}

	],
	"rate":3.1,
	"comments":[
		{
			"rate":10,
			"text": "An unnecessary sequel to a classic film, '101 Dalmations 2' is a prime example of Disney exploiting itself."
},
		{
			"rate":30,
			"text": "Let's face it, the first was groundbreaking as well as an all time classic, this one, well, lets just say it's not as breathtaking, but this still pretty entertaining, even though the plot was a little similar to the first. All in all, in terms of Disney pre/mid/sequals, this is definatly in my top 5."
},
		{
			"rate":30,
			"text": "Not the worst Disney sequel. It was good to see the animated gang again after the two uneventful live action adaptations."
},
		{
			"rate":20,
			"text": "Most Disney sequels are always lacking, and this one is no exception. The story line is relatively unique, but the idea of one puppy wanting to stand out in a crowd of 101 is all too predictable. I am surprised they did not try to make multiple sequels that allowed each puppy the chance to have their own adventure. Patch was my favorite growing up, but what dictates that he deserved his own movie? Maybe Rollie, Lucky or any of the other puppies would have been just as good, if not better? There could have been emotion evoked when the overweight puppy wanted to lose weight and be different but finally realized being himself was good enough, or something. Any way, the movie is okay, certainly only worth the watch if you are a die hard Disney fan like myself of love the Dalmatian franchise. The positive is that I would still love to own a Dalmatian."
},
		{
			"rate":40,
			"text": "loved it! I really loved Thunderbolt's personality...it was so wild and funny!! i really liked it when he said,Good gracious grady look at that! That cat is stuck in that tree! or when he said,MANGE...I COULD GET MANGE!!! Or TICKS, i hate ticks...they suck all the blood out of ya do ya hear me...THEY SUCK ALL THE BLOOD OUT OF YOU!! and MOTHER OF PASTERIZED MILK!!! and Get a hold of yourself man....YOU'RE OUT OF CONTROL!!!!"
},
		{
			"rate":5,
			"text": "There were only two good things about this movie. 1) Lars' cool little hand thing that my brother can do but I can't, and 2) Patch was VERY cute in the French-dubbed version."
},
		{
			"rate":40,
			"text": "The sequel to 101 Dalmatians, although over 40 years after the original, is surprisingly good. The characters still feel fresh and interesting, and the storyline in this one isn't a bad touch to the tales about this group of dogs."
},
		{
			"rate":25,
			"text": "Funny how all the half-way decent DTV Disney sequels all involve dogs or Jason Alexander. Also depressing."
},
		{
			"rate":30,
			"text": "62%The devil is back, only more timid. It's got nothing on the original."
},
		{
			"rate":35,
			"text": "Mantiene el humor y el entretenimiento de su antecesora, pero es muy clich? y poco memorable. Pero por lo menos agrada."
},
		{
			"rate":30,
			"text": "If nothing else, at least the people of Disney Toon Studios spelled 'dalmatians' right. Actually, this film is childish at worse and solid at best. I like the comic book look that improves upon that of the original's rough xerography, with improved use of color and line. On the negative side, the story doesn't feel very original, and the villain characters return only to make fools of themselves."
},
		{
			"rate":5,
			"text": "Totally overused the Dalmatian Kidnapping plot."
},
		{
			"rate":20,
			"text": "Nowhere near as good as the original, but pretty decent for a straight-to-video sequel."
},
		{
			"rate":10,
			"text": "An unnecessary sequel to a classic film, '101 Dalmations 2' is a prime example of Disney exploiting itself."
},
		{
			"rate":30,
			"text": "Not the worst Disney sequel. It was good to see the animated gang again after the two uneventful live action adaptations."
},
		{
			"rate":30,
			"text": "Let's face it, the first was groundbreaking as well as an all time classic, this one, well, lets just say it's not as breathtaking, but this still pretty entertaining, even though the plot was a little similar to the first. All in all, in terms of Disney pre/mid/sequals, this is definatly in my top 5."
},
		{
			"rate":25,
			"text": "Despite some of it's cute moments, this film never really took off the ground for me. This film had a decent idea but they just didn't have enough ideas to move the film completely forward. It's still watchable, but I wouldn't classify it as a great Disney sequel."
},
		{
			"rate":40,
			"text": "A sequel worthy of Disney! I really enjoyed the film! It has elements that will please both children and adults, combined with good characters ( new and old) and an excellent animation!"
},
		{
			"rate":30,
			"text": "One of the better disney sequels."
},
		{
			"rate":30,
			"text": "Its still one of the better direct to video sequels, but falls in comparison to the original."
},
		{
			"rate":40,
			"text": "The sequel to 101 Dalmatians, although over 40 years after the original, is surprisingly good. The characters still feel fresh and interesting, and the storyline in this one isn't a bad touch to the tales about this group of dogs."
},
		{
			"rate":25,
			"text": "Funny how all the half-way decent DTV Disney sequels all involve dogs or Jason Alexander. Also depressing."
},
		{
			"rate":50,
			"text": "The Puppies were Adorable. :D"
},
		{
			"rate":-1,
			"text": "sequel?! say whaaat??!"
},
		{
			"rate":30,
			"text": "62%The devil is back, only more timid. It's got nothing on the original."
},
		{
			"rate":35,
			"text": "Mantiene el humor y el entretenimiento de su antecesora, pero es muy clich? y poco memorable. Pero por lo menos agrada."
},
		{
			"rate":40,
			"text": "Good disney classic..."
},
		{
			"rate":25,
			"text": "interesting idea using thunderbolt, but overall, derivative and unoriginal. mediocre."
},
		{
			"rate":20,
			"text": "Most Disney sequels are always lacking, and this one is no exception. The story line is relatively unique, but the idea of one puppy wanting to stand out in a crowd of 101 is all too predictable. I am surprised they did not try to make multiple sequels that allowed each puppy the chance to have their own adventure. Patch was my favorite growing up, but what dictates that he deserved his own movie? Maybe Rollie, Lucky or any of the other puppies would have been just as good, if not better? There could have been emotion evoked when the overweight puppy wanted to lose weight and be different but finally realized being himself was good enough, or something. Any way, the movie is okay, certainly only worth the watch if you are a die hard Disney fan like myself of love the Dalmatian franchise. The positive is that I would still love to own a Dalmatian."
},
		{
			"rate":35,
			"text": "Amazingly, the sequel won this round. I just loved Patch and well the hero dog. Great continution of the orginal."
},
		{
			"rate":30,
			"text": "Better when I was younger."
},
		{
			"rate":30,
			"text": "If nothing else, at least the people of Disney Toon Studios spelled 'dalmatians' right. Actually, this film is childish at worse and solid at best. I like the comic book look that improves upon that of the original's rough xerography, with improved use of color and line. On the negative side, the story doesn't feel very original, and the villain characters return only to make fools of themselves."
},
		{
			"rate":40,
			"text": "Very cute. They did a good job finding people with similar voices to the ones from the original."
},
		{
			"rate":30,
			"text": "not as good as the original"
},
		{
			"rate":5,
			"text": "Totally overused the Dalmatian Kidnapping plot."
},
		{
			"rate":30,
			"text": "Better when I was younger."
},
		{
			"rate":50,
			"text": "love it that bad lady deserves the suffering at the end"
},
		{
			"rate":20,
			"text": "Nowhere near as good as the original, but pretty decent for a straight-to-video sequel."
},
		{
			"rate":10,
			"text": "An unnecessary sequel to a classic film, '101 Dalmations 2' is a prime example of Disney exploiting itself."
},
		{
			"rate":30,
			"text": "Not the worst Disney sequel. It was good to see the animated gang again after the two uneventful live action adaptations."
},
		{
			"rate":-1,
			"text": "sequel?! say whaaat??!"
},
		{
			"rate":30,
			"text": "Let's face it, the first was groundbreaking as well as an all time classic, this one, well, lets just say it's not as breathtaking, but this still pretty entertaining, even though the plot was a little similar to the first. All in all, in terms of Disney pre/mid/sequals, this is definatly in my top 5."
},
		{
			"rate":25,
			"text": "Despite some of it's cute moments, this film never really took off the ground for me. This film had a decent idea but they just didn't have enough ideas to move the film completely forward. It's still watchable, but I wouldn't classify it as a great Disney sequel."
},
		{
			"rate":50,
			"text": "The best dalmatians movie!!"
},
		{
			"rate":25,
			"text": "SEEN IT ...NICE FOR THE CHILDREN...I PERFER LADY AND THE TRAMP ANYDAY OVER THIS...CHECK MY PROFILE NEXT TIME..."
},
		{
			"rate":40,
			"text": "A sequel worthy of Disney! I really enjoyed the film! It has elements that will please both children and adults, combined with good characters ( new and old) and an excellent animation!"
},
		{
			"rate":40,
			"text": "One of my favorite cartoons when I was a kid."
},
		{
			"rate":40,
			"text": "Odd-ball... Love it :)"
},
		{
			"rate":30,
			"text": "One of the better disney sequels."
},
		{
			"rate":30,
			"text": "Its still one of the better direct to video sequels, but falls in comparison to the original."
},
		{
			"rate":25,
			"text": "A half 'n' half movie. But it lives up to it's orginal. 52%"
},
		{
			"rate":35,
			"text": "Amazingly, the sequel won this round. I just loved Patch and well the hero dog. Great continution of the orginal."
},
		{
			"rate":45,
			"text": "Disney!!!!!!!! again love it!"
},
		{
			"rate":-1,
			"text": "I wish they would stop making second and third movies to Disney movies that they made like a million years ago."
},
		{
			"rate":40,
			"text": "fine i just remmeber that was a good movie on my childness"
},
		{
			"rate":25,
			"text": "Surprisingly more entertaining than the previous live action adaptations, Patch's London Adventure focuses more on what made the original animated film loveable, but also at most times is way too childish for it's own good, which renders it highly inferiour to the original. That being said, Patch's London Adventure is slightly entertaining on a first watch, and Will Young's song to accompany it is surprisingly fitting."
},
		{
			"rate":30,
			"text": "yeah...liked it once"
},
		{
			"rate":40,
			"text": "Good disney classic..."
},
		{
			"rate":40,
			"text": "Being one of 101 takes its toll on Patch, who doesn't feel unique. When he's accidentally left behind on moving day, he meets his idol, Thunderbolt, who enlists him on a publicity campaign."
},
		{
			"rate":-1,
			"text": "um......has this even come out yet?"
},
		{
			"rate":-1,
			"text": "Wanna see,wanna see!"
},
		{
			"rate":30,
			"text": "this was cute but it was corny too!"
},
		{
			"rate":40,
			"text": ":fresh: [CENTER]This film shows that Patch is a real pup.[/CENTER]"
},
		{
			"rate":-1,
			"text": "what? that can't be right..."
},
		{
			"rate":25,
			"text": "A pretty good little kids movie, even though I personally think that seconds on Disney movies are pretty dissapointing, but little kids like them. 101 Dalmations was a lot better."
},
		{
			"rate":-1,
			"text": "SINCEEE WHENNN??????"
},
		{
			"rate":50,
			"text": "My favorite show when i was little."
},
		{
			"rate":25,
			"text": "I loved Disney's 101 Dalmatians and I was expecting the sequel to be at least half as good as the original. Unfortunately, this movie disgraces 101 Dalmatians and really ruins the whole theme in my opinion. I have shown this movie around multiple age groups, and it seems that kids too find it slower than the original and not as fun. Maybe the 101 Dalmatians series has lost it's charm, it sure seems that way with this awful sequel. It's a watchable movie, however, it just does not fit into the flow of the series. For that, I award it 2 1/2 stars, all though I so wanted to love this movie."
},
		{
			"rate":30,
			"text": "Like it usually is with Disney movies not as good as the first, but not horrible either."
},
		{
			"rate":40,
			"text": "its not as gd as the first animated 1 but ts still ok"
},
		{
			"rate":-1,
			"text": "I'm waiting for Disney Movie Club to release it & it will be added to my little girls Disney Collection"
},
		{
			"rate":25,
			"text": "Good, but not as good as the like older ones."
},
		{
			"rate":35,
			"text": "awww i remember seeing this"
},
		{
			"rate":30,
			"text": "Good movie and sad I love Patch"
},
		{
			"rate":25,
			"text": "ok so its another disney cash cow. This one has yet another simplistic plot consisting of one pup trying to be unique whilst he already is. However the real intrigue in this film comes from why on earth a woman who deals in fashion (i.e. Cruella De Ville) would suddenly become obsessed with the art world?"
},
		{
			"rate":-1,
			"text": "Reckon itll be lame but ill try it"
},
		{
			"rate":30,
			"text": "lol.. never saw the first cartoon but I saw this one."
},
		{
			"rate":50,
			"text": "another great sequal to an amazing movie"
},
		{
			"rate":40,
			"text": "Patch is really cute and loveable but the Cruella/artist scenes are boring. A shame, otherwise movie is great!"
},
		{
			"rate":35,
			"text": "verulega falleg teiknimynd"
},
		{
			"rate":45,
			"text": "You will still have a friends"
},
		{
			"rate":20,
			"text": "Cheesy, corny, icky."
},
		{
			"rate":35,
			"text": "Not the best but good!"
},
		{
			"rate":-1,
			"text": "Haven't Seen it butt looks cool"
},
		{
			"rate":-1,
			"text": "I just might see it with my nieces :)"
},
		{
			"rate":-1,
			"text": "not interested at all!!"
},
		{
			"rate":50,
			"text": "I love disney movies!!"
},
		{
			"rate":30,
			"text": "not as good as the original"
},
		{
			"rate":40,
			"text": "I'm willing to accept that my crying at a part in this movie does make me a loser."
},
		{
			"rate":50,
			"text": "Awwww....patch is sooo cute!"
},
		{
			"rate":40,
			"text": "Really enjoyed this movie"
},
		{
			"rate":35,
			"text": "Cute, but keep it for the kids (or die-hard 101 fans)"
},
		{
			"rate":40,
			"text": "Patches time 2 shine!!!!"
},
		{
			"rate":25,
			"text": "Sort of ok..I hate when Disney does sequels to their classics..LEAVE THEM ALONE!!!"
},
		{
			"rate":-1,
			"text": "maybe if I was like 5....."
},
		{
			"rate":50,
			"text": "good for the whole family"
},
		{
			"rate":40,
			"text": "DDDDDDDISSSSSSNEYYYY!!! @.@"
},
		{
			"rate":-1,
			"text": "i actually loved watch the 101 dalmations as a kid and still enjoy it =]"
},
		{
			"rate":40,
			"text": "this was good, doggg!"
},
		{
			"rate":15,
			"text": "I liked the first one more"
},
		{
			"rate":40,
			"text": "I liked it, was alright, had its own special charm."
},
		{
			"rate":-1,
			"text": "For goodness sake, NO!"
},
		{
			"rate":5,
			"text": "Don't mess with a classic story"
},
		{
			"rate":20,
			"text": "kinda (as in really) stupid....."
},
		{
			"rate":30,
			"text": "not nearly as good as the first, or even what I expected it to be"
},
		{
			"rate":-1,
			"text": "another fairly good disney sequel"
},
		{
			"rate":20,
			"text": "A pretty good little kids movie, even though I personally think that seconds on Disney movies are pretty dissapointing, but little kids like them. 101 Dalmations was a lot better."
},
		{
			"rate":35,
			"text": "Dalmatoins are the best! ^__^"
},
		{
			"rate":45,
			"text": "the continuing story from the orginal movie, great acting, wonderful, and whimsical."
},
		{
			"rate":15,
			"text": "Not as good as the first one"
},
		{
			"rate":35,
			"text": "Another one of my fave disney films... <33"
},
		{
			"rate":15,
			"text": "Yet another shameless effort by Disney to screw people out of money. Basically just an almost exact retelling of the original (and best) animated film."
},
		{
			"rate":50,
			"text": "all i have to say is awww"
},
		{
			"rate":30,
			"text": "aww, another cute disney movie;)"
},
		{
			"rate":35,
			"text": "cute but i liked the first one better"
},
		{
			"rate":-1,
			"text": "i loved the first one"
},
		{
			"rate":-1,
			"text": "I WANT TO SEE IT SO BADLY!!!!!!!!!!!!!!!!!!!!!!"
},
		{
			"rate":30,
			"text": "not as gud as the first one"
},
		{
			"rate":25,
			"text": "not as gd story line but gr8 nd puppies get cuter nd cuter"
},
		{
			"rate":10,
			"text": "this is such a bad movie disney will make any excuse to make a made-4-tv sequel"
},
		{
			"rate":45,
			"text": "A Disney continuation of the old classic in Home alone style. The fine message is that you're allways unic even if you sometimes don't feel like it and everyone can be a hero!"
},
		{
			"rate":-1,
			"text": "Im getting confused, Ok theres the 102 dalmations, then theres the 101 dalmations 2 which one is the sequel or are they both sequels?"
},
		{
			"rate":45,
			"text": "Lol i love the film."
},
		{
			"rate":25,
			"text": "not as good as the first"
},
		{
			"rate":-1,
			"text": "when did this movie come out?"
},
		{
			"rate":-1,
			"text": "i liked this second disney version of this movie its cool"
},
		{
			"rate":50,
			"text": "i watch it alot of time liao...it means it is nice!!1haha"
},
		{
			"rate":-1,
			"text": "I love Dalmatians!! They're so cute!!"
},
		{
			"rate":35,
			"text": "Not as good as the other, but noteable"
},
		{
			"rate":50,
			"text": "my two little one watch so do i .'i like both dalmation"
},
		{
			"rate":40,
			"text": "kool movie ! once again ! i love dogs"
},
		{
			"rate":30,
			"text": "I love THUNDERBOLT, the story was cute."
},
		{
			"rate":40,
			"text": "Very cute. They did a good job finding people with similar voices to the ones from the original."
},
		{
			"rate":25,
			"text": "was okay i liked the first one better"
},
		{
			"rate":50,
			"text": "But still not as good as the first one"
},
		{
			"rate":40,
			"text": "loved it! I really loved Thunderbolt's personality...it was so wild and funny!! i really liked it when he said,Good gracious grady look at that! That cat is stuck in that tree! or when he said,MANGE...I COULD GET MANGE!!! Or TICKS, i hate ticks...they suck all the blood out of ya do ya hear me...THEY SUCK ALL THE BLOOD OUT OF YOU!! and MOTHER OF PASTERIZED MILK!!! and Get a hold of yourself man....YOU'RE OUT OF CONTROL!!!!"
},
		{
			"rate":-1,
			"text": "i love walt disney films so i dont care what other people think at all"
},
		{
			"rate":25,
			"text": "its a good kids film"
},
		{
			"rate":40,
			"text": "it was ok i luv dogs duh u havent noticed?"
},
		{
			"rate":50,
			"text": "love it love iy love it- kimberly"
},
		{
			"rate":-1,
			"text": "I don't do disney that well"
},
		{
			"rate":25,
			"text": "liked the original one, shouldn't have made a second one"
},
		{
			"rate":25,
			"text": "Interesting, was a long time ago since I have seen it"
},
		{
			"rate":45,
			"text": "101 Dalmations is hell sad but number two is even sadder"
},
		{
			"rate":20,
			"text": "not nearly as good as its predecessor."
},
		{
			"rate":25,
			"text": "HA THAT WAS A FUNNY MOVIE"
},
		{
			"rate":5,
			"text": "There were only two good things about this movie. 1) Lars' cool little hand thing that my brother can do but I can't, and 2) Patch was VERY cute in the French-dubbed version."
},
		{
			"rate":20,
			"text": "Didn't really like this one.."
},
		{
			"rate":10,
			"text": "yawn...ZZZZZZZZZZZZZZ"
}	]
}