{
	"title": "Godspell (1973)",
	"synopsis":"Directors David Greene and John-Michael Tebelak collaborate with composer Stephen Schwartz to bring his wildly successful Broadway musical, Godspell, to the big screen. Told almost entirely in song, Godspell presents the gospel of St. Matthew updated to New York City of the late '60s/early '70s, featuring Jesus Christ as a wandering minstrel dressed like a circus clown. By blowing on an instrument that reaches only the ears of a select few around the bustling city, John the Baptist (David Haskell) summons nine simpatico hippies to a fountain in Central Park, where they revel in the waters of their baptism. When Christ (Victor Garber) joins the group, the free-flowing fraternal love solidifies into a desire to spread the word of God around the city. Outfitting themselves in a nearby junkyard that bursts with color, the lively group makes its way around a sparsely populated fairy-tale version of the city, re-enacting Biblical parables with great enthusiasm and flamboyance. Opting for a lighter yet more devout approach than its thematically similar Jesus Christ Superstar, Godspell features an equivalent number of hummable hit songs, notably Prepare Ye (The Way of the Lord), By My Side, and Day by Day. ~ Derek Armstrong, Rovi",
	"genre":
	[
		{"name":"Drama"},
		{"name":"Musical & Performing Arts"}
,
		{"name":"Classics"}

	],
	"rate":3.4,
	"comments":[
		{
			"rate":30,
			"text": "This movie walks a fine line between uplifting and annoying. For the most part I found it really annoying. Maybe I'll watch it again someday to give it another chance."
},
		{
			"rate":10,
			"text": "If it wasn't so obviously religious, I would suspect they were all on drugs. Shocking."
},
		{
			"rate":15,
			"text": "Cool idea, to have a hippy version of Jesus running around the streets and landmarks of New York City with his co-ed disciples, but everybody acts like complete idiots, most of the songs (except for Day By Day) are dreadful, and they tell boring stories. It's pantomime throughout. There's not much drama or conflict. I tried watching several times, and always got bored."
},
		{
			"rate":25,
			"text": "I can't give you one reason in the world why I watched this but I did. It's the Book of Matthew set in 70s Manhattan. I cursed Godspell for the first hour or so then I didn't mind and even liked some of the rest of it. The music got better and the thought of Jesus getting crucified always excites me a little. It's like the car accident you can't look away from--horror you have to see. File with 9/11, Nazis and the ensuing holocaust. Bad transition here but it's equally frightening and hilarious. The Goodwill outfits, the Jew fros and the fact that you're supposed to forget that several million people live in New York as a hippie theater troupe cavorts around the deserted city adds up to the theater of the absurd. But absurd in a very twisted and entertaining way. (Oh... and being under the influence helps...)"
},
		{
			"rate":30,
			"text": "Kinda cool but dated. Good music though."
},
		{
			"rate":30,
			"text": "I liked it, I thought the songs were good and it made me sad."
},
		{
			"rate":25,
			"text": "A little on the weird side with a couple of good songs. I was kind of bored by it and there are better Jesus-themed musicals out there. Grade: C+"
},
		{
			"rate":25,
			"text": "It's goofy, but the music is IRRESISTIBLE!!! I love Godspell but the movie just does not do it justice. And yet, the music is amazing regardless of the medium so this movie is still entirely enjoyable. Many great lessons are taught through these parables and this musical really is more than just a bunch of fun songs and dance. I'd like to see a modernized version of Godspell because when we see these lessons portrayed by flower children, it makes them seem like hippie ideals instead of moral lessons that we should all be taking to heart. The film also shows its 70's-ness through goofy moments that sometimes feel like a Scooby Doo montage and I wish that the movie could've been a bit more serious. I find it interesting that this movie was almost entirely cast from Broadway instead of from Hollywood but that's why the cast portrays the music so well. Alias fans will surely exclaim: Is that JACK BRISTOW!?!?!? when they see a very, very young Victor Garber and who knew that he could sing? The betrayal scene is sobering. Equally sobering is the shot of them dancing on top of the World Trade Center. The crucifixion becomes really awkward with so much jumping on the fences. I do love this musical but, because this movie is so dated, I recommend seeing Godspell on stage instead."
},
		{
			"rate":10,
			"text": "Watched because I heard rumours of te little theatre here doing Godspell next year. Good music, bad movie. Many people will not understand that John and Judas are played by the same actor. The thing is, I only understood that because of my knowledge of the musical. I have no idea at what point exactly it is when he changes from John to Judas in the show."
},
		{
			"rate":15,
			"text": "Meh. Pretty good. The only reason I gave it this many stars though, is because of the music. The man who was supposed to be Jesus really confused me. Was he Jesus, or was he just re-enacting? I loved the girl who sang Turn Back Oh Man. She was the best character. I understood all of the scenes cause they're from the Bible. But this movie was seriously confusing. Like, there were 12 disciples, not 9. Also, why were they re-enacting everything? I mean, what's the point? Also, the part where they're at the fence was weird. I mean, how did he die just standing there? And, it wasn't John the Baptist who betrayed him. This movie was SOOOOO confusing."
},
		{
			"rate":30,
			"text": "This was a cool film with the junk yard and getting baptized Central Park. The girl with the tiger puppet sort of freaked me out though. The end were everyone was screaming on the fence was sort of weird and I did not really get that scene. I liked the part when Jesus and the other guy that was baptizing did the dance with the canes."
},
		{
			"rate":30,
			"text": "This movie was weird! lol, but the cinematography was good. Should you see it? Yeah, if only to experience something really unique."
},
		{
			"rate":35,
			"text": "I think the script is a little too biblically literal for the visual concept, but other than that it's a great film. wonderful songs, brilliant cinematography, and a solid revival for old-school comedy (Vaudeville, pantomime, circus clowns, etc.) an entertaining musical, but it's certainly bizarre."
},
		{
			"rate":30,
			"text": "No where near as good as the stage version I directed last year, but it's worth it to see a young Victor Garber."
},
		{
			"rate":40,
			"text": "oh my goodness....i use to watch all the time....as a kid...lol....i think becuz of my babysitter....i thought it was some crazy hippy stuff at first....and than by the 5th time she made me watch it....i finally got it, lol....don't judge me...i was like 5 when i saw this!"
},
		{
			"rate":40,
			"text": "This musical is upbeat, 70's, and has a good moral meaning. Like many say, it reminds you of HAIR and JESUS CHRIST SUPERSTAR, but it rolls to a beat of its own. The story is of regular townspeople who follow the beliefs of one man (Victor Garber) and the ending affects of what happens. This musical is easily enjoyable with thought and laughs! If you can, make sure you see it soon! You only live DAY BY DAY!"
},
		{
			"rate":5,
			"text": "I despise this movie/musical. An interesting concept, I suppose, but just a stupid urban hippie New Testament with lousy songs."
},
		{
			"rate":50,
			"text": "victor garber was really good-looking when he was younger...yeah. Robin Lamont sings (one of) my favorite songs - Day By Day. oddly enough, in the musical we just completed - Working - she sings my solo song - Millwork. i absolutely love this movie."
},
		{
			"rate":45,
			"text": "If you can hear the message dressed in hippie clothes and pop music, this is a good movie. Obviously, I like it ... after all these years."
},
		{
			"rate":50,
			"text": "wow this musical is amazing. i just did it at school, and the music is awesome, it is FREAKIN HILARIOUS and it has a really good message. the music pretty much covers all genres (we beseech thee is a country song, prepare ye is a rock-type song, for example). it is just so random and funny, and it is super powerful."
},
		{
			"rate":35,
			"text": "Because it's Easter."
},
		{
			"rate":5,
			"text": "A stage to screen adaption done wrong. Now Godspell is a bad show in my opinion and putting it to screen makes it work. Also in the original show THEY ARE clowns that follow the second coming of Jesus in this...........to be honest I have no idea. I love Stephen Schwartz but Godspell is his worst work"
},
		{
			"rate":25,
			"text": "A lesson in how not to transfer a theatrical musical to the big screen. the only good thing the song 'day by day'."
},
		{
			"rate":25,
			"text": "A little on the weird side with a couple of good songs. I was kind of bored by it and there are better Jesus-themed musicals out there. Grade: C+"
},
		{
			"rate":-1,
			"text": "All for the Best, Fossetastic!!!!"
},
		{
			"rate":40,
			"text": "I strongly suggest giving this a chance. Though it may seem dated to many people, the songs of Stephen Schwartz truly light up this picture. You'll find yourself singing Day By Day for weeks!"
},
		{
			"rate":40,
			"text": "LOVED IT WHEN I WAS A KID !!!!!!!!!!! GREAT SOUNDTRACK !!!!!!!!!!!!!!!!!"
},
		{
			"rate":-1,
			"text": "Want to see it! Will have to take into account!"
},
		{
			"rate":40,
			"text": "As a Jewish guy and die-hard musical theatre buff, my religion had no effect on my love for Godspell. Its exuberant and relaxed nature, full-fledged energy, entertaining and lovable musical numbers and happy-go-lucky cast (which features some of the original Off-Broadway crew) have a tremendous impact on you. It's not only enjoyable, but also inspiring. I'll take this over Jesus Christ Superstar any day."
},
		{
			"rate":25,
			"text": "It's goofy, but the music is IRRESISTIBLE!!! I love Godspell but the movie just does not do it justice. And yet, the music is amazing regardless of the medium so this movie is still entirely enjoyable. Many great lessons are taught through these parables and this musical really is more than just a bunch of fun songs and dance. I'd like to see a modernized version of Godspell because when we see these lessons portrayed by flower children, it makes them seem like hippie ideals instead of moral lessons that we should all be taking to heart. The film also shows its 70's-ness through goofy moments that sometimes feel like a Scooby Doo montage and I wish that the movie could've been a bit more serious. I find it interesting that this movie was almost entirely cast from Broadway instead of from Hollywood but that's why the cast portrays the music so well. Alias fans will surely exclaim: Is that JACK BRISTOW!?!?!? when they see a very, very young Victor Garber and who knew that he could sing? The betrayal scene is sobering. Equally sobering is the shot of them dancing on top of the World Trade Center. The crucifixion becomes really awkward with so much jumping on the fences. I do love this musical but, because this movie is so dated, I recommend seeing Godspell on stage instead."
},
		{
			"rate":40,
			"text": "I come back to this movie every decade or so of my life, and I always walk away in love with it again.While in content it might diverge a bit from standard canon, in its soul i have always felt it was the BEST and truest to the essence telling of the Christ story ever made.Jesus after all, WAS a peace-loving, communal-living, drug-sharing hippie. (What drugs you ask? I remind you that the drug of choice in the time and place was alcohol, and refer you to a certain marriage scene in Cana.)JC Superstar is a far more impressive literary work. But Godspell to me says it all without the confusion and pretension that has crept in in the past 2000 years."
},
		{
			"rate":50,
			"text": "One of the things missing in so many Gospel stories is joy. The film overflows with a sense of joy and happiness. Having seen small productions of Godspell through out the years this film does a good job of putting the play onto a rather large stage (the city of New York) while still making the viewer feel they are in a 100 seat theater."
},
		{
			"rate":-1,
			"text": "My high school play version was better. Really, we had a perfect cast that should put the actors in the movie to shame. #St.CharlesPreparatorySchool"
},
		{
			"rate":5,
			"text": "I really didn't like this movie. I am a Christian and I found this movie offensive and stupid. I say this was rotten!"
},
		{
			"rate":25,
			"text": "Some decent music, but incredibly dated."
},
		{
			"rate":5,
			"text": "Worst piece of crap that I've ever seen. A bunch of mimes running around New York singing bible hymns. Turned it off eventually, but I watched for a good half hour becasue I was in such disbelief that someone could make such crap."
},
		{
			"rate":50,
			"text": "The most inspirational, happy-making movie ever!"
},
		{
			"rate":50,
			"text": "a great musical and who can resist Victor Garber in a 'fro"
},
		{
			"rate":-1,
			"text": "FAVORITE MUSICAL EVER!"
},
		{
			"rate":10,
			"text": "An almost unwatchable film version of the musical."
},
		{
			"rate":30,
			"text": "This movie walks a fine line between uplifting and annoying. For the most part I found it really annoying. Maybe I'll watch it again someday to give it another chance."
},
		{
			"rate":15,
			"text": "This movie was indeed terrible. But if you're looking for a good laugh, check this one out. Even though it's considered a drama/musical, you'll get the feeling that it is more like that of a comedy (a ridiculously bad comedy, at that). Regardless, avoid this monstrosity at all costs because you can never get that hour and forty-three minutes of your life back."
},
		{
			"rate":10,
			"text": "Watched because I heard rumours of te little theatre here doing Godspell next year. Good music, bad movie. Many people will not understand that John and Judas are played by the same actor. The thing is, I only understood that because of my knowledge of the musical. I have no idea at what point exactly it is when he changes from John to Judas in the show."
},
		{
			"rate":10,
			"text": "If it wasn't so obviously religious, I would suspect they were all on drugs. Shocking."
},
		{
			"rate":25,
			"text": "seems dated now but still has a few good songs."
},
		{
			"rate":40,
			"text": "A fun classic to watch although a bit cheaply made, even for its day (70's)"
},
		{
			"rate":50,
			"text": "The most inspirational, happy-making movie ever!"
},
		{
			"rate":35,
			"text": "Cute, I loved how they explaiined some of the stories from the Bible but overall, well, this is just not my kind of a movie.P.S. Gosh!! Still can't believe Victor Garber's hair looked like that :-O"
},
		{
			"rate":40,
			"text": "A beautiful film full of life, love, and heart! I really enjoyed it!"
},
		{
			"rate":50,
			"text": "Better than Jesus Christ: Superstar by a mile."
},
		{
			"rate":30,
			"text": "Kinda cool but dated. Good music though."
},
		{
			"rate":50,
			"text": "I'm a product of the 70's so I love this. I learned the New Testement because of this and have seen it performed a hundred times and love it every single time. It's a classic!"
},
		{
			"rate":10,
			"text": "There's one girl here--I couldn't tell you what her name is, because no one's name ever gets given--who, every time they show her, I think to myself, She's dressed like Holly Hobby. I don't know if anyone but me even remembers who Holly Hobby is, but take my word for it--she dresses like this one girl in [i]Godspell[/i]. Apparently, the composer of the thing gets very snippy if you use terms like flower child to describe the performers' appearance, but if he didn't want you to, he shouldn't have made them dress that way. Maybe they're supposed to be showing their joy in the Lord. I can accept that. It explains the ghastly capering. But I'm sorry; they're dressed like a combination of clowns and street people. The I'm upper-middle-class but I'm hip kind of street people. The full title of the thing--[i]Godspell: A Musical Based on the Gospel According to St. Matthew[/i]--should tell you what the story is. This version features a group of young people who hear the siren call of a Sergeant Pepper reject (David Haskell), apparently John the Baptist, who makes them walk away from their jobs and lives and things, gambol in a fountain, and suddenly and mysteriously change clothes to welcome the arrival of Victor Garber, who's done a lot better work since then. As, indeed, he bloody well should have, if he did anything at all. He has a star painted on his face, a 'fro, and a T-shirt with a bad representation of the Superman logo, also clown shoes, and he is Jesus. He and his followers gad about in a deserted New York, and how much [i]that[/i] must have cost to ensure I shudder to think. They recite the Bible at one another--I'm not sure which translation--and sing mostly quite bad songs that aren't actually Biblical in nature, for the most part. Okay, one of the songs is pretty good. Day by Day, with uncredited lyrics by St. Richard of Chichester--and isn't plagiarizing a saint a big-time sin?--is pretty good. I note, further, that four of the songs have uncredited lyricists. Oh, it's fine by me if the credits don't list, you know, Written by Stephen Schwartz based on Psalm 137. Anyone sufficiently inspired to go read a Bible after seeing this should be able to figure that out. But All Good Gifts appears, based on what IMDB says, to not only be written by Matthias Claudius, a German poet, but to be translated by one Jane M. Campbell. Neither are credited. I find this to be in [i]very[/i] poor taste. I really hope Stephen Schwartz isn't one of the people leading the fight against file sharing, frankly. What was it Jesus said about hypocrites . . . . As you may know, I'm a huge fan of [i]Jesus Christ Superstar[/i], even though I acknowledge that it isn't very good. The two works are from roughly the same era; the films came out in the same year. However, while both films are pretty dated, I think [i]Superstar[/i] is much less so. Oh, Herod's court, sure. But they dress Jesus in a plain white robe, much as he appears in most art, and the disciples, while wearing funky clothes, aren't, well, dressed like Holly Hobby. Most of them wouldn't cause much of a second glance if they were walking around in those clothes even today. They also fail to wear face paint. Further, there isn't the, you know, hideous frolicking. (I'm starting to run low on synonyms, here.) Oh, Simon the Zealot gets his in at one point, but mostly, it's real movement, either dancing or just walking. Unlike the Pixies of Jesus in [i]Godspell[/i], no one in [i]Superstar[/i] plays leapfrog as they walk. I am also, as a minor theologian, a little bewildered by the fact that Jesus is followed around by a mere eight people, cavorting or not. And one plays both John the Baptist and Judas, a fact not made clear to me until [i]very[/i] late in the film. (I was confused as to what John was still doing there, you know, with his head not on a platter.) What's more, four of the followers are women. (One, the late, great Lynne Thigpen, was Chief on the live action gameshow version of [i]Where in the World is Carmen Sandiego?[/i]) While I'm sure this brings modern, non-sexist balance to the manic glee of the production, it's bad scholarship, which grates on me more. There should be twelve guys. Twelve. And women, sure. Jesus had a lot of female followers; a lot are mentioned in the Bible. But they are not portrayed in that Gospel from which our story ostensibly comes as having anything like equal footing. Then again, in the Gospel, they interacted with other people. So there you are, I suppose."
},
		{
			"rate":30,
			"text": "I liked it, I thought the songs were good and it made me sad."
},
		{
			"rate":35,
			"text": "very interesting movie...that's all I can say...."
},
		{
			"rate":35,
			"text": "i love godspell with my uncle"
},
		{
			"rate":50,
			"text": "Christian hippies on the loose in New York!"
},
		{
			"rate":25,
			"text": "Weeeird musical. Some classic songs, but tooootally 70's... and they leave out the most important part of the Gospel: the Resurrection!!! Christ's death doesn't mean a thing if He didn't also rise again."
},
		{
			"rate":15,
			"text": "Meh. Pretty good. The only reason I gave it this many stars though, is because of the music. The man who was supposed to be Jesus really confused me. Was he Jesus, or was he just re-enacting? I loved the girl who sang Turn Back Oh Man. She was the best character. I understood all of the scenes cause they're from the Bible. But this movie was seriously confusing. Like, there were 12 disciples, not 9. Also, why were they re-enacting everything? I mean, what's the point? Also, the part where they're at the fence was weird. I mean, how did he die just standing there? And, it wasn't John the Baptist who betrayed him. This movie was SOOOOO confusing."
},
		{
			"rate":-1,
			"text": "This looks scary bad!"
},
		{
			"rate":50,
			"text": "SOOOOO GREAT!!!!!!!!!!"
},
		{
			"rate":-1,
			"text": "Our shool did this as our musical last year. I got to be an old man having a heart attack!"
},
		{
			"rate":40,
			"text": "im in the play @ the high school."
},
		{
			"rate":30,
			"text": "This was a cool film with the junk yard and getting baptized Central Park. The girl with the tiger puppet sort of freaked me out though. The end were everyone was screaming on the fence was sort of weird and I did not really get that scene. I liked the part when Jesus and the other guy that was baptizing did the dance with the canes."
},
		{
			"rate":30,
			"text": "This movie was weird! lol, but the cinematography was good. Should you see it? Yeah, if only to experience something really unique."
},
		{
			"rate":35,
			"text": "I think the script is a little too biblically literal for the visual concept, but other than that it's a great film. wonderful songs, brilliant cinematography, and a solid revival for old-school comedy (Vaudeville, pantomime, circus clowns, etc.) an entertaining musical, but it's certainly bizarre."
},
		{
			"rate":-1,
			"text": "This is a weird musical: the songs are incredible as it is the music but the lyrics, book and whole plot and characters are too preachy and kind of awful. I don't know what to expect from the movie as my favorite song from the musical isn't actually in the film. Anyways, I have to see it someday."
},
		{
			"rate":40,
			"text": "Great music and the numbers are playful and funny. The enactment of so many gospel stories is interesting. The film as a whole is trippy and entertaining."
},
		{
			"rate":50,
			"text": "the songs in this are so awesome!!!!stephen schwartz writes the coolest songs!"
},
		{
			"rate":50,
			"text": "Love it......It's dated but a classic!"
},
		{
			"rate":15,
			"text": "Um, no. Bringing Broadway shows to film just doesn't often translate. It took them 25 year to figure out how to do Chicago and after seeing Godspell, I'm thinking they should have taken as long if not longer. I'm all for musicals, even when the leads suddenly burst into song but the performances here are over-blown and dull. It's amazing to see how well an adaptation can be (Jesus Christ Superstar, Chicago, The Music Man, and almost anything from FOX in the 50's/60's) and how poorly it can be represented when you're basically doing the same thing; taking people singing on a stage setting and putting it to film in a real-life setting. JCS is about as basic as can be when it comes to it's main elements--the script is all music, the sets are a bare minimum and the performances carry over with great subtley, yet an there is an understanding with the filmmaker that the stage and screen are two different art forms but a story is a story. It just has to be handled differently, depending on the medium. This version of Godspell is just an out-door filming of actors turning in stage performances."
},
		{
			"rate":30,
			"text": "The cast act like major retards, but they're fun and talented at that. Victor Garbor steals the show and looks the best as Jesus. A little too preachy, but with catchy songs."
},
		{
			"rate":30,
			"text": "No where near as good as the stage version I directed last year, but it's worth it to see a young Victor Garber."
},
		{
			"rate":40,
			"text": "PREPARE YE THE WAY OF THE LORD!!"
},
		{
			"rate":50,
			"text": "owns it classic great one"
},
		{
			"rate":40,
			"text": "oh my goodness....i use to watch all the time....as a kid...lol....i think becuz of my babysitter....i thought it was some crazy hippy stuff at first....and than by the 5th time she made me watch it....i finally got it, lol....don't judge me...i was like 5 when i saw this!"
},
		{
			"rate":-1,
			"text": "Want to see it! Will have to take into account!"
},
		{
			"rate":5,
			"text": "Worst piece of crap that I've ever seen. A bunch of mimes running around New York singing bible hymns. Turned it off eventually, but I watched for a good half hour becasue I was in such disbelief that someone could make such crap."
},
		{
			"rate":25,
			"text": "I can't give you one reason in the world why I watched this but I did. It's the Book of Matthew set in 70s Manhattan. I cursed Godspell for the first hour or so then I didn't mind and even liked some of the rest of it. The music got better and the thought of Jesus getting crucified always excites me a little. It's like the car accident you can't look away from--horror you have to see. File with 9/11, Nazis and the ensuing holocaust. Bad transition here but it's equally frightening and hilarious. The Goodwill outfits, the Jew fros and the fact that you're supposed to forget that several million people live in New York as a hippie theater troupe cavorts around the deserted city adds up to the theater of the absurd. But absurd in a very twisted and entertaining way. (Oh... and being under the influence helps...)"
},
		{
			"rate":50,
			"text": "a great musical and who can resist Victor Garber in a 'fro"
},
		{
			"rate":30,
			"text": "im in the play but never saw the movie but i <3 the music in it"
},
		{
			"rate":10,
			"text": "I will be haunted forever."
},
		{
			"rate":40,
			"text": "This musical is upbeat, 70's, and has a good moral meaning. Like many say, it reminds you of HAIR and JESUS CHRIST SUPERSTAR, but it rolls to a beat of its own. The story is of regular townspeople who follow the beliefs of one man (Victor Garber) and the ending affects of what happens. This musical is easily enjoyable with thought and laughs! If you can, make sure you see it soon! You only live DAY BY DAY!"
},
		{
			"rate":40,
			"text": "Wow-freaky and awesome movie at the same time. A bunch of Jesus crazy hippies! I am in the play Godspell.. music is awesome..=)"
},
		{
			"rate":50,
			"text": "music is awesome and inspiring"
},
		{
			"rate":25,
			"text": "It's a better play than movie. I <3 you Chrissy Link."
},
		{
			"rate":45,
			"text": "This is a interesting play the plot is unreal."
},
		{
			"rate":45,
			"text": "A wonderfully psychedelic look at the gospel! So 70s. But Jesus is really cute in it!"
},
		{
			"rate":40,
			"text": "great musical. i love the New Testament in this hippie way. Great songs and a very good cast.Preparing the way of the Lord!!! =]"
},
		{
			"rate":45,
			"text": "A good introduction to musicals. It is a tad different than the stage version (which is better), but still pretty good."
},
		{
			"rate":5,
			"text": "I despise this movie/musical. An interesting concept, I suppose, but just a stupid urban hippie New Testament with lousy songs."
},
		{
			"rate":35,
			"text": "Love the soundtrack."
},
		{
			"rate":45,
			"text": "Excellent songs, Supreme story, especially for us Episcopalians!"
},
		{
			"rate":40,
			"text": "awesome musical from 70's"
},
		{
			"rate":50,
			"text": "victor garber was really good-looking when he was younger...yeah. Robin Lamont sings (one of) my favorite songs - Day By Day. oddly enough, in the musical we just completed - Working - she sings my solo song - Millwork. i absolutely love this movie."
},
		{
			"rate":-1,
			"text": "i wanna see this a lot"
},
		{
			"rate":50,
			"text": "Really good movie and play!"
},
		{
			"rate":40,
			"text": "LOVED IT WHEN I WAS A KID !!!!!!!!!!! GREAT SOUNDTRACK !!!!!!!!!!!!!!!!!"
},
		{
			"rate":5,
			"text": "awefull 70s acting and makeup and etc.... but the stageshow is really good"
},
		{
			"rate":45,
			"text": "If you can hear the message dressed in hippie clothes and pop music, this is a good movie. Obviously, I like it ... after all these years."
},
		{
			"rate":45,
			"text": "Yeah right. Compared to its contemporaries... I can actually enjoy this one."
},
		{
			"rate":50,
			"text": "Awesome!!!! We beseech thee, hear us!"
},
		{
			"rate":50,
			"text": "wow this musical is amazing. i just did it at school, and the music is awesome, it is FREAKIN HILARIOUS and it has a really good message. the music pretty much covers all genres (we beseech thee is a country song, prepare ye is a rock-type song, for example). it is just so random and funny, and it is super powerful."
},
		{
			"rate":50,
			"text": "i just did this play....the kid who did jesus was the coolest kid ever. ya, david yamaguchi rocks. jk, john, you rule."
},
		{
			"rate":50,
			"text": "Love the outfits. Even if they are dated! Good music as well."
},
		{
			"rate":35,
			"text": "This was a great movie! If you ever wanted to see a Christian musical, then this one is an amazing pick."
},
		{
			"rate":50,
			"text": "very good story line and good music"
},
		{
			"rate":50,
			"text": "i realize that im a total jackass for loving this film. becuz i think everything about it is bullshit, and i am rather antichristian. but wow these songs are fun! i love the whole hippy singing smiling gang of odd creative people. there is no plot, it just walks thru really random versus of mathew. well known stories. its so cute. fabulouso music!"
},
		{
			"rate":35,
			"text": "Did the play...Day By Day is the one of the worst songs to get stuck in your head"
},
		{
			"rate":40,
			"text": "Loved it! i think it helped that I love the musical, probably more than the fillm. It's a bit sad watching it now, with them dancing on the two towers in NY"
},
		{
			"rate":45,
			"text": "jonathan wilkes on stage with daniel mcpherson played the best jesus and judas ever"
},
		{
			"rate":-1,
			"text": "Prepare ye the way of the Lord!"
},
		{
			"rate":10,
			"text": "was in it in the mercury in colchester but didn't actually enjoy the film at all, very boring to be honest!"
},
		{
			"rate":40,
			"text": "Love this musical. Victor Garber shines as Jesus."
},
		{
			"rate":-1,
			"text": "I wonder what this is about, i think ill see it!"
},
		{
			"rate":50,
			"text": "Wonderful music, beautiful New York scenery(including a great dance scene on the twin towers) & just plain FUN telling of the book of Matthew! Have loved this movie my entire life! ^_^"
},
		{
			"rate":40,
			"text": "Excellent soundtrack to this movie. I haven't seen it in ages. but would love to see again."
},
		{
			"rate":35,
			"text": "Godspell is essentially the bible on LSD..."
},
		{
			"rate":35,
			"text": "I had to watch this movie in sections, but over all it was a pretty good movie."
},
		{
			"rate":40,
			"text": "Yes, I admit it's really dated, but I just think it's charming. I do love the music."
},
		{
			"rate":35,
			"text": "Day By Day - Robin Lamont & Company"
},
		{
			"rate":50,
			"text": "Love the music and can't help but enjoy watching a young Victor Garber play Jesus."
},
		{
			"rate":35,
			"text": "QUITE ODD. But for some reason it grew on me. Now I love it and have found some of the songs to have gotten stuck in my head from time to time. Probably one of the most random musicals I've ever seen, but that to me just adds to the charm. I think it's funny too."
},
		{
			"rate":35,
			"text": "I saw this for the first time at my aunt's house in Rhode Island, and it in turn introduced me to the theater version of this. Godspell is just such a fun show... so upbeat and energetic and full of fun. The cast of the movie is pretty good, definitely does justice to the score."
},
		{
			"rate":50,
			"text": "Wow this is soo funny & very cute for kids to learn the gospel of John."
},
		{
			"rate":50,
			"text": "[b][font=Times New Roman][size=3][color=#000000]Godspell[/color][/size][/font][/b] [color=#000000][font=Times New Roman]Rated TV PG for (dialogue?) and loud rock music[/font][/color] [color=#000000][font=Times New Roman]This movie/musical gets better every time I see it. I first saw it in 8th grade; then I saw it as a performed musical in 9th grade. And now I just saw it at the end of the 11th grade. Ah, it is quite nice to see old film classics.[/font][/color] [color=#000000][font=Times New Roman]The beginning of this movie has always seemed strange. People came to John the Baptist. He didn?t go around town performing 70?s special effects moves to get them. Still, it?s pretty delightful. Eventually, Jesus comes and John turns into Judas. I?ve never quite understood that. [/font][/color] [color=#000000][font=Times New Roman]The characters are very interesting. There?s the flirty girl who sings the enticing song to Jesus. There are the 2 black people who I think I confused even on this 2nd movie viewing (I?m pretty sure one of them is a guy). There?s the short man with the high voice who I had not remembered until I saw him again. There are a couple more girls, and then there?s Jesus who looks a little like Mitch Duggins but looks even more like Dan Petrie. Everyone is always so happy in this film. It made me feel happy overall too.[/font][/color] [color=#000000][font=Times New Roman]The references to biblical stories were delightful. I liked the one about the rich man and the 2 sons, although I always am slightly saddened when the fatted calf is caught. The Good Samaritan finger show was something I?ve thoroughly enjoyed all 3 times that I?ve seen it done. Another one that I remember is the story with the moral ?humbled will be exalted; exalted will be humbled?. Students studying the parables of Jesus (Mercy Montessori with Dr. Dewey) will be delighted (as I was) making the connections with this story and the gospel of John. [/font][/color] [color=#000000][font=Times New Roman]The upbeat music in this musical is both memorable and appropriate. The ?Day by day? theme is completely unforgettable. The Jesus? death song is also memorable. The lyrics of the song that Olivia?s character sang were unintelligible but I did like how she was starting to come on to Jesus (although Jesus acted oblivious). I don?t think she was even PG in the song though. Strange. ?It?s all for the best? was also played. Good, although not as memorable (and isn't ?t from yet some other production?). Although I can?t remember the rest of them, none had any problem in it. [/font][/color] [color=#000000][font=Times New Roman]I?m glad for once to see no violence in something depicting times long ago (Gladiator, Odyssey, Ten Commandments, all Carle movies). This shows that violence is not needed to give a good message. Jesus? passion does not have to be seen explicitly. No nails in hands/ feet with blood running down and no stab in the side. Those red ribbons seemed a little stupid at first but there couldn?t be anything better to portray it. Everything in this movie references the times of the late 60?s and the early 70?s. Although I didn?t live back then, I now feel like I have after watching Godspell.[/font][/color] [color=#000000][font=Times New Roman]Opening: Weird/Funny. 15[/font][/color] [color=#000000][font=Times New Roman]Middle: Quality material abounds here. The retellings were great. Happiness/joy was in every part. 15[/font][/color] [color=#000000][font=Times New Roman]Ending: A cherry on top to a great film. 15[/font][/color] [color=#000000][font=Times New Roman]Music: I wish I could give more. 10[/font][/color] [color=#000000][font=Times New Roman]Visual: Eye delights throughout. Red ribbons/ finger walks/ black and white film w/ Jesus playing the piano really fast/ pretty girls and plenty more. 10[/font][/color] [color=#000000][font=Times New Roman]Bad content: What bad content? 10[/font][/color] [color=#000000][font=Times New Roman]My expectations: Seeing this movie put a relief to the stressful week of the national spelling bee. Cheers to this superb musical. 25[/font][/color] [color=#000000][font=Times New Roman]Perfect score: 100%[/font][/color]"
}	]
}