{
	"title": "The World's Greatest Athlete (1973)",
	"synopsis":"One of the best of the early-1970s Disney farces, The World's Greatest Athlete stars Jan-Michael Vincent in the title role. A wild boy living off the land in the jungles of Africa, Vincent is discovered by coaches Tim Conway and John Amos. Cursed with a last-place college athletic lineup, Conway and Amos hope that Vincent will pull them out of their years-long slump. And he does, but not before several Disneyesque slapstick highlights, not to mention a handful of amusing special-effects gags (at one point, Conway is shrunk to mouse size by witch doctor Roscoe Lee Browne). Despite its formidable lineup of comedians-Conway, Billy DeWolfe, Nancy Walker, Vito Scotti et. al.--The World's Greatest Athlete's funniest line goes to guest star Howard Cosell! The script is the handiwork of Gerald Gardner and Dee Caruso, late of That Was the Week That Was and Get Smart. ~ Hal Erickson, Rovi",
	"genre":
	[
		{"name":"Comedy"},
		{"name":"Kids & Family"}

	],
	"rate":2.9,
	"comments":[
		{
			"rate":25,
			"text": "Remember loving this when I saw it as a kid. But the reason was because Jan-Michael Vincent was SO CUTE! I have to feeling that if I was to revisit this, that's the ONLY thing I would find good about it."
},
		{
			"rate":35,
			"text": "Great Disney live action movie from the 70's."
},
		{
			"rate":35,
			"text": "Enjoyable Disney outing."
},
		{
			"rate":40,
			"text": "Good live action disney movie. Good for the whole family. Very enjoyable. Jan-Michael was great as Nanu. He did a fantastic job."
},
		{
			"rate":45,
			"text": "One of Disney's better live action films (after Old Yeller, Poppins, Herbie & Dr. Syn), complete with examples of using voodoo as part of an athletic training regimen. With the usual batch of great supporting cast."
},
		{
			"rate":5,
			"text": "I remember this .... it was so cheesey"
},
		{
			"rate":45,
			"text": "One of Disney's better live action films (after Old Yeller, Poppins, Herbie & Dr. Syn), complete with examples of using voodoo as part of an athletic training regimen. With the usual batch of great supporting cast."
},
		{
			"rate":5,
			"text": "I remember this .... it was so cheesey"
},
		{
			"rate":50,
			"text": "AHHH JAN MICHEAL SAYS IT ALL"
},
		{
			"rate":35,
			"text": "Great Disney live action movie from the 70's."
},
		{
			"rate":25,
			"text": "Remember loving this when I saw it as a kid. But the reason was because Jan-Michael Vincent was SO CUTE! I have to feeling that if I was to revisit this, that's the ONLY thing I would find good about it."
},
		{
			"rate":35,
			"text": "Enjoyable Disney outing."
},
		{
			"rate":40,
			"text": "Still a fun movie to watch."
},
		{
			"rate":50,
			"text": "Another Disney Great"
},
		{
			"rate":30,
			"text": "Oh man! I remember seeing this flick as a kid. I think I really liked it. The ending stuck in my mind."
},
		{
			"rate":50,
			"text": "AHHH JAN MICHEAL SAYS IT ALL"
},
		{
			"rate":35,
			"text": "this was my favorite movie as a kid. i found it on ebay and 35 years later . . it was not as good"
},
		{
			"rate":-1,
			"text": "kool as i want 2 see dis. i want 2 see every movie in da wole wide world of course im a movie lover"
},
		{
			"rate":40,
			"text": "Good live action disney movie. Good for the whole family. Very enjoyable. Jan-Michael was great as Nanu. He did a fantastic job."
},
		{
			"rate":25,
			"text": "This is a very 'odd' movie but x) intertaing, Harry the tiger is very cute and it has a very interesting ending."
}	]
}