{
	"title": "The Nuttiest Nutcracker (1999)",
	"synopsis":"In this computer-animated update of the classic Christmas tale The Nutcracker, a girl in her early teens gets the bad news that she won't be able to see her parents for the holidays. Depressed by the news, she goes to sleep with her favorite toy, a nutcracker that resembles a toy soldier. When she wakes up, she discovers that the toy has come alive and she has shrunk to its size. She and the nutcracker are spirited off to Christmas Land, where our heroes are joined by fruits, vegetables, and even a pack of nuts, which all come to life as they do battle against Reginald the Rat King, who wants to stop Christmas from coming. The Nuttiest Nutcracker features James Belushi, Cheech Marin, and Phyllis Diller; R&B star Peabo Bryson performed two original songs on the soundtrack. ~ Mark Deming, Rovi",
	"genre":
	[
		{"name":"Animation"},
		{"name":"Kids & Family"}
,
		{"name":"Musical & Performing Arts"}
,
		{"name":"Classics"}
,
		{"name":"Science Fiction & Fantasy"}

	],
	"rate":2.9,
	"comments":[
		{
			"rate":25,
			"text": "when I first saw this movie, I was about 5 years old. I liked it, the humor and everything. Unfortunately time went by and now, I have rather mixed feelings about it, nostalgia sets in which tries to force me to like it more than I do. And with certain big lipped alligator moments like the bizarre All It Takes scene which takes us to Candy Land of all places and the gay scene of the mouse king and his sergeant dancing to one of the most beloved songs from the Nutcracker, it's worse than I remember. And not to mention the horrifyingly horrible computer graphics, this was made around the same time as things like VeggieTales and Toy Story, both of whom just about revolutionized CG, this... looks like it was made by some guy in his mom's basement. Limited motion and such. This movie is not terrible, but it's not the best either. With a little help from your inner child, you still may be able to enjoy this, but only around Christmas time."
},
		{
			"rate":-1,
			"text": "this movie is terrible and worst of all it's stupid"
},
		{
			"rate":5,
			"text": "The Nuttiest Nutcracker es sin duda la m?s mediocre adaptacion de El Cascanueces, pues no se aferra en nada al cuento original y se ve saturada de personajes insulsos, protagonistas flojos, humor pat?tico y una penosa animaci?n por computadora."
},
		{
			"rate":5,
			"text": "You don't need me to tell you that this film is garbage, but I will anyway."
},
		{
			"rate":25,
			"text": "when I first saw this movie, I was about 5 years old. I liked it, the humor and everything. Unfortunately time went by and now, I have rather mixed feelings about it, nostalgia sets in which tries to force me to like it more than I do. And with certain big lipped alligator moments like the bizarre All It Takes scene which takes us to Candy Land of all places and the gay scene of the mouse king and his sergeant dancing to one of the most beloved songs from the Nutcracker, it's worse than I remember. And not to mention the horrifyingly horrible computer graphics, this was made around the same time as things like VeggieTales and Toy Story, both of whom just about revolutionized CG, this... looks like it was made by some guy in his mom's basement. Limited motion and such. This movie is not terrible, but it's not the best either. With a little help from your inner child, you still may be able to enjoy this, but only around Christmas time."
},
		{
			"rate":-1,
			"text": "this movie is terrible and worst of all it's stupid"
},
		{
			"rate":5,
			"text": "The Nuttiest Nutcracker es sin duda la m?s mediocre adaptacion de El Cascanueces, pues no se aferra en nada al cuento original y se ve saturada de personajes insulsos, protagonistas flojos, humor pat?tico y una penosa animaci?n por computadora."
},
		{
			"rate":5,
			"text": "Obviously made for kids. The Theatrical Trailer pretty much will sum the movie up for you, and make you laugh hysterically at the stupidity of this movie. [Violence 1/10, Language 0/10, Sexual Content 1/10]"
},
		{
			"rate":-1,
			"text": "i watched this movie all the time"
},
		{
			"rate":5,
			"text": "You don't need me to tell you that this film is garbage, but I will anyway."
},
		{
			"rate":35,
			"text": "Very cute amusing when you're looking for something really brainless and christmas movie-ish"
},
		{
			"rate":-1,
			"text": "I wish I could watch every movie ever made but I sadly don't have the time. I'm not interested in this movie because I don't think I'll like it or don't know enough about it to think I might like it. I may still end up seeing it some day though."
},
		{
			"rate":35,
			"text": "I Love This Movie It Kind of Like Veggie Tales"
},
		{
			"rate":5,
			"text": "okay...the name says it all"
},
		{
			"rate":15,
			"text": "The sugar plum fairy is me??? Lol. Like, so predictable."
},
		{
			"rate":-1,
			"text": "You want to know what retarded looks like...Watch this!"
}	]
}