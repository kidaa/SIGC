{
	"title": "Change of Habit (1969)",
	"synopsis":"Dr. John Carpenter (Elvis Presley) helps the economically disadvantaged in an inner-city medical clinic. Three nuns are assigned to help out at the facility and are allowed to wear regular clothes instead of the traditional habits. Sister Michelle (Mary Tyler Moore) is the speech therapist who Dr. Carpenter would like to examine personally after hours. Along with the other sisters (Barbara McNair and Jane Elliot), Michelle is subjected to the criticism of the local parish priest (Regis Toomey) in the social experiment of non-traditional dress. Two spinsters even mistake the nuns for prostitutes without their habits. The priest wins out in the end, and the nuns must again don their habits. As the good doctor sings to the ailing children, Sister Michelle is transfixed both by a crucifix hanging on the wall and by Elvis Presley in an ironic and symbolic scene that flashes between the two icons. This was Presley's last studio feature and he welcomed the move from stifling screen images as he returned his focus to live performances and recording for the remainder of his illustrious career. ~ Dan Pavlides, Rovi",
	"genre":
	[
		{"name":"Drama"},
		{"name":"Musical & Performing Arts"}
,
		{"name":"Classics"}
,
		{"name":"Comedy"}

	],
	"rate":3.4,
	"comments":[
		{
			"rate":40,
			"text": "This movie is slightly more interesting than Elvis' usual movies, and it really helps to have a great actress like Mary Tyler Moore in the film too, but it's still not a fantastic movie. If you're an Elvis or Moore fan you'll like it, though."
},
		{
			"rate":20,
			"text": "depends on how you feel about mary tyler moore as a nun and elvis as a doctor!"
},
		{
			"rate":25,
			"text": "Ill conceived idea to begin with, it's not very well cast. As much as I like Mary Tyler Moore, she stinks in this. Not at all convincing, but it does mean well and has a few good scenes, but overall it doesn't work."
},
		{
			"rate":25,
			"text": "I love Elvis. I always have, he is the definition of cool. I also love musicals, so I am a fan of Elvis's films. This was Elvis's most serious, less singing role. He only sang four songs for this film, including the title song. This movie has my all time favorite Elvis song, Rubberneckin in it. This movie also has a stellar cast. Mary Tyler Moore, Jane Elliot, Barbara McNair, and even Ed Asner. Although, I do prefer Elvis with Ann-Margaret. They always made such a lovely couple. The only reason this movie gets 2.5 stars, is that i hated the ending. She was left with a choice and didn't make one. So, that kind of bothered me. Other then that Change Of Habit is a good Elvis movie, he did great in it."
},
		{
			"rate":35,
			"text": "very good elvis really good so was mary tyler moore great story line a must see for any elvis fan would have like a little more music"
},
		{
			"rate":10,
			"text": "This should not be rated G! Among other things is a very unrealistic, disturbing portrayal of a one-session cure for autism. Then there is the matter of the rape of a nun. Not recommended!"
},
		{
			"rate":45,
			"text": "very good movie! but i'm curious if michelle & john would ever hook up...i know i'd take him, he was sooo nice to look at in this movie lol"
},
		{
			"rate":35,
			"text": "I really enjoy this Elvis flick. Rubberneckin' get's me moving every time, it infectious. This was a departure for Elvis, one of his non heavy-laden musicals. It is definitely worth watching!"
},
		{
			"rate":25,
			"text": "Elvis as an inner-city doctor? Mary Tyler Moore and Barbara McNair as nuns? This final Elvis movie is a trip."
},
		{
			"rate":35,
			"text": "Change of Habit is a kool song and Elvis looks sexy in this movie, he is a doctor in it and he looks very sexy."
},
		{
			"rate":50,
			"text": "Elvis's best movie!! He actually plays a real guy who sings as a hobby. He was really, really good in this. He had amazing chemistry with Mary Tyler Moore."
},
		{
			"rate":40,
			"text": "Okay- so I like Elvis- and the love interest's name was Michele. Alright- so she was a nun- but still she got to kiss ELVIS!"
},
		{
			"rate":30,
			"text": "Change of Habit is a decent film. It is about Dr. John Carpenter who helps the economically disadvantaged in an inner-city medical clinic. Elvis Presley and Mary Tyler Moore give good performances. The screenplay is okay but a little slow in place. William A. Graham did an alright job directing this movie. I liked this motion picture but not one of my favorites."
},
		{
			"rate":25,
			"text": "A standard Elvis Presley vehicle that doesn't offer anything new to his r?sum?."
},
		{
			"rate":-1,
			"text": "[I Don't see the location where I can give it STARS.]I put this on for inspiration. All the songs are just good and yet REALLY GOOD. It's a feel good movie - I try not to look at MTM as she bugs me so much here. Elvis is an international star now, although looking a bit thin. He is human Elvis. I Love seeing lovely Barbara McNair in this and many other faces to recognize. The gift of helps;free clinics;social change;education importance;even black power is represented;cute old snoopy ladies;old church out of touch; mention of the war. The songs are what I listen for and that great Elvis voice. I love everybody especially Elvis when I watch this movie. (except MTM in this). Love the Dr. John Carpenter line ....this place can slow you down, don't you think? To MTM in the convent."
},
		{
			"rate":15,
			"text": "CHANGE OF HABIT (1969)"
},
		{
			"rate":25,
			"text": "Ill conceived idea to begin with, it's not very well cast. As much as I like Mary Tyler Moore, she stinks in this. Not at all convincing, but it does mean well and has a few good scenes, but overall it doesn't work."
},
		{
			"rate":30,
			"text": "Change of Habit is a decent film. It is about Dr. John Carpenter who helps the economically disadvantaged in an inner-city medical clinic. Elvis Presley and Mary Tyler Moore give good performances. The screenplay is okay but a little slow in place. William A. Graham did an alright job directing this movie. I liked this motion picture but not one of my favorites."
},
		{
			"rate":25,
			"text": "A standard Elvis Presley vehicle that doesn't offer anything new to his r?sum?."
},
		{
			"rate":25,
			"text": "How lucky was Mary Tyler Moore to land this part?"
},
		{
			"rate":-1,
			"text": "Would like to get round to watching."
},
		{
			"rate":40,
			"text": "Elvis last movie (TTWII and On Tour were next) and it's damn good. Elvis looked great before going to Vegas and Mary Tyler Moore is hot in this with that Nuns outfit;) The songs are great but Have a Happy seems out of place. The one thing I don't like about this movie is the Autism aspect of it..It seems very out of date and the scene with Elvis/Mary and the kid is very uncomfertable to watch. Well worth a watch even if you're not an Elvis fan."
},
		{
			"rate":15,
			"text": "J'adore les com?dies-romantiques musicales d'Elvis Presley, mais Change Of Habit est clairement l'une des plus mauvaises. Elvis interpr?tant un docteur, c'est d?j? moyen, mais Mary Tyler en soeur, il faut le faire!! Non, sinc?rement, je n'ai pas trop trip?."
},
		{
			"rate":20,
			"text": "7/20/2012: Ok. Nothing special. Cheesy acting and not his greatest musical effort."
},
		{
			"rate":15,
			"text": "One of the better Elvis movies but still lacking in script and direction. It has two great leads. The ending is pretty stupid and leaves one hanging!"
},
		{
			"rate":-1,
			"text": "[I Don't see the location where I can give it STARS.]I put this on for inspiration. All the songs are just good and yet REALLY GOOD. It's a feel good movie - I try not to look at MTM as she bugs me so much here. Elvis is an international star now, although looking a bit thin. He is human Elvis. I Love seeing lovely Barbara McNair in this and many other faces to recognize. The gift of helps;free clinics;social change;education importance;even black power is represented;cute old snoopy ladies;old church out of touch; mention of the war. The songs are what I listen for and that great Elvis voice. I love everybody especially Elvis when I watch this movie. (except MTM in this). Love the Dr. John Carpenter line ....this place can slow you down, don't you think? To MTM in the convent."
},
		{
			"rate":35,
			"text": "It's a really good Elvis one of my top 10. I like it because of the song in the opener."
},
		{
			"rate":40,
			"text": "This movie is slightly more interesting than Elvis' usual movies, and it really helps to have a great actress like Mary Tyler Moore in the film too, but it's still not a fantastic movie. If you're an Elvis or Moore fan you'll like it, though."
},
		{
			"rate":15,
			"text": "CHANGE OF HABIT (1969)"
},
		{
			"rate":35,
			"text": "Mary Tyler Moore was fantastic even she didn't look like nun and it feels for her natural to wear dresses (even she looks great in nuns clothes as well). Likewise Elvis who didn't look like doctor at all and playing doctor was weird but taking this just as entertainment he was good. Screenplay was mean in parts and story is ordinary and I even get irritated by the ending cause it feels like movie isn't finished. It's not something like other religious movies of the time. But I watched it as an entertainment without any deeper meaning and I enjoyed it."
},
		{
			"rate":25,
			"text": "Ill conceived idea to begin with, it's not very well cast. As much as I like Mary Tyler Moore, she stinks in this. Not at all convincing, but it does mean well and has a few good scenes, but overall it doesn't work."
},
		{
			"rate":25,
			"text": "Ill conceived idea to begin with, it's not very well cast. As much as I like Mary Tyler Moore, she stinks in this. Not at all convincing, but it does mean well and has a few good scenes, but overall it doesn't work."
},
		{
			"rate":25,
			"text": "I love Elvis. I always have, he is the definition of cool. I also love musicals, so I am a fan of Elvis's films. This was Elvis's most serious, less singing role. He only sang four songs for this film, including the title song. This movie has my all time favorite Elvis song, Rubberneckin in it. This movie also has a stellar cast. Mary Tyler Moore, Jane Elliot, Barbara McNair, and even Ed Asner. Although, I do prefer Elvis with Ann-Margaret. They always made such a lovely couple. The only reason this movie gets 2.5 stars, is that i hated the ending. She was left with a choice and didn't make one. So, that kind of bothered me. Other then that Change Of Habit is a good Elvis movie, he did great in it."
},
		{
			"rate":-1,
			"text": "I dont really care for movies that Elvis is in but because Mary Tyler Moore is in it I cant wait to watch it!"
},
		{
			"rate":50,
			"text": "Elvis ! and Mary Tyler Moore"
},
		{
			"rate":35,
			"text": "very good elvis really good so was mary tyler moore great story line a must see for any elvis fan would have like a little more music"
},
		{
			"rate":-1,
			"text": "no thanks not my kinda thing"
},
		{
			"rate":25,
			"text": "Why can't Elvis kiss his leading lady? Because she's a nun. If this movie was made in this decade, Elvis would be played by Brad Pit and He'd take th enun to bed."
},
		{
			"rate":-1,
			"text": "Yet another Elvis movie someday."
},
		{
			"rate":35,
			"text": "Change of Habit (1969) will not be remembered as one of Elvis Presley's best efforts, but this socially conscious drama represents an admirable change of pace. In his last fictional film, Elvis gives a believable performance in the improbable role of a ghetto doctor. He actually fares better than Mary Tyler Moore -- hopelessly miscast as a nun. Though ludicrous in spots, Change of Habit is easier to sit through than many of Presley's musicals."
},
		{
			"rate":50,
			"text": "Awww....Elvis is in..so it's great@!"
},
		{
			"rate":50,
			"text": "This is a very good movie"
},
		{
			"rate":-1,
			"text": "Absolutely not interested"
},
		{
			"rate":-1,
			"text": "Eek! An Elvis Presley movie that I must never watch!!!"
},
		{
			"rate":10,
			"text": "This should not be rated G! Among other things is a very unrealistic, disturbing portrayal of a one-session cure for autism. Then there is the matter of the rape of a nun. Not recommended!"
},
		{
			"rate":25,
			"text": "Did not find this Elvis picture to be in sync with his many others. Saw it one and that was enough."
},
		{
			"rate":50,
			"text": "It's Elvis...he's great in everything."
},
		{
			"rate":50,
			"text": "Classic owns it , ya just cant get enough E moovies"
},
		{
			"rate":-1,
			"text": "sounds really good :)"
},
		{
			"rate":50,
			"text": "It is a good one LOVE RENEE"
},
		{
			"rate":50,
			"text": "GREAT MOVIE!!!!!!!!!!!!!!!!!!!!!!!!!"
},
		{
			"rate":20,
			"text": "I've never liked these much, but I did see all of them - my mom is a Elvis fan see."
},
		{
			"rate":-1,
			"text": "elvis even admitted that these were all silly movies"
},
		{
			"rate":50,
			"text": "Watched so many times just to hear Elvis sing a Change of habit"
},
		{
			"rate":35,
			"text": "I really wanted to know what happened at the end :("
},
		{
			"rate":45,
			"text": "Best Elvis movie ever!!!!!!!!!!!!!!"
},
		{
			"rate":-1,
			"text": "excellent movie Presley's best yet!!!!"
},
		{
			"rate":45,
			"text": "I love Mary Tyler Moore. Surprised I have not seen this movie until today. Didn't realize how old Elvis really would be if he was alive. This was one of his earlier films. Enjoyed this one very much!"
},
		{
			"rate":50,
			"text": "Mary Tyler Moore was in for the acting not him. The movie was great"
},
		{
			"rate":-1,
			"text": "Would like to get round to watching."
},
		{
			"rate":40,
			"text": "Elvis again!I didn't really like how it just ended but other than that..I liked it :]"
},
		{
			"rate":40,
			"text": "Elvis last movie (TTWII and On Tour were next) and it's damn good. Elvis looked great before going to Vegas and Mary Tyler Moore is hot in this with that Nuns outfit;) The songs are great but Have a Happy seems out of place. The one thing I don't like about this movie is the Autism aspect of it..It seems very out of date and the scene with Elvis/Mary and the kid is very uncomfertable to watch. Well worth a watch even if you're not an Elvis fan."
},
		{
			"rate":45,
			"text": "very good movie! but i'm curious if michelle & john would ever hook up...i know i'd take him, he was sooo nice to look at in this movie lol"
},
		{
			"rate":50,
			"text": "I love this movie...It is an excellent movie...I mean when you got a movie with Elvis and Mary Tyler Moore in it...you cant go wrong."
},
		{
			"rate":-1,
			"text": "Elvis Presley and Mary Tyler Moore are two of my favorites! Both of them together is even better!"
},
		{
			"rate":50,
			"text": ": Elvis tried something different in his final narrative movie? but the results are oddly similar to his usual '60s formula. Here the King plays a doctor working in an inner-city free clinic, playing host to three Catholic nurses (who are really n..."
},
		{
			"rate":-1,
			"text": "Don't care for Elvis and any others that have to do with him"
},
		{
			"rate":30,
			"text": "this was one of the better ones. Good movie."
},
		{
			"rate":35,
			"text": "how can you not love elvis as a dr in the ghetto"
},
		{
			"rate":35,
			"text": "I really enjoy this Elvis flick. Rubberneckin' get's me moving every time, it infectious. This was a departure for Elvis, one of his non heavy-laden musicals. It is definitely worth watching!"
},
		{
			"rate":50,
			"text": "A GREAT ACTRESS WITH HIM! LOVED IT!"
},
		{
			"rate":50,
			"text": "My first drama. I think it was pretty amazing!"
},
		{
			"rate":25,
			"text": "Elvis as an inner-city doctor? Mary Tyler Moore and Barbara McNair as nuns? This final Elvis movie is a trip."
},
		{
			"rate":45,
			"text": "Oh Doctor,can I see you?/"
},
		{
			"rate":50,
			"text": "HOTTIEEEE!!!!! this was his only movie after 1968 comeback special"
},
		{
			"rate":45,
			"text": "great Elvis role and his last movie; he was great!"
},
		{
			"rate":-1,
			"text": "I watched some of this on tv"
},
		{
			"rate":20,
			"text": "this ones not to bad, since mary tyler moore is in it"
},
		{
			"rate":50,
			"text": "Anything with Elvis is Excellent!"
},
		{
			"rate":45,
			"text": "ha ya. the nun movie lol. that was good"
},
		{
			"rate":35,
			"text": "In my opinion, this was Elvis' best movie"
},
		{
			"rate":20,
			"text": "depends on how you feel about mary tyler moore as a nun and elvis as a doctor!"
},
		{
			"rate":50,
			"text": "one of his best without any singing"
},
		{
			"rate":30,
			"text": "i like this movie its so sweet"
},
		{
			"rate":50,
			"text": "I love this movie up there with my favorites!! Love Mary Tyler Moore in this movie as well!! I also have this movie!!"
},
		{
			"rate":50,
			"text": "he could be my doctor any day"
},
		{
			"rate":50,
			"text": "real great movie, wonderful, cute story. and a great cast."
},
		{
			"rate":35,
			"text": "Change of Habit is a kool song and Elvis looks sexy in this movie, he is a doctor in it and he looks very sexy."
},
		{
			"rate":45,
			"text": "elvis was a hottie in this one"
},
		{
			"rate":50,
			"text": "Elvis's best movie!! He actually plays a real guy who sings as a hobby. He was really, really good in this. He had amazing chemistry with Mary Tyler Moore."
},
		{
			"rate":40,
			"text": "I liked this one. Not quite like the others though."
},
		{
			"rate":40,
			"text": "Okay- so I like Elvis- and the love interest's name was Michele. Alright- so she was a nun- but still she got to kiss ELVIS!"
},
		{
			"rate":-1,
			"text": "I only wanna see it because of Mary Tyler Moore! I just can't stand Elvis!"
},
		{
			"rate":40,
			"text": "Different one for Elvis--I liked it alot! DVD."
},
		{
			"rate":45,
			"text": "the absalute best here he gets to introduce some of his inspirational stuff"
},
		{
			"rate":50,
			"text": "Elvis ' last movie...very good"
},
		{
			"rate":30,
			"text": "would be better if it was a younger elvis and not the old"
},
		{
			"rate":35,
			"text": "Elvis was an average actor at best."
},
		{
			"rate":45,
			"text": "Inspirational, dangeriously conservsial, serious yet enlightening. Mary tylor moore/beautifully believable Elvis Presley/acting excellence shown A choice between serving god/man"
},
		{
			"rate":50,
			"text": "Elvis at his most handsome!"
},
		{
			"rate":5,
			"text": "Quite a poor movie to say the least..... Certainly not worth the time to watch it."
},
		{
			"rate":-1,
			"text": "I would like to see this, i think..."
},
		{
			"rate":50,
			"text": "love l film, love him"
},
		{
			"rate":-1,
			"text": "We had this at our house and I never got a chance to see it."
},
		{
			"rate":30,
			"text": "Doktor Elvis m?ter nunnan Mary Tyler Moore. Annorlunda men inte alltid s? ?vertygande."
},
		{
			"rate":40,
			"text": "One of his good movies"
},
		{
			"rate":40,
			"text": "This has some really cool songs, it was when Elvis went gospel."
},
		{
			"rate":-1,
			"text": "sounds cool... never heard of it before though.."
},
		{
			"rate":25,
			"text": "How lucky was Mary Tyler Moore to land this part?"
},
		{
			"rate":20,
			"text": "Mary Tyler Moore as Elvis' leading lady? *shudder* Riiiight."
}	]
}