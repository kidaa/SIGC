{
	"title": "Down to the Bone (2003)",
	"synopsis":"Independent filmmaker Debra Granik makes her feature debut with the drama Down to the Bone, based on her award-winning short Snake Feed. Vera Farmiga stars as Irene, a working-class mother living in upstate New York. She struggles to keep her marriage together and raise two sons while keeping her cocaine addiction a secret. Hoping to make a change in her life, she tries to kick her drug habit. However, this proves even more difficult with the oncoming winter and a developing affair. Also starring Hugh Dillon and Clint Jordan, Down to the Bone premiered at the Sundance Film Festival in 2004. ~ Andrea LeVasseur, Rovi",
	"genre":
	[
		{"name":"Drama"}
	],
	"rate":3,
	"comments":[
		{
			"rate":40,
			"text": "More subtle than your usual drug addict movie. While desperate, this one doesn't end up on a street corner somewhere homeless, or into crime, but it does still show the devastation caused by addiction and is a moving and interesting film. I had never heard of Vera Farmiga before, but I will certainly look out for her in the future, her portrayal of this character was excellent."
},
		{
			"rate":35,
			"text": "Drawn out, but an extremely realistic film about drug abuse with some impressive performances, in particular Vera Farmiga, which is definitely a name I shall be taking notice of in the future, her role in this was faultless and she reminded me of Cate Blanchett in many ways."
},
		{
			"rate":30,
			"text": "[font=Century Gothic]In Private a Palestinian family is not only living in the battlefield of the occupied West Bank but now they must face the additional indignity of having the Israeli army occupy their house. The army gets the second floor(under no circumstances are the family to venture there) and the family must stay downstairs. After dark, they are confined to the living room. For the father, Mohammed, a learned, dignified man, being a refugee would be worse and he insists on staying.[/font][font=Century Gothic][/font] [font=Century Gothic]Private is a tense drama with realistic situations. The film boils the whole Arab-Israeli conflict down to a microcosm of a single house being occupied. Through the character of the father, other ways are suggested of fighting the Israeli army than violence. And the individual soldiers are not shown as being the villains.[/font][font=Century Gothic][/font] [font=Century Gothic][/font] [font=Georgia][color=navy]In Down to the Bone, Irene(Vera Farmiga) is a mother to two young sons and wife to a construction worker. She works as a cashier at a supermarket in upstate New York. She is also addicted to cocaine. And she has been using since high school. After one particularly embarrassing attempt to buy drugs fails, she realizes she needs help and enters rehab.[/color][/font][font=Georgia][color=#000080][/color][/font] [font=Georgia][color=#000080]Down to the Bone covers some familiar ground but does so in a totally unglamorous way. The movie does not offer any easy answers or solutions. The movie is anchored by a very strong performance by Vera Farmiga.[/color][/font]"
},
		{
			"rate":40,
			"text": "Down to the Bone emerges with an aura of unflinching authenticity, while Vera Farmiga delivers an astonishing performance."
},
		{
			"rate":35,
			"text": "This addiction drama is a bit choppy at times, but performances more than make up for that. Not just Vera Farmiga (who carries the movie), but even supporting cast, in part comprised of unprofessional actors, come across as amazingly realistic and natural. The bleak snow-covered surroundings create prefect mood for the story."
},
		{
			"rate":25,
			"text": "Farmiga puts on a wonderfully truthful little performance here, as the young mother wrestling with her hum drum home duties as a single parent coupled with her addiction to drugs. Though it is filmed in the film truth technique, the characters begin to lose some steam mid way through and their motivations become less than clear."
},
		{
			"rate":30,
			"text": "It's not exactly enjoyable, but like those HBO specials about drug abuse, you can't turn away for a second."
},
		{
			"rate":35,
			"text": "This movie probably will have a love or hate reviews- anyone who has never seen addiction can't relate to it in that sense. You see Vera in this movie struggle to become sober and a good parent and then becomes involved in a toxic relationship with someone she confided all of her skeletons to and was supposed to be able to trust. When she gets a reality check and with a clear head knows that in order to be the person she wants to be then she's got to cut ties or else get dragged down and lose everything important to her. I think that everyone can relate to that even if it wasn't based on addiction."
},
		{
			"rate":40,
			"text": "A film about drug addiction that is painfully honest and real. It doesn't glorify or stylize the subject matter."
},
		{
			"rate":45,
			"text": "Emotionally gripping and engrossing, with a phenomenal from Vera Farmiga. It's downbeat, without ever becoming overly depressing, and the characters are moving without ever overacting. The direction the film takes is different than most films with the same subject material, it has a home movie feel that sets it apart from the others. The only major flaw I can find is a less than satisfying climax."
},
		{
			"rate":45,
			"text": "Emotionally engaging and an incredible performance from Vera Farmiga who was so outstanding that for her not to even be nominated is a travesty and is an abomination to all things good and natural in this world. While I didn't find the ending wholly satisfying I still think that this film was powerful enough to be worthy of 4 1/2 stars."
},
		{
			"rate":35,
			"text": "This low budget film from director/writer Debra Granik, who is also responsible for her latter Winter's Bone, absolutely hit the mark. Down to the Bone is a vivid portrayal of drug abuse and family that boasts a stellar performance by Vera Farmiga. What sometimes appears slow and prodding, turns out to be calculated and striking. The film appeared to be shot using grainy handhelds; a most likely result of working with a shoestring budget. The visual appeal might not be there but hang in there, as the story and performances are topnotch."
},
		{
			"rate":40,
			"text": "A movie about why it isn't a good idea to go '13th stepping' in recovery. Farmiga's best performance yet."
},
		{
			"rate":45,
			"text": "An honest portrayal of drug use in a small town. Vera Farmiga is a actress to keep an eye on."
},
		{
			"rate":30,
			"text": "Oddly enough this is an earlier film from the same director that did Winters Bone which was nominated for Oscars a couple years back which I loved. This was okay but the only decent actress in the whole movie was Vera Farmiga. The rest of the cast seemed like they were drug of the streets to appear in it."
},
		{
			"rate":40,
			"text": "A mom of two, Irene (Vera Farmiga) has been addicted to cocaine for years. Her husband Steve (Clint Jordan) uses, but not nearly as much. When Irene tries to use her son's birthday check from Grandma to buy drugs, she knows it's time to check into rehab. She meets Bob (Hugh Dillon), a nurse in the rehab center who's been clean for five years, and they start to have an affair. Irene manages to get clean, but it's so fragile that the tiniest thing can plunge her right back into addiction, and does. This movie really brings home how easy it is to get caught up in that rat race and how incredibly hard it is to get off-and stay off. Vera Farmiga is great, as always. I thought it was very realistic and I liked the ending. I recommend Down to the Bone and give it an 85%."
},
		{
			"rate":20,
			"text": "Down to the Bone is realistic, which I like, but boring, which I don't. 4/10"
},
		{
			"rate":40,
			"text": "The great thing about netflix is how you can go back in time and catch movies you never seen before. I've always liked Vera Farmiga as an actress and Down to the Bone will show you why. This a great movie with great acting about a woman struggling with addiction in upstate NY. This movie does a great job at showing that staying clean is all about you and only you. That it is something you can only do for you and noone else. Great movie. A definite must see. Once again Vera shows why she is one of the best actresess in Hollywood."
},
		{
			"rate":40,
			"text": "DOWN TO THE BONE is a gritty tale that shows the effects of drug abuse and addiction on the user and the people in her life. Vera Farmiga delivers one of the besr performances of her carrer along with an outstanding directorial debut by Debra Granik (WINTER'S BONE)."
},
		{
			"rate":45,
			"text": "This AWESOME movie is about thirteenth stepping: when you get too lazy, too horny... you get distracted... you know, when you got that hole inside and you just need to fill it. Realistic and Powerfull."
},
		{
			"rate":30,
			"text": "It's not exactly enjoyable, but like those HBO specials about drug abuse, you can't turn away for a second."
},
		{
			"rate":35,
			"text": "This movie probably will have a love or hate reviews- anyone who has never seen addiction can't relate to it in that sense. You see Vera in this movie struggle to become sober and a good parent and then becomes involved in a toxic relationship with someone she confided all of her skeletons to and was supposed to be able to trust. When she gets a reality check and with a clear head knows that in order to be the person she wants to be then she's got to cut ties or else get dragged down and lose everything important to her. I think that everyone can relate to that even if it wasn't based on addiction."
},
		{
			"rate":40,
			"text": "A film about drug addiction that is painfully honest and real. It doesn't glorify or stylize the subject matter."
},
		{
			"rate":45,
			"text": "Emotionally gripping and engrossing, with a phenomenal from Vera Farmiga. It's downbeat, without ever becoming overly depressing, and the characters are moving without ever overacting. The direction the film takes is different than most films with the same subject material, it has a home movie feel that sets it apart from the others. The only major flaw I can find is a less than satisfying climax."
},
		{
			"rate":45,
			"text": "Emotionally engaging and an incredible performance from Vera Farmiga who was so outstanding that for her not to even be nominated is a travesty and is an abomination to all things good and natural in this world. While I didn't find the ending wholly satisfying I still think that this film was powerful enough to be worthy of 4 1/2 stars."
},
		{
			"rate":40,
			"text": "Down to the Bone emerges with an aura of unflinching authenticity, while Vera Farmiga delivers an astonishing performance."
},
		{
			"rate":35,
			"text": "This low budget film from director/writer Debra Granik, who is also responsible for her latter Winter's Bone, absolutely hit the mark. Down to the Bone is a vivid portrayal of drug abuse and family that boasts a stellar performance by Vera Farmiga. What sometimes appears slow and prodding, turns out to be calculated and striking. The film appeared to be shot using grainy handhelds; a most likely result of working with a shoestring budget. The visual appeal might not be there but hang in there, as the story and performances are topnotch."
},
		{
			"rate":40,
			"text": "A bleak and grimly beautiful film that follows the path of addiction without glamorisation or theatrics. However, this could be it's biggest weakness as the stakes never seem big enough, and the conflict not quite wrought enough. The film is centred around one fantastic performance that drives the realism of the entire film from start to finish.*Full Review Pending*"
},
		{
			"rate":35,
			"text": "This addiction drama is a bit choppy at times, but performances more than make up for that. Not just Vera Farmiga (who carries the movie), but even supporting cast, in part comprised of unprofessional actors, come across as amazingly realistic and natural. The bleak snow-covered surroundings create prefect mood for the story."
},
		{
			"rate":20,
			"text": "Good performance by Vera...but the story is old and been done time and time again."
},
		{
			"rate":-1,
			"text": "Would like to see at some stage."
},
		{
			"rate":40,
			"text": "A movie about why it isn't a good idea to go '13th stepping' in recovery. Farmiga's best performance yet."
},
		{
			"rate":40,
			"text": "Good movie for anyone who has tried to quit using."
},
		{
			"rate":45,
			"text": "An honest portrayal of drug use in a small town. Vera Farmiga is a actress to keep an eye on."
},
		{
			"rate":20,
			"text": "Real life situation is really grabbing, realistic, but this is not why we watch movies. Recommended for real life."
},
		{
			"rate":30,
			"text": "Oddly enough this is an earlier film from the same director that did Winters Bone which was nominated for Oscars a couple years back which I loved. This was okay but the only decent actress in the whole movie was Vera Farmiga. The rest of the cast seemed like they were drug of the streets to appear in it."
},
		{
			"rate":40,
			"text": "Real and authentic, reminds me of my home town and problems that consistently go on there (and probably still do), these kind of films are why I much rather watch indie movies. Better stories, often better acting and everything is down to the bone (no pun intended)"
},
		{
			"rate":40,
			"text": "A mom of two, Irene (Vera Farmiga) has been addicted to cocaine for years. Her husband Steve (Clint Jordan) uses, but not nearly as much. When Irene tries to use her son's birthday check from Grandma to buy drugs, she knows it's time to check into rehab. She meets Bob (Hugh Dillon), a nurse in the rehab center who's been clean for five years, and they start to have an affair. Irene manages to get clean, but it's so fragile that the tiniest thing can plunge her right back into addiction, and does. This movie really brings home how easy it is to get caught up in that rat race and how incredibly hard it is to get off-and stay off. Vera Farmiga is great, as always. I thought it was very realistic and I liked the ending. I recommend Down to the Bone and give it an 85%."
},
		{
			"rate":-1,
			"text": "sounds like a soap opera"
},
		{
			"rate":-1,
			"text": "i will watch it for a spectacular performance by Vera Farmiga"
},
		{
			"rate":20,
			"text": "Down to the Bone is realistic, which I like, but boring, which I don't. 4/10"
},
		{
			"rate":40,
			"text": "The great thing about netflix is how you can go back in time and catch movies you never seen before. I've always liked Vera Farmiga as an actress and Down to the Bone will show you why. This a great movie with great acting about a woman struggling with addiction in upstate NY. This movie does a great job at showing that staying clean is all about you and only you. That it is something you can only do for you and noone else. Great movie. A definite must see. Once again Vera shows why she is one of the best actresess in Hollywood."
},
		{
			"rate":40,
			"text": "DOWN TO THE BONE is a gritty tale that shows the effects of drug abuse and addiction on the user and the people in her life. Vera Farmiga delivers one of the besr performances of her carrer along with an outstanding directorial debut by Debra Granik (WINTER'S BONE)."
},
		{
			"rate":45,
			"text": "This AWESOME movie is about thirteenth stepping: when you get too lazy, too horny... you get distracted... you know, when you got that hole inside and you just need to fill it. Realistic and Powerfull."
},
		{
			"rate":40,
			"text": "Top notch acting that is mildly pointless - like the lives the characters lead... this movie also rejects the notion that being a crackhead is simply a result of not working"
},
		{
			"rate":45,
			"text": "I thought WINTER'S BONE was a strong film so I decided to check out Debra Granik's earlier film. Since Vera Farmiga is one of my favorite actresses it was an easy sell. DOWN TO THE BONE is just as solid. It's another drug culture film, but it's very understated and speaks about two films worth of ideas through crafty direction, smart writing and great performances. Granik seems to know as well as anybody that everything doesn't have to be spelled out or overly dramatized, that what you don't show or tell is as important as what you do. I'm impressed."
},
		{
			"rate":40,
			"text": "What this movie lacks in entertainment value, it more than makes up for with its sincerity and insight -- both qualities that are exemplified harrowingly by lead Vera Farmiga, whose performance here is of star-making caliber by anyone's measure, and should have been nominated for an Oscar."
},
		{
			"rate":45,
			"text": "i am a fan of ms. farmiga and she doesn't dissapoint in this film as she gives a stirring portrayal of an addict trying to get her life togerther.......a strong supporting cast of interesting and believable characters is a big plus also. from director debra granik, who also directed winter's bone, a possible oscar contender this year......highly recommended."
},
		{
			"rate":10,
			"text": "Junk movie! JUNK SUBJECT!"
},
		{
			"rate":45,
			"text": "Good Movie, also--The Departed, HBO Series--The Wire--ranked right after the godfather"
},
		{
			"rate":25,
			"text": "Good flick but I hated the ending"
},
		{
			"rate":35,
			"text": "Drawn out, but an extremely realistic film about drug abuse with some impressive performances, in particular Vera Farmiga, which is definitely a name I shall be taking notice of in the future, her role in this was faultless and she reminded me of Cate Blanchett in many ways."
},
		{
			"rate":35,
			"text": "Intense and incredibly raw. Love the bare bones digital camera work. Awesome acting by Farmiga and Dillon."
},
		{
			"rate":40,
			"text": "really enjoyable film"
},
		{
			"rate":25,
			"text": "Farmiga puts on a wonderfully truthful little performance here, as the young mother wrestling with her hum drum home duties as a single parent coupled with her addiction to drugs. Though it is filmed in the film truth technique, the characters begin to lose some steam mid way through and their motivations become less than clear."
},
		{
			"rate":35,
			"text": "Debra Granik come?ou a sua carreira cinematogr?fica na ?rea do document?rio. Sete anos ap?s a sua primeira obra ficcional, a curta metragem `Snake Feed`, d?-nos uma primeira longa. `Down to the Bone`, extens?o da sua primeira obra, ? um filme provindo do universo realista, um drama sobre a toxidepend?ncia que se baseou na observa??o durante alguns meses de uma fam?lia devastada pelo consumo de drogas. O olhar atento de Debra, em certas sequ?ncias a ro?ar o inspirador cinema verit?, est? aqui ao servi?o de uma mulher inteligente que inicialmente domina a droga e sem raz?o aparente esta passa a domin?-la a ela. Irene (Vera Farmiga) ? uma mulher casada com dois rapazes pequenos que trabalha como caixa num hiper-mercado. A fam?lia vive no norte de Nova Iorque levando uma vida de classe m?dia. O que come?ou por ser um constante e pequeno consumo de coca?na h? cerca de dez anos atr?s transformou-se agora num elevado grau de depend?ncia que deixa Irene profundamente alterada. Quando esta se prepara para utilizar um cheque que a av? ofereceu a uma das crian?as no anivers?rio, para comprar droga, tem um momento de esclarecimento. Decide inscrever-se numa cl?nica de reabilita??o sendo que tudo parece encaminhar-se na vida de Irene. A? conhece Bob (Hugh Dillon), um enfermeiro simp?tico que a ajuda a libertar-se da depend?ncia. O que come?a por ser uma amizade d? lugar a um romance, preenchendo assim Irene o vazio que lhe provoca a abstin?ncia t?xica. Mas quando Irene parece ter derrotado a depend?ncia, sofre um importante rev?s. A constru??o da narrativa desta obra estruturar-se de forma a compreendermos qual a natureza da depend?ncia, um contante murmurar ao ouvido, que avan?a e recua, que puxa todos os dias como se fosse o primeiro. N?o v?mos o tr?fico, nem pessoas a drogar-se de forma expl?cita nem nada do que ? habitual. Temos as pessoas, s? elas a bra?os com este problema e a forma como o encaram. Tudo ocorre de forma pl?cida, natural. E natural ? a forma como Irene, uma mulher inteligente, equilibrada, v? que a realidade paulatinamente se lhe vai escapando debaixo dos p?s. Tamb?m no final desta lenta narrativa de depend?ncia, distinguida com o grande pr?mio do J?ri da edi??o deste ano do Festibval de Sundance, vemos como os diversos acontecimentos n?o se atam em conclus?o conveniente. Apartam-se caminhos mas sem grandes defini??es, deixando lugar ? ambiguidade. Destaque para as interpreta??es de Vera Farmiga (fisicamente parecida com Cate Blanchett e Tilda Swinton) e Hugh Dillon que s?o muito verdadeiras. A cinematografia que priviligiou as rodagens em exteriores com as paisagens geladas do norte de Nova Iorque, usa o cinzento o azul transmitindo um ambiente quase glacial entre as personagens."
},
		{
			"rate":40,
			"text": "[font=Arial][color=black]Harrowing tale about a soon to be separated mother (an excellent Vera Farmiga) with a drug problem. She wants to come clean and tries, but just about everything conspires against her. She sends away her dopey--pun intended--boyfriend in the last scene, giving the ending a faint glimmer of hope.[/color][/font]"
},
		{
			"rate":-1,
			"text": "Would like to see at some stage."
},
		{
			"rate":25,
			"text": "it could happen to ANYONE, even you"
},
		{
			"rate":50,
			"text": "Unpredictable and real. One of the best drug addiction movies I've ever seen. Dare I even say better than [i]The Basketball Diaries[/i]? Sure, Leo was excellent to the extreme in that but the film wasn't as uncompromising as his portrayal."
},
		{
			"rate":50,
			"text": "Unpredictable and real. One of the best drug addiction movies I've ever seen. Dare I even say better than [i]The Basketball Diaries[/i]? Sure, Leo was excellent to the extreme in that but the film wasn't as uncompromising as his portrayal."
},
		{
			"rate":20,
			"text": "Real life situation is really grabbing, realistic, but this is not why we watch movies. Recommended for real life."
},
		{
			"rate":40,
			"text": "Good movie for anyone who has tried to quit using."
},
		{
			"rate":30,
			"text": "great performance by vera farmiga"
},
		{
			"rate":20,
			"text": "I really disliked this movie, but I thought Vera Farmiga did a good job."
},
		{
			"rate":-1,
			"text": "Departed chic? hell yeah"
},
		{
			"rate":35,
			"text": "While Vera Famiga's lead performance as the drug-abusing mother of two is surely the best of the year so far, the film itself is rather lacking in terms of depth, quality, and especially originality."
},
		{
			"rate":-1,
			"text": "I'd see this, it might turn out to be a good movie."
},
		{
			"rate":-1,
			"text": "looks like another realli good film"
},
		{
			"rate":-1,
			"text": "movies about drugs freak me out"
},
		{
			"rate":-1,
			"text": "sounds deep, i like the sound of this film"
},
		{
			"rate":20,
			"text": "erm i havent actually seen the film i pressed the rong button nd rated it :s"
},
		{
			"rate":-1,
			"text": "I haven't heard about this movie but it looks o.k"
},
		{
			"rate":-1,
			"text": "Fatal experiments?! Now we're talkin'!!"
},
		{
			"rate":-1,
			"text": "this look rly interesting.."
},
		{
			"rate":-1,
			"text": "no thankyou. looks to scary dor me"
},
		{
			"rate":-1,
			"text": "would like to see it but its kinda weird sounding"
},
		{
			"rate":-1,
			"text": "ooooo itz scary ....i tink ...lol.lol"
},
		{
			"rate":25,
			"text": "Drugs movie?? hmm sound liek not bad movie, i bet there's swores in that movie. am i right?"
},
		{
			"rate":-1,
			"text": "Never Seen it...looks cool"
},
		{
			"rate":-1,
			"text": "what the hell is dis bout"
},
		{
			"rate":-1,
			"text": "this movie wasn't bad, see it if theres nothing else to see"
},
		{
			"rate":-1,
			"text": "I don't mind, but looks too grueome"
},
		{
			"rate":-1,
			"text": "o tht looks quite good actually ... i didnt think it wud b it just looks borin lol !!!"
},
		{
			"rate":50,
			"text": "THIS MOVIE LOOKS SO INTERESTING AND THE GIRL ON THE COVER OF THE CASE OF THE MOVIE"
},
		{
			"rate":-1,
			"text": "Yeah sounds interesting"
},
		{
			"rate":-1,
			"text": "I would love to see this movie."
},
		{
			"rate":5,
			"text": "it as absolutely crap"
},
		{
			"rate":-1,
			"text": "seems like an interesting one"
},
		{
			"rate":-1,
			"text": "Another drug addict movie"
},
		{
			"rate":10,
			"text": "addictions bring people together ahah"
},
		{
			"rate":-1,
			"text": "sorry it sounds crap"
},
		{
			"rate":-1,
			"text": "After a series of nearly fatal experiences, Irene (Farmiga), an upstate New York drug addict and single mother of two boys, decides to check herself into a rehab center, where she meets and falls in love with a fellow reformed addict (Dillon). When o..."
},
		{
			"rate":-1,
			"text": "Dope, sex...13th steppin'. If I want that kind of action I will go to a Narcotics Anonomous meeting."
},
		{
			"rate":-1,
			"text": "I may change my mind."
},
		{
			"rate":-1,
			"text": "sounds freanky...wuhey!!"
},
		{
			"rate":-1,
			"text": "Down to the Bone.....WTF"
},
		{
			"rate":-1,
			"text": "Looks kinda scary and I like that"
},
		{
			"rate":35,
			"text": "didn't like it at first but now its ok"
},
		{
			"rate":-1,
			"text": "gawd! cocaine and drugs!!"
},
		{
			"rate":-1,
			"text": "nej nej nej ,, det verkar inte falla mig i smaken"
},
		{
			"rate":-1,
			"text": "havent heard alot about it but the story line definetly sparks an intrest"
},
		{
			"rate":-1,
			"text": "would watch, for the storyline sounds interesting."
},
		{
			"rate":-1,
			"text": "ewwww naaaa i dont think soo"
},
		{
			"rate":-1,
			"text": "interesting story line....maybe I'll check it out one of these days"
},
		{
			"rate":-1,
			"text": "OH it is movie is wonderful jejejeje"
},
		{
			"rate":-1,
			"text": "Sounds like a very good story..."
},
		{
			"rate":-1,
			"text": "NONOONONONONONON NO WAY GO AWAY"
},
		{
			"rate":-1,
			"text": "not sure if I want to see this one or not"
},
		{
			"rate":-1,
			"text": "sounds like a soap opera"
},
		{
			"rate":-1,
			"text": "i dont like scary movies that much!"
},
		{
			"rate":45,
			"text": "it seems like a good movie"
},
		{
			"rate":-1,
			"text": "nd again not interested"
},
		{
			"rate":-1,
			"text": "uh ok then sad but i want scariness"
},
		{
			"rate":-1,
			"text": "not my kind of movie."
},
		{
			"rate":-1,
			"text": "Hah, this sounds hilarious!"
},
		{
			"rate":-1,
			"text": "thisis the [nicse ] moveis in the world.......,,"
},
		{
			"rate":-1,
			"text": "har man h?rt det f?rut eller vad?"
},
		{
			"rate":-1,
			"text": "Sounds ok...but I aint running to see it."
},
		{
			"rate":-1,
			"text": "hmm?? mmmaaayyybbbeee"
},
		{
			"rate":-1,
			"text": "OMG i don't know any of thesse movies! i'm such a loser"
},
		{
			"rate":-1,
			"text": "Haven't seen it, want to tho!!!!"
},
		{
			"rate":-1,
			"text": "never heard of it but could be good"
},
		{
			"rate":30,
			"text": "Synopsis: After a series of nearly fatal experiences, Irene (Farmiga), an upstate New York drug addict and single mother of two boys, decides to check herself into a rehab center, where she meets and falls in love with a fellow reformed addict (Dillon). When o."
},
		{
			"rate":-1,
			"text": "Hate horror movies they scare the crap out of me like i watched The ring with my friends i kept having to leave it was so scary!!!!!"
},
		{
			"rate":-1,
			"text": "Yeah I'd watch this one."
},
		{
			"rate":-1,
			"text": "hmm synopsis sounded good till it got to the falls in love bit. could still be good though?"
},
		{
			"rate":50,
			"text": "den e as l?skig se den inte"
},
		{
			"rate":5,
			"text": "i didn't like because i hate scary movies"
},
		{
			"rate":-1,
			"text": "never heard of this movie"
},
		{
			"rate":-1,
			"text": "who are these people."
},
		{
			"rate":-1,
			"text": "Never heard of it..."
},
		{
			"rate":-1,
			"text": "it sounds like a good movie"
},
		{
			"rate":-1,
			"text": "druggys goin to rehab, wow"
},
		{
			"rate":-1,
			"text": "down to the bone?? is it down to the chicken bone?? or a dogs bone?? omg imma getting hungry"
},
		{
			"rate":-1,
			"text": "sounds interesting ill give it a shot"
},
		{
			"rate":25,
			"text": "I'm so over drug films, but this wasn't badly done on such a low budget."
},
		{
			"rate":-1,
			"text": "sounds good but dont really wanna see it"
},
		{
			"rate":35,
			"text": "saw it on a long haul flight with a baby. but it was still pretty good."
},
		{
			"rate":-1,
			"text": "hi i am dylan this film looks good but i havent seen it yet but i will sea it this film looks cool from dylan."
},
		{
			"rate":-1,
			"text": "looks interesting but its about low lives"
},
		{
			"rate":-1,
			"text": "really want to see it1!!!"
},
		{
			"rate":-1,
			"text": "wow hmm sounds interseting lol"
},
		{
			"rate":-1,
			"text": "sounds great rea,y wnat to see it"
},
		{
			"rate":-1,
			"text": "sounds good........."
},
		{
			"rate":-1,
			"text": "Doesn't this sound EXACTLY like 28 days???"
},
		{
			"rate":-1,
			"text": "sounds kool i wanna c it"
},
		{
			"rate":-1,
			"text": "not interested in Horror films."
},
		{
			"rate":-1,
			"text": "Dont really want 2 see this 1"
},
		{
			"rate":-1,
			"text": "dsomething different.check it on tv"
},
		{
			"rate":-1,
			"text": "I don't like dramas."
},
		{
			"rate":-1,
			"text": "never seen but looks good"
},
		{
			"rate":35,
			"text": "Vera Farmiga gives a very deep and sincere performance. She is excellent. The entire cast shines. The film didn't have much of a budget, but itis a remarkably powerful film. Debra Granik has a very thoughtful directorial style. Good score, simple but excellent cinematography. It certainly isn't a cheery feel good film, a very realistic story of drug addicts trying to recover."
},
		{
			"rate":40,
			"text": "An underrated film and one of the most impressive takes on addiction in ages. Vera is a beautifully devestating figure in cinema that should be brought into the mainstream. This is Dancer in the Dark for those who require flare."
},
		{
			"rate":30,
			"text": "[font=Century Gothic]In Private a Palestinian family is not only living in the battlefield of the occupied West Bank but now they must face the additional indignity of having the Israeli army occupy their house. The army gets the second floor(under no circumstances are the family to venture there) and the family must stay downstairs. After dark, they are confined to the living room. For the father, Mohammed, a learned, dignified man, being a refugee would be worse and he insists on staying.[/font][font=Century Gothic][/font] [font=Century Gothic]Private is a tense drama with realistic situations. The film boils the whole Arab-Israeli conflict down to a microcosm of a single house being occupied. Through the character of the father, other ways are suggested of fighting the Israeli army than violence. And the individual soldiers are not shown as being the villains.[/font][font=Century Gothic][/font] [font=Century Gothic][/font] [font=Georgia][color=navy]In Down to the Bone, Irene(Vera Farmiga) is a mother to two young sons and wife to a construction worker. She works as a cashier at a supermarket in upstate New York. She is also addicted to cocaine. And she has been using since high school. After one particularly embarrassing attempt to buy drugs fails, she realizes she needs help and enters rehab.[/color][/font][font=Georgia][color=#000080][/color][/font] [font=Georgia][color=#000080]Down to the Bone covers some familiar ground but does so in a totally unglamorous way. The movie does not offer any easy answers or solutions. The movie is anchored by a very strong performance by Vera Farmiga.[/color][/font]"
}	]
}