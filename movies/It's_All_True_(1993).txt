{
	"title": "It's All True (1993)",
	"synopsis":"Both a documentary and a unique exercise in film restoration, It's All True tells the complex story of Orson Welles' ill-fated attempts to make an anthology film about the life and culture of South America and concludes with a reconstruction of one of Welles' unfinished segments, edited together from rediscovered original footage. The idea for Welles' South American project was conceived by the American government as a sort of cultural exchange to improve relations with Latin America. Using interviews and period footage, the filmmakers relate how the project quickly turned sour, as both the Brazilian government and RKO studio executives objected to Welles early footage; indeed, thanks to a local witch doctor, the film could literally be said to be cursed. Although Welles persevered, RKO eventually withdrew support from the project. The failures of It's All True and The Magnificent Ambersons, which was damaged by studio cuts made while Welles was overseas, are thought by many to have irreparably damaged the director's Hollywood career. It's All True concludes with a partial reconstruction of the Four Men on a Raft segment, in which Welles tells the true story of a dramatic, thousand-mile raft journey by four Brazilian peasants. ~ Judd Blaise, Rovi",
	"genre":
	[
		{"name":"Documentary"},
		{"name":"Television"}
,
		{"name":"Special Interest"}

	],
	"rate":3.2,
	"comments":[
		{
			"rate":35,
			"text": "Interesting look at a little known Orson Welles project involving the capturing of footage of life in South America. Probably works a little better if they just released the footage and scrapped the documentary portion."
},
		{
			"rate":45,
			"text": "One of the great unfinished film projects and an excellent look into Welles' visionary style. Look for some gorgeous shots throughout and an overall feeling of 'being there' as the film takes you on its little trip."
},
		{
			"rate":50,
			"text": "It is a master piece, and a huge lesson about Brazil's culture. I am astonished with Orson's sight, and how he fell in love with Brazilians. It is impressive! Besides the fact that it is *still* all true. Nothing really changed."
},
		{
			"rate":35,
			"text": "Now this film is not going to be for everyone. However, if you are a film lover-especially of classics films of the 1940s or Orson Welles this will be something you won't want to miss. The U.S. Government had commissioned Welles, who was a hot ticket at the time because of CITIZEN KANE, to head to Brazil to make an anti-Nazi film for the U.S. Good Neighbor Policy. After months of shooting on his film, which undoubtably would have been a masterpice, was stopped and abandoned by RKO and the footage was just sort of stored away never to be seen. This devastated Welles and in the biography Welles On Welles he himself said it was probably the most devastating single thing that happened to him next to RKO butchering his film THE MAGNIFICENT AMBERSONS. This film consists of footage that he shot for that film and Welles magnificent documentary Four Men On A Boat. Now this is not for all tastes and I even found it a little boring but its fascinating seeing the young Welles and listening to him talk film and seeing the magnificent footage he shot. Directed by Richard Wilson, Myron Miesel and Bill Krohn. Released by Paramount Pictures and Rated G."
},
		{
			"rate":40,
			"text": "(DVD) (First Viewing, 7th Welles film) Billed as Orson Welles's Lost Masterpiece, this documentary presents about an hour of footage that Welles shot in South America for a film called IT'S ALL TRUE, including a rather remarkable 45 rough cut of Four Men on a Raft. The film's history may actually be of more interest than the film itself, but it was well worth a watch. I've been accepted to start reviewing for an online DVD review site, and this is the first film I review. I could have started with something much, much worse. Historical importance: [i]10/10[/i], the actual film: [i]8/10[/i]."
},
		{
			"rate":35,
			"text": "Interesting look at a little known Orson Welles project involving the capturing of footage of life in South America. Probably works a little better if they just released the footage and scrapped the documentary portion."
},
		{
			"rate":50,
			"text": "It is a master piece, and a huge lesson about Brazil's culture. I am astonished with Orson's sight, and how he fell in love with Brazilians. It is impressive! Besides the fact that it is *still* all true. Nothing really changed."
},
		{
			"rate":35,
			"text": "Now this film is not going to be for everyone. However, if you are a film lover-especially of classics films of the 1940s or Orson Welles this will be something you won't want to miss. The U.S. Government had commissioned Welles, who was a hot ticket at the time because of CITIZEN KANE, to head to Brazil to make an anti-Nazi film for the U.S. Good Neighbor Policy. After months of shooting on his film, which undoubtably would have been a masterpice, was stopped and abandoned by RKO and the footage was just sort of stored away never to be seen. This devastated Welles and in the biography Welles On Welles he himself said it was probably the most devastating single thing that happened to him next to RKO butchering his film THE MAGNIFICENT AMBERSONS. This film consists of footage that he shot for that film and Welles magnificent documentary Four Men On A Boat. Now this is not for all tastes and I even found it a little boring but its fascinating seeing the young Welles and listening to him talk film and seeing the magnificent footage he shot. Directed by Richard Wilson, Myron Miesel and Bill Krohn. Released by Paramount Pictures and Rated G."
},
		{
			"rate":45,
			"text": "One of the great unfinished film projects and an excellent look into Welles' visionary style. Look for some gorgeous shots throughout and an overall feeling of 'being there' as the film takes you on its little trip."
},
		{
			"rate":30,
			"text": "registro de um momento importante do cinema nacional que n?o foi ? frente por motiva??es meramente pol?ticas. com o que sobrou do epis?dio dos jangadeiros, d? pra entender porque orson welles foi t?o aclamado - os fragmentos t?m uma das fotografias mais fodas que j? vi."
},
		{
			"rate":40,
			"text": "All CLASSICS are GOOD"
},
		{
			"rate":40,
			"text": "(DVD) (First Viewing, 7th Welles film) Billed as Orson Welles's Lost Masterpiece, this documentary presents about an hour of footage that Welles shot in South America for a film called IT'S ALL TRUE, including a rather remarkable 45 rough cut of Four Men on a Raft. The film's history may actually be of more interest than the film itself, but it was well worth a watch. I've been accepted to start reviewing for an online DVD review site, and this is the first film I review. I could have started with something much, much worse. Historical importance: [i]10/10[/i], the actual film: [i]8/10[/i]."
}	]
}