{
	"title": "The Misadventures of Merlin Jones (1964)",
	"synopsis":"In this Disney family film, brainy college student Merlin Jones (Tommy Kirk) invents a mind-reading machine, but the consequences of its use prove to be a lot of trouble. With help from his girlfriend (Annette Funicello), he must set to right all that has gone wrong. The movie spawned the sequel Monkey's Uncle two years later. ~ John Bush, Rovi",
	"genre":
	[
		{"name":"Kids & Family"},
		{"name":"Classics"}
,
		{"name":"Science Fiction & Fantasy"}
,
		{"name":"Comedy"}

	],
	"rate":3.3,
	"comments":[
		{
			"rate":35,
			"text": "A fun Tommy Kirk, Annette Funicello pairing."
},
		{
			"rate":30,
			"text": "This movie is cute and a good family film for everyone, especially if you were a fan of Annette Funicello before she got involved with the peanut butter."
},
		{
			"rate":30,
			"text": "Made to Be Re-Aired on [i]Wonderful World of Color[/i] Actually, I have no evidence of that. According to Tommy Kirk himself, the movies made plenty of money for Disney; they did, after all, star Annette--who had a last name, but you wouldn't know it by the credits here. This, is turns out, is why they let him come back and make one even after it was discovered that he was gay. Which I hadn't known or cared about. But hey, 1964. Anyway, the movie really feels more like two episodes of a TV show strung back-to-back. This would not be without precedent; this wouldn't even be without precedent from Disney. On the other hand, Disney also did a lot of things which weren't just adapted TV shows but which could be broken down into episodes. I mean, let's be honest with ourselves. How many times did Night on Bald Mountain appear in Halloween specials over the years Disney was making Halloween clip shows? Kirk is the eponymous Merlin Jones. He's a sort of generic, all-around geek. When we first meet him, he is established as still having the hottest girlfriend on campus, Jennifer (Annette). He is supposed to pick her up and take her into town, but he gets caught up in his experiment studying brainwaves. Various things happen, and what with one thing and another, Merlin gets zapped so that he can read minds. He overhears the thoughts of Judge Holmsby (Leon Ames), who has just suspended Merlin's driver's license. Merlin thinks he hears the judge plotting out a crime, and he tries to get the police involved in it. Which, of course, involves getting them to believe he can read minds. In a later, vaguely related story, Merlin studies hypnosis. An experiment with Stanley the Chimp goes awry when Stanley goes after his keeper, Norman (Norman Grabowski). Hilarity then ensues over an attempt to find out if an honest man can be hypnotized into committing a crime--with Merlin getting fingered as the culprit. This is on the list of movies I saw long, long ago, when the Disney Channel played things out of the Disney Vault. It was on fairly regular play at the time, in fact. By the age of twelve, I think I had seen more Tommy Kirk, Dean Jones, and Young Kurt Russell movies than I had seen movies made in my own lifetime. I'd seen both [i]Dumbo[/i] and [i]Alice in Wonderland[/i] more times than I could count. And I have to tell you, I'm not sure that was a bad thing. For one, I can trace my knowledge of an important zoological fact to my exposure to Merlin Jones. Merlin is adamant--and correct--in his declarations that Stanley is not a monkey. Stanley is a chimpanzee, and chimpanzees are not monkeys. For starters, they don't have tails. It's interesting to note that only the villain of the second act ever calls him a monkey after getting that correction. Not subtle, perhaps, but the kind of detail which might be said to train a young film-watcher. You can always tell. I'm not sure I'd say there's chemistry between Tommy and Annette, though apparently, they did go out a few times so that he could pretend he wasn't gay. (This, for the record, makes [i]Catalina Caper[/i] even creepier.) However, they do manage to project genuine affection for one another. I'm not sure I ever imagined Merlin and Jennifer as getting married after college, but then, I'm pretty sure I never imagined them leaving good ol' Medfield College. (Which is one of the IMDB keywords for this movie.) They would stay there forever, along with all the other Disney characters attending the school--even the other one played by Tommy Kirk. I mean, for starters, I could never quite work out what kind of job Merlin was suited for. I think he's intended to be working toward something in psychology, but he's enough of a tinkerer to put together the ludicrous hat which appears on the DVD cover. I think we're supposed to think of him as a hard scientist, but psychology is not exactly a hard science. I'm not going to lie to you. Tommy Kirk is not a good actor, and this is not a good movie. On the other hand, he isn't a bad actor, and this isn't a bad movie. Unlike every other movie of his I've seen made after this one. In that I've seen two, and both had puppets in the corner. I've always been enchanted by the title [i]Dr. Goldfoot and the Bikini Machine[/i], but never actually enchanted enough to seek the movie out and watch it. It does not sound like it would be a quality film. He says he was considered for [i]The Searchers[/i], which by all accounts is not merely good but great, but he was at a party that got busted for pot, and there went that chance. Certainly I would say that Tommy Kirk was not a worse actor than many others of his generation who did much better. It really isn't fair. It also makes the days he was in Hollywood seem so innocent, that being at a party that got raided over marijuana could ruin your career. Or, indeed, that a party was raided over marijuana."
},
		{
			"rate":50,
			"text": ":fresh: [CENTER]A very useful 1960's comedy.[/CENTER]"
},
		{
			"rate":30,
			"text": "Made to Be Re-Aired on [i]Wonderful World of Color[/i] Actually, I have no evidence of that. According to Tommy Kirk himself, the movies made plenty of money for Disney; they did, after all, star Annette--who had a last name, but you wouldn't know it by the credits here. This, is turns out, is why they let him come back and make one even after it was discovered that he was gay. Which I hadn't known or cared about. But hey, 1964. Anyway, the movie really feels more like two episodes of a TV show strung back-to-back. This would not be without precedent; this wouldn't even be without precedent from Disney. On the other hand, Disney also did a lot of things which weren't just adapted TV shows but which could be broken down into episodes. I mean, let's be honest with ourselves. How many times did Night on Bald Mountain appear in Halloween specials over the years Disney was making Halloween clip shows? Kirk is the eponymous Merlin Jones. He's a sort of generic, all-around geek. When we first meet him, he is established as still having the hottest girlfriend on campus, Jennifer (Annette). He is supposed to pick her up and take her into town, but he gets caught up in his experiment studying brainwaves. Various things happen, and what with one thing and another, Merlin gets zapped so that he can read minds. He overhears the thoughts of Judge Holmsby (Leon Ames), who has just suspended Merlin's driver's license. Merlin thinks he hears the judge plotting out a crime, and he tries to get the police involved in it. Which, of course, involves getting them to believe he can read minds. In a later, vaguely related story, Merlin studies hypnosis. An experiment with Stanley the Chimp goes awry when Stanley goes after his keeper, Norman (Norman Grabowski). Hilarity then ensues over an attempt to find out if an honest man can be hypnotized into committing a crime--with Merlin getting fingered as the culprit. This is on the list of movies I saw long, long ago, when the Disney Channel played things out of the Disney Vault. It was on fairly regular play at the time, in fact. By the age of twelve, I think I had seen more Tommy Kirk, Dean Jones, and Young Kurt Russell movies than I had seen movies made in my own lifetime. I'd seen both [i]Dumbo[/i] and [i]Alice in Wonderland[/i] more times than I could count. And I have to tell you, I'm not sure that was a bad thing. For one, I can trace my knowledge of an important zoological fact to my exposure to Merlin Jones. Merlin is adamant--and correct--in his declarations that Stanley is not a monkey. Stanley is a chimpanzee, and chimpanzees are not monkeys. For starters, they don't have tails. It's interesting to note that only the villain of the second act ever calls him a monkey after getting that correction. Not subtle, perhaps, but the kind of detail which might be said to train a young film-watcher. You can always tell. I'm not sure I'd say there's chemistry between Tommy and Annette, though apparently, they did go out a few times so that he could pretend he wasn't gay. (This, for the record, makes [i]Catalina Caper[/i] even creepier.) However, they do manage to project genuine affection for one another. I'm not sure I ever imagined Merlin and Jennifer as getting married after college, but then, I'm pretty sure I never imagined them leaving good ol' Medfield College. (Which is one of the IMDB keywords for this movie.) They would stay there forever, along with all the other Disney characters attending the school--even the other one played by Tommy Kirk. I mean, for starters, I could never quite work out what kind of job Merlin was suited for. I think he's intended to be working toward something in psychology, but he's enough of a tinkerer to put together the ludicrous hat which appears on the DVD cover. I think we're supposed to think of him as a hard scientist, but psychology is not exactly a hard science. I'm not going to lie to you. Tommy Kirk is not a good actor, and this is not a good movie. On the other hand, he isn't a bad actor, and this isn't a bad movie. Unlike every other movie of his I've seen made after this one. In that I've seen two, and both had puppets in the corner. I've always been enchanted by the title [i]Dr. Goldfoot and the Bikini Machine[/i], but never actually enchanted enough to seek the movie out and watch it. It does not sound like it would be a quality film. He says he was considered for [i]The Searchers[/i], which by all accounts is not merely good but great, but he was at a party that got busted for pot, and there went that chance. Certainly I would say that Tommy Kirk was not a worse actor than many others of his generation who did much better. It really isn't fair. It also makes the days he was in Hollywood seem so innocent, that being at a party that got raided over marijuana could ruin your career. Or, indeed, that a party was raided over marijuana."
},
		{
			"rate":50,
			"text": ":fresh: [CENTER]A very useful 1960's comedy.[/CENTER]"
},
		{
			"rate":35,
			"text": "A fun Tommy Kirk, Annette Funicello pairing."
},
		{
			"rate":30,
			"text": "This movie is cute and a good family film for everyone, especially if you were a fan of Annette Funicello before she got involved with the peanut butter."
},
		{
			"rate":40,
			"text": "All CLASSICS are GOOD"
},
		{
			"rate":35,
			"text": "great for kids, good clean fun"
},
		{
			"rate":35,
			"text": "great movie with Tommy Kirk & Annette Funicello"
},
		{
			"rate":-1,
			"text": "Another good Disney film from childhood.."
},
		{
			"rate":40,
			"text": "My friends make fun of me b/c I still love this movie and own it!!!!"
}	]
}