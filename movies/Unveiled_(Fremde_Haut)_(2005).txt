{
	"title": "Unveiled (Fremde Haut) (2005)",
	"synopsis":"A woman assumes a new identity in order to start her life over again in this drama from Germany. Fariba Tarizi (Jasmin Tabatabai) is an Iranian woman who wants to leave her country to escape the persecution that comes with being a lesbian. Fariba manages to make her way to Germany before it's discovered that her passport and visa are forgeries; she applies for political asylum, but is told she doesn't have much of a chance of being allowed to stay. While held in custody by immigration officials, Fariba meets Siamak (Navid Akhavan), a young man who has also fled Iran and is terrified by the prospect of having to go back. Siamak panics and kills himself, and when Fariba discovers his body, she takes his belongings, dresses in his clothes, cuts her hair and escapes custody posing as a man. Making her way into Stuttgart, Fariba gets a job at a canning plant, where she becomes friendly with Anna (Anneke Kim Sarnau), a single mother who has no idea Fariba is really a woman. Fariba becomes infatuated with Anna, and Anna makes it clear she feels the same way, but Fariba isn't sure how to tell her that she's really a woman, a situation made all the more difficult when she learns Siamak must return to Iran in two weeks. ~ Mark Deming, Rovi",
	"genre":
	[
		{"name":"Art House & International"},
		{"name":"Drama"}

	],
	"rate":3.3,
	"comments":[
		{
			"rate":40,
			"text": "Well done story of alienation and having to hide one's true nature in order to survive day to day. Jasmin Tabatabai plays a woman fleeing persecution and possible death in her native Iran for loving the wrong person. She adopts the persona of a troubled young man who dies at the detention center where they are being held and tries to make her way in rural Germany. She faces prejudice and hatred even before anyone figures out who she really is. Heartrendingly sad, but a good mirror of society both in Germany and in our own US of A. Highlights the need we all have for affection and approval."
},
		{
			"rate":35,
			"text": "Unveiled is a powerful story of survival, prejudice and love that reminds me also of the movie Boys Don't Cry because of the situation of the lead character. The story is totally realistic and credible, exposing the intolerance of Muslin nations, in this case Iran, with lesbians. The direction of an unknown ( for me) Angelina Maccarone is sensitive, never letting the dramatic situation of Fariba being corny. The performances of Jasmin Tabatabai and Anneke Kim Sarnau are outstanding, and they show great chemistry in a beautiful and sad love story."
},
		{
			"rate":40,
			"text": "Slow-paced, subtely narrated and very touching film."
},
		{
			"rate":25,
			"text": "Very disappointing film. Not because it was bad, but because it had the potential to be very good and wasn't."
},
		{
			"rate":35,
			"text": "Very good film... Americans don't realize how good they have it here... what someone will sacrifice in other parts of the world, for our natural rights in the U.S.A."
},
		{
			"rate":30,
			"text": "unveiled ended before it peaked, or perhaps thats just me wishing for a fairytale ending for 'em. absorbing watch though, one of the more decent ones around."
},
		{
			"rate":50,
			"text": "A woman refugee pretending to be a man."
},
		{
			"rate":40,
			"text": "alman erkeklerine karsi duyduum tiksintiyi istemese de arttirdi bu film, onun disinda basroldeki JT'nin oyunculugunu ?ok begendim, ?ok tricky ve zekice yazilmis bir senaryo olma ihtiyaci hissetmeden kadin elinden ?iktigi belli, duygusal bir filmdi."
},
		{
			"rate":-1,
			"text": "Need to watch again, great movie"
},
		{
			"rate":45,
			"text": "(Fremde Haut) Poderoso, extraordinario, serio, tenso, cheio de suspense. Uma heroina que nao esqeceremos facilmente. Um dos filmes TOP 10 de 2005."
},
		{
			"rate":25,
			"text": "Very disappointing film. Not because it was bad, but because it had the potential to be very good and wasn't."
},
		{
			"rate":35,
			"text": "Very nice movie, I wish it gave us more at the end though..."
},
		{
			"rate":35,
			"text": "Thought provoking stuff, with twists aplenty."
},
		{
			"rate":10,
			"text": "didnt like this movie at all"
},
		{
			"rate":45,
			"text": "I really like the title; is approperiate for this movie in many ways, in terms of the character Fariba physically uncovering her chokar to assume a man's identity and then her coming out (pardon the puns) to Anne. Minus half a star for the open ending, which i hate in general. Open endings in some places are mysterious and compels you to wonder, 'So what happens next?' But then you realise you don't really get to know, and will need to imagine, whatever you'd like. The script didn't really provide much tender moments but the plot was original and slowly reeled me in as I watched. But few things weren't made clear: A, is Anne gay, at all? B, did she know that 'Siamak' is really Fariba, a woman? she didn't seem shocked or anything..."
},
		{
			"rate":-1,
			"text": "where are all the good movies at? this doesn't look good!"
}	]
}