{
	"title": "Nearly No Christmas (1981)",
	"synopsis":"In this heartwarming holiday tale, a family attempts to stay together in the face of holiday turmoil. ~ Sandra Brennan, Rovi",
	"genre":
	[
		{"name":"Kids & Family"}
	],
	"rate":0,
	"comments":[
	]
}