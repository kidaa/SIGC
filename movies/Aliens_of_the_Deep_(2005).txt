{
	"title": "Aliens of the Deep (2005)",
	"synopsis":"Few who witnessed the awesome spectacle of the Titanic emerging from the darkness of the ocean depths in James Cameron's Ghosts of the Abyss are likely to forget that spectacular sight, and now the groundbreaking filmmaker journeys even deeper into unexplored territory with this look at some of nature's most elusive creations in Aliens of the Deep. Using the basic concepts of astrobiology as his foundation, Cameron enlists the aid of talented marine biologists and NASA researchers to reveal the fascinating and seldom-seen life forms of the sea and use their biological makeup to create a speculative blueprint for what life on other planets might look like. In the super-heated, mineral-charged water of hydrothermal vents, creatures such as a six-foot-tall worm with crimson plumes dance alongside blind white crabs and some of the strangest creatures you're ever likely to see. If Ghosts of the Abyss brought viewers as close as they're ever likely to get to standing on the ocean floor and beholding the majesty of one of history's most notorious tragedies, Aliens of the Deep brings them as close as they may ever come to standing face-to-face with an alien life form. ~ Jason Buchanan, Rovi",
	"genre":
	[
		{"name":"Documentary"},
		{"name":"Special Interest"}

	],
	"rate":3.2,
	"comments":[
		{
			"rate":35,
			"text": "Pretty cool and a little hokey at times."
},
		{
			"rate":40,
			"text": "If there's two things that James Cameron can't get enough of, it's aliens and the sea. In this documentary, he combines both of his passions and turns it into a fascinating exploration of the strange creatures that inhabit the deep depths of the world's oceans. Not only that, but like the great visionary that he is, he alsogoes beyond those found on Earth and looks at how the ocean life might look like on one of Jupiter's many moons, namely Europa. It'd be cool to say the least if they went there for real someday, because after seeing this production, it seems the prospect of finding alien life there, is far from mere science fiction."
},
		{
			"rate":25,
			"text": "Im sort of confused after this doc. It didn't amaze me like other ones, but it isn't horrible, because it did present me to different creatures i've never seen before. It just doesn't compare to any of Disneynature's films. Keep in mind that I saw the 48 minute version not the 96 minute one. Overall Rating:48"
},
		{
			"rate":40,
			"text": "Beautiful and ghostly. Theses deep see mysterys are fantastic."
},
		{
			"rate":35,
			"text": "Interesting movie, makes you feel privledged being able to see these odd (to us) creatures."
},
		{
			"rate":35,
			"text": "they shoe you some things you cant even comprehend. Darwin cant explain more than half of what you see. He's a tool"
},
		{
			"rate":30,
			"text": "really cool and absolutely beautifully filmed, but the science was lacking. I would have liked to see an explanation of some kind of investigation into the animals more, rather then just showing me what they look like."
},
		{
			"rate":30,
			"text": "Fascinating brief introduction to astrobiology. Not much scientific rigor or explanation (and the last scene of alien underwater cities on Europa is absurd!), but it's a fun documentary."
},
		{
			"rate":25,
			"text": "This is one of the most boring things I've ever seen-most of it is just James Cameron talking about what he's doing and repeated shots of the same animals"
},
		{
			"rate":5,
			"text": "I'd give it no stars if I could. James Cameron is a dolt. He promised 6-foot 3-D worms and all he showed were shrimp. The only thing more ridiculous than his obsession with being the first is his ego."
},
		{
			"rate":40,
			"text": "beautiful documentary about new discoveries in the deep sea. the subs with the 320 degree viewing area look amazing. i wish they would've focused a bit more on marine life than the human side of the explorations though. amazing underwater footage, comparable to Attenborough."
},
		{
			"rate":5,
			"text": "I wish they would have gone more in depth with the few odd creatures they did show. Not too bad at first, but the end made me hate the whole thing."
},
		{
			"rate":30,
			"text": "A little long with the set-up stuff, but once we get into the water it's really neat. Fish with little lights built in...damn cool."
},
		{
			"rate":40,
			"text": "I haven't seen any of Cameron's other films of this ilk, so I don't know how this one compares. As a stand-alone, however, it held its own. Beautiful images. Fascinating information. A keeper for the adults and the kids in our family."
},
		{
			"rate":15,
			"text": "OMG, this movie is SO BORING. Even though it's less than an hour long, it just drags and drags. Nothing spectacular even happens until the very end. And then it's like, Finally!"
},
		{
			"rate":25,
			"text": "Some amazing views of life in the deep ocean which demeans itself by being a self-proclaimed prelude to more exciting space exploration. Too many wishy-washy semi-philosophical speeches and not enough science."
},
		{
			"rate":40,
			"text": "Aliens of the Deep is a great mix of science fiction and real world exploration and if either subject fascinates you (or even if you're mildly curious) this movie will really excite you. A lot of really interesting subjects are explored: the preparation as the deep sea dive is going to take place and the stories on how the equipment was designed and put together, the teamwork and ingenuity that overcomes problems that rise up out of nowhere, the similarities between two fields of scientists that come from (seemingly) to entirely different fields and how Earth's deep oceans and the vast reaches of spaces actually have a lot in common. There is some beautiful and bizarre sea life included in the film, but the focus is really the exploration, the gathering of data and the speculation on what could be out there. Although the film would have been better with more discoveries of strange sea life, the ones we do see are interesting and bizarre bottom-dwellers have been shown in many good documentaries and TV specials before so it's great to see something different. Some of the special effects are a bit dated but they're never really glaring and they more than do the job. It's a very smart movie that is bound to teach you more than a few things you didn't know about space and the deep sea life here on earth. It's really exciting to see passionate people realizing their dreams and going where no one else has gone before while telling us what we could be in store in the (hopefully) near future. It's a documentary that has a lot of smart ideas, a lot of creativity too and it does a lot more than present the facts to you; it encourages you to think about the subject and come to your own conclusions. (Extended cut on Dvd, January 1, 2013)"
},
		{
			"rate":35,
			"text": "I really enjoy deep sea exploration and learning about it. I wish I could have seen this in Imax."
},
		{
			"rate":25,
			"text": "Im sort of confused after this doc. It didn't amaze me like other ones, but it isn't horrible, because it did present me to different creatures i've never seen before. It just doesn't compare to any of Disneynature's films. Keep in mind that I saw the 48 minute version not the 96 minute one. Overall Rating:48"
},
		{
			"rate":35,
			"text": "This is a review of the extended version. PLOT/SUBJECT:James Cameron organizes a trip down in the Mediterranean Sea in special-made submersibles with atsronomers to explore the space below us as the astronomers plan to explore the space above them. It's pretty much a comparison from under space to outer space. I love marine biology so this was my kind, but its cross with astronomy kinda made it jump around too much. ACTING:Or people. Pretty cool people, but some got on my nerves. OTHER CONTENT:Don't think there was any score. But anyway, the creatures, depths, and living conditions were fantastic. But if you're going to watch the extended version all the way through, then drink a cup of coffee! It's very easy to fall asleep to. That's why it's rated lower: the jumps, nervy people, and the easiness to fall asleep. OVERALL,a good documentary with a great subject that can be kind of jumpy, some good and some nervy people, and the ability to make you fall asleep."
},
		{
			"rate":40,
			"text": "I haven't seen any of Cameron's other films of this ilk, so I don't know how this one compares. As a stand-alone, however, it held its own. Beautiful images. Fascinating information. A keeper for the adults and the kids in our family."
},
		{
			"rate":15,
			"text": "OMG, this movie is SO BORING. Even though it's less than an hour long, it just drags and drags. Nothing spectacular even happens until the very end. And then it's like, Finally!"
},
		{
			"rate":25,
			"text": "Some amazing views of life in the deep ocean which demeans itself by being a self-proclaimed prelude to more exciting space exploration. Too many wishy-washy semi-philosophical speeches and not enough science."
},
		{
			"rate":40,
			"text": "this is cool under water thing.."
},
		{
			"rate":40,
			"text": "Aliens of the Deep is a great mix of science fiction and real world exploration and if either subject fascinates you (or even if you're mildly curious) this movie will really excite you. A lot of really interesting subjects are explored: the preparation as the deep sea dive is going to take place and the stories on how the equipment was designed and put together, the teamwork and ingenuity that overcomes problems that rise up out of nowhere, the similarities between two fields of scientists that come from (seemingly) to entirely different fields and how Earth's deep oceans and the vast reaches of spaces actually have a lot in common. There is some beautiful and bizarre sea life included in the film, but the focus is really the exploration, the gathering of data and the speculation on what could be out there. Although the film would have been better with more discoveries of strange sea life, the ones we do see are interesting and bizarre bottom-dwellers have been shown in many good documentaries and TV specials before so it's great to see something different. Some of the special effects are a bit dated but they're never really glaring and they more than do the job. It's a very smart movie that is bound to teach you more than a few things you didn't know about space and the deep sea life here on earth. It's really exciting to see passionate people realizing their dreams and going where no one else has gone before while telling us what we could be in store in the (hopefully) near future. It's a documentary that has a lot of smart ideas, a lot of creativity too and it does a lot more than present the facts to you; it encourages you to think about the subject and come to your own conclusions. (Extended cut on Dvd, January 1, 2013)"
},
		{
			"rate":35,
			"text": "This is a review of the extended version. PLOT/SUBJECT:James Cameron organizes a trip down in the Mediterranean Sea in special-made submersibles with atsronomers to explore the space below us as the astronomers plan to explore the space above them. It's pretty much a comparison from under space to outer space. I love marine biology so this was my kind, but its cross with astronomy kinda made it jump around too much. ACTING:Or people. Pretty cool people, but some got on my nerves. OTHER CONTENT:Don't think there was any score. But anyway, the creatures, depths, and living conditions were fantastic. But if you're going to watch the extended version all the way through, then drink a cup of coffee! It's very easy to fall asleep to. That's why it's rated lower: the jumps, nervy people, and the easiness to fall asleep. OVERALL,a good documentary with a great subject that can be kind of jumpy, some good and some nervy people, and the ability to make you fall asleep."
},
		{
			"rate":20,
			"text": "James Cameron takes audiences to the depths of the ocean to encounter some of the strangest life forms on Earth, while inviting us to imagine what future explorers may someday find on other planets.Beautifully shot and there are moments of stunning cinematography, both the jelly fish scene and the octopus scenes are stunning.Still there are issues here, James Cameron can not resist a theatrical approach to the documentary, the build up to the dive is similar to the opening scenes of the film ?Titanic? and some of the dialogue is obviously staged and scripted, this is not Cameron?s fault he is a film maker at the end of the day, but this approach takes the edge of the realism of the film, and one cannot help but think there is a slight ego in Cameron's approach and reasons for making this documentary. The narrative of the documentary also goes off track, computer generated animation of space travel and exploration is interesting but nothing to do with what has come before, in investigating the earth?s seas. There is just not enough of the earth?s animals and too much on off track storylines and the Earths crust.A stunning visual feast, and the scenes of the Earths wild life is amazing but an attempt to expand the narrative of the documentary and the theatrical approval leaves the film in limbo somewhat."
},
		{
			"rate":35,
			"text": "I really enjoy deep sea exploration and learning about it. I wish I could have seen this in Imax."
},
		{
			"rate":35,
			"text": "Pretty cool and a little hokey at times."
},
		{
			"rate":25,
			"text": "Im sort of confused after this doc. It didn't amaze me like other ones, but it isn't horrible, because it did present me to different creatures i've never seen before. It just doesn't compare to any of Disneynature's films. Keep in mind that I saw the 48 minute version not the 96 minute one. Overall Rating:48"
},
		{
			"rate":35,
			"text": "This is a review of the extended version. PLOT/SUBJECT:James Cameron organizes a trip down in the Mediterranean Sea in special-made submersibles with atsronomers to explore the space below us as the astronomers plan to explore the space above them. It's pretty much a comparison from under space to outer space. I love marine biology so this was my kind, but its cross with astronomy kinda made it jump around too much. ACTING:Or people. Pretty cool people, but some got on my nerves. OTHER CONTENT:Don't think there was any score. But anyway, the creatures, depths, and living conditions were fantastic. But if you're going to watch the extended version all the way through, then drink a cup of coffee! It's very easy to fall asleep to. That's why it's rated lower: the jumps, nervy people, and the easiness to fall asleep. OVERALL,a good documentary with a great subject that can be kind of jumpy, some good and some nervy people, and the ability to make you fall asleep."
},
		{
			"rate":35,
			"text": "This is a review of the extended version. PLOT/SUBJECT:James Cameron organizes a trip down in the Mediterranean Sea in special-made submersibles with atsronomers to explore the space below us as the astronomers plan to explore the space above them. It's pretty much a comparison from under space to outer space. I love marine biology so this was my kind, but its cross with astronomy kinda made it jump around too much. ACTING:Or people. Pretty cool people, but some got on my nerves. OTHER CONTENT:Don't think there was any score. But anyway, the creatures, depths, and living conditions were fantastic. But if you're going to watch the extended version all the way through, then drink a cup of coffee! It's very easy to fall asleep to. That's why it's rated lower: the jumps, nervy people, and the easiness to fall asleep. OVERALL,a good documentary with a great subject that can be kind of jumpy, some good and some nervy people, and the ability to make you fall asleep."
},
		{
			"rate":20,
			"text": "Well its OK?.but at times very baggy?. and speaks down to the audience?..this was the hour and a half version I saw while the original IMAX movie was probably only 40 mins...so I don?t know?I like bits of it though."
},
		{
			"rate":20,
			"text": "James Cameron takes audiences to the depths of the ocean to encounter some of the strangest life forms on Earth, while inviting us to imagine what future explorers may someday find on other planets.Beautifully shot and there are moments of stunning cinematography, both the jelly fish scene and the octopus scenes are stunning.Still there are issues here, James Cameron can not resist a theatrical approach to the documentary, the build up to the dive is similar to the opening scenes of the film ?Titanic? and some of the dialogue is obviously staged and scripted, this is not Cameron?s fault he is a film maker at the end of the day, but this approach takes the edge of the realism of the film, and one cannot help but think there is a slight ego in Cameron's approach and reasons for making this documentary. The narrative of the documentary also goes off track, computer generated animation of space travel and exploration is interesting but nothing to do with what has come before, in investigating the earth?s seas. There is just not enough of the earth?s animals and too much on off track storylines and the Earths crust.A stunning visual feast, and the scenes of the Earths wild life is amazing but an attempt to expand the narrative of the documentary and the theatrical approval leaves the film in limbo somewhat."
},
		{
			"rate":25,
			"text": "5.8/10 -- Fairly interesting, especially when deep sea exploration lines up with outer space life hypothesis, however there's far too much crew and scientist biography while coverage of weird sea life gets a bit skimmed over. In the end, I suppose it's a matter of what you want from your under water documentary."
},
		{
			"rate":35,
			"text": "they shoe you some things you cant even comprehend. Darwin cant explain more than half of what you see. He's a tool"
},
		{
			"rate":40,
			"text": "james camerongoes bk under water after his doc, ghosts of the abyss, here discovering what lies under the ocean,and asks the question,could this help in searching for life on other planets, conditions aparently being similer, a interesting watch,with lots of technical aspects, and comments on nature, as usuall cameron, is in control, and he loves this life"
},
		{
			"rate":40,
			"text": "Beautiful and ghostly. Theses deep see mysterys are fantastic."
},
		{
			"rate":-1,
			"text": "No thankyou - Not interested"
},
		{
			"rate":40,
			"text": "If there's two things that James Cameron can't get enough of, it's aliens and the sea. In this documentary, he combines both of his passions and turns it into a fascinating exploration of the strange creatures that inhabit the deep depths of the world's oceans. Not only that, but like the great visionary that he is, he alsogoes beyond those found on Earth and looks at how the ocean life might look like on one of Jupiter's many moons, namely Europa. It'd be cool to say the least if they went there for real someday, because after seeing this production, it seems the prospect of finding alien life there, is far from mere science fiction."
},
		{
			"rate":30,
			"text": "really cool and absolutely beautifully filmed, but the science was lacking. I would have liked to see an explanation of some kind of investigation into the animals more, rather then just showing me what they look like."
},
		{
			"rate":30,
			"text": "amazing footage good stuff"
},
		{
			"rate":-1,
			"text": "Not interested in seeing this."
},
		{
			"rate":30,
			"text": "Fascinating brief introduction to astrobiology. Not much scientific rigor or explanation (and the last scene of alien underwater cities on Europa is absurd!), but it's a fun documentary."
},
		{
			"rate":-1,
			"text": "I'd rather see The Blue Planet another time. This look's thin."
},
		{
			"rate":30,
			"text": "Beautiful and fascinating imagery is marred by irritating chatter and narrative. Otherwise it's good. It is thought provoking."
},
		{
			"rate":-1,
			"text": "Isn't this shown at Universal Studios Florida?"
},
		{
			"rate":5,
			"text": "OMG..what a flaming pile of dogshit. I was SERIOUSLY disappointed in this film. The title is extremely misleading. I think I only saw a dozen fish in the entire movie. 90% of the film was talking about what went on with the pods and ships rather than the actual sea life. The geography and topography was the only interesting point of this film. Cameron needs to get over himself. Watch Planet Earth, or PBS or something other than this movie. What a wanker..."
},
		{
			"rate":20,
			"text": "I once again spoke too soon about Disney; they really need to make better movies."
},
		{
			"rate":25,
			"text": "This is one of the most boring things I've ever seen-most of it is just James Cameron talking about what he's doing and repeated shots of the same animals"
},
		{
			"rate":50,
			"text": "Beautiful look at undersea exploration, which is still in its infancy."
},
		{
			"rate":45,
			"text": "yea.cant wait to watch."
},
		{
			"rate":40,
			"text": "this is cool under water thing.."
},
		{
			"rate":35,
			"text": "Interesting movie, makes you feel privledged being able to see these odd (to us) creatures."
},
		{
			"rate":25,
			"text": "saw this on IMAX. It was fun to watch but kinda dumb at the end."
},
		{
			"rate":40,
			"text": "i een hadly watch this"
},
		{
			"rate":5,
			"text": "I'd give it no stars if I could. James Cameron is a dolt. He promised 6-foot 3-D worms and all he showed were shrimp. The only thing more ridiculous than his obsession with being the first is his ego."
},
		{
			"rate":40,
			"text": "beautiful documentary about new discoveries in the deep sea. the subs with the 320 degree viewing area look amazing. i wish they would've focused a bit more on marine life than the human side of the explorations though. amazing underwater footage, comparable to Attenborough."
},
		{
			"rate":40,
			"text": "Interesting documentary by James Cameron. Deep Ocean Exploration"
},
		{
			"rate":45,
			"text": "An excellent documentary of underwater life."
},
		{
			"rate":5,
			"text": "I wish they would have gone more in depth with the few odd creatures they did show. Not too bad at first, but the end made me hate the whole thing."
},
		{
			"rate":-1,
			"text": "hmmmmmm!!!!!!! cant belive i havent seen it yet,lol"
},
		{
			"rate":10,
			"text": "Utterly disappointing, silly and boring."
},
		{
			"rate":-1,
			"text": "i think it is intresting"
},
		{
			"rate":-1,
			"text": "Yeah, i'd like to see this, i think!"
},
		{
			"rate":35,
			"text": "Visualmente increible."
},
		{
			"rate":50,
			"text": "Aliens of the deep, is one of the best aliens films ever watched. it was such gr8 effect of better lived aliens and that is different as its miles under the water. Watch its great film..."
},
		{
			"rate":-1,
			"text": "Just the title makes me want to see it...of course I love sci-fi!"
},
		{
			"rate":15,
			"text": "A below average documentary that eschews a few minutes footage of extraordinary undersea creatures without giving its viewers any information on them. The scientists involved in this exploration are prone to oohs and aahs better left to the audience. In the end, Aliens of the Deep makes a pretty scrensaver, but dismal educational material."
},
		{
			"rate":40,
			"text": "Was amazing on the IMAX... great film"
},
		{
			"rate":35,
			"text": "was very good, but would liked to have seen more that i have never seen before..."
},
		{
			"rate":20,
			"text": "I thought I was going to be blown away by misfit animals and unheard of colors. Not quite. there were moments of intrigue, but this could have been shortened to a 1/2-1 hour special on the Discovery Channel."
},
		{
			"rate":50,
			"text": "I couldn't believe how good of an experience watching this on an IMAX theatre was. It was so huge and amazing to watch in 3D. James Cameron once again raises interesting questions and proves that his theories could not be too far from the truth. It was so intriguing to watch him actually go down thousands of feet in those submersibles. I loved how well the cinematography was. The ending, some people thought was weird but it really makes you think. Sometimes it makes me wonder if there is another kind of life down under water. Well, Aliens of the Deep was an extremely entertaining film that is on my list of the best Sci-Fi movies that I have seen."
},
		{
			"rate":-1,
			"text": "wow, that's quite a thought to say there are aliens who live in water, or something or other."
},
		{
			"rate":35,
			"text": "Very beautiful and interesting but nothing too spectacular."
},
		{
			"rate":30,
			"text": "A little long with the set-up stuff, but once we get into the water it's really neat. Fish with little lights built in...damn cool."
},
		{
			"rate":15,
			"text": "Not enough story. I got bored."
},
		{
			"rate":15,
			"text": "If anyone has seen the previous Disney IMAX documentary Ghosts of the Abyss you will know what a treat that was. Well don't expect a follow up of the same quality. Aliens of the Deep covers the same ground as the first and offers nothing new. In all honesty it wouldn't be a shock if Disney announced that this film was comprised entirely of material shot for Ghosts of the Abyss and went unused until someone thought they could pass it off as a seperate entity."
},
		{
			"rate":25,
			"text": "I actually paid ten dollars to see Aliens Of The Deep 3D at the IMAX theater. This is an okay but not very good movie. Don't go. First of all, this movie is NOT what it is being billed as. It is not an amazing adventure to visit the strange creatures that live in the depths of the oceans where light cannot penetrate - all those weird, glow-y, dinasaur-looking things. This is a fifty minute documentary about the search for life at ultimate depths - 2 miles down. The guy who did Terminator 2 spends a great deal of time talking about how we explore that deep (we use new submarine things and a little robot on a tether), who should explore (biologists, geologists and space scientists), and why we should explore (because one day we might send people to the oceans of Io, a Galilean moon of Jupiter, to meet ocean aliens who live in an underwater city which bears marked similarities to Vancouver). And what do we find when we finally get through the meetings, the discussions, the interviews and the animation to dive down there? What wonders do we encounter? Shrimp. Lots and lots of shrimp. The most shrimp of anything ever. The 3D aspect is also kind of silly. Although, now I know exactly what it feels like to stand in a conference room with James Cameron. He's shorter than I imagined. This isn't a very good documentary despite some interesting visuals. And I like documentaries. I even like Io. Heck, I like all the Galilean moons. But skip this unl;ess you really, really have nothing to do with your 8 year old."
}	]
}