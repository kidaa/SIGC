{
	"title": "That's Entertainment 2 (1976)",
	"synopsis":"This represents MGM's 1976 sequel to its enormously successful compilation film That's Entertainment (1974). In lieu of the multi-narrator device of the first film, director Gene Kelly chooses to limit the hosting chores to two people: himself, and his friendly rival Fred Astaire. Another departure from the first film was the decision to include comedy and dramatic highlights from MGM's past, with such stars as Greta Garbo (seen in a montage of I want to be alones), Greer Garson, Katharine Hepburn, Cary Grant, Jean Harlow, Wallace Beery, Marie Dressler, Red Skelton, the Marx Brothers, and Laurel and Hardy (though the last-named team's vignettes are culled from Hal Roach productions which were merely released by MGM). Be sure and catch That's Entertainment from the beginning for Saul Bass' opening credits, incorporating a variety of title-sequence styles: waves crashing on the shore, pages turning in a book, and a J. Arthur Rank-style gong. ~ Hal Erickson, Rovi",
	"genre":
	[
		{"name":"Documentary"},
		{"name":"Television"}
,
		{"name":"Musical & Performing Arts"}
,
		{"name":"Classics"}

	],
	"rate":3.6,
	"comments":[
		{
			"rate":35,
			"text": "This sequel brings you more of the same, musical and dance numbers from movies of the 40s and 50s, nothing new. Only watch if you liked the first one."
},
		{
			"rate":25,
			"text": "An interesting review of Hollywood's fancy past."
},
		{
			"rate":40,
			"text": "I think there's something to say about me if I prefer watching old movie clips to an actual breathing film. I liked the first one better, and the cohesiveness of this thing was definitely lacking, but the inclusion of KISS ME KATE made me very, very pleased."
},
		{
			"rate":45,
			"text": "This is a great movieLoads of great entertainmentGene Kelly and Fred Astaire host this nostalgic look at classic scenes from some of MGM's most-beloved comedies and musicals. The collection of clips features silver screen stars including Katharine H...( read more read more... )epburn and Spencer Tracy, Bing Crosby, Elizabeth Taylor, W.C. Fields, the Marx Brothers, Fred Astaire and Ginger Rogers, Greta Garbo, Cyd Charisse, Judy Garland, Debbie Reynolds, Frank Sinatra, John Barrymore, Judy Holliday and Clark Gable."
},
		{
			"rate":40,
			"text": "garbo is seen is rapid succession in some clips from her sound film triumphs in the second tribute to mgm's glory days."
},
		{
			"rate":40,
			"text": "Best than the first one !"
},
		{
			"rate":40,
			"text": "I miss the multiple host format of the predecessor, but still at its heart a similar warm nostalgic movie."
},
		{
			"rate":50,
			"text": "Here's another film rating."
},
		{
			"rate":35,
			"text": "I can't see how anyone could not enjoy this film. How can you go wrong with the best clips from dozens and dozens of classic movies? Well narrated by Gene Kelly and Fred Astaire. The main flaw is the continuity of the chosen clips. The editing could have been better, but it's great fun to watch no matter what."
},
		{
			"rate":40,
			"text": "Best than the first one !"
},
		{
			"rate":40,
			"text": "I miss the multiple host format of the predecessor, but still at its heart a similar warm nostalgic movie."
},
		{
			"rate":-1,
			"text": "where can I find it online?"
},
		{
			"rate":35,
			"text": "This sequel brings you more of the same, musical and dance numbers from movies of the 40s and 50s, nothing new. Only watch if you liked the first one."
},
		{
			"rate":50,
			"text": "Here's another film rating."
},
		{
			"rate":35,
			"text": "I can't see how anyone could not enjoy this film. How can you go wrong with the best clips from dozens and dozens of classic movies? Well narrated by Gene Kelly and Fred Astaire. The main flaw is the continuity of the chosen clips. The editing could have been better, but it's great fun to watch no matter what."
},
		{
			"rate":30,
			"text": "See review for That's Entertainment 1."
},
		{
			"rate":50,
			"text": "EXCELLENT.....Where hollywood came from and who got them to where we are now"
},
		{
			"rate":-1,
			"text": "No thankyou - Not interested."
},
		{
			"rate":45,
			"text": "This is a great movieLoads of great entertainmentGene Kelly and Fred Astaire host this nostalgic look at classic scenes from some of MGM's most-beloved comedies and musicals. The collection of clips features silver screen stars including Katharine H...( read more read more... )epburn and Spencer Tracy, Bing Crosby, Elizabeth Taylor, W.C. Fields, the Marx Brothers, Fred Astaire and Ginger Rogers, Greta Garbo, Cyd Charisse, Judy Garland, Debbie Reynolds, Frank Sinatra, John Barrymore, Judy Holliday and Clark Gable."
},
		{
			"rate":40,
			"text": "garbo is seen is rapid succession in some clips from her sound film triumphs in the second tribute to mgm's glory days."
},
		{
			"rate":50,
			"text": "A must see if you are into this sort of thing."
},
		{
			"rate":40,
			"text": "continued look at great film"
},
		{
			"rate":50,
			"text": "It was a joy to see Gene Kelly and Fred Astaire present this one."
},
		{
			"rate":40,
			"text": "Great musical compilation movie showing the talent that MGM produced at its peak"
},
		{
			"rate":-1,
			"text": "What better way to see my 2 favorite actors than in a movie together!"
},
		{
			"rate":-1,
			"text": "Haven't seen this yet."
},
		{
			"rate":35,
			"text": "Not as good as the first one.. but I still enjoyed it.."
},
		{
			"rate":40,
			"text": "More varied, weird and wonderful dances, comedy sketches and film clips from the golden age of MGM. No one can ever come close to Fred Astaires amazing talents!"
},
		{
			"rate":-1,
			"text": "I would like to see this, i think..."
},
		{
			"rate":45,
			"text": "Fun Documentary (all 3 of them.)"
},
		{
			"rate":50,
			"text": "MUSICALS ARE EXPLORED FURTHER..."
},
		{
			"rate":40,
			"text": "It's better than the first since it has comedy in it."
},
		{
			"rate":50,
			"text": "Awesome becuase Fred Astaire and Gene Kelly hosts it."
}	]
}