{
	"title": "Mountain Family Robinson (1979)",
	"synopsis":"Disgusted with city life, the family Robinson decides to chuck it all and head for the Rockies. There they find that rural living can be just as hectic, with hungry bears as well as the forestry service after them. ~ John Bush, Rovi",
	"genre":
	[
		{"name":"Drama"},
		{"name":"Action & Adventure"}
,
		{"name":"Kids & Family"}

	],
	"rate":3.4,
	"comments":[
		{
			"rate":35,
			"text": "Basically a remake of the Wilderness Family."
},
		{
			"rate":25,
			"text": "Part 3 of the Wilderness Family Saga. Family entertainment comparable to Little House On The Prairie but without the Michael Landon effect. Features several run-ins with wild animals amidst the vast Colorado Rockies background and a handful of happiness montages to the tune of Life Is So Wonderful. Heather Rattray would go 11 years before her next feature--Basket Case 2."
},
		{
			"rate":35,
			"text": "Basically a remake of the Wilderness Family."
},
		{
			"rate":40,
			"text": "good movies. great family films."
},
		{
			"rate":50,
			"text": "i love this movie and the other two cont. ones too"
},
		{
			"rate":35,
			"text": "Good Family / Adventure Movie !"
},
		{
			"rate":15,
			"text": "Earth hippies, do not try change what was a great family series."
},
		{
			"rate":35,
			"text": "I remember really liking it as a kid but I probably need to rewatch."
}	]
}