{
	"title": "The Out-of-Towners (1970)",
	"synopsis":"Ohio businessman Jack Lemmon is offered a golden job opportunity; all he has to do is relocate himself and wife Sandy Dennis to New York City. What follows has led some critics to complain that playwright Neil Simon has written a hate letter to Manhattan. Within a 36 hour period, the couple (a) loses their airplane luggage; (b) are forced to travel from Boston to New York in a greasy old train; ( c ) can't get any sort of service because virtually everyone in Fun City is on strike; (d) are mugged twice, once while they're asleep; (e) are reduced to sleeping on Central Park benches in their day clothes.....and so it goes, until the shabby, disheveled Lemmon tells his prospective bosses off, and he and his wife head back to Ohio---- almost. Punctuated by Sandy Dennis' plaintive Oh, my Gawwwwd, The Out of Towners tightens the screws and ups the ante on the classic comedy of errors formula. Filmed on location, the picture features a who's who of character actors (Milt Kamen, Anne Meara, Phil Bruns, Dolph Sweet, Richard Libertini, Paul Dooley, Robert Walden, Ron Carey etc. etc. etc.) When first shown on network television, the film was shorn of its closing punchline because of an eccentric censorship rule. ~ Hal Erickson, Rovi",
	"genre":
	[
		{"name":"Classics"},
		{"name":"Comedy"}

	],
	"rate":3.5,
	"comments":[
		{
			"rate":35,
			"text": "Sort of like After Hours, but less spooky."
},
		{
			"rate":30,
			"text": "I love Jack Lemmon, I also like Hiller as a director and Sandy Dennis is also a brilliant actress. I also liked the inventive story line, a true original of a much used structure. Unfortunately it?s a bit low on laughs and after a while it?s just an angry man shouting a lot. Its Jack Lemmon though, so It ok I guess. I have to say I prefer Planes, Trains and automobiles and After Hours more though!"
},
		{
			"rate":30,
			"text": "George Kellerman: Yes, what seems to be the problem now? Airline Stewardess: Well, I imagine that we've run into some bad weather. George Kellerman: You don't have to imagine just look out the window!"
},
		{
			"rate":35,
			"text": "Fantastic!"
},
		{
			"rate":30,
			"text": "I actuallly like the remake of this one better but it has moments."
},
		{
			"rate":35,
			"text": "One of those everything goes wrong movies done right. If you ever think you are having a bad day, pop this in and it'll have you smiling. The only setback is Sandy Dennis who gets on your nerves a little, but maybe intentionally so."
},
		{
			"rate":30,
			"text": "This ended up being simply 'okay' for me, as I never felt George was a very sympathetic character, more of a know it all type convinced that he could badger and bluster his way through any situation, which wore on my nerves from the time they're on the plane.I know why the film is supposed to be amusing, I just never really got any laughs out of it, more a few smiles here and there.Rental at best."
},
		{
			"rate":30,
			"text": "Reasonably entertaining Neil Simon comedy that is primarily interesting for its look at the perils of travel on the 1970's East Coast. What doesn't work is Jack Lemmon's endlessly irritating character, and the fact that both he and Sandy Dennis seem more like native New Yorkers than out-of-towners. Honestly, listen to Sandy Dennis' accent. That isn't New York? C'mon!"
},
		{
			"rate":45,
			"text": "A psychological satire perfect in it's pathos.A twisted comedy of irony perfectly portrayed in it's locations,writing,timeing and of course acting.A must see classic."
},
		{
			"rate":40,
			"text": "Hilarious movie, it shows just exactly how bad a day could be without losing anyone, or really anything, you really care about."
},
		{
			"rate":40,
			"text": "The original film with Jack Lemmon & Sandy Dennis NOT the god awful remake with Goldie Hawn & Steve Martin"
},
		{
			"rate":20,
			"text": "I fully expected to enjoy this movie. I mean, Jack Lemmon, Neil Simon, and New York City (albeit only NYC's chaotic side)... what could go wrong? And the answer is, I'm not entirely sure. Like I said, I kept expecting I'd enjoy it and then it just kind of fizzled. I usually love Neil Simon's writing, but here it felt less snappy and more tired. I usually find Jack Lemmon very funny, but in this movie his performance also got tired. I don't think I actually laughed once during this movie... That can't be a good sign. The whole thing felt so unrelentingly BLEAK, too. I don't mind stories all about horrible situations - I adored Lemony Snicket's A Series of Unfortunate Events - but somehow this just left me feeling depressed. I noticed almost no background music during the entire movie, perhaps that had something to do with it? It would be interesting to see how the remake compares with this. Perhaps someday."
},
		{
			"rate":40,
			"text": "It may be a little cliched, but The Out-of-Towners sports a very funny script by Neil Simon, a wonderfully 60's score by Quincy Jones, and two hilarious performances from the always great Jack Lemmon and the riotous Sandy Dennis."
},
		{
			"rate":20,
			"text": "I was expecting more a tighter script and a more funnier dialogue. I mean the title pretty much explains the plot, so you would think then the focus would be on the comedy. It was funny at times but I feel that this is more of a comedic acting by Jack Lemmon and Sandy Dennis. What fails is that there is soo much to make funny from this plot but it didn't feel like it was taken advantage of, I mean there are great examples of the could it get any worse comedys. Look at Planes, Trains and Automobiles, Ironically enough starred Steve Martin who also starred in the out-of-towners remake, but that is a great example that is not only really funny but also sweetly sentimental (to those who've seen it!!).This film needed either a better plot or better comedy script, but it lacks both which is a shame."
},
		{
			"rate":40,
			"text": "Until I RT gets the problem fixed my reviews will be quite short. A mid-west couple travels to NYC and endures the most uncomfortable and agonizing visit due to a parade of misfortunes and miscalculations. From almost the start their trip is a nightmare that doesn't let up. Jack Lemmon and Sandy Dennis star in this Neil Simon scripted movie. Recommended, but quite exasperating."
},
		{
			"rate":45,
			"text": "This is one of the greats I discovered on TCM for I found Jack Lemmons 2nd Best Comedy (Some Like It Hot is 1st Ofcourse). This film just leads to many misfortunes and at many points you expect the best, but you are so wrong. This film is very funny, but this film also gives a great moral value."
},
		{
			"rate":40,
			"text": "Before Planes, Trains & Automobiles were the Out of Towners. Jack Lemmon & Sandy Dennis are a riot as they spend one night full of misadventures. Far better than the more recent version with Steve Martin & Goldie Hawn. Look for Anne Meara & a quick moment with Paula Prentiss. Great fun & witty dialogue by Neil Simon."
},
		{
			"rate":40,
			"text": "This movie is hilarious while simultaneously one of the most stressful movies I've ever seen. They never stop getting screwed over"
},
		{
			"rate":50,
			"text": "I love this movie. It's not a 4 star movie, but my liking the film made up for that here on RT. I'll give it 10/10."
},
		{
			"rate":40,
			"text": "It may be a little cliched, but The Out-of-Towners sports a very funny script by Neil Simon, a wonderfully 60's score by Quincy Jones, and two hilarious performances from the always great Jack Lemmon and the riotous Sandy Dennis."
},
		{
			"rate":30,
			"text": "George Kellerman: Yes, what seems to be the problem now? Airline Stewardess: Well, I imagine that we've run into some bad weather. George Kellerman: You don't have to imagine just look out the window!"
},
		{
			"rate":20,
			"text": "I was expecting more a tighter script and a more funnier dialogue. I mean the title pretty much explains the plot, so you would think then the focus would be on the comedy. It was funny at times but I feel that this is more of a comedic acting by Jack Lemmon and Sandy Dennis. What fails is that there is soo much to make funny from this plot but it didn't feel like it was taken advantage of, I mean there are great examples of the could it get any worse comedys. Look at Planes, Trains and Automobiles, Ironically enough starred Steve Martin who also starred in the out-of-towners remake, but that is a great example that is not only really funny but also sweetly sentimental (to those who've seen it!!).This film needed either a better plot or better comedy script, but it lacks both which is a shame."
},
		{
			"rate":20,
			"text": "I was expecting more a tighter script and a more funnier dialogue. I mean the title pretty much explains the plot, so you would think then the focus would be on the comedy. It was funny at times but I feel that this is more of a comedic acting by Jack Lemmon and Sandy Dennis. What fails is that there is soo much to make funny from this plot but it didn't feel like it was taken advantage of, I mean there are great examples of the could it get any worse comedys. Look at Planes, Trains and Automobiles, Ironically enough starred Steve Martin who also starred in the out-of-towners remake, but that is a great example that is not only really funny but also sweetly sentimental (to those who've seen it!!).This film needed either a better plot or better comedy script, but it lacks both which is a shame."
},
		{
			"rate":40,
			"text": "An exhausting fun film. Very watchable."
},
		{
			"rate":40,
			"text": "Dennis & Lemmon at their best"
},
		{
			"rate":30,
			"text": "I actuallly like the remake of this one better but it has moments."
},
		{
			"rate":35,
			"text": "One of those everything goes wrong movies done right. If you ever think you are having a bad day, pop this in and it'll have you smiling. The only setback is Sandy Dennis who gets on your nerves a little, but maybe intentionally so."
},
		{
			"rate":40,
			"text": "Until I RT gets the problem fixed my reviews will be quite short. A mid-west couple travels to NYC and endures the most uncomfortable and agonizing visit due to a parade of misfortunes and miscalculations. From almost the start their trip is a nightmare that doesn't let up. Jack Lemmon and Sandy Dennis star in this Neil Simon scripted movie. Recommended, but quite exasperating."
},
		{
			"rate":25,
			"text": "More of a dark comedy than a curious comedy but still more watchable than the remake. Great job by Sandy Dennis as the wife."
},
		{
			"rate":45,
			"text": "This is one of the greats I discovered on TCM for I found Jack Lemmons 2nd Best Comedy (Some Like It Hot is 1st Ofcourse). This film just leads to many misfortunes and at many points you expect the best, but you are so wrong. This film is very funny, but this film also gives a great moral value."
},
		{
			"rate":40,
			"text": "Before Planes, Trains & Automobiles were the Out of Towners. Jack Lemmon & Sandy Dennis are a riot as they spend one night full of misadventures. Far better than the more recent version with Steve Martin & Goldie Hawn. Look for Anne Meara & a quick moment with Paula Prentiss. Great fun & witty dialogue by Neil Simon."
},
		{
			"rate":30,
			"text": "This ended up being simply 'okay' for me, as I never felt George was a very sympathetic character, more of a know it all type convinced that he could badger and bluster his way through any situation, which wore on my nerves from the time they're on the plane.I know why the film is supposed to be amusing, I just never really got any laughs out of it, more a few smiles here and there.Rental at best."
},
		{
			"rate":40,
			"text": "This movie is hilarious while simultaneously one of the most stressful movies I've ever seen. They never stop getting screwed over"
},
		{
			"rate":50,
			"text": "I love this movie. It's not a 4 star movie, but my liking the film made up for that here on RT. I'll give it 10/10."
},
		{
			"rate":30,
			"text": "I love Jack Lemmon, I also like Hiller as a director and Sandy Dennis is also a brilliant actress. I also liked the inventive story line, a true original of a much used structure. Unfortunately it?s a bit low on laughs and after a while it?s just an angry man shouting a lot. Its Jack Lemmon though, so It ok I guess. I have to say I prefer Planes, Trains and automobiles and After Hours more though!"
},
		{
			"rate":35,
			"text": "In all likelihood, the only G-rated movie ever to contain a child molestation joke."
},
		{
			"rate":40,
			"text": "3/4--The Out-of-Towners is a hilarious look on anything that could go wrong. A delightful comedy classic that is somewhat underrated and forgotten."
},
		{
			"rate":25,
			"text": "**1/2 (out of four) Neil Simon's play about an Ohio executive (Jack Lemmon) and his wife (Sandy Dennis) who visit New York City and eveything goes wrong. There is some fun here and many things are easy to relate to. Lemmon is best of all. His onery whining almost saves the film. But, in the end it is just too much and the things that go wrong become more and more tiresome."
},
		{
			"rate":35,
			"text": "A good old fashioned comedy with Jack Lennon in good form. Not the greatest work Lemmon ever did but still an enjoyable watch. Sandy Dennis as his long suffering wife is also pretty good. The comedy centres around a small town couple going to the apple - New York. They are unused to the treatment they receive and it's misdaventure after misadventure. Neil Simon's comedy was remade in 1990 with Steve Martin and Goldie Hawn but it never matched the original. Decent light hearted comedy for a lazy Sunday afternoon."
},
		{
			"rate":40,
			"text": "Dennis & Lemmon at their best"
},
		{
			"rate":40,
			"text": "Classic, howler. One calamity after another beset this couple."
},
		{
			"rate":30,
			"text": "Reasonably entertaining Neil Simon comedy that is primarily interesting for its look at the perils of travel on the 1970's East Coast. What doesn't work is Jack Lemmon's endlessly irritating character, and the fact that both he and Sandy Dennis seem more like native New Yorkers than out-of-towners. Honestly, listen to Sandy Dennis' accent. That isn't New York? C'mon!"
},
		{
			"rate":40,
			"text": "one of the funniest movies i've EVER seen :D"
},
		{
			"rate":-1,
			"text": "no thanks not my thing"
},
		{
			"rate":45,
			"text": "One of the greatest movie ever made. This movie was written directly for the screen by Neil Simon, and his mastery of creating a hilarious but also surreal story perfectly works in this uproarious comedy. The luggage sequence, being dumped down in the middle of the central park, breaking off the tooth scene, to name a few. Two main characters, namely George and Gwen Kellerman, are played best by always-wonderful Jack Lemmon and Sandy Dennis. This movie could have been just a stressful and noisy film without their lovable chemistry. I heard Steve Martin and Goldie Hawn are now remaking this movie, I don't know what kind of success the new version will get, but I am sure, by all means, they can't even get close to the quality of this original one(the original was in the early 70s, and its now 90s!!). So, whether seeing the new one or not, see this one first!"
},
		{
			"rate":40,
			"text": "was a hilarious movie, it way better then the newer one..."
},
		{
			"rate":40,
			"text": "The business trip from hell! Just when you think it can't get any worse for poor George and Gwen Kellerman, it does. Neil Simon throws hardball after hardball and doesn't let up. Even though our leading couple is ignorant and annoying as hell, Jack Lemmon and Sandy Dennis give them an endearing touch. Definitely one of the funniest movies I've seen in a long time, and a must see for any traveler."
},
		{
			"rate":50,
			"text": "lemmon and dennis, what a pair!"
},
		{
			"rate":50,
			"text": "incredible, the stuff that happens to these 2. the laughs are neverending and jack lemmon says and does some really funny stuff, anyone will enjoy this, its soo funny and very different."
},
		{
			"rate":50,
			"text": "This movie was so funny! *plugs nose* Oh my gawd!"
},
		{
			"rate":50,
			"text": "lol that is all i can say"
},
		{
			"rate":30,
			"text": "Lemmon is always watchable even when he is an annoying little bitch in this film. The script is a little weak but the actors squeeze the most out of it."
},
		{
			"rate":45,
			"text": "A psychological satire perfect in it's pathos.A twisted comedy of irony perfectly portrayed in it's locations,writing,timeing and of course acting.A must see classic."
},
		{
			"rate":40,
			"text": "Hilarious movie, it shows just exactly how bad a day could be without losing anyone, or really anything, you really care about."
},
		{
			"rate":30,
			"text": "George Kellerman: Yes, what seems to be the problem now? Airline Stewardess: Well, I imagine that we've run into some bad weather. George Kellerman: You don't have to imagine just look out the window!"
},
		{
			"rate":40,
			"text": "Excellent comedy as relevant in 2008 as it was in 1970. Check it!"
},
		{
			"rate":25,
			"text": "What can go wrong does go wrong. It's the vacation from hell."
},
		{
			"rate":50,
			"text": "Jack Lemmon was hysterical in this movie. His poor character can't catch a break and he just seems to make things worse with all the carrying on he did. You're fourth on my list to be sued."
},
		{
			"rate":-1,
			"text": "jack lemmon is a ledgend"
},
		{
			"rate":45,
			"text": "OLD MOVIE BUT IT'S SO FUNNY. ANYTHING THAT COULD HAPPEN WILL"
},
		{
			"rate":25,
			"text": "It's ok, better than the re-make (not very hard though) but it's just not my kind of movie."
},
		{
			"rate":30,
			"text": "i saw this at the weekend and it goes to show that travelling is full of pitfalls when things do not go to plan, it is not really a bad film."
},
		{
			"rate":10,
			"text": "albeit having some funny moments the film was just moan moan moan moan and frankly i get enough of that shit from the older folks where i work. last thing i wanna do is sit down for 2 hours and hear some couple complain constantly. jack lemmon couldn't save this.. and he only knows why he decided it would be a good idea to act in this film with this script that can only have been written with the sole idea of pissing people off"
},
		{
			"rate":50,
			"text": "Could not stop laughing"
},
		{
			"rate":45,
			"text": "Great movie! Hilarious!"
},
		{
			"rate":40,
			"text": "I love this movie!! Lots of laughs - especially if you find frustrating people funny. Please don't even think of comparing this original to the re-make starring Goldie Hawn."
},
		{
			"rate":25,
			"text": "If only she would stop breaking the 2nd commandment, it would be one of the funniest movies ever."
},
		{
			"rate":-1,
			"text": "Heard about it, but no."
},
		{
			"rate":45,
			"text": "Just watched again and remembered how hilarious this movie is - the best !!!!!!!"
},
		{
			"rate":45,
			"text": "Angst and misery mixed with comedy..love it. Harold and I are George and Gwem. Better than the re-make."
},
		{
			"rate":35,
			"text": "Funny, Funny, Movie !"
},
		{
			"rate":50,
			"text": "LOVE this film starring Jack Lemmon and Sandy Dennis. They're great together. Don't watch the lame remake--it's horrible. This is the one to watch!"
},
		{
			"rate":-1,
			"text": "Goldie and Steve pair up again in this riotous comedy. She looks amazing, and they are as funny as ever. Features a cameo by Ernie Sabella."
},
		{
			"rate":45,
			"text": "Very Funny Very Good"
},
		{
			"rate":35,
			"text": "What could go wrong...Will go wrong."
},
		{
			"rate":40,
			"text": "The original film with Jack Lemmon & Sandy Dennis NOT the god awful remake with Goldie Hawn & Steve Martin"
},
		{
			"rate":45,
			"text": "A nightmare of a trip that's hilarious because it isn't you!"
},
		{
			"rate":20,
			"text": "didnt like this one........."
},
		{
			"rate":40,
			"text": "sandy dennis is brilliant as lemmon's ditsy wife."
},
		{
			"rate":20,
			"text": "I really didn't like either version of this film. I thought they were both rather un funny"
},
		{
			"rate":40,
			"text": "ok two words... JACK LEMMON"
},
		{
			"rate":20,
			"text": "I fully expected to enjoy this movie. I mean, Jack Lemmon, Neil Simon, and New York City (albeit only NYC's chaotic side)... what could go wrong? And the answer is, I'm not entirely sure. Like I said, I kept expecting I'd enjoy it and then it just kind of fizzled. I usually love Neil Simon's writing, but here it felt less snappy and more tired. I usually find Jack Lemmon very funny, but in this movie his performance also got tired. I don't think I actually laughed once during this movie... That can't be a good sign. The whole thing felt so unrelentingly BLEAK, too. I don't mind stories all about horrible situations - I adored Lemony Snicket's A Series of Unfortunate Events - but somehow this just left me feeling depressed. I noticed almost no background music during the entire movie, perhaps that had something to do with it? It would be interesting to see how the remake compares with this. Perhaps someday."
},
		{
			"rate":50,
			"text": "it is so good. its really funny too."
},
		{
			"rate":35,
			"text": "ONe of the most funny Neil Simon comedies I've ever seen..Original,better than the remake"
}	]
}