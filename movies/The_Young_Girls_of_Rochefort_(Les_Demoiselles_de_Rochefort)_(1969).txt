{
	"title": "The Young Girls of Rochefort (Les Demoiselles de Rochefort) (1969)",
	"synopsis":"Jacques Demy directed this frothy tribute to the Hollywood musicals of the 1940s, a follow-up to his earlier success The Umbrellas of Cherbourg (1964). Twin sisters Delphine and Solange (played by real-life sisters Catherine Deneuve and Fran?oise Dorleac) live in the small coastal town of Rochefort, where they run a school teaching dancing and music. Both feel frustrated in Rochefort, and they dream of travelling to Paris, where they believe romance and opportunity awaits them. Meanwhile, their single mother, Yvonne (Danielle Darrieux), who runs a cafe in town, pines for her lost love, Simon (Michel Piccoli). One day, one of Yvonne's regular customers, a sailor with an artistic bent named Maxence (Jacques Perrin), shows her a painting of the imaginary girl of his dreams, and she looks just like Delphine, whom he's never met. Meanwhile, Simon has returned to Rochefort, bringing with him a close friend, American pianist Andy Miller (Gene Kelly); Simon has made friends with Solange and introduces her to Andy, who immediately falls in love with her. Sadly, Les Demoiselles de Rochefort was Fran?oise Dorleac's last film; she died in an auto accident shortly after completing the picture. ~ Mark Deming, Rovi",
	"genre":
	[
		{"name":"Romance"},
		{"name":"Musical & Performing Arts"}
,
		{"name":"Art House & International"}
,
		{"name":"Classics"}

	],
	"rate":3.9,
	"comments":[
		{
			"rate":35,
			"text": "The musical numbers seriously look like they came out of a porno - but a charming, classy, softcore kind of porno (especially the twins one!). That's pretty much how I'd describe this entire movie - innocently, campily sexual."
},
		{
			"rate":45,
			"text": "Marvelous, whimsical and fun. I can only rank this film second to the Umbrellas of Cherbourg."
},
		{
			"rate":40,
			"text": "the young girls of rochefort is the dynamic french musical classic with a fairly young angelic catherine deneuve. and it is restricted to people who have an acceptance to the cliched parisian naivete of french romanticism. the storyline is delineated with an innocent stroke of ever after fairy tale which could be mindless but exquisitedly cherographed and the music is brighteningly melodic. how about the story? absolutely corny and it's about love(amour) again. it's about two female ingenunes who seek for the ideal dream lovers. it shows its fatalistic belief in love and bond arranged in the heaven that is not a topic suited for cynist. but it's so contrived to the histrionic pretense that is meant not to be taken seriously for relaxation. but the stage effects of backset, constumes and techicolor tones are well-adjusted and that makes it highly watchable and warmingly cute if you disregard their smug poise to sing and dance on the streets and exclaims i'm in love! and this silliness requires the leads who are charismatic enough to sell it, catherine deneuve and francoise dorleac would be in this niche with their looks and light-weighted voice. it is amazing to see deneuve kicking her leg, holding her girlish hat in musical then stripped off naked as the masochist in belle de jour which is also released in 1968. the young girls of rochefort proves the diverse caliber of catherine deneuve."
},
		{
			"rate":40,
			"text": "Enjoyed this film more than expected! Surprising how different it is from The Umbrellas of Cherbourg. For starters, it's in color, and *what* colors! The hue of every shirt, pant and dress is carefully selected, and every shot is loaded with brightly painted walls, cars, etc. Eye candy? You bet. The story is also considerably more upbeat than the melancholy Cherbourg -- wow, what a date movie this would be. Though I have no idea why Demy chose to include an incongruous subplot about an unsolved murder. Huh? In any case, the rest is all about love, love, love. Multiple characters are separated but destined to come together (not only potential lovers, but parents and children), so the ticklish suspense is waiting for this person and *that* person to meet or reunite. But the plot isn't especially important, because the fun is mostly about Michel Legrand's elegant, oh-so-French songs. And of course, it's touching to see Gene Kelly dancing on film for one of the last times in his career.An added emotional resonance of The Young Girls of Rochefort is that it stars Catherine Deneuve and her sister Francoise Dorleac, who are obviously having a ball singing and dancing together. And tragically, Dorleac was killed in a car accident during the same year. At the age of 25. I can't even imagine the intense mix of feelings which this film must arouse in Deneuve today."
},
		{
			"rate":40,
			"text": "After watching several darker films per day the past 2 or 3 days, this was a break. Nothing but fun and sap."
},
		{
			"rate":40,
			"text": "demy did a companion piece to 'cherbourg' with this through-danced piece that pays homage to the happy-go-lucky freed unit musicals released by mgm. the choreography is fun, bumptious classic jazz and it is danced off the screen by the likes of george chakiris and grover dale. as added luster, gene kelly makes a cameo."
},
		{
			"rate":45,
			"text": "Catherine Deneuve and Francoise Dorleac are perfect together in this film! Great costumes, music, and scenery. Definitely a must-see for fans of French cinema!"
},
		{
			"rate":35,
			"text": "Jacques Demy follows up Umbrellas of Cherbourg with this further musical spectacle, but this time the dialogue isn't entirely sung and there is dancing, much dancing (and, in fact, Gene Kelly is here, but dubbed). For the record, I like Umbrellas better for its melancholy score and mood; here, things are cheery and the focus is on love gained or regained, not love lost. Your willingness to get on board with non-stop musical numbers will determine your enjoyment; I always feel bad for the anonymous dancers in any picture and there are plenty here. Keep an eye on the camera moves -- they show the director's talent."
},
		{
			"rate":45,
			"text": "I'm so addicted to that movie soundtrack right now"
},
		{
			"rate":45,
			"text": "Beautiful twin sisters Delphine (Deneuve) and Solange (Dorl?ac) want romance but are stuck in the plain town of Rochefort, having to support their younger brother and mother (Darrieux). But when two song and dance men (Chakiris and Dale) come to town, Rochefort suddenly transforms into an even happier town, and soon Delphine and Solange's dreams come true. I almost find it embarrassing at how much I enjoyed this excellent French musical Les Demoiselles de Rochefort. Don't get me wrong, I love French cinema, and I love to see all of these talented actors get together in one movie, but when you look at the title and even the front cover it just looks gay, not to sound too hostile. So I guess I can call this one of my guilty pleasures. Okay, enough of the confessions, I'll get to the review. Known for being one of the few French equivalents to Singin' in the Rain, Les Demoiselles de Rochefort is a colorful and extremely well done musical that not only puts you in a good mood, but it leaves you speechless with all of the new sights you've never seen before in a musical. The colorful sets and costumes make some big eye candy, as well as the sisters Catherine Deneuve and Fran?oise Dorl?ac in some very early roles. Though well done in those departments, the film excels as well in creating a good story, and though you can't sing along to any of the songs, the tune stays with you, which is one of the good signs that you're seeing a great movie. For other plus sides, we get to see Gene Kelly in one of his last musical roles, and director Jacques Demy doing what he does best. Les Demoiselles de Rochefort may be a guilty pleasure when I'm talking about movies, but otherwise, it's still one of my favorite musicals. Recommended."
},
		{
			"rate":25,
			"text": "The dance numbers are spiffy. The casualness with which everyone in the movie accepts everything from french fries to a dismembered body is odd and left me disengaged. If the people in the movie don't care, neither do I."
},
		{
			"rate":50,
			"text": "Musical, franc?s, com Catherine Deneuve n?o tem como ser ruim. Mas esse ? mt foda, vc fica apreensivo e o final n?o ? clich?. Recomendo mt"
},
		{
			"rate":30,
			"text": "It is impossible not to enjoy Jacques Demy & Michel Legrand, but this film is all color & immature love, with no narrative frame except artificial obstacles and superficial expressions of love."
},
		{
			"rate":50,
			"text": "Viewed 11/29/03 (DVD) (First Viewing) And I thought that [b]Les Parapluies de Cherbourg[/b] couldn't be topped. But with [b]Les Demoiselles de Rochefort[/b], director Jacques Demy pulls out all stops and creates a daring and utterly delightful film that matches the masterful [b]Parapluies[/b] step for step. Catherine Deneuve, the star of [b]Parapluries[/b], is teamed with her older sister Fran?ois Dorl?ac to star as a pair of ambitious twins eager to get out of the small town of Rochefort to find fame, fortune and love in Paris (Deneuve is a dancer, Dorl?ac a musician). But first is the town festival over the weekend. Little do the twins realize how much their lives will change over this one weekend. Also starring is George Chakiris, Michel Piccoli (who was Denueve's co-star in another '67 film- Bu?uel's [b]Belle de Jour[/b]), Jacques Perrin and French film legend Danielle Darrieux as the twin's mother. Somehow, [b]Demoiselles[/b] feels completely different than Hollywood musicals. Somehow, it's seems lighter and airier and more fresh than anything I've ever seen from a Hollywood's diverse offerings. It's marked by a refreshing inhibition, and a bubbly tone that's infectious. I can't think of a Hollywood musical I could compare it to. And though it showcases Demy's dazzling eye for color and art direction, it's nothing like [b]Parapluies[/b] either. It's a unique cinematic experience. Nobody can quite create a world quite like Demy. His Rochefort is a highly stylized riot of color, with costumes to match. From the impossibly blue fountain in the village square to Deneuve and Dorl?ac's fantastic clothes, [b]Demoiselles[/b] is a feast for the eyes. The music, provided by New Wave music legend Michel Legrand, is catchy and and suitably upbeat. Ghislain Cloquet's cinematography is simply superb. But underneath these eye-popping trappings is a story surprisingly symmetrical -the twin's lives seem to mirror the other's. Characters, destined for one another, spend the whole film missing each other by seconds. Surprising secrets surface abound throughout. The number of coincidences could only be carefully created by a writer or a screenwriter, but one never looks for reality in a musical anyway, so it fits perfectly. I'm not a huge fan of the musical genre, though they're always enjoyable now and then. And [b]Les Demoiselles de Rochefort[/b] ranks as one of the very best there is. (Note: In 1996, Demy's widow, female New Wave auteur Agn?s Varda, supervised a restoration of the film. It's one of the best restorations I've ever experienced. The DVD is the only way to see this film.)"
},
		{
			"rate":50,
			"text": "For me, this is probably the cinematic version of a deliciously sweet and luxurious but light and airy dessert. Three women in the titular place long for love with the men they seek just around the corner. It's fluffy stuff but the filmmaking is simply breathtaking. It's obviously heavily inspired by classic MGM musicals (including a supporting role from Gene Kelly!) but manages not to feel like a cheap imitation. This is from the director of another French musical The Umbrellas of Cherbourg, Jacques Demy. This musical considerably lighter in tone and features dance numbers this time around. But like Cherbourg, this also features a memorable score courtesy of composer Michel Legrand. As a huge fan of movie musicals, I never wanted this one to end!"
},
		{
			"rate":35,
			"text": "Jacques Demy follows up Umbrellas of Cherbourg with this further musical spectacle, but this time the dialogue isn't entirely sung and there is dancing, much dancing (and, in fact, Gene Kelly is here, but dubbed). For the record, I like Umbrellas better for its melancholy score and mood; here, things are cheery and the focus is on love gained or regained, not love lost. Your willingness to get on board with non-stop musical numbers will determine your enjoyment; I always feel bad for the anonymous dancers in any picture and there are plenty here. Keep an eye on the camera moves -- they show the director's talent."
},
		{
			"rate":45,
			"text": "I'm so addicted to that movie soundtrack right now"
},
		{
			"rate":45,
			"text": "Beautiful twin sisters Delphine (Deneuve) and Solange (Dorl?ac) want romance but are stuck in the plain town of Rochefort, having to support their younger brother and mother (Darrieux). But when two song and dance men (Chakiris and Dale) come to town, Rochefort suddenly transforms into an even happier town, and soon Delphine and Solange's dreams come true. I almost find it embarrassing at how much I enjoyed this excellent French musical Les Demoiselles de Rochefort. Don't get me wrong, I love French cinema, and I love to see all of these talented actors get together in one movie, but when you look at the title and even the front cover it just looks gay, not to sound too hostile. So I guess I can call this one of my guilty pleasures. Okay, enough of the confessions, I'll get to the review. Known for being one of the few French equivalents to Singin' in the Rain, Les Demoiselles de Rochefort is a colorful and extremely well done musical that not only puts you in a good mood, but it leaves you speechless with all of the new sights you've never seen before in a musical. The colorful sets and costumes make some big eye candy, as well as the sisters Catherine Deneuve and Fran?oise Dorl?ac in some very early roles. Though well done in those departments, the film excels as well in creating a good story, and though you can't sing along to any of the songs, the tune stays with you, which is one of the good signs that you're seeing a great movie. For other plus sides, we get to see Gene Kelly in one of his last musical roles, and director Jacques Demy doing what he does best. Les Demoiselles de Rochefort may be a guilty pleasure when I'm talking about movies, but otherwise, it's still one of my favorite musicals. Recommended."
},
		{
			"rate":25,
			"text": "The dance numbers are spiffy. The casualness with which everyone in the movie accepts everything from french fries to a dismembered body is odd and left me disengaged. If the people in the movie don't care, neither do I."
},
		{
			"rate":40,
			"text": "It can't quite compare to The Umbrellas of Cherbourg, but it does have George Chakiris, Gene Kelly, Francoise Dorleac and of course Catherine Deneuve."
},
		{
			"rate":40,
			"text": "After watching several darker films per day the past 2 or 3 days, this was a break. Nothing but fun and sap."
},
		{
			"rate":35,
			"text": "The musical numbers seriously look like they came out of a porno - but a charming, classy, softcore kind of porno (especially the twins one!). That's pretty much how I'd describe this entire movie - innocently, campily sexual."
},
		{
			"rate":50,
			"text": "Musical, franc?s, com Catherine Deneuve n?o tem como ser ruim. Mas esse ? mt foda, vc fica apreensivo e o final n?o ? clich?. Recomendo mt"
},
		{
			"rate":45,
			"text": "The Young Girls of Rochefort seemed to suffer an unfortunate fate in that it was made when the large scale musicals were going out of style. This is an absolutely fantastic musical - unquestionably French, undoubtedly from the 1960s and with all the zest of a big budget Hollywood production. The viewer accompanies a fair coming to the town of Rochefort - crossing a bridge has never been more lively (especially with George Chakiris from West Side Story in the group). In Rochefort we meet two beautiful talented sisters (Deneuve and Dorleac) who sing simply about being sisters. Their mother (Darrieux) works at a cafe and she has had a son with another man 10 years ago but said she left to Mexico. Meanwhile, a sailor (Perrin) is looking for his ideal woman in the form of Deneuve but hasn't met her yet. And then there are two friends - a music store owner (Piccoli) and a foreign musician (Kelly). Worth seeing alone for a cast like this. But Demy also fills the screen with color and memorable tunes by Michel LeGrand. Demy films all this with tremendous skill (it would be nice if some more modern directors of musicals took a look at this film as a template). The film is so full of joy it makes the viewer fall in love with the characters and Rochefort. The film has multiple love stories, the characters are all fun and positive, it is impossible not to like this film. It is whimsical and perhaps a little cliche, but really, even the best of Vincent Minnelli had these qualities!"
},
		{
			"rate":30,
			"text": "It is impossible not to enjoy Jacques Demy & Michel Legrand, but this film is all color & immature love, with no narrative frame except artificial obstacles and superficial expressions of love."
},
		{
			"rate":20,
			"text": "Was forced to sit through this in high school and didn't like it then. Soppy chick flick en francais. Won't waste my time with it again."
},
		{
			"rate":45,
			"text": "Demy's The Young Girls of Rochefort is the polar opposite of the last act in The Umbrellas of Cherbourg. There, two old lovers ran into each other having both moved on with their lives. The colors are desaturated, the town is dark and empty, and snow pours down. The Young Girls of Rochefort, on the other hand, doesn't have a single frame without an orgy of pastel colors and smiles. Even in the film's most awkward moments, involving a serial killer tormenting women in the town, the citizens of Rochefort smile, dance, and sing about beheading. It's a bit of a loveletter to the Hollywood musical with it's own French flare, and it's result is a completely breezy, free-wheeling, and enjoyable piece of work. The plot is convoluted as all hell, but here's the jist of it: a fair comes to the town of Rochefort, and along with it comes two carnies, Etienne (George Chakiris) and Bill (Grover Dale), looking for romance. They dance around a refreshment stand owned by Yvonne (Daniell Darrieux), the mother of two twin daughters, Delphine (Catherine Deneuve) and Solange (Francoise Dorleac), and a son from another man, Boubou (Patrick Jeantet). The father of Boubou is Simon Dame (Michel Piccoli), who owns a music store in town. Yvonne immaturely abandoned Simon before they wed, telling him that she's run away to Mexico for the sole reason that she didn't want to be called Madame Dame should they marry.Additionally, we meet a sailor, Maxence (Jacques Perrin), who excels in painting and poetry. He's ventured the world seeking his ideal woman whom he has painted and framed on his wall. The painting, unbeknownced to him, is of Delphine, however the potential lovers continue to just miss each other around town throughout the film. There's a third major romance between Solange and Andy Miller (Gene Kelly), a famous musician and friend of Simon. Solange bumps into him in the street, not knowing his identity, and drops a piece of music that he takes. When he plays the piece in Simon's store, where Solange had just visited, Simon comments on how familiar the tune is.It takes a little while to figure out who's who, but Demy is not at all conservative in his developing moments. The opening moments of the film, in fact, run quite a bit too long - it's never unpleasant to watch, but at times I did grow a bit restless. That being said, however, the last half hour of the film is stunning. The fair scenes are beautifully composed, as are the meetings between the lovers. When a set of lovers unite in the music store, their reunion is simply movie magic - a wonderful dance number that guarantees to put a big smile on your face.The Young Girls of Rochefort certainly didn't leave the same impression that The Umbrellas of Cherboug did on me, however it's another extremely enjoyable musical by Demy. As someone who had largely dismissed the genre prior to discovering his work, i'm astonished by just how much joy these films have given me. This film, although not perfect, is a delight to watch and one you should go out of your way to see."
},
		{
			"rate":40,
			"text": "Il s'agit d'un autre tr?s beau film de Jacques Demy, quoi que j'y pr?f?re Les Parapluies de Cherbourg. Toute l'?quipe est merveilleuse, il est agr?able de voir Catherine Deneuve avec sa soeur (avant son malheureux accident). Int?ressant aussi l'ajout de Gene Kelly qui parle fran?ais!"
},
		{
			"rate":40,
			"text": "demy did a companion piece to 'cherbourg' with this through-danced piece that pays homage to the happy-go-lucky freed unit musicals released by mgm. the choreography is fun, bumptious classic jazz and it is danced off the screen by the likes of george chakiris and grover dale. as added luster, gene kelly makes a cameo."
},
		{
			"rate":5,
			"text": "A very disappointing musical from Jacques Demy - terrible story and boring songs. The costumes are farcical: trousers tucked into white boots? You can see the terrible glint of Xanadu in Gene Kelly's droopy eyes. Much better to watch Umbrellas of Cherbourg again."
},
		{
			"rate":45,
			"text": "Marvelous, whimsical and fun. I can only rank this film second to the Umbrellas of Cherbourg."
},
		{
			"rate":-1,
			"text": "Not at all interested"
},
		{
			"rate":40,
			"text": "the young girls of rochefort is the dynamic french musical classic with a fairly young angelic catherine deneuve. and it is restricted to people who have an acceptance to the cliched parisian naivete of french romanticism. the storyline is delineated with an innocent stroke of ever after fairy tale which could be mindless but exquisitedly cherographed and the music is brighteningly melodic. how about the story? absolutely corny and it's about love(amour) again. it's about two female ingenunes who seek for the ideal dream lovers. it shows its fatalistic belief in love and bond arranged in the heaven that is not a topic suited for cynist. but it's so contrived to the histrionic pretense that is meant not to be taken seriously for relaxation. but the stage effects of backset, constumes and techicolor tones are well-adjusted and that makes it highly watchable and warmingly cute if you disregard their smug poise to sing and dance on the streets and exclaims i'm in love! and this silliness requires the leads who are charismatic enough to sell it, catherine deneuve and francoise dorleac would be in this niche with their looks and light-weighted voice. it is amazing to see deneuve kicking her leg, holding her girlish hat in musical then stripped off naked as the masochist in belle de jour which is also released in 1968. the young girls of rochefort proves the diverse caliber of catherine deneuve."
},
		{
			"rate":-1,
			"text": "I'm not even interested in seeing this movie."
},
		{
			"rate":40,
			"text": "a wonderful musical, every song easily gets stuck in your head. The color scheme in this movie is amazing as well."
},
		{
			"rate":40,
			"text": "A fantastic French Musical! the songs are fantastic!"
},
		{
			"rate":5,
			"text": "First movie I did not manage to stand more than 5 minutes (despite all my efforts)"
},
		{
			"rate":-1,
			"text": "Classified as a classic = Interested."
},
		{
			"rate":45,
			"text": "Catherine Deneuve and Francoise Dorleac are perfect together in this film! Great costumes, music, and scenery. Definitely a must-see for fans of French cinema!"
},
		{
			"rate":30,
			"text": "Jacques Demy, I sure do love you."
},
		{
			"rate":40,
			"text": "All CLASSICS are GOOD"
},
		{
			"rate":-1,
			"text": "a ripe romanticism is made palatable by a game cast and intelligent film-making."
},
		{
			"rate":45,
			"text": "Perfect homage to the Hollywood musicals. Though in creating this and Umbrellas, Demy bettered them, cementing his position as one of the best directors ever, though he is constantly forgotten."
},
		{
			"rate":-1,
			"text": "Haven't seen it, nor do I own it, but I plan to see it soon!"
},
		{
			"rate":25,
			"text": "the storyline, the people, and the fashion all looks funny.. can't believe I finish the movie"
},
		{
			"rate":10,
			"text": "Unbelievably creepy."
},
		{
			"rate":50,
			"text": "Viewed 11/29/03 (DVD) (First Viewing) And I thought that [b]Les Parapluies de Cherbourg[/b] couldn't be topped. But with [b]Les Demoiselles de Rochefort[/b], director Jacques Demy pulls out all stops and creates a daring and utterly delightful film that matches the masterful [b]Parapluies[/b] step for step. Catherine Deneuve, the star of [b]Parapluries[/b], is teamed with her older sister Fran?ois Dorl?ac to star as a pair of ambitious twins eager to get out of the small town of Rochefort to find fame, fortune and love in Paris (Deneuve is a dancer, Dorl?ac a musician). But first is the town festival over the weekend. Little do the twins realize how much their lives will change over this one weekend. Also starring is George Chakiris, Michel Piccoli (who was Denueve's co-star in another '67 film- Bu?uel's [b]Belle de Jour[/b]), Jacques Perrin and French film legend Danielle Darrieux as the twin's mother. Somehow, [b]Demoiselles[/b] feels completely different than Hollywood musicals. Somehow, it's seems lighter and airier and more fresh than anything I've ever seen from a Hollywood's diverse offerings. It's marked by a refreshing inhibition, and a bubbly tone that's infectious. I can't think of a Hollywood musical I could compare it to. And though it showcases Demy's dazzling eye for color and art direction, it's nothing like [b]Parapluies[/b] either. It's a unique cinematic experience. Nobody can quite create a world quite like Demy. His Rochefort is a highly stylized riot of color, with costumes to match. From the impossibly blue fountain in the village square to Deneuve and Dorl?ac's fantastic clothes, [b]Demoiselles[/b] is a feast for the eyes. The music, provided by New Wave music legend Michel Legrand, is catchy and and suitably upbeat. Ghislain Cloquet's cinematography is simply superb. But underneath these eye-popping trappings is a story surprisingly symmetrical -the twin's lives seem to mirror the other's. Characters, destined for one another, spend the whole film missing each other by seconds. Surprising secrets surface abound throughout. The number of coincidences could only be carefully created by a writer or a screenwriter, but one never looks for reality in a musical anyway, so it fits perfectly. I'm not a huge fan of the musical genre, though they're always enjoyable now and then. And [b]Les Demoiselles de Rochefort[/b] ranks as one of the very best there is. (Note: In 1996, Demy's widow, female New Wave auteur Agn?s Varda, supervised a restoration of the film. It's one of the best restorations I've ever experienced. The DVD is the only way to see this film.)"
}	]
}