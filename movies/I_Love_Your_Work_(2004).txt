{
	"title": "I Love Your Work (2004)",
	"synopsis":"A hot young celebrity discovers fame can be a toxic substance in this independent drama. Gray Evans (Giovanni Ribisi) is a successful actor in his late twenties who would seem to have it made. Gray is married to an attractive actress with a solid career of her own, Mia Lang (Franka Potente), he's got several projects in the works, he gets lots of fan mail, and he gets to hang out at ritzy parties with his heroes. But Gray is far from happy; his marriage to Mia is starting to fall apart, and he's being driven to distraction by his obsessive belief that a fan is stalking him. As Gray struggles to separate his delusions from reality, he finds himself indulging in a bit of stalking of his own, as he begins following John (Joshua Jackson), a clerk at a video store who is a big fan of his movies. The way Gray sees it, John is happier than he is, John's pretty wife, Jane (Marisa Coughlan), loves him while Mia doesn't care for him any more, and all in all he'd just as soon trade lives with the guy. In the midst of all this, Gray has recently run into Shana (Christina Ricci), a former flame he'd like to reconnect with. Directed by actor Adam Goldberg, I Love Your Work features a number of major stars in cameo roles, including Vince Vaughn, Jason Lee, and Elvis Costello. ~ Mark Deming, Rovi",
	"genre":
	[
		{"name":"Art House & International"},
		{"name":"Drama"}

	],
	"rate":2.9,
	"comments":[
		{
			"rate":35,
			"text": "I personally believe this movie is way underrated, I suppose to like a movie of this calibre one needs to have an acquired taste to it. It flies beneath the radar because it tackles I suppose what you could call the 'truth' of the movie business and even the human psyche as a whole. The first time I saw simply the end of it, and it awed me...that I recommended it to Dij but hadn't seen the film in its entirety to earlier today.In this movie we learn of an actor, Gray Evans [Ribsi], who is married to actress, Mia Lang [Potente] and their marriage is suffering terribly. On top of this he finds that he is slowly losing his grips upon reality and the entire story becomes his entire paranoid obsession with a young film student, John Eckhart [Jackson]. This young character seems to remind him of himself before he caught the famous bug and became a completely different person.Various things occur where he tries to mend his relationship with Mia, or when he tries to portray their relationship to the public as completely normal whilst the tabloids proclaim them being 'stormy and tempestuous'. There are some parts in this film that get extremely confusing as it shows how easily his dreams become lucid and although he thinks they are real they just devour him completely.The finale, which I'm not going to share is quite something indeed. An ironic metaphor in itself.What drew me to this film was the dark premise of the story, Franka Potente [who is one of my favourite actresses] and Ribsi, of course.This movie simply shows that Ribsi is quite believable in playing yet another role where one is masquerading as 'normal' person whilst facing turmoils within themselves.Not for everyone, and I've noticed many actually aren't interested at all in this. I find this story quite deep and utterly terrifying."
},
		{
			"rate":20,
			"text": "Very much like an acid trip. Not anything worth remembering."
},
		{
			"rate":30,
			"text": "[font=Century Gothic]In I Love Your Work, Gray(Giovanni Ribisi) apparently has it all. He is a famous movie star, married to a beautiful actress, Mia(Franka Potente), and is also an amateur photographer. But that veneer is starting to crack. His marriage is coming apart at the seams, Elvis Costello is leaving messages and Gray fears he is being stalked. One day, he wanders into the safe haven of a video store where he meets store employee John(Joshua Jackson), a fan, and his girlfriend, Jane(Marisa Coughlan), an artist.[/font][font=Century Gothic][/font] [font=Century Gothic]I Love Your Work is an intriguing movie about the price of fame and being careful what you ask for. But it also serves as a commentary on the symbiotic relationship between the audience and the performers and what each hopes to gain in the bargain.[/font]"
},
		{
			"rate":35,
			"text": "Good film, kind've weird and a little disturbing though. Great performance by Giovanni Ribisi."
},
		{
			"rate":20,
			"text": "This film had absolutely everything in it for me to love - hell, adore it!, but in the end I just couldn't find anything that interesting in it's characters other than the actors playing them."
},
		{
			"rate":25,
			"text": "Wasn't sure what to make of this dramatic riches-based story, although I did like seeing some sharp acting in some moments. Ribisi is odd in more than a few scenes, but seems entertaining overall.I wasn't expecting Nicky Katt to spontaneously burst into song, but I guess director Goldberg had a vision.I passed up the chance to see 'The Green Mile' while watching this film. I should've watched the Stephen King feature instead."
},
		{
			"rate":35,
			"text": "Giovanni Ribisi does a great job in this film... cracked out and crazed... good stuff, with a strange variety of big name actors/actress'... weird movie... but interesting..."
},
		{
			"rate":30,
			"text": "I finally caught I Love Your Work this past week and I have to say that Giovanni Ribisi really amazes me as an actor! He really really nailed the whole paranoid-rich-actor-wanting-a-simpler-life bit. And it worked! I completely sympathized for him. This little flick made me NOT want to be famous. The rest of the cast was all top notch too, including Jason Lee, one of my all-time favorites. He plays a creepy little character who apparently harasses actors until they get mad and knock them out in full view of a cop. Joshua Jackson plays a video store clerk who wants to make movies and eventually helps Gray Evans (Ribisi) finally crack. Christina Ricci plays the gal who Gray wishes he could be with and live the life that she and her boyfriend John (Jackson) actually have. They are sweet and romantic and very much in love, even though they don't have the rich life. It doesn't help that Evans thinks his wife Mia Lang (played by Franka Potente) is having an affair with none other than Elvis Costello! This cat has got a whole lot going on in his life, going to premiers, filming a movie, and always partying. Like I said, finally, he cracks and boy does he do it well. I've always loved watching Giovanni peform, whether it was in Gone In 60 Seconds, Lost In Translation, My Name Is Earl or another amazing performance in The Gift. This was the first and only directorial effort so far by Adam Goldberg, who also co-wrote, produced and edited. A classic auteur in the truest sense. I unfortunately gave this flick three stars just because I believe the story was a bit weak and the ending wasn't the clearest. But it was still a very good movie that was very moving and powerful. And if you want to see a great actor performing a great role, try checking this one out!"
},
		{
			"rate":30,
			"text": "I would've gave it more than 3 stars if it wouldn't have left me so confused.I understand what happened but my god!what is it with the crazies Giovani Ribisi always plays?Interesting movie but definately not for those who like the plot laid out nicely on a platter."
},
		{
			"rate":25,
			"text": "Definitely not a big fan of this movie, and am starting to realize Ribisi isn't one of my favorites. Barely gets that last half star because... well, I guess it had its moments."
},
		{
			"rate":10,
			"text": "I like everyone involved (Goldberg, Ribisi, Ricci, Lee, etc.). However, I did not like anything about this boring piece of steaming cowshit."
},
		{
			"rate":10,
			"text": "This movie had an interesting premise but after watching it, I found it to be quite boring didn't really see any point to the story! Another one of those films that makes absolutely no sense and loses the viewer as the movie continues to progress!"
},
		{
			"rate":35,
			"text": "i love the acting - ribisi has long been a favorite of mine. i thought i was enjoying the movie but afterward i wasn't so sure. i think it's a pretty good flick but i'd have to see it again to be sure."
},
		{
			"rate":50,
			"text": "This movie is so Crazy it's perfect! Talk about obsession and someone being unstable, this movie is one of the best dramas I've ever seen~"
},
		{
			"rate":50,
			"text": "i love this movie but people dont get it. joshua jackson did an awesome job in this movie. it was kind of like the movie buffalo 66. i want to see more movies by this guy!!!"
},
		{
			"rate":5,
			"text": "The mostly lazy of screenwriting. Young actor is bothered by his fame and people recognizing him until it serves his own ends and then wields it like a weapon. YAWN.. It's like any hack movie that set in California, with characters who are actors or writers.. yes write about what you know.. so learn something and show it don't trot out the same old wank about how hard life is when you are famous.The cast, with some fine actors, drown in a sea of nothingness and can only have appeared because of the director. Uninteresting shot and low on budget and just about every other score."
},
		{
			"rate":30,
			"text": "as kind of pretentious art films go, this one's not unbearable. it's got more than a few nice touches. recommendable? just barely, because of what it does with the stalker scenario. can't promise i won't lower its rating at a later date."
},
		{
			"rate":15,
			"text": "In spite of a cast full of young, compelling actors and a battery of clever cinematic tricks, I Love Your Work came out as a preposterous, confused mess. Full of squandered potential, this film is perfectly performed and shot, but based on a script that's got far more ambition than intelligence."
},
		{
			"rate":0,
			"text": ":rotten: [b][i]I love your Work is one of the most horrendous, pointless and boring films I've seen in my whole life. It is an attempt to show the audience the disadvantages of being famous, but the movie is so sloppily done that I never cared about it, I just saw Giovanni Ribisi playing a paranoic actor who actually sucked at his profession surrounded by furniture actors with irrelevant dialogues, unnecessary scenes and extremely confusing and incoherent screenplay. I'm sorry to admit it but I fell asleep while I was intending to watch this crap.[/i][/b]"
},
		{
			"rate":0,
			"text": "It's an ugly and boring film. The fil tried to show the disadvantages of being a public star but it was badly made."
},
		{
			"rate":-1,
			"text": "I just love Adam Goldberg."
},
		{
			"rate":5,
			"text": "The mostly lazy of screenwriting. Young actor is bothered by his fame and people recognizing him until it serves his own ends and then wields it like a weapon. YAWN.. It's like any hack movie that set in California, with characters who are actors or writers.. yes write about what you know.. so learn something and show it don't trot out the same old wank about how hard life is when you are famous.The cast, with some fine actors, drown in a sea of nothingness and can only have appeared because of the director. Uninteresting shot and low on budget and just about every other score."
},
		{
			"rate":30,
			"text": "I finally caught I Love Your Work this past week and I have to say that Giovanni Ribisi really amazes me as an actor! He really really nailed the whole paranoid-rich-actor-wanting-a-simpler-life bit. And it worked! I completely sympathized for him. This little flick made me NOT want to be famous. The rest of the cast was all top notch too, including Jason Lee, one of my all-time favorites. He plays a creepy little character who apparently harasses actors until they get mad and knock them out in full view of a cop. Joshua Jackson plays a video store clerk who wants to make movies and eventually helps Gray Evans (Ribisi) finally crack. Christina Ricci plays the gal who Gray wishes he could be with and live the life that she and her boyfriend John (Jackson) actually have. They are sweet and romantic and very much in love, even though they don't have the rich life. It doesn't help that Evans thinks his wife Mia Lang (played by Franka Potente) is having an affair with none other than Elvis Costello! This cat has got a whole lot going on in his life, going to premiers, filming a movie, and always partying. Like I said, finally, he cracks and boy does he do it well. I've always loved watching Giovanni peform, whether it was in Gone In 60 Seconds, Lost In Translation, My Name Is Earl or another amazing performance in The Gift. This was the first and only directorial effort so far by Adam Goldberg, who also co-wrote, produced and edited. A classic auteur in the truest sense. I unfortunately gave this flick three stars just because I believe the story was a bit weak and the ending wasn't the clearest. But it was still a very good movie that was very moving and powerful. And if you want to see a great actor performing a great role, try checking this one out!"
},
		{
			"rate":-1,
			"text": "I think I saw this...?"
},
		{
			"rate":-1,
			"text": "anyone seen my heating pad?///"
},
		{
			"rate":10,
			"text": "I dont know what to think of this flick"
},
		{
			"rate":35,
			"text": "A very good psychological drama. Gray's distress in the world he lives in is touching. Some scenes were very well put together. Great actor performances, except for the wife (Potente). The best was Yehud."
},
		{
			"rate":30,
			"text": "as kind of pretentious art films go, this one's not unbearable. it's got more than a few nice touches. recommendable? just barely, because of what it does with the stalker scenario. can't promise i won't lower its rating at a later date."
},
		{
			"rate":20,
			"text": "had no idea what was going on. way to artsy for me."
},
		{
			"rate":15,
			"text": "In spite of a cast full of young, compelling actors and a battery of clever cinematic tricks, I Love Your Work came out as a preposterous, confused mess. Full of squandered potential, this film is perfectly performed and shot, but based on a script that's got far more ambition than intelligence."
},
		{
			"rate":20,
			"text": "nice technique by adam goldberg, but the plot is very exhausting... though the ambivalent message (reality-film, past-present, mask-honesty) is installed very well."
},
		{
			"rate":30,
			"text": "Not what I was expecting but dark and different. It was quite confusing at first in a good way then in a long boring bad way then finally again in a decent way. Interesting take on celebrity and Ribisni is certainly talented at these psychological pieces. Worth a watch to see if you can figure it all out but don't expect to really be blown away."
},
		{
			"rate":40,
			"text": "A movie star named Gray Evans becomes troubled by the downfall of his marriage, difficulties with the press, and the haunting obsession he develops with a young girl named Shana. When Adam Goldberg directs a movie starring the greatest actor and the greatest actress of all time (Giovanni Ribisi and Christina Ricci), plus the great additions of Joshua Jackson and Judy Greer, an awesome result is inevitable. With a dead original story, some weird but spectacular lighting and visual effects, and a solid soundtrack, 'I Love Your Work' is a real example of some of the strongest talent in the movie industry collaberating for the right reasons. Outstanding psychological drama, that obviously hasn't gotten the credit it deserves..."
},
		{
			"rate":0,
			"text": ":rotten: [b][i]I love your Work is one of the most horrendous, pointless and boring films I've seen in my whole life. It is an attempt to show the audience the disadvantages of being famous, but the movie is so sloppily done that I never cared about it, I just saw Giovanni Ribisi playing a paranoic actor who actually sucked at his profession surrounded by furniture actors with irrelevant dialogues, unnecessary scenes and extremely confusing and incoherent screenplay. I'm sorry to admit it but I fell asleep while I was intending to watch this crap.[/i][/b]"
},
		{
			"rate":0,
			"text": "It's an ugly and boring film. The fil tried to show the disadvantages of being a public star but it was badly made."
},
		{
			"rate":35,
			"text": "Finally got around to seeing this (thankies Bannan). Certainly portayed the trappings of fame as I had always imagined it to be...far from glitz and glamour but rather suffocating, maddening and even terrifying. Solid performance by Giovanni Risbi."
},
		{
			"rate":-1,
			"text": "I LOVE GIOVANNI RIBISI"
},
		{
			"rate":40,
			"text": "weird not the best but i like the weirdness of it"
},
		{
			"rate":30,
			"text": "I would've gave it more than 3 stars if it wouldn't have left me so confused.I understand what happened but my god!what is it with the crazies Giovani Ribisi always plays?Interesting movie but definately not for those who like the plot laid out nicely on a platter."
},
		{
			"rate":30,
			"text": "A strange look at the consequences of celebrity and the culture surrounding it. At times the film becomes confusing as it blurs reality with fantasy but this often makes Gray Evans a more relatable character."
},
		{
			"rate":35,
			"text": "I personally believe this movie is way underrated, I suppose to like a movie of this calibre one needs to have an acquired taste to it. It flies beneath the radar because it tackles I suppose what you could call the 'truth' of the movie business and even the human psyche as a whole. The first time I saw simply the end of it, and it awed me...that I recommended it to Dij but hadn't seen the film in its entirety to earlier today.In this movie we learn of an actor, Gray Evans [Ribsi], who is married to actress, Mia Lang [Potente] and their marriage is suffering terribly. On top of this he finds that he is slowly losing his grips upon reality and the entire story becomes his entire paranoid obsession with a young film student, John Eckhart [Jackson]. This young character seems to remind him of himself before he caught the famous bug and became a completely different person.Various things occur where he tries to mend his relationship with Mia, or when he tries to portray their relationship to the public as completely normal whilst the tabloids proclaim them being 'stormy and tempestuous'. There are some parts in this film that get extremely confusing as it shows how easily his dreams become lucid and although he thinks they are real they just devour him completely.The finale, which I'm not going to share is quite something indeed. An ironic metaphor in itself.What drew me to this film was the dark premise of the story, Franka Potente [who is one of my favourite actresses] and Ribsi, of course.This movie simply shows that Ribsi is quite believable in playing yet another role where one is masquerading as 'normal' person whilst facing turmoils within themselves.Not for everyone, and I've noticed many actually aren't interested at all in this. I find this story quite deep and utterly terrifying."
},
		{
			"rate":35,
			"text": "Good film, kind've weird and a little disturbing though. Great performance by Giovanni Ribisi."
},
		{
			"rate":20,
			"text": "This film had absolutely everything in it for me to love - hell, adore it!, but in the end I just couldn't find anything that interesting in it's characters other than the actors playing them."
},
		{
			"rate":25,
			"text": "Definitely not a big fan of this movie, and am starting to realize Ribisi isn't one of my favorites. Barely gets that last half star because... well, I guess it had its moments."
},
		{
			"rate":20,
			"text": "Every one is amazing in this movie its just the movie thats not so amazing.(n)"
},
		{
			"rate":30,
			"text": "it was a liitle hard to get into, and a bit wierd...and sort of confusing. but i still think giovanni ribisi was really good in it."
},
		{
			"rate":-1,
			"text": "Can't wait to see this."
},
		{
			"rate":40,
			"text": "What kept me glued was the relationship between Gary Evans and his wife. Their love is strangely sarcastic, and when it is, its hilarious."
},
		{
			"rate":50,
			"text": "MY FAVORITE MOVIE EVER i LOOOOVE giovanni ribisi"
},
		{
			"rate":-1,
			"text": "A FLIXSTER MEMBER DESCRIBED THIS AS A  DARK INSIGHT INTO A DELUDED MIND. SOUNDS LIKE MY TYPE OF MOVIE."
},
		{
			"rate":10,
			"text": "I like everyone involved (Goldberg, Ribisi, Ricci, Lee, etc.). However, I did not like anything about this boring piece of steaming cowshit."
},
		{
			"rate":25,
			"text": "Wasn't sure what to make of this dramatic riches-based story, although I did like seeing some sharp acting in some moments. Ribisi is odd in more than a few scenes, but seems entertaining overall.I wasn't expecting Nicky Katt to spontaneously burst into song, but I guess director Goldberg had a vision.I passed up the chance to see 'The Green Mile' while watching this film. I should've watched the Stephen King feature instead."
},
		{
			"rate":40,
			"text": "very stylish and starts off great and seems to only get better but repeats itself into oblivion."
},
		{
			"rate":15,
			"text": "I like Giovanni Ribisi but this movie does not cut it, horrible"
},
		{
			"rate":35,
			"text": "Giovanni Ribisi's raw expression is unleashed in this film. The trailer is disconcerting so only watch the movie. A dark insight into a deluded mind."
},
		{
			"rate":50,
			"text": "This was a VERY strange movie. It was twisted. It was deep."
},
		{
			"rate":20,
			"text": "I bought this movie becuase of the cast but not really all that good I mean you get a look into a celebrity's life and how it could drive you crazy I guess and then have you become the stocker"
},
		{
			"rate":30,
			"text": "i love adam goldberg, and this was a creative idea but, at times had too many twist and turns..."
},
		{
			"rate":35,
			"text": "An exceptional film! Surprisingly well done for one not so well known. Great acting, well put together."
},
		{
			"rate":10,
			"text": "This movie had an interesting premise but after watching it, I found it to be quite boring didn't really see any point to the story! Another one of those films that makes absolutely no sense and loses the viewer as the movie continues to progress!"
},
		{
			"rate":15,
			"text": "So F-ed up! But still solid performances from Giovanni and (my fav) Franka!!"
},
		{
			"rate":-1,
			"text": "i love all thinks wich you done"
},
		{
			"rate":30,
			"text": "[font=Century Gothic]In I Love Your Work, Gray(Giovanni Ribisi) apparently has it all. He is a famous movie star, married to a beautiful actress, Mia(Franka Potente), and is also an amateur photographer. But that veneer is starting to crack. His marriage is coming apart at the seams, Elvis Costello is leaving messages and Gray fears he is being stalked. One day, he wanders into the safe haven of a video store where he meets store employee John(Joshua Jackson), a fan, and his girlfriend, Jane(Marisa Coughlan), an artist.[/font][font=Century Gothic][/font] [font=Century Gothic]I Love Your Work is an intriguing movie about the price of fame and being careful what you ask for. But it also serves as a commentary on the symbiotic relationship between the audience and the performers and what each hopes to gain in the bargain.[/font]"
},
		{
			"rate":30,
			"text": "Not what I expected based on the synopsis on the box. Giovanni Ribisi was stand-out. Less thought-provoking, but more of a different take on the stalker-thriller genre."
},
		{
			"rate":35,
			"text": "i love the acting - ribisi has long been a favorite of mine. i thought i was enjoying the movie but afterward i wasn't so sure. i think it's a pretty good flick but i'd have to see it again to be sure."
},
		{
			"rate":-1,
			"text": "i like to whotch this movey whith my frendes"
},
		{
			"rate":50,
			"text": "This movie is so Crazy it's perfect! Talk about obsession and someone being unstable, this movie is one of the best dramas I've ever seen~"
},
		{
			"rate":20,
			"text": "If this was trying to be weird, it just wasn't weird enough. I found myself losing interest."
},
		{
			"rate":40,
			"text": "This was definately a twisted movie. I liked the suspense though. Go josh & giovanni!"
},
		{
			"rate":30,
			"text": "I Love Your Work is the exact sentence I was going to use anyhow!"
},
		{
			"rate":50,
			"text": "i love this movie but people dont get it. joshua jackson did an awesome job in this movie. it was kind of like the movie buffalo 66. i want to see more movies by this guy!!!"
},
		{
			"rate":45,
			"text": "holy hell! kylan like."
},
		{
			"rate":35,
			"text": "Ok, this was really good but the plot really confused me. A paradigm between cinema and real-life explored in this film, although I had issues working out when was 'real-life' and when was film?"
},
		{
			"rate":20,
			"text": ":rotten: [font=Comic Sans MS][size=4][color=darkorchid]I had to give some thought on this movie. I think it was trying to get into the mind of this guy's mind, while he was having a melt-down. And maybe that is why I had a hard time with it. At first I was going to give it a 7/10. The more I thought about it the more I really didn't like it.[/color][/size][/font][font=Comic Sans MS][size=4][color=#9932cc]Maybe if it was given to another director (Adam Goldberg) and writer, could have been better. Maybe they need to see The Snake Pit. A very good movie about mental breakdown. Maybe even [/color][/size][/font][font=Comic Sans MS][size=4][color=#9932cc]A Beautiful Mind (Which Goldberg stared in).[/color][/size][/font][font=Comic Sans MS][size=4][color=#9932cc]It had a all star cast......Giovanni Rabisi, Jason Lee, Vince Vaughn, Christina Ricci and Joshua Jackson.[/color][/size][/font][font=Comic Sans MS][size=4][color=#9932cc]Even Elvis Costello. [/color][/size][/font][font=Comic Sans MS][size=4][color=#9932cc]I was hopping for better.[/color][/size][/font][font=Comic Sans MS][size=4][color=#9932cc]Donna A.[/color][/size][/font][font=Comic Sans MS][size=4][color=#9932cc][/color][/size][/font]"
},
		{
			"rate":5,
			"text": "Weak movie. VERY WEAK"
},
		{
			"rate":20,
			"text": "Very much like an acid trip. Not anything worth remembering."
},
		{
			"rate":-1,
			"text": "yeah looks good, ill see this!"
},
		{
			"rate":-1,
			"text": "Like I already said, I'm a sucker for Franke Potente."
},
		{
			"rate":30,
			"text": "[size=3]Not the best of times these days. I'm bummed because Tower Records is finally giving up the ghost. As usual, I ran into one message board where the resident weenys were complaining that the stores' prices were too high for CDs and DVDs but you could at least buy porn cheap. Attitudes like that are what killed this company. In a world where people seem to be getting more and more narrow-minded and get hostile at being exposed to anything new, Tower was still naive enough to carry everything under the sun, not just a great selection of rock, but blues, classical, soundtracks, jazz, African, reggae and any other genre you could think of. [/size][size=3] I've bought many an album and CD at that store, usually something I couldn't find anywhere else. Just this week alone as the going out of business signs went up, I could still find rare stuff: Ayetet Rose Gottlieb, Robin Holcomb, Orchestre Nationale de Jazz, Harry Partch, Vince Martin, The Ides Of March, Carla Bozulich. And there are plenty of wonderful things still there, some of which will probably gather dust until I come back over the next few weeks, Stone The Crows, The Beautiful South, Big Mama Thornton, The DIVA Jazz Orchestra, Nina Simone, Alan Skidmore, Mickey Katz, Morton Feldman, John Martyn, Boris, Anthony Coleman, Serge Gainsbourg, yards of John Cage and Elliott Carter, Red Foley, Candye Kane, Gong, even a mix CD featuring Double Dee & Steinski. Damn!...universes of sound only a few music fanatics like me are intersted in hearing. There's one wonderful store left in DC that still carries exotic music, Melody Records but considering the entire chain...the world just got a little bit colder.[/size]"
},
		{
			"rate":30,
			"text": "it's sad that giovanni ribisi has to go to these lengths to do great work...given the chance, he'd be an oscar winner"
},
		{
			"rate":35,
			"text": "Quite a mind fuck, but very well done!"
},
		{
			"rate":20,
			"text": "Disappointing. I'm fascinated by celebrity culture, but I found myself bored by these characters."
},
		{
			"rate":5,
			"text": "Utterly unwatchable. Irritating characters. Useless plot. What a waste of my time."
},
		{
			"rate":5,
			"text": "Turned it off half way through. Rubbish"
},
		{
			"rate":15,
			"text": "This was a very slow movie... It had its good moments. It was pretty twisted and f*@$%d up."
},
		{
			"rate":35,
			"text": "Giovanni Ribisi does a great job in this film... cracked out and crazed... good stuff, with a strange variety of big name actors/actress'... weird movie... but interesting..."
},
		{
			"rate":40,
			"text": "directed by adam goldberg... thats pretty sweet. i really enjoyed the intensity of giovanni's character. great flick."
},
		{
			"rate":-1,
			"text": "CooL! Somehow............"
},
		{
			"rate":-1,
			"text": "I just love Adam Goldberg."
},
		{
			"rate":35,
			"text": "great cast, put to good use"
},
		{
			"rate":35,
			"text": "It was very odd and twisted. Great Cast! Could have used more jason lee and vince vahn. Its hard to feel sorry for a movie star... Especially a movie star that goes crazy. But it was a good movie. Slow at some parts, overall good."
},
		{
			"rate":30,
			"text": "yes very dark - good twists"
},
		{
			"rate":20,
			"text": "Totally not what I expected. Disappointing :("
},
		{
			"rate":40,
			"text": "I love movies like this. Definately worth checking out."
},
		{
			"rate":15,
			"text": "Neither one was very enjoyable. Franka Potente has the most hilarious scene at the zoo in I Love Your Work. Is it just me, or is it annoying when Kevin Kline pretends to have an accent because throughout the movie I was just starting to hate him for it."
},
		{
			"rate":-1,
			"text": "Fuck all dark psychological dramas... they suck shit!! :D"
},
		{
			"rate":20,
			"text": "The film tries so hard to be artsy and meaningful it forgets to entertain. Boring and pointless, in spite of an appealing cast. Giovanni Ribisi tries, but it's hard to save a poorly written screenplay and misguided direction."
}	]
}