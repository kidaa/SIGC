{
	"title": "The Adventures of Pinocchio (1996)",
	"synopsis":"Unlike the more familiar animated Pinocchio by Disney, there are no song interludes here, and characters added to the story by Disney (such as Jiminy Cricket) are not included. Producer Francis Ford Coppola and director Steve Barron, (known for the first Teenage Mutant Ninja Turtles film) closely adhere to Carlo Collodi's 1883 novel and use the visually timeless setting of a Czechoslovakian village. Jim Henson's puppet studio skillfully brings this Pinocchio to life. Long ago, in his youth, Gepetto (Martin Landau) loved but did not court Leona (Genvieve Bujold), who married Gepetto's brother instead. In that earlier time, he carved her initials with his onto a tree. Now his brother is dead, and though he still feels for Leona, he is still too shy to woo her. Instead, the old puppet-maker goes into the forest and cuts down a tree in order to make a puppet just for himself. The tree is the same one he carved his initials into when he was younger, and it has the magic of his love in it. Soon after the puppet Pinocchio is made, he comes to life. Aside from being made of wood, he begins to live the life of a perfectly normal little boy. He even goes to school. Lorenzini, an evil magician who runs a children's puppet show, hears of Pinocchio and wants to use him in his show. Lorenzini lures children to his show, only to later turn them into donkeys. Donkeys are useful creatures, and Lorenzini makes a lot of money selling them. Through many trials and tribulations, the puppet-boy earns the right to become the human boy Pinocchio (Jonathan Taylor Thomas). ~ Clarke Fountain, Rovi",
	"genre":
	[
		{"name":"Science Fiction & Fantasy"},
		{"name":"Kids & Family"}

	],
	"rate":2.6,
	"comments":[
		{
			"rate":40,
			"text": "The puppetry is some of the best ive ever seen in a film, well it was made by the Jim Henson workshop"
},
		{
			"rate":5,
			"text": "This movie is a travesty. A piece of the original Pinocchio died with the creation of this film."
},
		{
			"rate":15,
			"text": "...........................thank you that is all."
},
		{
			"rate":40,
			"text": "This is a beautiful Pinochio Movie. It makes you gasp, lol, and smile at the end."
},
		{
			"rate":40,
			"text": "A quality live-action telling of the Pinocchio story with a real puppet as Pinocchio and Martin Landau as Gepetto. It's very imaginative with nice visuals. I think they did it really well for an enjoyable movie."
},
		{
			"rate":30,
			"text": "It's beyond the Disney classic, & it's creepy-as hell. But it was a little entertaining for me when I was a kid."
},
		{
			"rate":15,
			"text": "Really bad. Jonathan Taylor Thomas should have been aborted."
},
		{
			"rate":35,
			"text": "Oh, I feel like I've seen this. OHMIGOSH Jonathan Taylor Thomas?! Using the Mufasa Principle, it must be good."
},
		{
			"rate":40,
			"text": "I've seen a number of movies featuring Pinocchio, and as I could tell with the plot and story, this one's the best that I've ever seen. It's the live-action version featuring real actors. The Adventures Of Pinocchio tells the story of Geppetto (Martin Landau) carving, from a tree, a puppet named Pinocchio. The puppet comes to life and looks for fun while traveling out in the world that was not familiar to him- all part of the quest to become a real human boy. It's nice to see Jonathan Taylor Thomas (remember Home Improvement?) playing the titular marionette character, and when I first saw the evil Lorenzini, I didn't knew that I first saw Rob Schneider years before recognizing him as a male gigolo, an animal, and a hot chick. Also, there's a cricket character who's his conscience, and his name's Pepe. This is the best adaptation of the popular children's story of Pinocchio just yet, and I give this movie four stars."
},
		{
			"rate":30,
			"text": "I actually prefer this one over the animated one just because it was just better for me I dont know what it was but I just would pick this one over the other anyday."
},
		{
			"rate":45,
			"text": "This is the one movie that keeps gushing tears from my every time i watch it and every minute watched also...love it!"
},
		{
			"rate":10,
			"text": "The original is a better film in every way."
},
		{
			"rate":35,
			"text": "Why have people forgotten about this movie?"
},
		{
			"rate":15,
			"text": "I saw this movie with my kids, and frankly, they could not stay focused on it. The movie jumped around a lot. I tended to get lost. I did however, like Rob Schneider, I felt he did a pretty good job. All in all my summary is if you have seen the Walt Disney version, DON'T see this one! The animated version is much more better. Although they did it 90's style, for instance in the Disney version the boys going to the island smoked cigars; this version, they drank the water and you know the rest.."
},
		{
			"rate":20,
			"text": "It's certainly no classic."
},
		{
			"rate":5,
			"text": "Scared me in my younger years..."
},
		{
			"rate":20,
			"text": "Eh, I didn't care for this at all."
},
		{
			"rate":25,
			"text": "Pinocchio is obnoxious, rude & spoiled.... not the same puppet who became a boy in the original Disney Cartoon tale. A grimm's story without the beloved Jimmy Cricket feels criminal."
},
		{
			"rate":30,
			"text": "Well, Disney's classic it most certainly isn't, but the excellent visual effects by Jim Henson's Creature Shop make this watchable."
},
		{
			"rate":5,
			"text": "If I could have no stars I would....."
},
		{
			"rate":10,
			"text": "The original is a better film in every way."
},
		{
			"rate":35,
			"text": "Why have people forgotten about this movie?"
},
		{
			"rate":15,
			"text": "I saw this movie with my kids, and frankly, they could not stay focused on it. The movie jumped around a lot. I tended to get lost. I did however, like Rob Schneider, I felt he did a pretty good job. All in all my summary is if you have seen the Walt Disney version, DON'T see this one! The animated version is much more better. Although they did it 90's style, for instance in the Disney version the boys going to the island smoked cigars; this version, they drank the water and you know the rest.."
},
		{
			"rate":30,
			"text": "A nice retelling of the classic tale of the puppet who wants to be a real boy. It's quite well done, some puppets are incredibly well designed & the sets & costumes of the film are marvelous. It might not be the best Pinocchio ever but it's a pleasant one to watch."
},
		{
			"rate":20,
			"text": "It's certainly no classic."
},
		{
			"rate":5,
			"text": "Scared me in my younger years..."
},
		{
			"rate":20,
			"text": "Eh, I didn't care for this at all."
},
		{
			"rate":25,
			"text": "Seen it mediocre yet creepy looking doll!"
},
		{
			"rate":25,
			"text": "Pinocchio is obnoxious, rude & spoiled.... not the same puppet who became a boy in the original Disney Cartoon tale. A grimm's story without the beloved Jimmy Cricket feels criminal."
},
		{
			"rate":20,
			"text": "its unbelievable what some filmmakers will do. THERE CANNOT BE A PINOCCHIO 2!! what were they thinking? this ...(shudder) sequel just ruins the originality of they 1940 animated flick. the sets are magnificent, but the acting of some and the storyline is to be ashamed of. only jonathan taylor thomas' presence and the wonderful setting design saves this film. and they almost miss that."
},
		{
			"rate":10,
			"text": "Yikes. What can I say? It was the year of JTT."
},
		{
			"rate":50,
			"text": "this is my favourite Pinocchio movie!"
},
		{
			"rate":30,
			"text": "Well, Disney's classic it most certainly isn't, but the excellent visual effects by Jim Henson's Creature Shop make this watchable."
},
		{
			"rate":5,
			"text": "If I could have no stars I would....."
},
		{
			"rate":45,
			"text": "I don't know why many hate it, it's actually a very good live-action adaptation of the original Pinocchio and it has it's own kind of magic. All the acting is good and special effects are the best of it's period."
},
		{
			"rate":50,
			"text": "A graceful, charming & entertaining flick that brings back happy memories of my childhood."
},
		{
			"rate":40,
			"text": "Enchanting visual effects and story, this film is a very underrated classic."
},
		{
			"rate":-1,
			"text": "I love this movie :)"
},
		{
			"rate":40,
			"text": "The puppetry is some of the best ive ever seen in a film, well it was made by the Jim Henson workshop"
},
		{
			"rate":30,
			"text": "It's beyond the Disney classic, & it's creepy-as hell. But it was a little entertaining for me when I was a kid."
},
		{
			"rate":20,
			"text": "(from The Watermark, 08/03/96) New Line Cinema takes a stab at portraying the Carlo Collodi (and Disney) classic in live action, and finds a surprising amount of sentiment in the story. Puppet maker Geppetto (sweetly portrayed by Landau) dreams of having a son of his own. One day, he carves a wooden marionette that comes to life, and after learning several life lessons, becomes the real son Geppetto never had. Pinocchio (voiced by Thomas) is mostly achieved through puppetry, with the occasional help of some jolty-looking computer animation. The film's biggest fault is in the character of Pepe the Cricket (Disney's Jiminy Cricket). The hyperactive computer-generated insect is not pleasant to see or hear (voice provided by Charlie's Angels' David Doyle) nor does he stay still long enough for the audience to connect with him. It is a glaring error to inject so much goofball show and so little warmth into the creature that is, in fact, Pinocchio's spiritual guide and conscience. Queer Quotient: Nothing of any note to the community. Even elderly bachelor Geppetto - who is also a single parent - is given a female love interest."
},
		{
			"rate":5,
			"text": "This movie is a travesty. A piece of the original Pinocchio died with the creation of this film."
},
		{
			"rate":5,
			"text": "hate this movie! it was completely boring"
},
		{
			"rate":40,
			"text": "loved it! my daughter even more with JTT in it! lol"
},
		{
			"rate":15,
			"text": "...........................thank you that is all."
},
		{
			"rate":40,
			"text": "This is a beautiful Pinochio Movie. It makes you gasp, lol, and smile at the end."
},
		{
			"rate":20,
			"text": "Pinocchio freaks me out!! Looks well wierd!"
},
		{
			"rate":35,
			"text": ":fresh: [CENTER]Nice, but it's far from the Disney classic.[/CENTER]"
},
		{
			"rate":15,
			"text": "Really bad. Jonathan Taylor Thomas should have been aborted."
},
		{
			"rate":30,
			"text": "Interesante... prefiero dead silence!"
},
		{
			"rate":30,
			"text": "I like this Movie, finally I could watch it from the beginning, it is really funny. I always enjoy to see Jonathan Taylor Thomas `s Movies when he was a small boy, he was really cute, already adored him in the Show Home Improvement with Tim Allen. He was really funny in that Show. A great Movie for all Pinocchio Fans. I was so lost in the story that I was really surprised when Pinocchio turned in to this Sweet little Boy Jonathan Thomas Taylor. Great Movie. Just to get straight. There is nothing creepy in this Movie, it is a Family Movie. There are different version of Pinocchio and one of them is creepy it is called Pinocchio`s Revenge but not this."
},
		{
			"rate":50,
			"text": "an classic now a cool actor bets 2 be in an remade movie"
},
		{
			"rate":45,
			"text": "JTT! This was a great adaptation of the cartoon and it helped that JTT was in it."
},
		{
			"rate":20,
			"text": "Cute movie. The boy looked more terrifying than fun."
},
		{
			"rate":25,
			"text": "Flaw: heh heh, quite retarded."
},
		{
			"rate":50,
			"text": "I LUV THIS MOVIE SINCE I WAS LITTLE"
},
		{
			"rate":15,
			"text": "this was terrible only watched about 20 mins of it"
},
		{
			"rate":50,
			"text": "i loved this movie when I was little!!!!"
},
		{
			"rate":35,
			"text": "Poor thing, he cannot tell a lie and get away with it. His Nose Grows."
},
		{
			"rate":30,
			"text": "This is a surprisingly enjoyable spin on the classic tale of the wooden puppet who dreams of being a real boy. Fresh, funny, and well-acted, this film keeps the attention of both young and old. A great one to watch if you're tired of the Disney version."
},
		{
			"rate":40,
			"text": "I loved this movie as a kid. So i would say it's a good kids movie.."
},
		{
			"rate":35,
			"text": "Oh, I feel like I've seen this. OHMIGOSH Jonathan Taylor Thomas?! Using the Mufasa Principle, it must be good."
},
		{
			"rate":40,
			"text": "gotta luv pinoocchio!"
},
		{
			"rate":50,
			"text": "this film i absolutley awsome it is so much better than the disney cartoon version i urge people to watch it as it is one of the best films ever"
},
		{
			"rate":50,
			"text": "Its a great movie for those who like hreos"
},
		{
			"rate":-1,
			"text": "Geppetto is a lonely puppetmaker whose wooden creatures are the only family he knows. Years ago, he and his sweetheart carved their names into an old pine tree in Hidden Valley. But Geppetto was unable to declare his love, and she married his brother..."
},
		{
			"rate":25,
			"text": "I LIKE THIS MOVIE BUT DAMN PINOCCHIO KINDA MAKES U MAD CAUSE HE'S SO DAMN STUPID."
},
		{
			"rate":40,
			"text": "I've seen a number of movies featuring Pinocchio, and as I could tell with the plot and story, this one's the best that I've ever seen. It's the live-action version featuring real actors. The Adventures Of Pinocchio tells the story of Geppetto (Martin Landau) carving, from a tree, a puppet named Pinocchio. The puppet comes to life and looks for fun while traveling out in the world that was not familiar to him- all part of the quest to become a real human boy. It's nice to see Jonathan Taylor Thomas (remember Home Improvement?) playing the titular marionette character, and when I first saw the evil Lorenzini, I didn't knew that I first saw Rob Schneider years before recognizing him as a male gigolo, an animal, and a hot chick. Also, there's a cricket character who's his conscience, and his name's Pepe. This is the best adaptation of the popular children's story of Pinocchio just yet, and I give this movie four stars."
},
		{
			"rate":30,
			"text": "I've seen this, it's cute & funny & good."
},
		{
			"rate":40,
			"text": "A masterful master piece brought to life."
},
		{
			"rate":25,
			"text": "Seen it mediocre yet creepy looking doll!"
},
		{
			"rate":30,
			"text": "I actually prefer this one over the animated one just because it was just better for me I dont know what it was but I just would pick this one over the other anyday."
},
		{
			"rate":30,
			"text": "It's okay. I used to be afriad of the whale. Cute film."
},
		{
			"rate":30,
			"text": "Cute but strange in good and bad ways."
},
		{
			"rate":30,
			"text": "This movie is all right."
},
		{
			"rate":-1,
			"text": "I saw this movie years ago and from what i can remember the wooden boy creeped me out."
},
		{
			"rate":50,
			"text": "great movie. w a moral to it and probably allegory too based on the book by Lodi"
},
		{
			"rate":20,
			"text": "This verison is creepy, it was like children of corn with Pinocchio."
},
		{
			"rate":35,
			"text": "I luv Jonathan Taylor Thomas!!!!"
},
		{
			"rate":35,
			"text": "Not the best Pinocchio story I've laid eyes on, but still pretty good."
},
		{
			"rate":20,
			"text": "I liked the cartoon much better."
},
		{
			"rate":45,
			"text": "This is the one movie that keeps gushing tears from my every time i watch it and every minute watched also...love it!"
},
		{
			"rate":20,
			"text": "not really a good movie"
},
		{
			"rate":5,
			"text": "Hate it, hate it, hate it, hate it."
},
		{
			"rate":5,
			"text": "Ok guys this movie was way too long and really boring"
},
		{
			"rate":10,
			"text": "HAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHA"
},
		{
			"rate":20,
			"text": "No, the real three boy don't suceeds that's much, wich the Wallt Disney film do,"
},
		{
			"rate":10,
			"text": "Overdone, and yet underdone at the same time - its charms don't make up for its annoyances."
},
		{
			"rate":40,
			"text": "Its a good family movie"
},
		{
			"rate":10,
			"text": "I've seen worse. I really have."
},
		{
			"rate":40,
			"text": "This was a great adaptation of a Disney classic."
},
		{
			"rate":30,
			"text": "It was good to see it as a real movie."
},
		{
			"rate":50,
			"text": "Lay off my friend woody!!!"
},
		{
			"rate":50,
			"text": "awwwwww i wishonly it was true"
},
		{
			"rate":45,
			"text": "A good movie for kids and has a lesson to teach kids so it is an overall good movie"
},
		{
			"rate":20,
			"text": "Very creepy, much like I'm sure the real legend was."
},
		{
			"rate":20,
			"text": "Disney one is so much better..."
},
		{
			"rate":-1,
			"text": "I didnt like the cartoon one so I probably wont like this one!"
},
		{
			"rate":10,
			"text": "not bad disney one was miles better"
},
		{
			"rate":25,
			"text": "I liked this when I was a kid, but when I rewatched it a year ago... it wasn't that great."
},
		{
			"rate":15,
			"text": "this pinocchio aqlways scared the crap out of me"
},
		{
			"rate":25,
			"text": "The cartoon is vastly superior."
},
		{
			"rate":40,
			"text": "wow this movie is so cute"
},
		{
			"rate":25,
			"text": "seen it a dozen time on tv.... its ok and there a sum parts i like about it....but its still just a copy of the original cartoon one...which i dn't like"
},
		{
			"rate":40,
			"text": "It was okay ~ but a good movie"
},
		{
			"rate":40,
			"text": "i love jonathon taylor thomas (L)"
},
		{
			"rate":50,
			"text": "love dis film i is well kl i don't care what any1 says lol"
},
		{
			"rate":35,
			"text": "Jonathan Taylor Thomas, need I say more? It was a really cute movie."
},
		{
			"rate":30,
			"text": "I rally liked this one"
},
		{
			"rate":50,
			"text": "Love this film, one of my all timers!"
},
		{
			"rate":40,
			"text": "who wouldn't like it...?"
},
		{
			"rate":-1,
			"text": "OMG!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
},
		{
			"rate":35,
			"text": "This was a cute movie."
},
		{
			"rate":35,
			"text": "kinda kiddish dontcha think"
},
		{
			"rate":30,
			"text": "Aww, this was scary!! Only because of the puppet thing...I'm scared of those things"
},
		{
			"rate":35,
			"text": "Oh I remember this movie! I completely forgot about it. It was a decent movie but it won't ever beat the original Disney Pinocchio."
},
		{
			"rate":30,
			"text": "Not great, not terrible, kindo middle of the road."
},
		{
			"rate":25,
			"text": "[font=Tahoma][color=lightblue]The movie version of the classic story of the wooden boy who comes to life and wants to become a real boy. I think Martin Landau was great as Geppeto and I guess that JTT made a good Pinocchio. The movie followed the story pretty well, although i missed Jimminy Cricket. LOL. The puppetier work was great and i thought it was very realistic. [/color][/font]"
},
		{
			"rate":25,
			"text": "eh. It is a better live adaptation of the classic."
},
		{
			"rate":40,
			"text": "I used to watch this movie over and over again :) Really entertaining."
},
		{
			"rate":20,
			"text": "its unbelievable what some filmmakers will do. THERE CANNOT BE A PINOCCHIO 2!! what were they thinking? this ...(shudder) sequel just ruins the originality of they 1940 animated flick. the sets are magnificent, but the acting of some and the storyline is to be ashamed of. only jonathan taylor thomas' presence and the wonderful setting design saves this film. and they almost miss that."
},
		{
			"rate":-1,
			"text": "I didn't like it.... ummmm ok"
},
		{
			"rate":30,
			"text": "Great for a childerns movie"
},
		{
			"rate":40,
			"text": "A lovely adaptation of a masterfully written script from a much loved and adored children's classic."
},
		{
			"rate":-1,
			"text": "again, stop trying to recreate disney movies"
},
		{
			"rate":5,
			"text": "argh get it away frm meeeeeee"
},
		{
			"rate":15,
			"text": "always kinda creeped me out a bit.."
},
		{
			"rate":10,
			"text": "This one just creeped me out."
},
		{
			"rate":35,
			"text": "I never really appreciated Pinocchio until I saw this version."
},
		{
			"rate":45,
			"text": "i love the walk to anna!!!!! thats why we r soul mates!! hahaha"
},
		{
			"rate":5,
			"text": "i think i remember watching this - if i rememebr corectly is scared the hell out of me! - i was only little..."
},
		{
			"rate":35,
			"text": "A good movie,with a good new twist on a classic!"
},
		{
			"rate":30,
			"text": "If you've seen the original Disney cartoon based on an Italian story about a wooden puppet who becomes a real boy, then you'll probably know the story already. This is basically same story again but just a few special effect and few extra things. Martin Landau as Geppetto is a nice guy, although you don't see him a lot. The kid who plays Pinocchio is just weird and idiotic, I mean, first he doesn't know what's going on, and suddenly he has ideas of his own about things. Adequate, although it's okay for kids."
},
		{
			"rate":10,
			"text": "I LOVE JTT! HE IS SO HOTT! (Well when he is older)"
},
		{
			"rate":10,
			"text": "Worst idea ever made into the worst movie ever. Live action pinocchio with JTT?"
},
		{
			"rate":20,
			"text": "Hahah Pinnocchio never did make it very big."
},
		{
			"rate":30,
			"text": "great movie.. not to miss out... nice to watch this when there you have nothing to look foward to.."
},
		{
			"rate":30,
			"text": "oooh JTT, what happened to you?"
}	]
}