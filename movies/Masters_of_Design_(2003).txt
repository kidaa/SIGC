{
	"title": "Masters of Design (2003)",
	"synopsis":"MASTER OF DESIGN DVD1 Interviews with the best Designers in the world with never-seen-before-behind-the-scenes footage of their workshop and collections. 1 PARIS Christian Liaigre - Architect & Interior designer to Karl Lagerfeld, Marc Jacobs, Ralph Lauren, Rupert Murdoch, building houses on Bora Bora...Christian Liaigre combines French tradition with eastern Fung Shui. 2 LONDON Anoushka Hempel - This British design powerhouse includes fashion, interiors and now commercial spaces [for Van Cleef & Arpels etc...] in her repertoire. 3 PARIS Andree Putman - Legendary designer/stylist - interior designer for Morgans in NY ,incubator of Montana, Mugler, Miyake. 4 SYDNEY Engelen Moore- Moore worked on Norman Foster's HK Shaghai Bank and was awarded by Jean Nouvel. 5 NEW YORK Juan-Pablo Molyneaux - Interior designer to multi-billionaires 6 SYDNEY Glen Murkutt Internationally awarded architect 7 PARIS Jacques Garcia - Interior designer to the Sultan of Brunei 8 LONDON John Stefanidis - Superstar Interior designer - currently working on Architect I. M . Pei?s only London commission. 9 MILAN Angelica Frescobaldi - Italo-British Interior Design 10 SYDNEY George Freeman -ex-Knoll [US] Interior Designer THE MASTERS OF DESIGN is part of the Stylist TV Program - The Stylist is a who's who of the cutting edge of Fashion, Jewellery, Interiors Architecture, Gastronomy and Art. Producer Kostas Metaxas describes the Stylists as a mixture of VANITY FAIR and LIFESTYLES OF THE RICH & FAMOUS. It is a Luxury Travel experience, as it travels to the major cities of the world and seeks out the most creative talent from the vast field of the applied [luxury] arts.",
	"genre":
	[
		{"name":"Documentary"}
	],
	"rate":0,
	"comments":[
	]
}