{
	"title": "Heidi (1993)",
	"synopsis":"This Disney-produced made-for-TV version of the classic children's tale features Noley Thornton as Heidi, the plucky girl from the Alps, with Jane Seymour as a mean-spirited governess and Jason Robards as Heidi's kindly grandfather. ~ Mark Deming, Rovi",
	"genre":
	[
		{"name":"Drama"},
		{"name":"Kids & Family"}

	],
	"rate":3.2,
	"comments":[
		{
			"rate":35,
			"text": "One of my favourite children's story books. Heidi is an orphan manipulated by her cousin. She is sent to live with her grandfather, then taken to be a companion of a wheel chair bound girl in the city. Heart warming movie."
},
		{
			"rate":35,
			"text": "Sweet version of oft told tale. Robards is crusty perfection as Grandfather, Sian Phillips is a warm and wonderful Grandmama and her clothes are eye-popping. Jane Seymour starts out a perfect monster as Fraulein Rottenmeir but manages to humanize her character somewhat, she never becomes likable but not the stick figure martinet the character is usually portrayed. The rest of the cast is uniformly enjoyable and the scenery, both indoors and out, is gorgeous!"
},
		{
			"rate":30,
			"text": "Loooong! Over three hours."
},
		{
			"rate":40,
			"text": "Aw. This movie melts my heart a little bit with it's sappy sweetness."
},
		{
			"rate":50,
			"text": "A classic story by Johanna Spyri. Haven't watched it for awhile. I read the book at least once a year. I got the book from one of my uncles for Christmas one year and have read it over and over."
},
		{
			"rate":50,
			"text": "This is one of those movies I can watch often. This particular Heidi I like better than most of the other movies."
},
		{
			"rate":45,
			"text": "I love this children's tale! Especially this specific movie! I know the Shirley Temple is the most known. This the vest though! It is a good storyline!"
},
		{
			"rate":50,
			"text": "It is an excellent movie and is beautifully acted. Noley Thorton is wonderful as Heidi and Lexi Randall's performance as Clara is amazing. Jason Robards is great as grandfather and Jane Seymour is excellent, character you love to hate."
},
		{
			"rate":40,
			"text": "I' VE ONLY SEEN THE SHIRLY TEMPLE ONE"
},
		{
			"rate":35,
			"text": "i loved this story and the movie as a kid."
},
		{
			"rate":35,
			"text": "One of my favourite children's story books. Heidi is an orphan manipulated by her cousin. She is sent to live with her grandfather, then taken to be a companion of a wheel chair bound girl in the city. Heart warming movie."
},
		{
			"rate":50,
			"text": "I really love and will Treasure this Movie called Heidi forever in my heart!!!! Peace out!!!!_Matt Burden"
},
		{
			"rate":30,
			"text": "Loooong! Over three hours."
},
		{
			"rate":50,
			"text": "this is a classical story that can make everyone to feel happiness:X:XX:"
},
		{
			"rate":40,
			"text": "aw so sad i cried more than once."
},
		{
			"rate":35,
			"text": "Sweet version of oft told tale. Robards is crusty perfection as Grandfather, Sian Phillips is a warm and wonderful Grandmama and her clothes are eye-popping. Jane Seymour starts out a perfect monster as Fraulein Rottenmeir but manages to humanize her character somewhat, she never becomes likable but not the stick figure martinet the character is usually portrayed. The rest of the cast is uniformly enjoyable and the scenery, both indoors and out, is gorgeous!"
},
		{
			"rate":25,
			"text": "It reminds me of the old animation. The movie is similar to Missing in America in which a kid changes the life of an old person living alone in the mountain, but this one has a happy ending."
},
		{
			"rate":5,
			"text": "Absolutely wretched. Very bad acting and waaaay too long."
},
		{
			"rate":45,
			"text": "I love this children's tale! Especially this specific movie! I know the Shirley Temple is the most known. This the vest though! It is a good storyline!"
},
		{
			"rate":30,
			"text": "A decent Disney film. I haven't watched this since I was a kid but I remember enjoying it. On of the few Disney films that was not animation. A solid story and acting to what I remember. Good decent watch for kids and the family to watch."
},
		{
			"rate":-1,
			"text": "the book was gr8!!!!!!want 2 c the movie."
},
		{
			"rate":50,
			"text": "I really love and will Treasure this Movie called Heidi forever in my heart!!!! Peace out!!!!_Matt Burden"
},
		{
			"rate":50,
			"text": "Love this version of Heidi"
},
		{
			"rate":40,
			"text": "I' VE ONLY SEEN THE SHIRLY TEMPLE ONE"
},
		{
			"rate":40,
			"text": "A sweet story of a girl who has many adventures, a little sad in parts of it but it is a good movie"
},
		{
			"rate":40,
			"text": "Aw. This movie melts my heart a little bit with it's sappy sweetness."
},
		{
			"rate":25,
			"text": "this was interesting"
},
		{
			"rate":-1,
			"text": "I love the story of Heidi!"
},
		{
			"rate":-1,
			"text": "No thankyou - Not interested"
},
		{
			"rate":50,
			"text": "Jason Robards....the eyes of a grandfather"
},
		{
			"rate":50,
			"text": "Heartbreaking movie...just to cute to not like it."
},
		{
			"rate":25,
			"text": "I haven't seen it FOREVER"
},
		{
			"rate":-1,
			"text": "I own it, so I might as well see it once."
},
		{
			"rate":5,
			"text": "omg that movie was freakyyyyy"
},
		{
			"rate":40,
			"text": "HAI I LIKE THIS MOVIE and also the book."
},
		{
			"rate":45,
			"text": "I love this movie! Seen since I was little."
},
		{
			"rate":35,
			"text": "Really good. Sad parts."
},
		{
			"rate":-1,
			"text": "I saw the Shirley Temple version"
},
		{
			"rate":35,
			"text": "i loved this story and the movie as a kid."
},
		{
			"rate":10,
			"text": "It just not the same classic...."
},
		{
			"rate":50,
			"text": "A classic story by Johanna Spyri. Haven't watched it for awhile. I read the book at least once a year. I got the book from one of my uncles for Christmas one year and have read it over and over."
},
		{
			"rate":20,
			"text": "Not my pick, but I saw it and lived through the ordeal. Never again though."
},
		{
			"rate":50,
			"text": "my fave movie nung bata pa ako...i cried here..."
},
		{
			"rate":50,
			"text": "I love this Heidi!!! It's much better than the older one with shirley Temple. Peter the goat boy is so cute."
},
		{
			"rate":50,
			"text": "SO HEARTTOUCHING!!!!"
},
		{
			"rate":-1,
			"text": "I used to watch the show but the movie... i'm not so sure about it"
},
		{
			"rate":5,
			"text": "i do not like heidi movies!!"
},
		{
			"rate":-1,
			"text": "I love this classic!!!"
},
		{
			"rate":40,
			"text": "Reminds me of my grandfather"
},
		{
			"rate":-1,
			"text": "didnt care 4 it as a kid n still dont"
},
		{
			"rate":50,
			"text": "I ABSOULUTELY LUV THIS MOVIE WHEN I WAS YOUNGER BUT I STILL LIKE IT AND I EVEN HVE THE STORY BOOK AST ONE OF MY FAV BOOK"
},
		{
			"rate":40,
			"text": "this used to be, like my fave. movie when i was like 7"
},
		{
			"rate":50,
			"text": "tjsa wat zal ik erover zeggen... super film om te kijken toen ik vroeger ziek was ;)"
},
		{
			"rate":50,
			"text": "my all time favorite"
},
		{
			"rate":-1,
			"text": "How can you do Heidi without Shirley Temple?! I'm upset that they are trying to redo shirley temple, no one can come close to shirley."
},
		{
			"rate":30,
			"text": "One of the 1,000 different versions is still warm and fuzzy."
},
		{
			"rate":-1,
			"text": "love that little girl"
},
		{
			"rate":45,
			"text": "All of these movie like this one remind me of my self"
},
		{
			"rate":50,
			"text": "i love this movie its another classic but this is the only good version"
},
		{
			"rate":45,
			"text": "It's a heartwarming tale."
},
		{
			"rate":35,
			"text": "This is an old movie i love it!!"
},
		{
			"rate":35,
			"text": "yes, im a shirley temple fan. this is a good movie, though, and a good book."
},
		{
			"rate":50,
			"text": "This movie made me cry."
},
		{
			"rate":50,
			"text": "so pretty much i have never watched this movie without crying. i love it."
},
		{
			"rate":50,
			"text": "the first movie to ever make me cry"
},
		{
			"rate":35,
			"text": "favorite to watch with my sisys when i was little!!"
},
		{
			"rate":-1,
			"text": "only seen it oncee cant remember"
},
		{
			"rate":-1,
			"text": "Nobody, do you hear me? Nobody can ever, ever, replace Shirley Temple as Heidi. It could never be done. Why do they even try?"
},
		{
			"rate":35,
			"text": "old and good family movie"
},
		{
			"rate":10,
			"text": "my sister has this film... not my kinda thing"
},
		{
			"rate":45,
			"text": "my nephew loved this movie"
},
		{
			"rate":5,
			"text": "Waste of TIME!! Very Weak movie, DO NOT WATCH, if you do end up watching it, side effects may include: Suicide, murder ramapages, road ramapage, any kinds of ways of harming yourself, and many others..."
},
		{
			"rate":-1,
			"text": "i wanted to see this now i have at school"
},
		{
			"rate":30,
			"text": "freaky cos' she speaks irish, but quite a good film"
},
		{
			"rate":30,
			"text": "I want to chat with you please allow me"
},
		{
			"rate":40,
			"text": "All that I remember is that the old man scared me."
},
		{
			"rate":-1,
			"text": "I had a friend named Heidi."
},
		{
			"rate":45,
			"text": "Love Heidi love her story"
},
		{
			"rate":50,
			"text": "I didnt mean this version i mean the latest 1 with the girl from in america in it but heidi is a very good film"
},
		{
			"rate":40,
			"text": "A Classic, how can anyone hate Heidi?"
},
		{
			"rate":40,
			"text": "HEidi :) no wonder its good lol"
},
		{
			"rate":45,
			"text": "Heartfelt story of a grandfather hardened by life and softened by the love af a grandchild."
},
		{
			"rate":35,
			"text": "It was worth preempting the football game!"
},
		{
			"rate":30,
			"text": "this is a classic movie...althoug i did enjoy the book more"
},
		{
			"rate":-1,
			"text": "i hav to see the movie loved the book soo much it was brill"
},
		{
			"rate":35,
			"text": "When I was a child, I used to watch this like all the time with ma mom, I really liked it. Now I barely remember it,'couse I havn't seen it in years. But I give this a 3 and a half just becouse of how I remember it."
},
		{
			"rate":50,
			"text": "This is one of those movies I can watch often. This particular Heidi I like better than most of the other movies."
},
		{
			"rate":50,
			"text": "Also a really good classic. If you haven't seen it, go rent it or buy it TODAY!!"
},
		{
			"rate":-1,
			"text": "hmm i have a family dinner tonight, THIS SOUNDS LAME"
},
		{
			"rate":50,
			"text": "cuz its wicked n sweet"
},
		{
			"rate":50,
			"text": "It is an excellent movie and is beautifully acted. Noley Thorton is wonderful as Heidi and Lexi Randall's performance as Clara is amazing. Jason Robards is great as grandfather and Jane Seymour is excellent, character you love to hate."
},
		{
			"rate":50,
			"text": "Heidi is actually one of the best books that I've ever read so I expected a lot from this movie. The performances from everyone were just great, I especially liked the man who played the Grandpa. I think that this movie stayed loyal to the book, and I liked how it was really long, and they didn't edit it to make it shorter. There wasn't much more that I could have asked from this movie and I wasn't dissapointed."
},
		{
			"rate":50,
			"text": "i used to love this movie so much...i still do, its so cute!! i need to watch it again"
},
		{
			"rate":-1,
			"text": "hmmm... looks intresting!!! lol"
}	]
}