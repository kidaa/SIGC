{
	"title": "The Railway Children (1970)",
	"synopsis":"British character actor Lionel Jeffries both directed The Railway Children and adapted its screenplay from the novel by E. Nesbit. Dinah Sheridan plays the mother of three children who must live in reduced circumstances when her husband (Ian Cuthbertson), a government official, is arrested on a false charge of treason. The kids adapt rather well to their new environment, a community located on the edges of a railway. They befriend a kindly porter (Bernard Cribbins) and a wealthy gent (William Mervyn), both of whom strive to prove their father's innocence. ~ Hal Erickson, Rovi",
	"genre":
	[
		{"name":"Drama"},
		{"name":"Television"}
,
		{"name":"Kids & Family"}
,
		{"name":"Art House & International"}

	],
	"rate":3.6,
	"comments":[
		{
			"rate":40,
			"text": "A classic British Children's story brought to life in this 1970's adaptation, which itself has become a bit of a classic in its own right. To be honest I was unfamiliar with the story until I saw a brilliant theatre production of this on the unused platform in London's Waterloo station. Now, I liked it but the story is a little dated, it's definitely posh kids literature and when you break down the story and writing it is a little insulting at times - I guess it's just a little old-fashioned but totally forgivable. There are some wonderful ideas in the story and as corney as it might be, it's full of tear-jerking moments and quite famous for it. It's not usually my kind of thing but I'd be lying if I said I didn't really enjoy it. Directed by the much loved and sadly missed Lionel Jeffries."
},
		{
			"rate":35,
			"text": "This nearly perfect cinematic rendition of Edith Nesbit's popular children's novel follows the lives of Roberta (Bobbie), Phyllis, and Peter, and their mother, after their father is unfairly accused of treason and sent to prison. They go to live in an almost uninhabitable house in the country which stands near a railway line ? mum writes stories to make enough money for food and candles, while the children spend much of their time around the railway station and, specifically, waving to one particular train to 'send their love to father'.Always an involving and clever novel, the characters are here brought to life under the perceptive direction of Lionel Jeffries (better known as a fine character actor). Jenny Agutter plays Bobbie, while Sally Thomsett and Gary Warren are her sister and brother. Their mother is Dinah Sheridan, while the other memorable characters are played by Bernard Cribbins (Perks the railway-man) and William Mervyn (the old gentleman on the train).'The Railway Children' is gentle entertainment from another age, but does its job beautifully. As we watch Bobbie grow up with the worries of an absent parent jostling against her own needs both to be alone and to have fun, we can only rejoice when events come together at the close of the picture. Throughout we have a sense of time and place ? be it from the steam trains, the university paper chase, or the red flannelette petticoats worn by the girls (and used to avert disaster!)."
},
		{
			"rate":40,
			"text": "The Railway Children is a bit slow at first, but it picks up pace and soon is an enjoyable affair that is well acted and witty. Jenny Agutter and Sally Thomsett are highlights, but Lionel Jeffries's The Railway Children would not be complete without the wonderful Bernard Cribbins as Perks."
},
		{
			"rate":45,
			"text": "A forgotten gem, this is a heartwarming, engaging, emotional family film, and a true testament to 'They Just Don't Make 'Em Like They Used To!'"
},
		{
			"rate":20,
			"text": "the story is about, kids, railways, poor, injustice of the father, not really sure and I don't really care"
},
		{
			"rate":35,
			"text": "Somehow, the quaint English mannerisms and insistent goodwill to all of god's creatures did not make me want to puke."
},
		{
			"rate":50,
			"text": "One of the best films ever made. An unequivocally charming story, superbly cast and magnificently filmed. A quintessentially 'English' film depicting an snapshot of a bygone time of innocence, camaraderie and goodwill to your fellow man. 10/10"
},
		{
			"rate":20,
			"text": "The movie had a nice idea of a father being wrongly imprisoned and the family falling into despair but it was quite unintresting"
},
		{
			"rate":50,
			"text": "A beautiful Film That i enjoy always :)"
},
		{
			"rate":50,
			"text": "poignant, nostalgic, innocent, moving. the type of childhood you didn't get, but you wish you had. two scenes in this movie i defy you not to cry at... bobby's birthday and of course when the mist clears at the station. jenny agutter wonderful and serene throughout"
},
		{
			"rate":35,
			"text": "[font=Tahoma]Full review to come.[/font]"
},
		{
			"rate":10,
			"text": "You'd think that a film that starts with a bunch of upper class snobs having to move to Yorkshire and live in poverty would be a decent story however by the time that the first night in their ramshckle cottage was over then the silverware was out. This is pretty much how the film continues; it's all way too nice. Plus, I'm not sure I'll ever be able to watch Walkabout or An American Werewolf in London in the same light again now that I know what an ugly child Jenny Agutter was."
},
		{
			"rate":40,
			"text": "The Railway Children is a bit slow at first, but it picks up pace and soon is an enjoyable affair that is well acted and witty. Jenny Agutter and Sally Thomsett are highlights, but Lionel Jeffries's The Railway Children would not be complete without the wonderful Bernard Cribbins as Perks."
},
		{
			"rate":45,
			"text": "A forgotten gem, this is a heartwarming, engaging, emotional family film, and a true testament to 'They Just Don't Make 'Em Like They Used To!'"
},
		{
			"rate":20,
			"text": "the story is about, kids, railways, poor, injustice of the father, not really sure and I don't really care"
},
		{
			"rate":40,
			"text": "Told through the naive and innocent eyes of 3 children, The Railway Children is still one of the most beloved books Britain. The film adaptation, directed by the actor Lionel Jeffries in his directorial debut, is like To Kill A Mockingbird in its innocence through serious subject matter being a backdrop. The 3 children are superb in their roles (the actresses were much older than their characters were), and the playful innocence of the tiny adventures they go on are lovingly imaginative. And the last curtain call segment? Magnificent stuff."
},
		{
			"rate":35,
			"text": "Somehow, the quaint English mannerisms and insistent goodwill to all of god's creatures did not make me want to puke."
},
		{
			"rate":30,
			"text": "It's dated, it's more than slightly twee, and it's very tame compared to more modern takes on splintered families. But it's genuine, heartfelt and honest. I can't believe I hadn't seen it before. I stayed with it, enjoying the scenes with Bernard Cribbins and looking forward to the inevitable happy ending. But even I was floored by Daddy, my Daddy. How come it's so dusty in our living room?"
},
		{
			"rate":35,
			"text": "Where was this movie when I was a kid?"
},
		{
			"rate":40,
			"text": "This is a gentle film, and it's unusual to hear accents like that nowadays. The most well-known action scene is actually quite understated rather than being a big set piece, but it works well. The film is a decent adaptation of Nesbit's book, even though they had to chop some scenes out, and it's similar to The New Treasure Seekers with the theme of genteel poverty. The children make mistakes, usually with good intentions, but they then have to face the consequences of their actions, and that sets it above several other films (e.g. Son of Rambow)."
},
		{
			"rate":40,
			"text": "Jenny Agutter and Bernard Cribbins star in this enduring and delightfully lovely big screen adaptation of Edith Nesbit's classic children's story."
},
		{
			"rate":50,
			"text": "Saw it when it first came out in 1970, and was blown away then and it can still get me. Can't always wacth it BECAUSE it's so good, and the scene on the platform...Daddy, my daddy!! still gets me. Thank you Lionel Jeffries!!"
},
		{
			"rate":50,
			"text": "The best feel good movie."
},
		{
			"rate":40,
			"text": "A classic British Children's story brought to life in this 1970's adaptation, which itself has become a bit of a classic in its own right. To be honest I was unfamiliar with the story until I saw a brilliant theatre production of this on the unused platform in London's Waterloo station. Now, I liked it but the story is a little dated, it's definitely posh kids literature and when you break down the story and writing it is a little insulting at times - I guess it's just a little old-fashioned but totally forgivable. There are some wonderful ideas in the story and as corney as it might be, it's full of tear-jerking moments and quite famous for it. It's not usually my kind of thing but I'd be lying if I said I didn't really enjoy it. Directed by the much loved and sadly missed Lionel Jeffries."
},
		{
			"rate":35,
			"text": "This nearly perfect cinematic rendition of Edith Nesbit's popular children's novel follows the lives of Roberta (Bobbie), Phyllis, and Peter, and their mother, after their father is unfairly accused of treason and sent to prison. They go to live in an almost uninhabitable house in the country which stands near a railway line ? mum writes stories to make enough money for food and candles, while the children spend much of their time around the railway station and, specifically, waving to one particular train to 'send their love to father'.Always an involving and clever novel, the characters are here brought to life under the perceptive direction of Lionel Jeffries (better known as a fine character actor). Jenny Agutter plays Bobbie, while Sally Thomsett and Gary Warren are her sister and brother. Their mother is Dinah Sheridan, while the other memorable characters are played by Bernard Cribbins (Perks the railway-man) and William Mervyn (the old gentleman on the train).'The Railway Children' is gentle entertainment from another age, but does its job beautifully. As we watch Bobbie grow up with the worries of an absent parent jostling against her own needs both to be alone and to have fun, we can only rejoice when events come together at the close of the picture. Throughout we have a sense of time and place ? be it from the steam trains, the university paper chase, or the red flannelette petticoats worn by the girls (and used to avert disaster!)."
},
		{
			"rate":50,
			"text": "One of the best films ever made. An unequivocally charming story, superbly cast and magnificently filmed. A quintessentially 'English' film depicting an snapshot of a bygone time of innocence, camaraderie and goodwill to your fellow man. 10/10"
},
		{
			"rate":20,
			"text": "The movie had a nice idea of a father being wrongly imprisoned and the family falling into despair but it was quite unintresting"
},
		{
			"rate":50,
			"text": "A beautiful Film That i enjoy always :)"
},
		{
			"rate":-1,
			"text": "The Railway Children is a 1970 British film based on the novel by E. Nesbit."
},
		{
			"rate":50,
			"text": "well umn just seen this movie 4 the 1st time n think that this is a good movie 2 watch...its got a good cast of actors/actressess throughout this movie..i think that both dinah sheridan n bernard cribbins play good parts thoroughout this movie...i think that the director of this drama/tv/art house/international movie had done a good job of directing this movie because you never know what 2 expect throughout this movie..its a good movie 2 watch n its enjoyable"
},
		{
			"rate":30,
			"text": "A simple war time film that looks at the events surounding a couple of children and the railway tha they visit every day."
},
		{
			"rate":-1,
			"text": "really wana see this"
},
		{
			"rate":50,
			"text": "poignant, nostalgic, innocent, moving. the type of childhood you didn't get, but you wish you had. two scenes in this movie i defy you not to cry at... bobby's birthday and of course when the mist clears at the station. jenny agutter wonderful and serene throughout"
},
		{
			"rate":45,
			"text": "A classic kids film I can watch again and again. Helps I had a crush on Jenny Agutter I guess."
},
		{
			"rate":15,
			"text": "This is boring no matter what my parents tell me"
},
		{
			"rate":45,
			"text": "a true classic, everyone has to see it"
},
		{
			"rate":50,
			"text": "i love this film its a about the children whose dad is a doctor and he gets taken away and the mum sells her house and they go live in a country side and then they go on a journey and it has a happy ending as well"
},
		{
			"rate":25,
			"text": "exceeded my expectations. i've been putting off watching this for ages because i was assuming it was going to be a boring and dry childrens film. it was easily a family film and moved along at a comfortable pace. the whole story about the three children and their adventures near the railway is delightful viewing. wow, i sound like an old woman"
},
		{
			"rate":30,
			"text": "350 ratings? And this is a classic as well !! :O I dont believe it. Heart-warming film!"
},
		{
			"rate":-1,
			"text": "Saw this years ago - it was a great family-type movie, something that seems increasingly rare these days!"
},
		{
			"rate":30,
			"text": "its realy good its funnie but sad at the same time"
},
		{
			"rate":25,
			"text": "this is a classic film and still brings a tear to my eye"
},
		{
			"rate":40,
			"text": "A memorable and deeply moving family movie."
},
		{
			"rate":50,
			"text": "Makes me cry every time. A childhood favourite..."
},
		{
			"rate":40,
			"text": "A children's classic for all to see. I saw Jenny Agutter in the London West-End version of Peter Shaffer's 'Equus' recently. She looks alot younger here!"
},
		{
			"rate":50,
			"text": "I loved the book and I love the film!"
},
		{
			"rate":50,
			"text": "I cry every time!! i love it!!!!!!"
},
		{
			"rate":50,
			"text": "A truly heart warming Sunday afternoon film when you are feeling a little worse for ware! Also a few tear jerker moments when Bobby runs to met her father at the platform throught the steam of the train!"
},
		{
			"rate":-1,
			"text": "This might be another good one . . ."
},
		{
			"rate":45,
			"text": "Another Childhood Fav"
},
		{
			"rate":45,
			"text": "The best family fim ever"
},
		{
			"rate":-1,
			"text": "looks dumb even though i cant see the pic right now"
},
		{
			"rate":30,
			"text": "It really old but cool."
},
		{
			"rate":40,
			"text": "it is gd but quite long but still gd it is my mums fav film"
},
		{
			"rate":45,
			"text": "Towards the end when thier father comes home still makes me cry no matter how many times i watch it"
},
		{
			"rate":50,
			"text": "it doesn't matter how many times I've seen it - the end still makes me cry!"
},
		{
			"rate":40,
			"text": "great story, nice christmas season film"
},
		{
			"rate":50,
			"text": "A good traditional british film."
},
		{
			"rate":50,
			"text": "Never fails to make me cry!"
},
		{
			"rate":40,
			"text": "this is a wkd movie i still watch it now wen its cums on da telly"
},
		{
			"rate":50,
			"text": "YAAAAAAAAAAAAY. I cry every damn time. Amazing."
},
		{
			"rate":35,
			"text": "i been to the place where it was made"
},
		{
			"rate":40,
			"text": "Aww, this is lovely. Not sure I've ever seen it all the way through, but it's on telly ever Christmas, and I always catch the last half-hour and weep like a baby."
},
		{
			"rate":35,
			"text": "Absolute classic, though I prefer Jeffries' lesser known 'The Amazing Mr. Blunden'"
},
		{
			"rate":30,
			"text": "Forgot about this one, was ok for its time, kinda boring in parts though"
},
		{
			"rate":30,
			"text": "Youve got to love this film."
},
		{
			"rate":35,
			"text": "[font=Tahoma]Full review to come.[/font]"
}	]
}