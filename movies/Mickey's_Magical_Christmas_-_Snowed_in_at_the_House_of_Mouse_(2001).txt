{
	"title": "Mickey's Magical Christmas - Snowed in at the House of Mouse (2001)",
	"synopsis":"This home video release finds an interesting way to repackage older Disney cartoons. Mickey's Magical Christmas: Snowed in at the House of Mouse, finds Mickey, Minnie, Goofy, Pluto, and all of the other beloved Disney animated characters unable to leave a holiday party. In order to pass the time, the gang watches a compilation of holiday-themed short films. There is something for every member of the family on this look at Disney older efforts. ~ Perry Seibert, Rovi",
	"genre":
	[
		{"name":"Animation"},
		{"name":"Kids & Family"}

	],
	"rate":3.3,
	"comments":[
		{
			"rate":30,
			"text": "A decent enough variety show of Disney Christmases past and present. It doesn't have much Disney magic, though it's great to see all the favourite characters together. The 'films' highlights are an older animated short featuring Chip N Dale, and Mickey's Christmas Carol. The latter takes up the entire second half, so those familiar with it may feel short changed. A perfect hours worth of material to distract the kids at least."
},
		{
			"rate":25,
			"text": "A lighthearted anthology of holiday tales, Mickey's Magical Christmas: Snowed in at the House of Mouse is entertaining but overly contrived. When the House of Mouse is snowed in, Mickey and his friends attempt to cheer up Donald by putting on a show and reminding him of Christmases past. The film's a mixture of new animated sketches and classic Disney shorts, such as Mickey's Christmas Carol. Unfortunately, the House of Mouse is a hard contrivance to get over, as it supposes that all Disney characters exist in the same universe. And, the through story connecting all of the shorts is weak and uninteresting. Mickey's Magical Christmas: Snowed in at the House of Mouse has some fun parts, but it doesn't really work as a cohesive film."
},
		{
			"rate":25,
			"text": "Do not open until XmasMickey and all his friends from the Disney universe have collaborated for a holiday party. Unfortunately, Donald does not feel too compelled to partake in the events. Mickey tells numerous Christmas tales, starring our infamous Disney characters, in hopes of lifting Donald's Christmas spirit. Can Mickey and friends cheer Donald up?I don't want to be the mouse king. This is ridiculous.Tony Craig (Leroy & Stitch and Stitch! The Movie), Roberts Gannaway (Secret of Wings, Leroy & Stitch and Stitch! The Movie), and Rick Schneider (Mickey's House of Villains) collaborate to deliver Mickey's Magical Christmas. The storylines for these short stories are good and includes the infamous Scrooge story with Scrooge McDuck. The animation is classic Disney from the mid 90s.Haven't you ever wished upon a star? Your dreams may just come true.We watched this together as a family the day we celebrated Christmas off Netflix. The idea of mixing super heroes and villains into one story had potential, but was more of a marketing ploy to deliver three short stories. Overall, this was an average Disney picture that I wouldn't go out of my way to see. We have a winner and here is your prize.Grade: C"
},
		{
			"rate":30,
			"text": "good variety of Disney Characters and episodes of short Christmas holiday stories but, lacks the Disney magic as I thought they were trying to do too much, but, I'm sure the kids enjoyed it just the same"
},
		{
			"rate":25,
			"text": "A lighthearted anthology of holiday tales, Mickey's Magical Christmas: Snowed in at the House of Mouse is entertaining but overly contrived. When the House of Mouse is snowed in, Mickey and his friends attempt to cheer up Donald by putting on a show and reminding him of Christmases past. The film's a mixture of new animated sketches and classic Disney shorts, such as Mickey's Christmas Carol. Unfortunately, the House of Mouse is a hard contrivance to get over, as it supposes that all Disney characters exist in the same universe. And, the through story connecting all of the shorts is weak and uninteresting. Mickey's Magical Christmas: Snowed in at the House of Mouse has some fun parts, but it doesn't really work as a cohesive film."
},
		{
			"rate":35,
			"text": "Funny and sometimes heart-warming.A great Christmas special for kids (and grown-ups too)!"
},
		{
			"rate":30,
			"text": "This is a fun Christmas film because it's not just a new story -- yes, the whole Disney crew is snowed in and you get to see a ton of classic characters -- however, you get to experience several classic Mickey Mouse winter cartoons that are a joy to behold."
},
		{
			"rate":15,
			"text": "In retrospect, this movie is mediocre. You're better off watching Mickey's Christmas Carol on its own because that was the highlight for me when I watched this as a kid."
},
		{
			"rate":40,
			"text": "This film is a lot of fun, and touching. The cartoons were great and this is a film I watch every Christmas."
},
		{
			"rate":25,
			"text": "Do not open until XmasMickey and all his friends from the Disney universe have collaborated for a holiday party. Unfortunately, Donald does not feel too compelled to partake in the events. Mickey tells numerous Christmas tales, starring our infamous Disney characters, in hopes of lifting Donald's Christmas spirit. Can Mickey and friends cheer Donald up?I don't want to be the mouse king. This is ridiculous.Tony Craig (Leroy & Stitch and Stitch! The Movie), Roberts Gannaway (Secret of Wings, Leroy & Stitch and Stitch! The Movie), and Rick Schneider (Mickey's House of Villains) collaborate to deliver Mickey's Magical Christmas. The storylines for these short stories are good and includes the infamous Scrooge story with Scrooge McDuck. The animation is classic Disney from the mid 90s.Haven't you ever wished upon a star? Your dreams may just come true.We watched this together as a family the day we celebrated Christmas off Netflix. The idea of mixing super heroes and villains into one story had potential, but was more of a marketing ploy to deliver three short stories. Overall, this was an average Disney picture that I wouldn't go out of my way to see. We have a winner and here is your prize.Grade: C"
},
		{
			"rate":30,
			"text": "good variety of Disney Characters and episodes of short Christmas holiday stories but, lacks the Disney magic as I thought they were trying to do too much, but, I'm sure the kids enjoyed it just the same"
},
		{
			"rate":25,
			"text": "A lighthearted anthology of holiday tales, Mickey's Magical Christmas: Snowed in at the House of Mouse is entertaining but overly contrived. When the House of Mouse is snowed in, Mickey and his friends attempt to cheer up Donald by putting on a show and reminding him of Christmases past. The film's a mixture of new animated sketches and classic Disney shorts, such as Mickey's Christmas Carol. Unfortunately, the House of Mouse is a hard contrivance to get over, as it supposes that all Disney characters exist in the same universe. And, the through story connecting all of the shorts is weak and uninteresting. Mickey's Magical Christmas: Snowed in at the House of Mouse has some fun parts, but it doesn't really work as a cohesive film."
},
		{
			"rate":35,
			"text": "Funny and sometimes heart-warming.A great Christmas special for kids (and grown-ups too)!"
},
		{
			"rate":30,
			"text": "This is a fun Christmas film because it's not just a new story -- yes, the whole Disney crew is snowed in and you get to see a ton of classic characters -- however, you get to experience several classic Mickey Mouse winter cartoons that are a joy to behold."
},
		{
			"rate":15,
			"text": "In retrospect, this movie is mediocre. You're better off watching Mickey's Christmas Carol on its own because that was the highlight for me when I watched this as a kid."
},
		{
			"rate":-1,
			"text": "Will have to watch this one. Not necassarily good or bad. Will see. Want to watch!"
},
		{
			"rate":20,
			"text": "i like the old mickey movies"
},
		{
			"rate":40,
			"text": "I love Disney's House of Mouse, so I got this DVD, and I'm definitely not disappointed."
},
		{
			"rate":40,
			"text": "This film is a lot of fun, and touching. The cartoons were great and this is a film I watch every Christmas."
},
		{
			"rate":35,
			"text": "A lighthearted mix of classic and new holiday themed Disney shorts. Mickey, Donald and Goofy are always welcome around my house."
},
		{
			"rate":-1,
			"text": "my favorite movie and beautiful movie in the world for me"
},
		{
			"rate":50,
			"text": "i could watch over again"
},
		{
			"rate":45,
			"text": "Some christmas fun!!"
},
		{
			"rate":35,
			"text": "mickey mouse is the business.lol"
},
		{
			"rate":5,
			"text": "This was terrible, almost forgot that Disney used to produce these cash-ins lowquality products all the time until parents protested.Basically, Mickey has a place called House of Mouse where he invites all other Disney characters that tell jokes with one-liners. After a while, they play either a 1: A terribly boring original cartoon with terrible animation and awful music.2: A cartoon that is over 30 years old and shown every single christmas, so you've probably seen them before.One scene made me feel like I was high on drugs. Seriously. X_xThe music was so awful."
},
		{
			"rate":30,
			"text": "great kids movie and adults alike"
},
		{
			"rate":20,
			"text": "An ok movie at the time..."
},
		{
			"rate":35,
			"text": "hahahahahahahahahahahahaha"
},
		{
			"rate":35,
			"text": "haha christmas is awsome"
},
		{
			"rate":35,
			"text": "good collection of classic disney holiday cartoons"
},
		{
			"rate":35,
			"text": "Great for the little ones."
},
		{
			"rate":-1,
			"text": "not freaken interested"
},
		{
			"rate":50,
			"text": "a7baaaaaaaaaaah i love mickey what can i do im crazy about mickey"
},
		{
			"rate":45,
			"text": "going to add every disney movie to my already got it pile"
},
		{
			"rate":-1,
			"text": "Will have to watch this one. Not necassarily good or bad. Will see. Want to watch!"
},
		{
			"rate":45,
			"text": "i dont even know why i like this! =)"
},
		{
			"rate":35,
			"text": "this movie is cute but dumb but i still liked it alout."
},
		{
			"rate":40,
			"text": "I love Disney's House of Mouse, so I got this DVD, and I'm definitely not disappointed."
},
		{
			"rate":50,
			"text": "I love mickey mouse!!!"
},
		{
			"rate":45,
			"text": "mickey..... you are my legend"
},
		{
			"rate":50,
			"text": "I just love mickey mouse"
},
		{
			"rate":-1,
			"text": "This is the story- true story, of a gaggle of washed up Disney characters, unwillingly stranded in a house, who have their lives taped, to see what happens when cartoons stop getting along, and start getting real!... Snowed in at the House of Mouse!"
},
		{
			"rate":5,
			"text": "Haha watched this one christmas with my sister who got it for a gift... we slept through it."
},
		{
			"rate":50,
			"text": "i love mickey mouse no matter how old i am"
},
		{
			"rate":50,
			"text": "I LOVE THE MOUSE.....HE IS THE GREATEST"
},
		{
			"rate":-1,
			"text": "I have no children. Thus, no."
},
		{
			"rate":-1,
			"text": "I could let this go by if it was only about a mouse with a constant annoying smile on his face and gloves on his hands (mouse - hands???)...But i do not tolerate christmas."
},
		{
			"rate":35,
			"text": "Every single Disney movie is always an excellent choice for kids"
},
		{
			"rate":25,
			"text": "Aww chirstmas movie."
},
		{
			"rate":45,
			"text": "ahh i love this movie"
},
		{
			"rate":-1,
			"text": "I can't stand mickey mouse he is to sickly sweet. Ironically pluto the dog is my fav disney cartoon character."
},
		{
			"rate":50,
			"text": "i heart mickeymouse!!"
},
		{
			"rate":35,
			"text": "gr8 xmas film to watch"
},
		{
			"rate":40,
			"text": "This movie is so cute!! A good, fun family film!"
},
		{
			"rate":40,
			"text": "This is a great movie to watch at christmas time with the kids"
},
		{
			"rate":45,
			"text": "i love Mickey?"
},
		{
			"rate":50,
			"text": "it's a great christmas to watch with the whole family"
},
		{
			"rate":50,
			"text": "never seeen any thing cooler"
},
		{
			"rate":-1,
			"text": "I love all these christmassy movies!!"
},
		{
			"rate":50,
			"text": "i really enjoyed this film"
},
		{
			"rate":30,
			"text": "don't really remember it.. i don' t think i was really watching it.."
},
		{
			"rate":35,
			"text": "Hehe, it's Micky Mouse!!"
},
		{
			"rate":40,
			"text": "Cute and enjoyable. Good fun for kids."
},
		{
			"rate":50,
			"text": "micky is soooo adorable hes been famous since i think the 20's or 30's......."
},
		{
			"rate":50,
			"text": "Great Disney Christmas DVD"
},
		{
			"rate":50,
			"text": "Any thing with these dudes is neat"
}	]
}