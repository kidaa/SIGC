{
	"title": "Emmanuel's Gift (2004)",
	"synopsis":"While being born with physical handicaps presents challenges no matter where you're born or under what economic circumstances, in many respects it's even more difficult when you're raised in Ghana, a Third World nation where much of the country's population lives in severe poverty and conventional wisdom has it that those born with physical deformities have been cursed by the gods, and are fit to do little more than eke out a living as beggars. Emmanuel Ofosu Yeboah was a 27-year-old Ghanan who was born with only one leg; the shame led his father to abandon the family, but his mother was determined to see her son grow up with strength and dignity, and with the help of a prosthetic leg, Yeboah was able to walk and care for himself. Determined to show his countrymen that the handicapped were capable of more than most were willing to acknowledge, Yeboah contacted an American organization called the Challenged Athletes Foundation, and with their help, set out on an unusual quest -- learning to ride a racing bike, and then piloting it across the nation of Ghana. Emmanuel's Gift is a documentary which chronicles Yeboah's remarkable life and his courageous journey, as well as the impact it had on his family as well as on handicapped people throughout the African continent. Oprah Winfrey serves as narrator. ~ Mark Deming, Rovi",
	"genre":
	[
		{"name":"Documentary"},
		{"name":"Special Interest"}

	],
	"rate":3.7,
	"comments":[
		{
			"rate":35,
			"text": "Inspirational documentary about Emmanuel, a one legged man from Ghana, Africa. He not only overcame his own disabilities but became a champion for the disabled in his country."
},
		{
			"rate":20,
			"text": "Emmanuel Ofosu Yeboah was born with a severly deformed right leg in Ghana, where physical deformaties essentially amount to a predestined career as a street beggar. Yeboah's mother kept him even though his father had walked out and other friends suggested that she kill the child to spare him and his family a life of misery. Instead, Yeboah surprised the entire country when he took to riding a bicycle across Ghana with the help of the California-based Challenged Atheletes Foundation, who supplied the bike. Yeboah ended up becoming such a celebrity in his home country that he was invited to America to bike in the San Diego triathalon. He later had his deformed leg replaced with a prosthetic and went back to Ghana to fight for the rights of the disabled. His celebrity status made it impossible for the government to ignore diabled rights, and he inspired a country where reportedly ten percent of the population is physically disabled to have more respect for those that are less physically capable. Inspiring story, right? Well, it's all told in [i]Emmanuel's Gift,[/i] a new documentary that's so Oprah-friendly that Winfrey herself even provided the monotonal narration. In fact, it's so Oprah-friendly that it almost feels like an 80-minute infomercial for charities that help Ghana, which I'm all for supporting, but I don't really need to pay nine bucks to see. The problem certainly isn't in Yebolah's story, a fascinating tale of triumph that actually has managed to cause real changes in a destitute country. The problem is that filmmakers Lisa Lax and Nancy Stern don't really know where to go with the story outside of simply showing how inspirational Yebolah is, causing the film to ramble into sub-plots like lengthy biographies of other disabled (American) athletes that, well, knew Yebolah and Yebolah's own personal life, the footage of which seems oddly stilted. We're built up to this big meeting between Yebolah and the father who abandoned him only to have the whole topic dropped before they actually meet, and the ending shows Yebolah with his wife and child, two people who've never shown up in the film before. It's kind of unforgivable that such important details of the lead character's life are given the short shrift (the film never even bothers to tell us what his nickname, The Pozo, means), so the fact that instead we get something that feels occasionally like it's thirty seconds away from an appearance by Sally Struthers makes things even more frustrating. We're given plenty of shots of the disabled in Ghana, and while it never feels like exploitation, it treads a fine line. The film seems to have been made with the thesis of Give to this charity that we've chosen as the best, which is the same thing that can be gotten from a one-minute commercial we're forced to sit through because the kindly old man has impored us not to change the channel. [i]Emmanuel's Gift[/i] starts hitting the right notes when it explores the political aspects of the Ghanan ignorance of the disabled. It's hard not to be affected when a politician mentions that the disabled were not [i]intentionally[/i] ignored or when a tribal king, whose tribe has always thought that the disabled would bring disease, cheerfully creates a whole ceremony around Yebolah. Scenes with a disabled woman trying to go legit selling bracelets rather than begging on the street to feed her children, only to have the bracelets stolen, are heartbreaking and provide more impact than any of the scenes involving Yebolah himself. Sadly, these moments are few and far between, and much of [i]Emmanuel's Gift[/i] is politics and conflict-free. If [i]Emmanuel's Gift[/i] succeeds, it's because the story behind it is powerful enough to overtake the shoddy documentary that tells it. It's telling that Yebolah himself is inspirational with his casual attitude that doesn't seem to be trying to impress anyone, as though what he's doing is perfectly normal, where [i]Emmanuel's Gift[/i] fails to be inspirational mostly because it tries way too hard to be exactly that."
},
		{
			"rate":50,
			"text": "What an incredibly inspirational film. It starts out slowly, and you begin to wonder if watching this is worth it, but the directors continue to reveal layer after layer of the story that makes what happens all the more amazing. I would highly recommend watching this."
},
		{
			"rate":45,
			"text": "Amazing, exceptional.... one person can do so much!!"
},
		{
			"rate":35,
			"text": "Inspirational documentary about Emmanuel, a one legged man from Ghana, Africa. He not only overcame his own disabilities but became a champion for the disabled in his country."
},
		{
			"rate":45,
			"text": "It will touch your life."
},
		{
			"rate":25,
			"text": "This was a very inspirational and emotional movie."
},
		{
			"rate":45,
			"text": "Inspiring to ME, because I TOO have a physical disability.I watched in school!"
},
		{
			"rate":50,
			"text": "wonderful documentary"
},
		{
			"rate":50,
			"text": "Soooo informative. We all need to know more about how other people live."
},
		{
			"rate":-1,
			"text": "I wonder what this is about, i think ill see it!"
},
		{
			"rate":-1,
			"text": "So shoot me - I love Oprah"
},
		{
			"rate":20,
			"text": "Emmanuel Ofosu Yeboah was born with a severly deformed right leg in Ghana, where physical deformaties essentially amount to a predestined career as a street beggar. Yeboah's mother kept him even though his father had walked out and other friends suggested that she kill the child to spare him and his family a life of misery. Instead, Yeboah surprised the entire country when he took to riding a bicycle across Ghana with the help of the California-based Challenged Atheletes Foundation, who supplied the bike. Yeboah ended up becoming such a celebrity in his home country that he was invited to America to bike in the San Diego triathalon. He later had his deformed leg replaced with a prosthetic and went back to Ghana to fight for the rights of the disabled. His celebrity status made it impossible for the government to ignore diabled rights, and he inspired a country where reportedly ten percent of the population is physically disabled to have more respect for those that are less physically capable. Inspiring story, right? Well, it's all told in [i]Emmanuel's Gift,[/i] a new documentary that's so Oprah-friendly that Winfrey herself even provided the monotonal narration. In fact, it's so Oprah-friendly that it almost feels like an 80-minute infomercial for charities that help Ghana, which I'm all for supporting, but I don't really need to pay nine bucks to see. The problem certainly isn't in Yebolah's story, a fascinating tale of triumph that actually has managed to cause real changes in a destitute country. The problem is that filmmakers Lisa Lax and Nancy Stern don't really know where to go with the story outside of simply showing how inspirational Yebolah is, causing the film to ramble into sub-plots like lengthy biographies of other disabled (American) athletes that, well, knew Yebolah and Yebolah's own personal life, the footage of which seems oddly stilted. We're built up to this big meeting between Yebolah and the father who abandoned him only to have the whole topic dropped before they actually meet, and the ending shows Yebolah with his wife and child, two people who've never shown up in the film before. It's kind of unforgivable that such important details of the lead character's life are given the short shrift (the film never even bothers to tell us what his nickname, The Pozo, means), so the fact that instead we get something that feels occasionally like it's thirty seconds away from an appearance by Sally Struthers makes things even more frustrating. We're given plenty of shots of the disabled in Ghana, and while it never feels like exploitation, it treads a fine line. The film seems to have been made with the thesis of Give to this charity that we've chosen as the best, which is the same thing that can be gotten from a one-minute commercial we're forced to sit through because the kindly old man has impored us not to change the channel. [i]Emmanuel's Gift[/i] starts hitting the right notes when it explores the political aspects of the Ghanan ignorance of the disabled. It's hard not to be affected when a politician mentions that the disabled were not [i]intentionally[/i] ignored or when a tribal king, whose tribe has always thought that the disabled would bring disease, cheerfully creates a whole ceremony around Yebolah. Scenes with a disabled woman trying to go legit selling bracelets rather than begging on the street to feed her children, only to have the bracelets stolen, are heartbreaking and provide more impact than any of the scenes involving Yebolah himself. Sadly, these moments are few and far between, and much of [i]Emmanuel's Gift[/i] is politics and conflict-free. If [i]Emmanuel's Gift[/i] succeeds, it's because the story behind it is powerful enough to overtake the shoddy documentary that tells it. It's telling that Yebolah himself is inspirational with his casual attitude that doesn't seem to be trying to impress anyone, as though what he's doing is perfectly normal, where [i]Emmanuel's Gift[/i] fails to be inspirational mostly because it tries way too hard to be exactly that."
}	]
}