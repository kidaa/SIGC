package org.sigc.crawler;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.StringTokenizer;

import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Crawler {

	public void crawl() {

		int movieError = 0;
		int totalMovies = 0;
		int first = 1;
		File file;
		Document doc;
		Elements newsHeadlines;
		Elements reviews;
		Elements title;
		Elements synopsis;
		Elements spans;
		Elements audienceReviews;
		FileWriter writer;
		long startTime = System.currentTimeMillis();
		try {
			System.out.println("INFO: Create DataSet");


			System.out.println("INFO: Get Movies List");
			doc = Jsoup.connect("http://www.rottentomatoes.com/movie/allMovies/").timeout(10*1000).get();
			newsHeadlines = doc.select("loc");
			System.out.println("INFO: Find " + newsHeadlines.size() +" movies");
			//			System.out.println(newsHeadlines.get(0).text());
			//			
			totalMovies = newsHeadlines.size();
			for (int i = 0; i < newsHeadlines.size(); i++) {

				System.out.println("INFO: Movie " + i + " of " + totalMovies);
				System.out.println("INFO: Add Movie");
				//writer.append("//\n//\n");

				// Get movie Page
				try {
					doc = Jsoup.connect(newsHeadlines.get(i).text()).timeout(10*1000).get();
				}catch(HttpStatusException ex) {
					movieError++;
					continue;
				}

				// Get Movie Title
				title = doc.select("h1.movie_title");
				file = new File("movies2/" + title.text().toString().replaceAll("\\<.*?>","").replaceAll(" ", "_") + ".txt");
				if (!file.exists()) {
					try {
						file.createNewFile();
					} catch(IOException ioe) {
						System.out.println("ERROR: IOException - " + ioe.getMessage());
						continue;
					}
				} else {
					continue;
				}

				writer = new FileWriter("movies2/" + title.text().toString().replaceAll("\\<.*?>","").replaceAll(" ", "_") + ".txt");

				System.out.println("INFO: Movie Title - " + title.text().toString().replaceAll("\\<.*?>",""));
				writer.append("{\n\t\"title\": \"" + title.text().toString().replaceAll("\"","") + "\",\n");
				writer.flush();

				// Get Movie Synopsis
				synopsis = doc.select("#movieSynopsis");
				System.out.println("INFO: Get Synopsis");
				writer.append("\t\"synopsis\":\""+synopsis.text().toString().replaceAll("\"","")+"\",\n");
				writer.flush();

				// Get Movie Genres
				System.out.println("INFO: Get Genre");
				spans = doc.select("span");
				writer.append("\t\"genre\":\n\t[\n");
				first = 1;
				for (Element e: spans) {
					if(e.hasAttr("itemprop")){
						if (e.attr("itemprop").equals("genre")){
							if (first == 1) {
								writer.append("\t\t{\"name\":\""+e.text().toString().replaceAll("\"","")+"\"}");
								first = 0;
							} else {
								writer.append(",\n\t\t{\"name\":\""+e.text().toString().replaceAll("\"","")+"\"}\n");
							}

							writer.flush();
						}
					}
				}
				writer.append("\n\t],\n"); // End of Genre
				writer.flush();

				// Get Movie Audience Review
				System.out.println("INFO: Get Audience Review");
				audienceReviews = doc.select("p.critic_stats");
				if (audienceReviews.size() > 2) {
					String review = audienceReviews.get(2).text();
					review = review.replace("liked it ","");

					StringTokenizer st = new StringTokenizer(review, " ");
					String key = null;
					while(st.hasMoreTokens()) {
						key = st.nextToken();
						if (key.contains("/")) {
							st = new StringTokenizer(key, "/");
							writer.append("\t\"rate\":" + st.nextToken() + ",\n");
							writer.flush();
						}
					}
				} else {
					writer.append("\t\"rate\":0,\n");
					writer.flush();
				}

				try {
					int pageNum = 1;
					System.out.println("INFO: Start Get comments");
					writer.append("\t\"comments\":[\n");
					first = 1;
					do {
						doc = Jsoup.connect(newsHeadlines.get(i).text() + "/reviews/?type=user&page="+pageNum).timeout(10*1000).get();
						reviews = doc.select(".user_review");
						Elements score = null;;
						if (reviews.size() != 0) {
							for (Element el : reviews) {
								score = el.select("div.scoreWrapper");

								String rating = score.select("span")
									.removeClass("small")
									.removeClass("rating")
									.removeClass("stars")
									.attr("class");
								el.select("span").remove();
								el.select(".scoreWrapper").remove();
								el.select("br").remove();
								if (first == 1) {
									writer.append("\t\t{\n\t\t\t\"rate\":"+ getCommentScore(rating) + ",\n\t\t\t\"text\": \"" 
											+ el.text().toString().replaceAll("\"","") + "\"\n}");
									first = 0;
								} else {
									writer.append(",\n\t\t{\n\t\t\t\"rate\":"+ getCommentScore(rating) + ",\n\t\t\t\"text\": \"" 
											+ el.text().toString().replaceAll("\"","") + "\"\n}");
								}
								writer.flush();
							}
							//							System.out.println("------------------------");
							System.out.println("INFO: Comment Page: "+ pageNum);
							pageNum++;
						} else {
							writer.append("\t]\n}");
							System.out.println("INFO: End Get Comments\n\n");
							break;
						}
					} while(true);
				}catch(IOException e1){
					System.out.println("ERROR: " + e1.getMessage());
					e1.printStackTrace();
				} catch(Exception e) {
					System.out.println("ERROR: " + e.getMessage());
					e.printStackTrace();
					writer.append("\t]\n}");
					continue;
				}
				writer.close();
			}

			System.out.println("INFO: Done!");
			System.out.println("INFO: Failed to parse" + movieError);
			long endTime   = System.currentTimeMillis();
			long totalTime = endTime - startTime;
			NumberFormat formatter = new DecimalFormat("#0.00000");
			System.out.println("INFO: Total Time: " + formatter.format((totalTime) / 1000d) + " s");
		} catch (IOException e) {
			System.out.println("ERROR: " + e.getMessage());
			e.printStackTrace();
		}
	}

	public int getCommentScore(String score) {
		if (score.equals("score00")) {
			return 0;
		} else if (score.equals("score05")) {
			return 5;
		} else if (score.equals("score10")) {
			return 10;
		} else if (score.equals("score15")) {
			return 15;
		} else if (score.equals("score20")) {
			return 20;
		} else if (score.equals("score25")) {
			return 25;
		} else if (score.equals("score30")) {
			return 30;
		} else if (score.equals("score35")) {
			return 35;
		} else if (score.equals("score40")) {
			return 40;
		} else if (score.equals("score45")) {
			return 45;
		} else if (score.equals("score50")) {
			return 50;
		}
		return -1;
	}
}

