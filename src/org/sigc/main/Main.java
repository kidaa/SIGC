package org.sigc.main;

import org.sigc.analysis.TopicModeling;
import org.sigc.crawler.Crawler;

import java.util.Scanner;

/**
 * Created by alex on 5/20/14.
 */
public class Main {
	public static Scanner sc;

	public static void crawler_menu(){
		System.out.println("Crawler Menu");
	}

	public static void topic_modeling_menu(){
		System.out.println("Topic Modeling Menu");
	}

	public static void sentiment_analysis_menu(){
		System.out.println("Sentiment Analysis Menu");
	}

	public static void classification_menu(){
		System.out.println("Classification Menu");
	}

	public static void main_menu(){
		while(true){
			System.out.println("\n--MENU--");
			System.out.println("1 - Get data from RottenTomatoes");
			System.out.println("2 - Topic Modeling");
			System.out.println("3 - Sentiment Analysis");
			System.out.println("4 - Classification");
			System.out.println("5 - Exit");
			System.out.print("> ");
			String s = sc.nextLine();
			System.out.print("\n");
			if(s.equals("1")){
				crawler_menu();
			} else if(s.equals("2")){
				topic_modeling_menu();
			} else if(s.equals("3")){
				sentiment_analysis_menu();
			} else if(s.equals("4")){
				classification_menu();
			} else{
				break;
			}
		}
	}

	public static void main(String[] args){
		sc = new Scanner(System.in);
		main_menu();
	}
}
