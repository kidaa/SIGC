package org.sigc.main;

import com.google.gson.Gson;
import org.sigc.data.Data;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Reader{
	public Data readFile(String path) throws IOException {
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		String str = new String(encoded, StandardCharsets.UTF_8);
		try {
			Data data = new Gson().fromJson(str, Data.class);
			return data;
		} catch(com.google.gson.JsonSyntaxException e){
			System.out.println(path);
		}
		return null;
	}

	public List<Data> readDirectory(String path) throws IOException {
		final File folder = new File(path);
		List<Data> list = new ArrayList<Data>();

		if(!folder.isDirectory()){
			list.add(readFile(path));
			return list;
		}

		for (final File file: folder.listFiles()){
			if(!file.isDirectory() && file.getName().endsWith(".txt")){
				Data tmp = readFile(file.getAbsolutePath());
				if(tmp!=null) {
					list.add(tmp);
				}
			}
		}
		return list;
	}
}
