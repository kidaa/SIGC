package org.sigc.main;

import com.google.gson.Gson;
import org.sigc.data.Data;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;

/**
 * Created by alex on 5/27/14.
 */
public class Writer {
	public void writeToFile(Data data, String path) throws IOException {
		Gson gson = new Gson();
		String str = gson.toJson(data);

		File myFile = new File(path);
		myFile.createNewFile();
		FileOutputStream fOut = new FileOutputStream(myFile);
		OutputStreamWriter myOutWriter =new OutputStreamWriter(fOut);
		myOutWriter.append(str);
		myOutWriter.close();
		fOut.close();
	}
}
