package org.sigc.data;

import java.util.List;

public class Topic {
	public int id;
	public double prob;
	public List<String> words;
}
