package org.sigc.data;

import java.util.List;

public class Comment {
	public int rate;
	public String text;
	public List<Topic> topics;
	public String sentimentAnalysis;
}
