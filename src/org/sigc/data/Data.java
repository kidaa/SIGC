package org.sigc.data;

import java.util.List;

public class Data {
	public String title;
	public String synopsis;
	public List<Topic> topics;
	public String sentimentAnalysis;
	public List<Genre> genre;
	public double rate;
	public List<Comment> comments;
}


