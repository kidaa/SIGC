package org.sigc.analysis;

import cc.mallet.types.*;
import cc.mallet.pipe.*;
import cc.mallet.topics.*;

import java.util.*;
import java.util.regex.*;
import java.io.*;

import com.google.gson.Gson;
import org.sigc.data.*;
import org.sigc.main.*;
import org.sigc.main.Reader;

public class TopicModeling {
	public ParallelTopicModel model;
	public Pipe pipe;
	public InstanceList instances;

	public void createModel(int numTopics, int numThreads, int numIterations) throws IOException {
		this.model = new ParallelTopicModel(numTopics, 1.0, 0.01);

		this.model.addInstances(instances);
		this.model.setNumThreads(numThreads);
		this.model.setNumIterations(numIterations);
		this.model.estimate();

		FileOutputStream fout = new FileOutputStream("model.obj");
		ObjectOutputStream oos = new ObjectOutputStream(fout);
		oos.writeObject(model);

	}

	public List<Topic> getStringTopics(String str){
		InstanceList testing = new InstanceList(this.pipe);
		testing.addThruPipe(new Instance(str, null, "test instance", null));

        TopicInferencer inferencer = this.model.getInferencer();
		double[] testProbabilities = inferencer.getSampledDistribution(testing.get(0), 10, 1, 5);
		Alphabet dataAlphabet = instances.getDataAlphabet();

		ArrayList<Topic> topics = new ArrayList<Topic>();

		for(int i=0;i<testProbabilities.length;i++){
			double prob = testProbabilities[i];
			if(prob>=0.10) {
				//System.out.println(i + "\t" + prob +"\t"+dataAlphabet.lookupObject(model.getSortedWords().get(i).iterator().next().getID()));
				Topic t = new Topic();
				ArrayList<String> words = new ArrayList<String>();
				Iterator<IDSorter> iterator = model.getSortedWords().get(i).iterator();
				t.id = i;
				t.prob = prob;
				for(int j=0;j<5 && iterator.hasNext();j++){
					words.add(dataAlphabet.lookupObject(iterator.next().getID()).toString());
				}
				t.words = words;
				topics.add(t);
			}
		}
		return topics;
	}

	public void createPipe(){
		ArrayList<Pipe> pipeList = new ArrayList<Pipe>();

		// Pipes: lowercase, tokenize, remove stopwords, map to features
		pipeList.add( new CharSequenceLowercase() );
		pipeList.add( new CharSequence2TokenSequence(Pattern.compile("\\p{L}[\\p{L}\\p{P}]+\\p{L}")) );
		pipeList.add( new TokenSequenceRemoveStopwords(new File("stoplists/en.txt"), "UTF-8", false, false, false) );
		pipeList.add( new TokenSequence2FeatureSequence() );

		this.pipe = new SerialPipes(pipeList);
	}

	public static void main(String[] args) throws IOException {
		TopicModeling tp = new TopicModeling();
		tp.createPipe();

		org.sigc.main.Reader reader = new Reader();
		List<Data> data = reader.readDirectory(args[0]);

        tp.instances = new InstanceList(tp.pipe);
		for(Data d: data){
            tp.instances.addThruPipe(new Instance(d.synopsis,null,null,null));
        }
        tp.createModel(200, 2, 2000);

		int i=0;
		int count = data.size();
		for(Data d: data){
	        d.topics = tp.getStringTopics(d.synopsis);
			System.out.println("Movie: "+(++i)+"/"+count);
			org.sigc.main.Writer writer = new org.sigc.main.Writer();
			writer.writeToFile(d, "movies5/" + d.title.replace(' ', '_') + ".txt");
		}

	}
}
