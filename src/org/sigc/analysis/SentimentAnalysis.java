package org.sigc.analysis;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.sentiment.SentimentCoreAnnotations;
import edu.stanford.nlp.util.CoreMap;
import org.apache.commons.cli.*;
import org.sigc.data.*;
import org.sigc.main.Reader;
import org.sigc.main.Writer;

public class SentimentAnalysis {
	public StanfordCoreNLP pipeline;

	SentimentAnalysis(){
		Properties props = new Properties();
		props.setProperty("annotators", "tokenize, ssplit, parse, sentiment");

		this.pipeline = new StanfordCoreNLP(props);
	}

	public static void main(String[] args) throws ParseException, IOException {
		Reader reader = new Reader();
		List<Data> data = reader.readDirectory(args[0]);

		SentimentAnalysis sa = new SentimentAnalysis();
		int i=0;
		int count = data.size();
		for(Data d: data) {
			System.out.println("Movie: "+(++i)+"/"+count);
			d.sentimentAnalysis = sa.getSentiment(d.synopsis);
			int j=0;
			int count2=d.comments.size();
			for(Comment c: d.comments){
				System.out.println("Movie: "+(i)+"/"+count+" - Comment: "+(++j)+"/"+count2);
				c.sentimentAnalysis = sa.getSentiment(c.text);
			}

			Writer writer = new Writer();
			writer.writeToFile(d, "movies3/" + d.title.replace(' ', '_') + ".txt");
		}
	}

	public String getSentiment(String line){
		double count = 0;
		double res=0;
		if (line != null && line.length() > 0) {
			Annotation annotation = pipeline.process(line);
			for (CoreMap sentence : annotation.get(CoreAnnotations.SentencesAnnotation.class)) {
				count++;
				if(sentence.get(SentimentCoreAnnotations.ClassName.class).equals("Very positive")){
					res+=5;
				} else if(sentence.get(SentimentCoreAnnotations.ClassName.class).equals("Positive")){
					res+=4;
				} else if(sentence.get(SentimentCoreAnnotations.ClassName.class).equals("Neutral")){
					res+=3;
				} else if(sentence.get(SentimentCoreAnnotations.ClassName.class).equals("Negative")){
					res+=2;
				} else if(sentence.get(SentimentCoreAnnotations.ClassName.class).equals("Very negative")){
					res+=1;
				}
			}
		}
		res = Math.round(res/count);
		switch ((int)res){
			case 1:
				return "Very negative";
			case 2:
				return "Negative";
			case 3:
				return "Neutral";
			case 4:
				return "Very positive";
			case 5:
				return "Positive";
			default:
				return "None";
		}
	}
}
